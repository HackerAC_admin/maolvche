/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 80016
Source Host           : localhost:3306
Source Database       : maolvche

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2020-11-14 09:11:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tab_admins
-- ----------------------------
DROP TABLE IF EXISTS `tab_admins`;
CREATE TABLE `tab_admins` (
  `adminNo` char(8) NOT NULL COMMENT '管理员编号',
  `admName` varchar(8) NOT NULL COMMENT '姓名',
  `sex` char(2) NOT NULL COMMENT '性别',
  `admEmail` varchar(20) NOT NULL COMMENT '邮箱',
  `phone` varchar(11) NOT NULL COMMENT '联系电话',
  `admType` varchar(8) NOT NULL COMMENT '类别',
  PRIMARY KEY (`adminNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_admins
-- ----------------------------
INSERT INTO `tab_admins` VALUES ('20111201', 'hackerac', ' 男', 'hackerac', '17385120965', '运维管理员');

-- ----------------------------
-- Table structure for tab_bgmsaccount
-- ----------------------------
DROP TABLE IF EXISTS `tab_bgmsaccount`;
CREATE TABLE `tab_bgmsaccount` (
  `admAccNo` int(11) NOT NULL AUTO_INCREMENT COMMENT '后台账户编号',
  `adminNo` char(8) NOT NULL COMMENT '管理员编号',
  `loginName` varchar(10) NOT NULL COMMENT '登录名',
  `admPwd` varchar(16) NOT NULL COMMENT '密码',
  `nickName` varchar(16) NOT NULL COMMENT '昵称',
  PRIMARY KEY (`admAccNo`),
  KEY `bgms_admin` (`adminNo`),
  CONSTRAINT `bgms_admin` FOREIGN KEY (`adminNo`) REFERENCES `tab_admins` (`adminNo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_bgmsaccount
-- ----------------------------
INSERT INTO `tab_bgmsaccount` VALUES ('1', '20111201', 'admxff001', '123456', 'HackerAC');

-- ----------------------------
-- Table structure for tab_carbasicconfiguration
-- ----------------------------
DROP TABLE IF EXISTS `tab_carbasicconfiguration`;
CREATE TABLE `tab_carbasicconfiguration` (
  `basicconNo` varchar(12) NOT NULL COMMENT '基本配置编号',
  `Firm` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '厂商',
  `carModel` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '车型',
  `outColor` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '漆色',
  `inColor` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '内饰颜色',
  `maxSpeed` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最高速度',
  `oilConsumption` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '油耗',
  `parkingbrakeType` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '驻车制动类型',
  PRIMARY KEY (`basicconNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_carbasicconfiguration
-- ----------------------------

-- ----------------------------
-- Table structure for tab_carbody
-- ----------------------------
DROP TABLE IF EXISTS `tab_carbody`;
CREATE TABLE `tab_carbody` (
  `carBodyNo` varchar(12) NOT NULL COMMENT '车身配置编号',
  `carLength` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '车长',
  `carHeight` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '车高',
  `carWidth` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '车宽',
  `wheelBase` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '轴距',
  `carWeight` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '整车质量',
  `minGround` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最小离地间隙',
  `carStructure` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '车身结构',
  `fueltankVolume` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '油箱容积',
  `luggagecarrierVolume` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '行李厢容积',
  PRIMARY KEY (`carBodyNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_carbody
-- ----------------------------

-- ----------------------------
-- Table structure for tab_carengine
-- ----------------------------
DROP TABLE IF EXISTS `tab_carengine`;
CREATE TABLE `tab_carengine` (
  `carEngineNo` varchar(12) NOT NULL COMMENT '发动机配置编号',
  `engineType` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '发动机型号',
  `outputVolume` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '排量',
  `airInlet` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '进气方式（如自然吸气、涡轮增压）',
  `cylindersNo` int(2) NOT NULL COMMENT '气缸数',
  `maxhorsePower` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最大马力',
  `maxPower` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最大功率',
  `maxpowerSpeed` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最大功率转速',
  `fuelType` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '燃油（如汽油）',
  `fuelLabel` int(5) NOT NULL COMMENT '燃油标号（如92）',
  `oilDrive` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '供油方式（如多点电喷）',
  PRIMARY KEY (`carEngineNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_carengine
-- ----------------------------

-- ----------------------------
-- Table structure for tab_creditfile
-- ----------------------------
DROP TABLE IF EXISTS `tab_creditfile`;
CREATE TABLE `tab_creditfile` (
  `creaditFileNo` int(11) NOT NULL AUTO_INCREMENT COMMENT '信用资料编号',
  `userAcc` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户编号',
  `topic` varchar(20) DEFAULT NULL COMMENT '主题',
  `fileName` varchar(10) DEFAULT NULL COMMENT '文件名',
  `fileSrc` varchar(20) DEFAULT NULL COMMENT '文件路径',
  `sendTime` datetime DEFAULT NULL COMMENT '上传日期',
  PRIMARY KEY (`creaditFileNo`),
  KEY `credit_user` (`userAcc`),
  CONSTRAINT `credit_user` FOREIGN KEY (`userAcc`) REFERENCES `tab_webaccount` (`userAcc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_creditfile
-- ----------------------------

-- ----------------------------
-- Table structure for tab_empaccount
-- ----------------------------
DROP TABLE IF EXISTS `tab_empaccount`;
CREATE TABLE `tab_empaccount` (
  `eplyAccNo` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工账户编号',
  `employeeNo` varchar(12) NOT NULL COMMENT '员工编号',
  `loginName` varchar(10) NOT NULL COMMENT '登录名',
  `eplyPwd` varchar(16) NOT NULL COMMENT '密码',
  `nickName` varchar(16) NOT NULL COMMENT '昵称',
  PRIMARY KEY (`eplyAccNo`),
  KEY `AccountemployeeNo` (`employeeNo`),
  CONSTRAINT `tab_empaccount_ibfk_1` FOREIGN KEY (`employeeNo`) REFERENCES `tab_empinfor` (`employeeNo`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_empaccount
-- ----------------------------

-- ----------------------------
-- Table structure for tab_empchange
-- ----------------------------
DROP TABLE IF EXISTS `tab_empchange`;
CREATE TABLE `tab_empchange` (
  `employeeNo` varchar(12) NOT NULL COMMENT '员工编号',
  `changeDate` date NOT NULL COMMENT '变更日期',
  `beJob` varchar(8) NOT NULL COMMENT '变更前职务',
  `afJob` varchar(8) NOT NULL COMMENT '变更后职务',
  `Remarks` varchar(10) NOT NULL COMMENT '备注',
  PRIMARY KEY (`employeeNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_empchange
-- ----------------------------

-- ----------------------------
-- Table structure for tab_empcondition
-- ----------------------------
DROP TABLE IF EXISTS `tab_empcondition`;
CREATE TABLE `tab_empcondition` (
  `empCondiotionNo` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工状态编号',
  `employeeNo` varchar(12) NOT NULL COMMENT '员工编号',
  `outDate` date NOT NULL COMMENT '离职日期',
  `outRes` varchar(50) NOT NULL COMMENT '离职原因',
  `Remarks` varchar(10) NOT NULL COMMENT '备注',
  PRIMARY KEY (`empCondiotionNo`),
  KEY `employeeNo` (`employeeNo`),
  CONSTRAINT `tab_empcondition_ibfk_1` FOREIGN KEY (`employeeNo`) REFERENCES `tab_empinfor` (`employeeNo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_empcondition
-- ----------------------------

-- ----------------------------
-- Table structure for tab_empinfor
-- ----------------------------
DROP TABLE IF EXISTS `tab_empinfor`;
CREATE TABLE `tab_empinfor` (
  `employeeNo` varchar(12) NOT NULL COMMENT '员工编号',
  `employeeName` varchar(8) NOT NULL COMMENT '员工姓名',
  `employeeSex` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '员工性别',
  `employeeidNo` varchar(18) NOT NULL COMMENT '员工身份证号',
  `employeeEmail` varchar(18) NOT NULL COMMENT '员工邮箱',
  `employeePhone` varchar(11) NOT NULL COMMENT '员工联系电话',
  `employeeAddress` varchar(16) DEFAULT NULL COMMENT '员工联系地址',
  `Job` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '当前职务',
  `inDate` date NOT NULL COMMENT '入职日期',
  PRIMARY KEY (`employeeNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_empinfor
-- ----------------------------

-- ----------------------------
-- Table structure for tab_emporder
-- ----------------------------
DROP TABLE IF EXISTS `tab_emporder`;
CREATE TABLE `tab_emporder` (
  `getOrderNo` varchar(12) NOT NULL COMMENT '接单号',
  `employeeNo` varchar(12) NOT NULL COMMENT '接单员工编号',
  `newCarOrderNo` varchar(12) NOT NULL COMMENT '新车订单编号',
  `getOrderTime` datetime NOT NULL COMMENT '接单时间',
  `State` varchar(8) NOT NULL COMMENT '处理进度(进行中、已完成)',
  PRIMARY KEY (`getOrderNo`),
  KEY `OrderemployeeNo` (`employeeNo`),
  KEY `OrdernewCarOrderNo` (`newCarOrderNo`),
  CONSTRAINT `tab_emporder_ibfk_1` FOREIGN KEY (`employeeNo`) REFERENCES `tab_empinfor` (`employeeNo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tab_emporder_ibfk_2` FOREIGN KEY (`newCarOrderNo`) REFERENCES `tab_newcarorder` (`newCarOrderNo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_emporder
-- ----------------------------

-- ----------------------------
-- Table structure for tab_exchange
-- ----------------------------
DROP TABLE IF EXISTS `tab_exchange`;
CREATE TABLE `tab_exchange` (
  `exchangedNo` int(11) NOT NULL COMMENT '退换编号',
  `carNo` varchar(16) NOT NULL COMMENT '汽车编号',
  `userAcc` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户编号',
  `applyDate` datetime NOT NULL COMMENT '申请日期',
  `exchangedDate` datetime DEFAULT NULL COMMENT '退换日期',
  `auditResult` varchar(20) DEFAULT NULL COMMENT '审核结果',
  `state` varchar(4) NOT NULL COMMENT '状态（已提交，正在审核，正在退换，已完成）',
  PRIMARY KEY (`exchangedNo`),
  KEY `exchange_user` (`userAcc`),
  CONSTRAINT `exchange_user` FOREIGN KEY (`userAcc`) REFERENCES `tab_webaccount` (`userAcc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_exchange
-- ----------------------------

-- ----------------------------
-- Table structure for tab_favorites
-- ----------------------------
DROP TABLE IF EXISTS `tab_favorites`;
CREATE TABLE `tab_favorites` (
  `favoritesNo` int(11) NOT NULL COMMENT '收藏编号',
  `carId` varchar(12) NOT NULL COMMENT '汽车编号',
  `userAcc` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户编号',
  `addTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '收藏时间',
  PRIMARY KEY (`favoritesNo`),
  KEY `like_user` (`userAcc`),
  CONSTRAINT `like_user` FOREIGN KEY (`userAcc`) REFERENCES `tab_webaccount` (`userAcc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_favorites
-- ----------------------------

-- ----------------------------
-- Table structure for tab_feedback
-- ----------------------------
DROP TABLE IF EXISTS `tab_feedback`;
CREATE TABLE `tab_feedback` (
  `feedBackNo` int(11) NOT NULL AUTO_INCREMENT COMMENT '反馈编号',
  `userAcc` varchar(12) NOT NULL COMMENT '用户账号',
  `topic` varchar(20) NOT NULL COMMENT '主题',
  `content` text NOT NULL COMMENT '内容',
  `sendTime` datetime NOT NULL COMMENT '投诉反馈日期',
  `handleResult` varchar(20) DEFAULT NULL COMMENT '处理结果',
  `handleTime` datetime DEFAULT NULL COMMENT '处理日期',
  PRIMARY KEY (`feedBackNo`),
  KEY `feedback_user` (`userAcc`),
  CONSTRAINT `feedback_user` FOREIGN KEY (`userAcc`) REFERENCES `tab_webaccount` (`userAcc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_feedback
-- ----------------------------

-- ----------------------------
-- Table structure for tab_gearbox
-- ----------------------------
DROP TABLE IF EXISTS `tab_gearbox`;
CREATE TABLE `tab_gearbox` (
  `gearboxNo` varchar(12) NOT NULL COMMENT '变速箱配置编号',
  `gearsNum` int(5) NOT NULL COMMENT '档位个数',
  `transmissionType` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '变速箱类型（自动变速箱或手动变速箱）',
  PRIMARY KEY (`gearboxNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_gearbox
-- ----------------------------

-- ----------------------------
-- Table structure for tab_helpguide
-- ----------------------------
DROP TABLE IF EXISTS `tab_helpguide`;
CREATE TABLE `tab_helpguide` (
  `helpGuidNo` int(11) NOT NULL AUTO_INCREMENT COMMENT '帮助向导编号',
  `module` char(8) NOT NULL COMMENT '模块',
  `topic` varchar(20) NOT NULL COMMENT '主题',
  `content` text NOT NULL COMMENT '内容',
  `state` char(8) NOT NULL COMMENT '状态',
  `updateTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`helpGuidNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_helpguide
-- ----------------------------

-- ----------------------------
-- Table structure for tab_instlmnrecord
-- ----------------------------
DROP TABLE IF EXISTS `tab_instlmnrecord`;
CREATE TABLE `tab_instlmnrecord` (
  `instlmntNo` int(11) NOT NULL COMMENT '分期还款编号',
  `planNo` char(3) NOT NULL COMMENT '分期方案编号',
  `userAcc` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户编号',
  `lastMoney` float NOT NULL COMMENT '剩余未缴金额',
  `repaymentDate` datetime NOT NULL COMMENT '还款日期',
  `repaymentMoney` float NOT NULL COMMENT '还款金额',
  PRIMARY KEY (`instlmntNo`),
  KEY `instlmt_user` (`userAcc`),
  KEY `instlmn_plan` (`planNo`),
  CONSTRAINT `instlmn_plan` FOREIGN KEY (`planNo`) REFERENCES `tab_instlplan` (`planNo`),
  CONSTRAINT `instlmt_user` FOREIGN KEY (`userAcc`) REFERENCES `tab_webaccount` (`userAcc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_instlmnrecord
-- ----------------------------

-- ----------------------------
-- Table structure for tab_instlplan
-- ----------------------------
DROP TABLE IF EXISTS `tab_instlplan`;
CREATE TABLE `tab_instlplan` (
  `planNo` char(3) NOT NULL COMMENT '分期方案编号',
  `percent` int(2) NOT NULL COMMENT '首付比例',
  `times` int(2) NOT NULL COMMENT '期数',
  PRIMARY KEY (`planNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_instlplan
-- ----------------------------

-- ----------------------------
-- Table structure for tab_maintain
-- ----------------------------
DROP TABLE IF EXISTS `tab_maintain`;
CREATE TABLE `tab_maintain` (
  `maintainNo` int(11) NOT NULL COMMENT '养护编号',
  `carNo` varchar(16) NOT NULL COMMENT '汽车编号',
  `userAcc` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户编号',
  `maintainItem` varchar(30) NOT NULL COMMENT '养护日期',
  `price` float NOT NULL COMMENT '费用',
  `maintainDate` datetime NOT NULL COMMENT '养护日期',
  `employeeNo` varchar(12) NOT NULL COMMENT '员工编号',
  PRIMARY KEY (`maintainNo`),
  KEY `maintain_user` (`userAcc`),
  CONSTRAINT `maintain_user` FOREIGN KEY (`userAcc`) REFERENCES `tab_webaccount` (`userAcc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_maintain
-- ----------------------------

-- ----------------------------
-- Table structure for tab_newcarorder
-- ----------------------------
DROP TABLE IF EXISTS `tab_newcarorder`;
CREATE TABLE `tab_newcarorder` (
  `newCarOrderNo` varchar(12) NOT NULL COMMENT '新车订单编号',
  `userAcc` varchar(12) NOT NULL COMMENT '用户账号',
  `newCarNo` varchar(12) NOT NULL COMMENT '新车编号',
  `sendTime` datetime NOT NULL COMMENT '提交时间',
  `planNo` char(3) NOT NULL COMMENT '分期方案编号',
  `status` varchar(5) DEFAULT NULL COMMENT '订单处理状态',
  `result` varchar(10) DEFAULT NULL COMMENT '处理结果',
  PRIMARY KEY (`newCarOrderNo`),
  KEY `ordernewCarNo` (`newCarNo`),
  CONSTRAINT `tab_newcarorder_ibfk_1` FOREIGN KEY (`newCarNo`) REFERENCES `tab_newcars` (`newcarNo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_newcarorder
-- ----------------------------

-- ----------------------------
-- Table structure for tab_newcars
-- ----------------------------
DROP TABLE IF EXISTS `tab_newcars`;
CREATE TABLE `tab_newcars` (
  `newcarNo` varchar(12) NOT NULL COMMENT '汽车编号',
  `carEngineNo` varchar(12) NOT NULL COMMENT '发动机配置编号',
  `basicconNo` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '基本配置编号',
  `carBodyNo` varchar(12) NOT NULL COMMENT '车身配置编号',
  `gearboxNo` varchar(12) NOT NULL COMMENT '变速箱配置编号',
  `carName` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '汽车名称',
  `carIdType` varchar(20) NOT NULL COMMENT '汽车型号',
  `releaseDate` date NOT NULL COMMENT '发售日期',
  `salePrice` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '发售价格',
  `currentPrice` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '目前售价',
  `Status` varchar(6) NOT NULL COMMENT '销售状态',
  PRIMARY KEY (`newcarNo`),
  KEY `carEngineNo` (`carEngineNo`),
  KEY `basicconNo` (`basicconNo`),
  KEY `carBodyNo` (`carBodyNo`),
  KEY `gearboxNo` (`gearboxNo`),
  CONSTRAINT `tab_newcars_ibfk_1` FOREIGN KEY (`basicconNo`) REFERENCES `tab_carbasicconfiguration` (`basicconNo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tab_newcars_ibfk_2` FOREIGN KEY (`carBodyNo`) REFERENCES `tab_carbody` (`carBodyNo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tab_newcars_ibfk_3` FOREIGN KEY (`carEngineNo`) REFERENCES `tab_carengine` (`carEngineNo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tab_newcars_ibfk_4` FOREIGN KEY (`gearboxNo`) REFERENCES `tab_gearbox` (`gearboxNo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_newcars
-- ----------------------------

-- ----------------------------
-- Table structure for tab_picture
-- ----------------------------
DROP TABLE IF EXISTS `tab_picture`;
CREATE TABLE `tab_picture` (
  `pictureNo` int(11) NOT NULL AUTO_INCREMENT COMMENT '图片编号',
  `newcarNo` varchar(12) NOT NULL COMMENT '汽车编号',
  `pictureSrc` varchar(20) NOT NULL COMMENT '图片地址',
  PRIMARY KEY (`pictureNo`),
  KEY `newcarNo` (`newcarNo`),
  CONSTRAINT `tab_picture_ibfk_1` FOREIGN KEY (`newcarNo`) REFERENCES `tab_newcars` (`newcarNo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_picture
-- ----------------------------

-- ----------------------------
-- Table structure for tab_record
-- ----------------------------
DROP TABLE IF EXISTS `tab_record`;
CREATE TABLE `tab_record` (
  `recoredNo` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志编号',
  `userAcc` varchar(12) NOT NULL COMMENT '用户账号',
  `loginIP` varchar(20) NOT NULL COMMENT '登录ip',
  `loginTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '登录时间',
  `loginArea` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`recoredNo`),
  KEY `record_user` (`userAcc`),
  CONSTRAINT `record_user` FOREIGN KEY (`userAcc`) REFERENCES `tab_webaccount` (`userAcc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_record
-- ----------------------------

-- ----------------------------
-- Table structure for tab_usercarinfo
-- ----------------------------
DROP TABLE IF EXISTS `tab_usercarinfo`;
CREATE TABLE `tab_usercarinfo` (
  `userCarInfoNo` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户汽车详情编号',
  `userNewCarNo` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '汽车编号',
  `userSecondCarNo` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '配置编号',
  `userAcc` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户编号',
  `uptDate` datetime NOT NULL COMMENT '购买时间',
  PRIMARY KEY (`userCarInfoNo`),
  KEY `usercarinfo_user` (`userAcc`),
  CONSTRAINT `usercarinfo_user` FOREIGN KEY (`userAcc`) REFERENCES `tab_webaccount` (`userAcc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_usercarinfo
-- ----------------------------

-- ----------------------------
-- Table structure for tab_userinfo
-- ----------------------------
DROP TABLE IF EXISTS `tab_userinfo`;
CREATE TABLE `tab_userinfo` (
  `userNo` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户编号',
  `name` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '姓名',
  `sex` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '性别',
  `idNo` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '身份证号码',
  `userEmail` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(11) NOT NULL COMMENT '联系电话',
  `address` varchar(16) DEFAULT NULL COMMENT '联系地址',
  `photo` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`userNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_userinfo
-- ----------------------------
INSERT INTO `tab_userinfo` VALUES ('201112000001', 'hackerac', '男', '522130199910042014', 'hackerac@163.com', '17385120965', '杭州市', null);
INSERT INTO `tab_userinfo` VALUES ('201112000002', '罗家豪', '男', '123456789012345678', 'luojiahao@163.com', '18145678901', '深圳市', null);
INSERT INTO `tab_userinfo` VALUES ('201112000004', '张皓', '男', '987654321012345678', 'zhanghao@129.com', '19765432101', '兰州市', null);

-- ----------------------------
-- Table structure for tab_webaccount
-- ----------------------------
DROP TABLE IF EXISTS `tab_webaccount`;
CREATE TABLE `tab_webaccount` (
  `userAcc` varchar(12) NOT NULL COMMENT '账户编号',
  `userNo` char(12) NOT NULL COMMENT '用户编号',
  `userName` varchar(12) NOT NULL COMMENT '用户名',
  `userpwd` varchar(16) NOT NULL COMMENT '密码',
  `nickName` varchar(16) NOT NULL COMMENT '昵称',
  `registerDate` datetime NOT NULL COMMENT '注册日期',
  `accType` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账户类型',
  `headImage` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`userAcc`),
  KEY `account_user` (`userNo`),
  CONSTRAINT `account_user` FOREIGN KEY (`userNo`) REFERENCES `tab_userinfo` (`userNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tab_webaccount
-- ----------------------------
INSERT INTO `tab_webaccount` VALUES ('201112000001', '201112000001', 'xff', '123456', 'hackerac', '2020-11-12 14:26:38', '买家', null);
INSERT INTO `tab_webaccount` VALUES ('201112000002', '201112000002', 'ljh', '123456', 'luojiahao', '2020-11-12 14:31:25', '卖家', null);
INSERT INTO `tab_webaccount` VALUES ('201112000003', '201112000004', 'zh', '123456', 'zhanghao', '2020-11-12 14:33:56', '买家', null);
