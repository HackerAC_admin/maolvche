package com.maolvche.controller;

import com.maolvche.pojo.*;
import com.maolvche.service.AfterSaleSerService;
import com.maolvche.vo.WebUser;
import jdk.nashorn.internal.runtime.ECMAException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author HackerAC
 */
@Controller
public class AfterSalesController {
    @Resource
    AfterSaleSerService afterSaleSerService;

    /**
     * 查询登录用户的信用资料
     */
    @RequestMapping(value = "/creditFiles")
    @ResponseBody
    public List<CreditFile> getCreditFiles(HttpSession session) {
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        return afterSaleSerService.getCreditFileByUserAcc(webUser.getAccountInfo().getUseracc());
    }

    /**
     * 上传用户信用资料
     */
    @RequestMapping(value = "/sentCreditFile")
    @ResponseBody
    public int addCreditFiles(HttpSession session, @RequestParam("file") MultipartFile file, HttpServletRequest request,@RequestParam("topic")String topic) {

        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");

        CreditFile creditFile = new CreditFile();

        // 上传文件名
        String filename = file.getOriginalFilename();
        if(!file.isEmpty()&&filename!=null){
            // 上传路径
//            String path = request.getSession().getServletContext().getRealPath("/images/userCreditFile/");
            String path = "F:\\idea_workspace1\\maolvche\\src\\main\\webapp\\images\\userCreditFile";
            System.out.println(path);
            // 判断路径是否存在，不存在就创建一个
            File filepath = new File(path,filename);
            if(!filepath.getParentFile().exists()){
                filepath.getParentFile().mkdirs();
            }
            // 将文件保存到一个目标文件中
            try {
                file.transferTo(new File(path+File.separator+filename));
                creditFile.setFilename(filename);
                creditFile.setFilesrc("images/userCreditFile/"+filename);
                creditFile.setSendtime(new Date());
                creditFile.setTopic(topic);
                creditFile.setUseracc(webUser.getAccountInfo().getUseracc());
                //添加数据库信息
                return afterSaleSerService.sendCreditFile(creditFile);
            }catch (Exception e){
                return 0;
            }
        }else {
            return 0;
        }
    }

    /**
     * 删除信用资料
     */
    @RequestMapping(value = "/delCreditFile")
    @ResponseBody
    public int delCreditFile(HttpSession session, @RequestParam("creditFileNo") String fileNo) {
        return (int)(afterSaleSerService.delCreditFile(fileNo));
    }

    /**
     * 查询登录用户的分期还款记录
     */
    @RequestMapping(value = "/instRecords")
    @ResponseBody
    public List<InstRecord> getInstRecords(HttpSession session) {
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        //获取当前用户初始分期信息保存到session
        session.setAttribute("userInstlInfo",afterSaleSerService.selInstInfoByUserAcc(webUser.getAccountInfo().getUseracc()));
        return afterSaleSerService.selInstRecordByUserAcc(webUser.getAccountInfo().getUseracc());
    }
    /**
     * 新增还款记录
     */
    @RequestMapping(value = "/addInstRecords")
    @ResponseBody
    public int addInstRecords(HttpSession session,@RequestParam("payMoney")String payMoney) {
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        //获取当前用户的还款记录
        List<InstRecord> instlist = afterSaleSerService.selInstRecordByUserAcc(webUser.getAccountInfo().getUseracc());
        //获取最后一次记录
        InstRecord instRecord = instlist.get((instlist.size()-1));
        //获取剩余未还金额。
        float lastmoney = instRecord.getLastmoney();

        //更新还款记录信息
        instRecord.setLastmoney(lastmoney-Float.parseFloat(payMoney));
        instRecord.setRepaymentdate(new Date());
        instRecord.setRepaymentmoney(Float.parseFloat(payMoney));
        instRecord.setInstlmntno(null);

        return afterSaleSerService.addInstRecord(instRecord);
    }



    /**
     * 查询登录用户的养护记录
     */
    @RequestMapping(value = "/maintainRecords")
    @ResponseBody
    public List<Maintain> getMaintainRecords(HttpSession session) {
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        return afterSaleSerService.getMaintainInfoByUserAcc(webUser.getAccountInfo().getUseracc());
    }

    /**
     * 查询登录用户的退换记录
     */
    @RequestMapping(value = "/exchangeRecords")
    @ResponseBody
    public List<Exchange> getExchangeRecords(HttpSession session) {
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        return afterSaleSerService.getExchangeByUserAcc(webUser.getAccountInfo().getUseracc());
    }

    /**
     * 登录用户新增退换申请
     */
    @RequestMapping(value = "/addExchangeRecord")
    @ResponseBody
    public int addExchangeRecord(HttpSession session,@RequestParam("carNo")String exchangeCarId,@RequestParam("reason")String reason) {
        //业务操作标志
        int flag;
        Exchange exchange=new Exchange();
        //获取登录用户
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        String userAcc=webUser.getAccountInfo().getUseracc();
        //查询该用户是否提交过该辆车的退换申请
        if(afterSaleSerService.getExchangeByUserAccAndCarNo(userAcc,exchangeCarId)==null) {
            exchange.setApplydate(new Date());
            exchange.setCarno(exchangeCarId);
            exchange.setUseracc(userAcc);
            exchange.setState("未处理");
            exchange.setExchangereason(reason);
            flag=afterSaleSerService.addExchangeApplication(exchange);
        }else{
            flag=2;
        }
        return flag;
    }

    /**
     * 查询登录用户的反馈记录
     */
    @RequestMapping(value = "/userFeedbackRecords")
    @ResponseBody
    public List<Feedback> getUserFeedbackRecords(HttpSession session) {
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        return afterSaleSerService.getFeedBackByUserAcc(webUser.getAccountInfo().getUseracc());
    }

    /**
     * 用户提交新反馈
     */
    @RequestMapping(value = "/addFeedbackRecord")
    @ResponseBody
    public int addFeedbackRecord(HttpSession session, @RequestParam("fbcontends")String fbcontends, @RequestParam("topic")String topic) {
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        Feedback feedback = new Feedback();
        feedback.setFbcontents(fbcontends);
        feedback.setTopic(topic);
        feedback.setUseracc(webUser.getAccountInfo().getUseracc());
        feedback.setSendtime(new Date());
        feedback.setHandleresult("未处理");
        return afterSaleSerService.addFeedbackInfo(feedback);
    }
    /**
     * 用户删除投诉反馈
     */
    @RequestMapping(value = "/delFeedbackRecord")
    @ResponseBody
    public int delFeedbackRecord(HttpSession session, @RequestParam("fbid")String fbid) {
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        Feedback feedback = new Feedback();
        feedback.setFeedbackno(Integer.parseInt(fbid));
        return afterSaleSerService.delFeedBack(feedback);
    }


    /**
     *请求用户拥有的汽车
     */
    @RequestMapping(value = "/getUserCars")
    @ResponseBody
    public List<UserCarInfo> getUserCar(HttpSession session){
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        return afterSaleSerService.selUserCarByUserAcc(webUser.getAccountInfo().getUseracc());
    }



}
