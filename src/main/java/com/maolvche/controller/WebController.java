package com.maolvche.controller;

import com.maolvche.service.AfterSaleSerService;
import com.maolvche.service.BGMShelpGuideService;
import com.maolvche.vo.NewCarCard;
import com.maolvche.vo.WebUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author HackerAC
 * 本控制器处理网站所有跳转业务。
 * thisNav表示导航栏的导航显示位置
 * 0代表没有，1表示首页，2表示买新车，3表示买二手车...
 */
@Controller
public class WebController {
    @Resource
    AfterSaleSerService afterSaleSerService;

    @Resource
    BGMShelpGuideService bgmShelpGuideService;

    /**
     * 跳转到主页index.jsp
     */
    @RequestMapping(value = "/main")
    public ModelAndView visitIndex(ModelAndView modelAndView, HttpSession session){
        //如果没有设置过城市，则默认显示天津的车源
        String thisCity = (String)session.getAttribute("thisCity");
        if("".equals(thisCity)||thisCity==null){
            session.setAttribute("thisCity","天津");
        }

        //查询当前车源城市最新添加的前10辆二手车List集合，保存到session，名为tenOfSecondCar


        //查询最新添加的前10辆新车List集合，保存到session，名为tenOfNewCar




        //设置导航栏位置
        modelAndView.addObject("thisNav",1);
        //返回首页
        modelAndView.setViewName("index");
        return modelAndView;
    }
    /**
     * 选择城市跳转到相应车源城市主页index.jsp
     */
    @RequestMapping(value = "/cityMain")
    public ModelAndView visitIndex(HttpSession session,ModelAndView modelAndView, @RequestParam("city")String city){

        //设置车源地
        session.setAttribute("thisCity",city);

        //查询当前车源城市最新添加的前10辆二手车List集合，保存到session，名为tenOfSecondCar




        //设置导航栏位置
        modelAndView.addObject("thisNav",1);
        modelAndView.setViewName("index");
        return modelAndView;
    }

    /**
     * 跳转到登录页login.jsp
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/login")
    public ModelAndView toLogin(ModelAndView modelAndView){
        modelAndView.addObject("thisNav",0);
        modelAndView.setViewName("login");
        return modelAndView;
    }

    /**
     * 跳转到注册页register.jsp
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/register")
    public ModelAndView toRegister(ModelAndView modelAndView){
        modelAndView.addObject("thisNav",0);
        modelAndView.setViewName("register");
        return modelAndView;
    }

    /**
     * 跳转到买新车页buyCarIndex.jsp
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/buyCarIndex")
    public ModelAndView toBuyCarIndex(ModelAndView modelAndView){
        modelAndView.addObject("thisNav",2);
        modelAndView.setViewName("buyCarIndex");
        return modelAndView;
    }

    /**
     * 跳转到买二手车页buySecondCarIndex.jsp
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/buySecondCarIndex")
    public ModelAndView toBuySecondCarIndex(ModelAndView modelAndView){
        /*
        *thisNav表示导航栏的导航显示位置
        * 0代表没有，1表示首页，2表示买新车，3表示买二手车...
        */
        modelAndView.addObject("thisNav",3);
        modelAndView.setViewName("buySecondCarIndex");
        return modelAndView;
    }

    /**
     * 跳转到新车详情页newCarDetail.jsp
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/newCarDetail")
    public ModelAndView toNewCarDetail(ModelAndView modelAndView){
        //根据前端传递来的汽车编号，查询汽车的以下信息
        //NewCarCard类里的所有信息
        //汽车基本配置信息封装为CarBaseConfig对象，命名为carBaseConfig存入modelAndView。
        //车身信息封装 命名为carBody
        //发动机信息封装 命名为carEngine
        //变速箱信息封装 命名为carGearBox
        //以上都要查询出来按照相应命名后保存到modelAndView中。




        //设置导航栏位置
        modelAndView.addObject("thisNav",2);
        //返回到汽车详情页
        modelAndView.setViewName("newCarDetail");
        return modelAndView;
    }

    /**
     * 跳转到个人中心userPersonalCenter.jsp
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/toPersonalCenter")
    public ModelAndView toPersonalCenter(HttpSession session,ModelAndView modelAndView, HttpServletRequest request){
        modelAndView.addObject("thisNav",0);
        if("http://localhost:8080/maolvche_war/toBuyByAml3".equals(request.getHeader("Referer"))){
            modelAndView.addObject("prePage","toBuyByAml3");
        }
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        //获取用户初始分期信息保存到session
        session.setAttribute("userInstlInfo",afterSaleSerService.selInstInfoByUserAcc(webUser.getAccountInfo().getUseracc()));

        modelAndView.setViewName("userPersonalCenter");
        return modelAndView;
    }

    /**
     * 跳转全款购车第一步
     */
    @RequestMapping(value = "/toBuyByAml1")
    public ModelAndView toBuyByAml1(HttpSession session,ModelAndView modelAndView){
        WebUser webUser=(WebUser)session.getAttribute("LOGIN_USER");
        if(webUser==null){
            modelAndView.setViewName("login");
        }else{
            modelAndView.setViewName("buyByAmlPage1");
        }
        modelAndView.addObject("thisNav",2);
        return modelAndView;
    }
    /**
     * 跳转全款购车第二步
     */
    @RequestMapping(value = "/toBuyByAml2")
    public ModelAndView toBuyByAml2(ModelAndView modelAndView){
        modelAndView.addObject("thisNav",2);
        modelAndView.setViewName("buyByAmlPage2");
        return modelAndView;
    }
    /**
     * 跳转全款购车第三步
     */
    @RequestMapping(value = "/toBuyByAml3")
    public ModelAndView toBuyByAml3(ModelAndView modelAndView){
        modelAndView.addObject("thisNav",2);
        modelAndView.setViewName("buyByAmlPage3");
        return modelAndView;
    }

    /**
     * 跳转帮助向导页的关于我们
     */
    @RequestMapping(value = "/toHelpabout")
    public ModelAndView toHelpabout(ModelAndView modelAndView){
        modelAndView.addObject("navId",0);
        modelAndView.setViewName("HelpGuide");
        return modelAndView;
    }

    /**
     * 跳转帮助向导页的交易帮助
     */
    @RequestMapping(value = "/toHelppaymentHelp")
    public ModelAndView toHelppaymentHelp(ModelAndView modelAndView){
        modelAndView.addObject("navId",1);
        modelAndView.setViewName("HelpGuide");
        return modelAndView;
    }

    /**
     * 跳转帮助向导页的售后服务
     */
    @RequestMapping(value = "/toHelpafterServiceHelp")
    public ModelAndView toafterServiceHelp(ModelAndView modelAndView){
        modelAndView.addObject("navId",2);
        modelAndView.setViewName("HelpGuide");
        return modelAndView;
    }

    /**
     * 跳转帮助向导页的卖家服务
     */
    @RequestMapping(value = "/toHelpsalerServiceHelp")
    public ModelAndView toHelpsalerServiceHelp(ModelAndView modelAndView){
        modelAndView.addObject("navId",3);
        modelAndView.setViewName("HelpGuide");
        return modelAndView;
    }

    /**
     * 跳转帮助向导页的投诉反馈
     */
    @RequestMapping(value = "/toHelpfeedbackHelp")
    public ModelAndView toHelpfeedbackHelp(ModelAndView modelAndView){
        modelAndView.addObject("navId",4);
        modelAndView.setViewName("HelpGuide");
        return modelAndView;
    }

    /**
     * 跳转到关于我们
     */
    @RequestMapping(value = "/about")
    public ModelAndView toAboutUs(ModelAndView modelAndView){
        modelAndView.addObject("helps",bgmShelpGuideService.queryHelpGuideList2("module","关于我们"));
        modelAndView.setViewName("about");
        return modelAndView;
    }

    /**
     * 跳转到交易帮助
     */
    @RequestMapping(value = "/paymentHelp")
    public ModelAndView topaymentHelp(ModelAndView modelAndView){
        modelAndView.addObject("helps",bgmShelpGuideService.queryHelpGuideList2("module","交易"));
        modelAndView.setViewName("paymentHelp");
        return modelAndView;
    }

    /**
     * 跳转到售后服务
     */
    @RequestMapping(value = "/afterServiceHelp")
    public ModelAndView toAfterServiceHelp(ModelAndView modelAndView){
        modelAndView.addObject("helps",bgmShelpGuideService.queryHelpGuideList2("module","售后"));
        modelAndView.setViewName("afterServiceHelp");
        return modelAndView;
    }

    /**
     * 跳转到卖家服务
     */
    @RequestMapping(value = "/salerServiceHelp")
    public ModelAndView toSalerServiceHelp(ModelAndView modelAndView){
        modelAndView.addObject("helps",bgmShelpGuideService.queryHelpGuideList2("module","卖家"));
        modelAndView.setViewName("salerServiceHelp");
        return modelAndView;
    }

    /**
     * 跳转到投诉反馈
     */
    @RequestMapping(value = "/feedbackHelp")
    public ModelAndView toFeedbackHelp(ModelAndView modelAndView){
        modelAndView.addObject("helps",bgmShelpGuideService.queryHelpGuideList2("module","投诉"));
        modelAndView.setViewName("feedbackHelp");
        return modelAndView;
    }


}
