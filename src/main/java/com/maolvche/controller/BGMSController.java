package com.maolvche.controller;

import com.maolvche.pojo.*;
import com.maolvche.service.*;
import com.maolvche.vo.WebUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import sun.applet.Main;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * @author HackerAC
 * 本控制器处理网站所有跳转业务。
 * thisNav表示导航栏的导航显示位置
 * 0代表没有，1表示首页，2表示买新车，3表示买二手车...
 */
@Controller
public class BGMSController {
    @Resource
    BGMSfeedBackService bgmSfeedBackService;

    @Resource
    AfterSaleSerService afterSaleSerService;

    @Resource
    BGMSexchangeService bgmSexchangeService;

    @Resource
    BGMSmainTainService bgmSmainTainService;

    @Resource
    BGMSinstlPlanService bgmSinstlPlanService;

    @Resource
    BGMSloginRecordService bgmSloginRecordService;

    @Resource
    BGMShelpGuideService bgmShelpGuideService;

    @Resource
    BGMSinstlRecordService bgmSinstlRecordService;
    /**
     * 查询所有投诉反馈管理信息
     */
    @RequestMapping(value = "/feedbackRecords")
    @ResponseBody
    public List<Feedback> getFeedbackRecords() {
        return bgmSfeedBackService.getAllFeedBacks();
    }

    /**
     * 添加投诉反馈回复
     */
    @RequestMapping(value = "/addFeedbackReplay")
    @ResponseBody
    public int addFeedbackReplay(HttpSession session,@RequestParam("feedbackid")String feedbackid,@RequestParam("content")String content) {
        Feedback feedback=new Feedback();
        int id=Integer.parseInt(feedbackid);
        feedback.setFeedbackno(id);
        feedback.setHandleresult(content);
        return bgmSfeedBackService.uptFeedBack(feedback);
    }

    /**
     * 查询所有用户提交的资料
     */
    @RequestMapping(value = "/BGMScreditFiles")
    @ResponseBody
    public List<CreditFile> getAllCreditFile() {
        return afterSaleSerService.getAllCreditFile();
    }

    /**
     * 根据查找项查询用户资料
     */
    @RequestMapping(value = "/queryBGMScreditFiles")
    @ResponseBody
    public List<CreditFile> queryCreditFile(@RequestParam("queryItem")String queryItem,@RequestParam("itemValue")String itemValue) {
        return afterSaleSerService.queryCreditFile(queryItem,itemValue);
    }

    /**
     * 查询所有退换申请
     */
    @RequestMapping(value = "/BgExchangeRecords")
    @ResponseBody
    public List<Exchange> getAllExchanges() {
        return bgmSexchangeService.getAllExchange();
    }

    /**
     * 根据查询项查询退换申请
     */
    @RequestMapping(value = "/queryExchange")
    @ResponseBody
    public List<Exchange> queryExchanges(@RequestParam("queryItem")String item,@RequestParam("itemValue")String value) {
        return bgmSexchangeService.queryExchanges(item,value);
    }

    /**
     * 添加退换审核结果
     */
    @RequestMapping(value = "/addExchangeReplay")
    @ResponseBody
    public int addExchangeReplay(@RequestParam("ifExchange")String ifExchange,@RequestParam("exchangeId")String exchangeId,@RequestParam("content")String content) {

        Exchange exchange=new Exchange();
        int id=Integer.parseInt(exchangeId);
        exchange.setExchangedno(id);
        exchange.setAuditresult(content);
        if("y".equals(ifExchange)){
            exchange.setExchangeddate(new Date());
        }
        return bgmSexchangeService.uptExchange(exchange);
    }

    /**
     * 根据退换编号删除退换记录
     */
    @RequestMapping(value = "/delExchange")
    @ResponseBody
    public int delExchange(@RequestParam("exchangeId")String exchangeId) {
        return bgmSexchangeService.delExchange(exchangeId);
    }

    /**
     * 查询所有维保养护记录
     */
    @RequestMapping(value = "/BGmaintainRecord")
    @ResponseBody
    public List<Maintain> getAllMaintain() {
        return bgmSmainTainService.selAllMaintainRecord();
    }

    /**
     * 添加维保养护记录
     */
    @RequestMapping(value = "/addMaintainRecord")
    @ResponseBody
    public int addMaintainRecord(HttpSession session,@RequestParam("useracc")String useracc,
                                 @RequestParam("carNo")String carNo,@RequestParam("MaintainItems")String MaintainItems,
                                 @RequestParam("price")String price) {
        EmpAccount empAccount = (EmpAccount)session.getAttribute("BGMS_EMPLOY");

        Maintain maintain = new Maintain();
        maintain.setMaintainno(null);
        maintain.setCarno(carNo);
        maintain.setMaintainitem(MaintainItems);
        maintain.setMaintaindate(new Date());
        float pri = Float.parseFloat(price);
        maintain.setUseracc(useracc);
        maintain.setPrice(pri);

        if(empAccount!=null){
            maintain.setEmployeeno(empAccount.getEmployeeno());
        }

        return bgmSmainTainService.addMaintainRecord(maintain);
    }

    /**
     * 根据养护维保编号删除维保记录
     */
    @RequestMapping(value = "/delMaintainRecord")
    @ResponseBody
    public int delMaintainRecord(@RequestParam("id")String id) {
        return bgmSmainTainService.delMaintainRecord(id);
    }

    /**
     * 根据查询项查询养护维保记录
     */
    @RequestMapping(value = "/queryMaintainRecord")
    @ResponseBody
    public List<Maintain> queryMaintainRecord(@RequestParam("item")String item,@RequestParam("value")String value) {
        return bgmSmainTainService.queryMaintainRecord(item,value);
    }

    /**
     * 查询所有分期方案
     */
    @RequestMapping(value = "/instlPlans")
    @ResponseBody
    public List<InstPlan> getAllInstlPlan() {
        return bgmSinstlPlanService.getAllinstPlan();
    }

    /**
     * 添加分期方案
     */
    @RequestMapping(value = "/addInstlPlan")
    @ResponseBody
    public String addMaintainRecord(HttpSession session,@RequestParam("planNo")String planNo,@RequestParam("percent")String percent,
                                 @RequestParam("times")String times) {

        InstPlan instPlan = new InstPlan();
        instPlan.setPlanno(planNo);
        int pst = Integer.parseInt(percent);
        instPlan.setPercent(pst);
        int tms = Integer.parseInt(times);
        instPlan.setTimes(tms);

        List<InstPlan> instPlans = bgmSinstlPlanService.selinstPlan(instPlan);

        if(instPlans.size()!=0){
            String selplanNo = instPlans.get(0).getPlanno();
            int selPst = instPlans.get(0).getPercent();
            int times1 = instPlans.get(0).getTimes();

            if(selplanNo!=null&& !"".equals(selplanNo)){
                return "planNoError";
            }else if(selPst==pst&&times1==tms){
                return "planError";
            }else{
                return "bigError";
            }
        }else{
            bgmSinstlPlanService.addInstPlan(instPlan);
            return "ok";
        }
    }

    /**
     * 根据编号删除分期方案
     */
    @RequestMapping(value = "/delInstlPlan")
    @ResponseBody
    public int delInstlPlan(@RequestParam("id")String id) {
        int flag=0;
        try {
            flag=bgmSinstlPlanService.delInstPlan(id);
        }catch (Exception e){
            flag=2;
        }
        return flag;
    }

    /**
     * 根据查询项查询分期方案
     */
    @RequestMapping(value = "/queryInstlPlan")
    @ResponseBody
    public List<InstPlan> queryInstlPlan(@RequestParam("item")String item,@RequestParam("value")String  value) {
        InstPlan instPlan = new InstPlan();
        if("pst".equals(item)){
            int pst1=Integer.parseInt(value);
            instPlan.setPercent(pst1);
        }else{
            int tms1=Integer.parseInt(value);
            instPlan.setTimes(tms1);
        }
        return bgmSinstlPlanService.selinstPlan(instPlan);
    }

    /**
     * 查询所有分期记录
     */
    @RequestMapping(value = "/instlRecords")
    @ResponseBody
    public List<InstRecord> getAllinstlRecords() {
        return bgmSinstlRecordService.getAllInstRecords();
    }


    /**
     * 添加分期记录
     */
    @RequestMapping(value = "/addInstlRecords")
    @ResponseBody
    public int addInstlRecords(@RequestParam("planNo")String planNo,@RequestParam("userAcc")String userAcc,
                                    @RequestParam("repaymentMoney")String repaymentMoney,@RequestParam("lastMoney")String lastMoney) {
        InstRecord instRecord = new InstRecord();
        instRecord.setInstlmntno(null);
        instRecord.setPlanno(planNo);
        instRecord.setUseracc(userAcc);
        float rpmtMoney = Float.parseFloat(repaymentMoney);
        instRecord.setRepaymentmoney(rpmtMoney);
        instRecord.setRepaymentdate(new Date());
        float lastMoney2 = Float.parseFloat(lastMoney);
        instRecord.setLastmoney(lastMoney2);
        return bgmSinstlRecordService.addInstlRecords(instRecord);
    }

    /**
     * 删除分期记录
     */
    @RequestMapping(value = "/delinstlRecord")
    @ResponseBody
    public int delinstlRecord(@RequestParam("id")String id) {
        return bgmSinstlRecordService.delInstRecord(id);
    }

    /**
     * 根据查询项查询分期方记录
     */
    @RequestMapping(value = "/queryInstlRecord")
    @ResponseBody
    public List<InstRecord> queryInstlRecord(@RequestParam("queryItem")String queryItem,@RequestParam("itemValue")String  itemValue) {
        return bgmSinstlRecordService.queryInstRecord(queryItem,itemValue);
    }

    /**
     * 查询所有登录日志
     */
    @RequestMapping(value = "/allLoginRecords")
    @ResponseBody
    public List<LoginRecord> allLoginRecords() {
        return bgmSloginRecordService.getAllRecord();
    }

    /**
     * 根据查询项查询登录日志
     */
    @RequestMapping(value = "/queryLoginRecord")
    @ResponseBody
    public List<LoginRecord> queryLoginRecords(@RequestParam("queryItem")String queryItem,@RequestParam("itemValue")String itemValue) {
        return bgmSloginRecordService.queryLoginRecord(queryItem,itemValue);
    }

    /**
     * 查询所有帮助向导信息
     */
    @RequestMapping(value = "/helpGuides")
    @ResponseBody
    public List<HelpGuide> allhelpGuides() {
        return bgmShelpGuideService.getAllHelpGuideList();
    }

    /**
     * 根据查询项查询帮助向导信息
     */
    @RequestMapping(value = "/queryHelpGuide")
    @ResponseBody
    public List<HelpGuide> queryHelpGuide(@RequestParam("queryItem")String queryItem,@RequestParam("itemValue")String  itemValue) {
        return bgmShelpGuideService.queryHelpGuideList(queryItem,itemValue);
    }

    /**
     * 添加帮助向导
     */
    @RequestMapping(value = "/addHelpGuide")
    @ResponseBody
    public int addHelpGuide(@RequestParam("module")String module,@RequestParam("topic")String  topic,
                            @RequestParam("content")String content,@RequestParam("state")String  state) {
        HelpGuide helpGuide = new HelpGuide();
        helpGuide.setHelpguidno(null);
        helpGuide.setModule(module);
        helpGuide.setTopic(topic);
        helpGuide.setContent(content);
        helpGuide.setState(state);
        helpGuide.setUpdatetime(new Date());
        return bgmShelpGuideService.addHelpGuide(helpGuide);
    }

    /**
     * 删除帮助向导
     */
    @RequestMapping(value = "/delHelpGuide")
    @ResponseBody
    public int  delHelpGuide(@RequestParam("id")String id) {
        return bgmShelpGuideService.delHelpGuide(id);
    }


}
