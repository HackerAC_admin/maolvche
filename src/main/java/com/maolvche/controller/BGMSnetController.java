package com.maolvche.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 后台管理页面跳转路由逻辑控制器
 * @author HackerAC
 */
@Controller
public class BGMSnetController {
    /**
     * 跳转到后台员工登录页
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/bgmsemplogin")
    public ModelAndView visitEmpLogin(ModelAndView modelAndView){
        modelAndView.setViewName("BGMSLoginEmp");
        return modelAndView;
    }
    /**
     * 跳转到管理员登录页
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/bgmsadmlogin")
    public ModelAndView visitAdmLogin(ModelAndView modelAndView){

        modelAndView.setViewName("BGMSLoginAdm");
        return modelAndView;
    }

    /**
     * 跳转到欢迎页
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/toWelcome")
    public ModelAndView toWelcome(ModelAndView modelAndView){

        modelAndView.setViewName("BGMSwelcome");
        return modelAndView;
    }

    /**
     * 跳转到后台投诉反馈管理页
     */
    @RequestMapping(value = "toBgfeedbackList")
    @ResponseBody
    public ModelAndView toFeedbackPage(HttpSession session,ModelAndView modelAndView) {
        modelAndView.setViewName("BGMSfeedBackList");
        return modelAndView;
    }

    /**
     * 跳转到用户资料管理页
     */
    @RequestMapping(value = "BGMSusrCreditFileList")
    @ResponseBody
    public ModelAndView toBGMSusrCreditFilePage(ModelAndView modelAndView) {
        modelAndView.setViewName("BGMSusrCreditFileList");
        return modelAndView;
    }

    /**
     * 跳转到退换管理页
     */
    @RequestMapping(value = "toExchangeList")
    @ResponseBody
    public ModelAndView toExchangeList(ModelAndView modelAndView) {
        modelAndView.setViewName("BGMSexchangeList");
        return modelAndView;
    }

    /**
     * 跳转到维保养护信息管理页
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/toMaintain")
    public ModelAndView toMaintainPage(ModelAndView modelAndView){

        modelAndView.setViewName("BGMSmainTain");
        return modelAndView;
    }

    /**
     * 跳转到管理员信息管理页
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/toAdminList")
    public ModelAndView toAdminList(ModelAndView modelAndView){

        modelAndView.setViewName("BGMSadminList");
        return modelAndView;
    }

    /**
     * 跳转到分期方案管理页
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/toInstlPlan")
    public ModelAndView toInstlPlan(ModelAndView modelAndView){

        modelAndView.setViewName("BGMSinstPlan");
        return modelAndView;
    }

    /**
     * 跳转到分期还款记录管理页
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/toInstlRecordPage")
    public ModelAndView toInstlRecordPage(ModelAndView modelAndView){
        modelAndView.setViewName("BGMSinstRecordList");
        return modelAndView;
    }

    /**
     * 跳转到网站用户登录日志记录管理页
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/toLoginRecordPage")
    public ModelAndView toLoginRecordPage(ModelAndView modelAndView){
        modelAndView.setViewName("BGMSwebLoginRecord");
        return modelAndView;
    }

    /**
     * 跳转到帮助向导信息管理页
     * @param modelAndView
     * @return
     */
    @RequestMapping(value = "/toHelpGuidePage")
    public ModelAndView toHelpGuidePage(ModelAndView modelAndView){
        modelAndView.setViewName("BGMShelpGuideList");
        return modelAndView;
    }

}
