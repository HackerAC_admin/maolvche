package com.maolvche.serviceimpl;

import com.maolvche.pojo.NewCar;
import com.maolvche.service.NewCarService;
import com.maolvche.vo.CarSelOption;
import com.maolvche.vo.NewCarCard;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewCarServiceImpl implements NewCarService {

    /**
     * 查询最新添加的前10辆新车
     *
     * @return com.maolvche.vo包下的NewCarCard类List集合
     */
    @Override
    public List<NewCarCard> getTenLatestNewCar() {
        return null;
    }

    /**
     * 根据查询条件中不为空的值进行查询
     *
     * @param carSelOption
     */
    @Override
    public List<NewCar> selByOptions(CarSelOption carSelOption) {
        return null;
    }
}
