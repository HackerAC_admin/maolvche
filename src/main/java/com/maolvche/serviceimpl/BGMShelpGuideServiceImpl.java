package com.maolvche.serviceimpl;

import com.maolvche.dao.HelpGuideMapper;
import com.maolvche.pojo.HelpGuide;
import com.maolvche.pojo.HelpGuideExample;
import com.maolvche.service.BGMShelpGuideService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class BGMShelpGuideServiceImpl implements BGMShelpGuideService {
    @Resource
    HelpGuideMapper helpGuideMapper;

    @Override
    public List<HelpGuide> getAllHelpGuideList() {
        return helpGuideMapper.selectByExampleWithBLOBs(null);
    }

    @Override
    public List<HelpGuide> queryHelpGuideList(String item, String value) {
        HelpGuideExample helpGuideExample = new HelpGuideExample();
        HelpGuideExample.Criteria criteria = helpGuideExample.createCriteria();

        if("mdl".equals(item)){
            criteria.andModuleEqualTo(value);
            return helpGuideMapper.selectByExampleWithBLOBs(helpGuideExample);
        }else  if("tpc".equals(item)){
            criteria.andTopicEqualTo(value);
            return helpGuideMapper.selectByExampleWithBLOBs(helpGuideExample);
        }else{
            return helpGuideMapper.selectByExampleWithBLOBs(null);
        }
    }

    @Override
    public List<HelpGuide> queryHelpGuideList2(String item, String value) {
        HelpGuideExample helpGuideExample = new HelpGuideExample();
        HelpGuideExample.Criteria criteria = helpGuideExample.createCriteria();
        criteria.andStateEqualTo("已发布");

        if("module".equals(item)){
            criteria.andModuleEqualTo(value);
        }else{
            helpGuideExample=null;
        }
        return helpGuideMapper.selectByExampleWithBLOBs(helpGuideExample);
    }

    @Override
    public int addHelpGuide(HelpGuide helpGuide) {
        return helpGuideMapper.insert(helpGuide);
    }

    @Override
    public int uptHelpGuide(HelpGuide helpGuide) {
        return helpGuideMapper.updateByPrimaryKeySelective(helpGuide);
    }

    @Override
    public int delHelpGuide(String no) {
        int id = Integer.parseInt(no);
        return helpGuideMapper.deleteByPrimaryKey(id);
    }
}
