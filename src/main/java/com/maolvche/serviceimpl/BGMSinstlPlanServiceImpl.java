package com.maolvche.serviceimpl;

import com.maolvche.dao.InstPlanMapper;
import com.maolvche.pojo.InstPlan;
import com.maolvche.pojo.InstPlanExample;
import com.maolvche.service.BGMSinstlPlanService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class BGMSinstlPlanServiceImpl implements BGMSinstlPlanService {
    @Resource
    InstPlanMapper instPlanMapper;

    /**
     * 查询所有分期方案
     *
     * @return
     */
    @Override
    public List<InstPlan> getAllinstPlan() {
        return instPlanMapper.selectByExample(null);
    }

    /**
     * 查询分期方案
     *
     * @param instPlan
     * @return
     */
    @Override
    public List<InstPlan> selinstPlan(InstPlan instPlan) {
        InstPlanExample instPlanExample = new InstPlanExample();
        InstPlanExample.Criteria criteria = instPlanExample.createCriteria();
        criteria.andPlannoEqualTo(instPlan.getPlanno());
        criteria.andPercentEqualTo(instPlan.getPercent());
        criteria.andTimesEqualTo(instPlan.getTimes());
        return instPlanMapper.selectByExample(instPlanExample);
    }

    /**
     * 添加新的分期方案
     *
     * @param instPlan
     * @return
     */
    @Override
    public int addInstPlan(InstPlan instPlan) {
        return instPlanMapper.insert(instPlan);
    }

    /**
     * 根据id删除分期方案
     *
     * @param id
     * @return
     */
    @Override
    public int delInstPlan(String id) {
        return instPlanMapper.deleteByPrimaryKey(id);
    }
}
