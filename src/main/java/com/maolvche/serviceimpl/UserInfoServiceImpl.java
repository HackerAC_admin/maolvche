package com.maolvche.serviceimpl;

import com.maolvche.dao.UserCarInfoMapper;
import com.maolvche.dao.UserInfoMapper;
import com.maolvche.pojo.UserInfo;
import com.maolvche.pojo.UserInfoExample;
import com.maolvche.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author HackerAC
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Resource
    UserInfoMapper userInfoMapper;

    /**
     * 根据用户编号查询用户信息
     *
     * @param userNo 用户编号
     * @return UserInfo 用户信息
     */
    @Override
    public UserInfo selByUserNo(String userNo) {
        return userInfoMapper.selectByPrimaryKey(userNo);
    }

    /**
     * 根据手机号查询完整用户信息
     *
     * @param phone 手机号
     * @return UserInfo 完整用户信息
     */
    @Override
    public UserInfo selByUserPhone(String phone) {
        UserInfoExample userInfoExample = new UserInfoExample();
        UserInfoExample.Criteria criteria= userInfoExample.createCriteria();
        criteria.andPhoneEqualTo(phone);
        List<UserInfo> userInfoList = userInfoMapper.selectByExample(userInfoExample);
        if(userInfoList.size()!=0){
            return userInfoList.get(0);
        }else{
            return null;
        }
    }

    /**
     * 新增用户信息
     *
     * @param userInfo
     */
    @Override
    public int addUserInfo(UserInfo userInfo) {
        return userInfoMapper.insertSelective(userInfo);
    }

    /**
     * 删除用户信息
     *
     * @param userInfo
     */
    @Override
    public int delUserInfo(UserInfo userInfo) {
        return userInfoMapper.deleteByPrimaryKey(userInfo.getUserno());
    }


}
