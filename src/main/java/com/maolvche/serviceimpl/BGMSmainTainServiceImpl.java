package com.maolvche.serviceimpl;

import com.maolvche.dao.MaintainMapper;
import com.maolvche.pojo.Maintain;
import com.maolvche.pojo.MaintainExample;
import com.maolvche.service.BGMSmainTainService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BGMSmainTainServiceImpl implements BGMSmainTainService {
    @Resource
    MaintainMapper maintainMapper;
    /**
     * 查询所有维保养护记录
     *
     * @return
     */
    @Override
    public List<Maintain> selAllMaintainRecord() {
        return maintainMapper.selectByExample(null);
    }

    /**
     * 添加维保养护记录
     *
     * @param maintain
     * @return
     */
    @Override
    public int addMaintainRecord(Maintain maintain) {
        return maintainMapper.insertSelective(maintain);
    }

    /**
     * 删除维保养护记录
     *
     * @param maintainNo
     * @return
     */
    @Override
    public int delMaintainRecord(String maintainNo) {
        int id = Integer.parseInt(maintainNo);
        return maintainMapper.deleteByPrimaryKey(id);
    }

    /**
     * 根据查询项查询维保养护记录
     *
     * @param queryitem
     * @param value
     * @return
     */
    @Override
    public List<Maintain> queryMaintainRecord(String queryitem, String value) {
        MaintainExample maintainExample = new MaintainExample();
        MaintainExample.Criteria criteria = maintainExample.createCriteria();

        if("carid".equals(queryitem)){
            criteria.andCarnoEqualTo(value);
            return maintainMapper.selectByExample(maintainExample);
        }else if("acc".equals(queryitem)){
            criteria.andUseraccEqualTo(value);
            return maintainMapper.selectByExample(maintainExample);
        }else if("emplyId".equals(queryitem)){
            criteria.andEmployeenoEqualTo(value);
            return maintainMapper.selectByExample(maintainExample);
        }else{
            return maintainMapper.selectByExample(null);
        }
    }
}
