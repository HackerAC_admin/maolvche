package com.maolvche.serviceimpl;

import com.maolvche.dao.LoginRecordMapper;
import com.maolvche.pojo.LoginRecord;
import com.maolvche.pojo.LoginRecordExample;
import com.maolvche.service.BGMSloginRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class BGMSloginRecordServiceImpl implements BGMSloginRecordService {
    @Resource
    LoginRecordMapper loginRecordMapper;
    /**
     * 获取所有用户的登录日志
     *
     * @return
     */
    @Override
    public List<LoginRecord> getAllRecord() {
        return loginRecordMapper.selectByExample(null);
    }

    /**
     * 根据查询项查询登录日志
     *
     * @param loginRecord
     * @return
     */
    @Override
    public List<LoginRecord> queryLoginRecord(String item , String value) {

        LoginRecordExample loginRecordExample = new LoginRecordExample();
        LoginRecordExample.Criteria criteria = loginRecordExample.createCriteria();
        if("acc".equals(item)){
            criteria.andUseraccEqualTo(value);
            return loginRecordMapper.selectByExample(loginRecordExample);
        }else if("ipAddr".equals(item)){
            criteria.andLoginipEqualTo(value);
            return loginRecordMapper.selectByExample(loginRecordExample);
        }else{
            return loginRecordMapper.selectByExample(null);
        }
    }


}
