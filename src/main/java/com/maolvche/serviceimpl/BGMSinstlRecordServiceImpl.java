package com.maolvche.serviceimpl;

import com.maolvche.dao.InstRecordMapper;
import com.maolvche.pojo.InstRecord;
import com.maolvche.pojo.InstRecordExample;
import com.maolvche.pojo.Maintain;
import com.maolvche.pojo.MaintainExample;
import com.maolvche.service.BGMSinstlPlanService;
import com.maolvche.service.BGMSinstlRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class BGMSinstlRecordServiceImpl implements BGMSinstlRecordService {
    @Resource
    InstRecordMapper instRecordMapper;

    /**
     * 查询所有分期还款记录
     * @return
     */
    @Override
    public List<InstRecord> getAllInstRecords() {
        return instRecordMapper.selectByExample(null);
    }

    /**
     * 删除分期还款记录
     * @param id
     * @return
     */
    @Override
    public int delInstRecord(String id) {
        int instlNo = Integer.parseInt(id);
        return instRecordMapper.deleteByPrimaryKey(instlNo);
    }

    /**
     * 根据查询项查询分期还款记录
     * @param queryitem
     * @param value
     * @return
     */
    @Override
    public List<InstRecord> queryInstRecord(String queryitem, String value) {
        InstRecordExample instRecordExample = new InstRecordExample();
        InstRecordExample.Criteria criteria = instRecordExample.createCriteria();

        if("acc".equals(queryitem)){
            criteria.andUseraccEqualTo(value);
            return instRecordMapper.selectByExample(instRecordExample);
        }else{
            return instRecordMapper.selectByExample(null);
        }
    }

    @Override
    public int addInstlRecords(InstRecord instRecord) {
        return instRecordMapper.insert(instRecord);
    }
}
