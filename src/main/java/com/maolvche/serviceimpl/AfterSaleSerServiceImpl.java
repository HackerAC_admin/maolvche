package com.maolvche.serviceimpl;

import com.maolvche.dao.*;
import com.maolvche.pojo.*;
import com.maolvche.service.AfterSaleSerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
/**
 * @author HackerAC
 */
@Service
public class AfterSaleSerServiceImpl implements AfterSaleSerService {
    @Resource
    CreditFileMapper creditFileMapper;

    @Resource
    MaintainMapper maintainMapper;

    @Resource
    ExchangeMapper exchangeMapper;

    @Resource
    FeedbackMapper feedbackMapper;

    @Resource
    InstRecordMapper instRecordMapper;

    @Resource
    InstPlanMapper instPlanMapper;

    @Resource
    UserInstlInfoMapper userInstlInfoMapper;

    @Resource
    UserCarInfoMapper userCarInfoMapper;

    /**
     * 信审资料查询
     *
     * @param userAcc 用户账户编号
     */
    @Override
    public List<CreditFile> getCreditFileByUserAcc(String userAcc) {
        CreditFileExample creditFileExample = new CreditFileExample();
        CreditFileExample.Criteria criteria = creditFileExample.createCriteria();
        criteria.andUseraccEqualTo(userAcc);
        return creditFileMapper.selectByExample(creditFileExample);
    }

    /**
     * 查询所有信审资料(后台)
     */
    @Override
    public List<CreditFile> getAllCreditFile() {
        return creditFileMapper.selectByExample(null);
    }

    /**
     * 根据查找项查询用户信用资料
     *
     */
    @Override
    public List<CreditFile> queryCreditFile(String query,String value) {
        CreditFileExample creditFileExample = new CreditFileExample();
        CreditFileExample.Criteria criteria = creditFileExample.createCriteria();
        if("".equals(value)||value==null){
            return creditFileMapper.selectByExample(null);
        }else if("acc".equals(query)){
            criteria.andUseraccEqualTo(value);
            return creditFileMapper.selectByExample(creditFileExample);
        }else if("topic".equals(query)){
            criteria.andTopicEqualTo(value);
            return creditFileMapper.selectByExample(creditFileExample);
        }else{
            criteria.andSendtimeLessThanOrEqualTo(new Date());
            return creditFileMapper.selectByExample(creditFileExample);
        }
    }

    /**
     * 提交信审资料
     *
     * @param creditFile 信审资料信息
     */
    @Override
    public int sendCreditFile(CreditFile creditFile) {
        return creditFileMapper.insert(creditFile);
    }

    /**
     * 删除信审资料
     *
     * @param creditFileNo 信审资料编号
     */
    @Override
    public int delCreditFile(String creditFileNo) {
        return creditFileMapper.deleteByPrimaryKey(Integer.parseInt(creditFileNo));
    }

    /**
     * 养护记录查询
     *
     * @param userAcc 用户账户编号
     * @return 养护记录集合
     */
    @Override
    public List<Maintain> getMaintainInfoByUserAcc(String userAcc) {
        MaintainExample maintainExample = new MaintainExample();
        MaintainExample.Criteria criteria = maintainExample.createCriteria();
        criteria.andUseraccEqualTo(userAcc);
        return maintainMapper.selectByExample(maintainExample);
    }

    /**
     * 退换记录查询
     *
     * @param userAcc 用户账户编号
     */
    @Override
    public List<Exchange> getExchangeByUserAcc(String userAcc) {
        ExchangeExample exchangeExample = new ExchangeExample();
        ExchangeExample.Criteria criteria = exchangeExample.createCriteria();
        criteria.andUseraccEqualTo(userAcc);
        return exchangeMapper.selectByExample(exchangeExample);
    }
    /**
     * 根据汽车id和用户账号查询退换记录
     *
     * @param userAcc 用户账户编号
     * @param carid 汽车id
     */
    @Override
    public Exchange getExchangeByUserAccAndCarNo(String userAcc,String carid) {
        ExchangeExample exchangeExample = new ExchangeExample();
        ExchangeExample.Criteria criteria = exchangeExample.createCriteria();
        criteria.andUseraccEqualTo(userAcc);
        criteria.andCarnoEqualTo(carid);
        if(exchangeMapper.selectByExample(exchangeExample).size()!=0){
            return exchangeMapper.selectByExample(exchangeExample).get(0);
        }else{
            return null;
        }
    }

    /**
     * 新增退换申请
     *
     * @param exchange 退换记录
     */
    @Override
    public int addExchangeApplication(Exchange exchange) {

        return exchangeMapper.insertSelective(exchange);
    }

    /**
     * 投诉反馈查询
     *
     * @param userAcc 用户账户编号
     */
    @Override
    public List<Feedback> getFeedBackByUserAcc(String userAcc) {
        FeedbackExample feedbackExample = new FeedbackExample();
        FeedbackExample.Criteria criteria = feedbackExample.createCriteria();
        criteria.andUseraccEqualTo(userAcc);
        return feedbackMapper.selectByExampleWithBLOBs(feedbackExample);
    }

    /**
     * 新增投诉反馈
     *
     * @param feedback 投诉反馈信息
     */
    @Override
    public int addFeedbackInfo(Feedback feedback) {
        return feedbackMapper.insertSelective(feedback);
    }

    /**
     * 删除投诉反馈
     *
     * @param feedback
     * @return
     */
    @Override
    public int delFeedBack(Feedback feedback) {
        return feedbackMapper.deleteByPrimaryKey(feedback.getFeedbackno());
    }

    /**
     * 分期还款记录查询
     *
     * @param userAcc 用户账户编号
     */
    @Override
    public List<InstRecord> selInstRecordByUserAcc(String userAcc) {
        InstRecordExample instRecordExample = new InstRecordExample();
        InstRecordExample.Criteria criteria = instRecordExample.createCriteria();
        criteria.andUseraccEqualTo(userAcc);
        return instRecordMapper.selectByExample(instRecordExample);
    }

    /**
     * 根据分期方案编号查询分期方案
     * @param planNo
     */
    @Override
    public InstPlan selInstPlanByNo(String planNo) {
        return instPlanMapper.selectByPrimaryKey(planNo);
    }

    /**
     * 新增分期还款记录
     *
     * @param instRecord 还款记录
     * @return 记录数
     */
    @Override
    public int addInstRecord(InstRecord instRecord) {
        return instRecordMapper.insertSelective(instRecord);
    }

    /**
     * 根据用户账号查询用户分期信息
     *
     * @param userAcc
     */
    @Override
    public UserInstlInfo selInstInfoByUserAcc(String userAcc) {
        UserInstlInfoExample userInstlInfoExample = new UserInstlInfoExample();
        UserInstlInfoExample.Criteria criteria = userInstlInfoExample.createCriteria();
        criteria.andUseraccEqualTo(userAcc);
        List<UserInstlInfo> userInstlInfos = userInstlInfoMapper.selectByExample(userInstlInfoExample);
        if(userInstlInfos.size()!=0){
            return userInstlInfos.get(0);
        }else {
            return null;
        }
    }

    /**
     * 根据用户账号查询用户拥有的汽车
     *
     * @param userAcc
     */
    @Override
    public List<UserCarInfo> selUserCarByUserAcc(String userAcc) {
        UserCarInfoExample userCarInfoExample = new UserCarInfoExample();
        UserCarInfoExample.Criteria criteria = userCarInfoExample.createCriteria();
        criteria.andUseraccEqualTo(userAcc);
        return userCarInfoMapper.selectByExample(userCarInfoExample);
    }
}
