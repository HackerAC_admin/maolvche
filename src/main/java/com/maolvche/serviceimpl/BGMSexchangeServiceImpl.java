package com.maolvche.serviceimpl;

import com.maolvche.dao.ExchangeMapper;
import com.maolvche.pojo.Exchange;
import com.maolvche.pojo.ExchangeExample;
import com.maolvche.pojo.Feedback;
import com.maolvche.service.BGMSexchangeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class BGMSexchangeServiceImpl implements BGMSexchangeService {

    @Resource
    ExchangeMapper exchangeMapper;

    /**
     * 查询所有退换记录信息
     *
     * @return 所有退换记录
     */
    @Override
    public List<Exchange> getAllExchange() {
        return exchangeMapper.selectByExample(null);
    }

    /**
     * 根据退换记录编号删除
     *
     * @param exchangeNo
     * @return
     */
    @Override
    public int delExchange(String exchangeNo) {
        int id = Integer.parseInt(exchangeNo);
        return exchangeMapper.deleteByPrimaryKey(id);
    }

    /**
     * 更新退换记录
     *
     * @param exchange
     * @return
     */
    @Override
    public int uptExchange(Exchange exchange) {
        return exchangeMapper.updateByPrimaryKeySelective(exchange);
    }

    /**
     * 查询项查询退换记录
     *
     * @param query
     * @param value
     * @return
     */
    @Override
    public List<Exchange> queryExchanges(String query, String value) {
        ExchangeExample exchangeExample = new ExchangeExample();
        ExchangeExample.Criteria criteria = exchangeExample.createCriteria();

        if("acc".equals(query)){
            criteria.andUseraccEqualTo(value);
            return exchangeMapper.selectByExample(exchangeExample);
        }else if("carId".equals(query)){
            criteria.andCarnoEqualTo(value);
            return exchangeMapper.selectByExample(exchangeExample);
        }else if("state".equals(query)){
            criteria.andStateEqualTo(value);
            return exchangeMapper.selectByExample(exchangeExample);
        }else{
            return exchangeMapper.selectByExample(null);
        }

    }
}
