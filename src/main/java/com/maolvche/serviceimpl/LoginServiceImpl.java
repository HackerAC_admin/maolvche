package com.maolvche.serviceimpl;

import com.maolvche.dao.*;
import com.maolvche.pojo.*;
import com.maolvche.service.LoginService;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author HackerAC
 */
@Service
public class LoginServiceImpl implements LoginService {
    @Resource
    WebAccountMapper webAccountMapper;

    @Resource
    BgAccountMapper bgAccountMapper;

    @Resource
    EmpAccountMapper empAccountMapper;

    @Resource
    LoginRecordMapper loginRecordMapper;

    @Resource
    UserInfoMapper userInfoMapper;

    /**
     * 根据用户名和密码查询网站用户是否存在
     *
     * @param webUserName 网站用户名
     * @param pwd 密码
     * @return 是否存在该用户
     */
    @Override
    public WebAccount selByUsernameAndPwd(String webUserName, String pwd) {
        WebAccountExample webAccountExample = new WebAccountExample();
        webAccountExample.clear();
        WebAccountExample.Criteria criteria=webAccountExample.createCriteria();
        criteria.andUsernameEqualTo(webUserName);
        criteria.andUserpwdEqualTo(pwd);
        return webAccountMapper.selectByExample(webAccountExample).get(0);
    }

    /**
     * 根据用户编号和密码查询网站用户是否存在
     *
     * @param userNo 用户编号
     * @param pwd 密码
     * @return 是否存在该用户
     */
    @Override
    public WebAccount selByUserNoAndPwd(String userNo, String pwd) {
        WebAccountExample webAccountExample = new WebAccountExample();
        webAccountExample.clear();
        WebAccountExample.Criteria criteria=webAccountExample.createCriteria();
        criteria.andUsernoEqualTo(userNo);
        criteria.andUserpwdEqualTo(pwd);
        return webAccountMapper.selectByExample(webAccountExample).get(0);
    }

    /**
     * 根据用户名和密码查询管理员用户是否存在
     *
     * @param bgUserName 管理员用户名
     * @param pwd        密码
     * @return 管理员用户
     */
    @Override
    public BgAccount selAdminByUsernameAndPwd(String bgUserName, String pwd) {
        BgAccountExample bgAccountExample = new BgAccountExample();
        bgAccountExample.clear();
        BgAccountExample.Criteria criteria = bgAccountExample.createCriteria();
        criteria.andLoginnameEqualTo(bgUserName);
        criteria.andAdmpwdEqualTo(pwd);
        return bgAccountMapper.selectByExample(bgAccountExample).get(0);
    }

    /**
     * 根据用户名和密码查询员工用户是否存在
     *
     * @param bgUserName 员工用户名
     * @param pwd        密码
     * @return 员工用户
     */
    @Override
    public EmpAccount selEmployByUsernameAndPwd(String bgUserName, String pwd) {
        EmpAccountExample empAccountExample = new EmpAccountExample();
        empAccountExample.clear();
        EmpAccountExample.Criteria criteria = empAccountExample.createCriteria();
        criteria.andLoginnameEqualTo(bgUserName);
        criteria.andEplypwdEqualTo(pwd);
        return empAccountMapper.selectByExample(empAccountExample).get(0);
    }

    /**
     * 新增登陆日志
     *
     * @param loginRecord 登陆日志
     */
    @Override
    public void insertLoginRecord(LoginRecord loginRecord) {
        loginRecordMapper.insert(loginRecord);
    }

    /**
     * 根据用户账号查询登陆记录
     *
     * @param userAcc 用户账户编号
     * @return 日志记录集合
     */
    @Override
    public List<LoginRecord> selLoginRecordByUserAcc(String userAcc) {
        LoginRecordExample loginRecordExample = new LoginRecordExample();
        LoginRecordExample.Criteria criteria = loginRecordExample.createCriteria();
        criteria.andUseraccEqualTo(userAcc);
        return loginRecordMapper.selectByExample(loginRecordExample);
    }

    /**
     * 删除登录日志
     *
     * @param recordNo 将被删除的日志id
     */
    @Override
    public void delLoginRecord(String recordNo) {
        int no = Integer.parseInt(recordNo);
        loginRecordMapper.deleteByPrimaryKey(no);
    }

    /**
     * 修改密码
     *
     */
    @Override
    public int uptPwdByAccAndNewPwd(WebAccount webAccount) {
        return webAccountMapper.updateByPrimaryKeySelective(webAccount);
    }

    /**
     * 修改用户基本信息
     *
     */
    @Override
    public int uptUserInfo(UserInfo userInfo) {
        return userInfoMapper.updateByPrimaryKeySelective(userInfo);
    }

    /**
     * 修改用户账户信息
     *
     */
    @Override
    public int uptUserAccInfo(WebAccount webAccount) {
        return webAccountMapper.updateByPrimaryKeySelective(webAccount);
    }

    /**
     * 添加用户账号信息
     *
     * @param webAccount
     */
    @Override
    public int addUserAccInfo(WebAccount webAccount) {
        return webAccountMapper.insertSelective(webAccount);
    }

    /**
     * 删除用户账号信息
     *
     * @param webAccount
     */
    @Override
    public int delUserAccInfo(WebAccount webAccount) {
        return webAccountMapper.deleteByPrimaryKey(webAccount.getUseracc());
    }

    /**
     * 根据用户名查询账号信息
     *
     * @param username
     * @return 账号信息
     */
    @Override
    public WebAccount getWebAccountByUsername(String username) {
        WebAccountExample webAccountExample = new WebAccountExample();
        WebAccountExample.Criteria criteria = webAccountExample.createCriteria();
        criteria.andUsernameEqualTo(username);
        List<WebAccount> webAccountList = webAccountMapper.selectByExample(webAccountExample);
        if(webAccountList.size()!=0){
            return webAccountList.get(0);
        }else{
            return null;
        }
    }

}
