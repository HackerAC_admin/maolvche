package com.maolvche.serviceimpl;

import com.maolvche.dao.FeedbackMapper;
import com.maolvche.pojo.Feedback;
import com.maolvche.service.BGMSfeedBackService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class BGMSfeedBackServiceImpl implements BGMSfeedBackService {
    @Resource
    FeedbackMapper feedbackMapper;
    /**
     * 查询所有投诉反馈信息
     *
     * @return 所有投诉反馈
     */
    @Override
    public List<Feedback> getAllFeedBacks() {
        return feedbackMapper.selectByExampleWithBLOBs(null);
    }

    /**
     * 根据投诉反馈编号删除
     *
     * @param feedBackNo
     * @return
     */
    @Override
    public int delFeedBack(String feedBackNo) {
        int id = Integer.parseInt(feedBackNo);
        return feedbackMapper.deleteByPrimaryKey(id);
    }

    /**
     * 更新投诉反馈(更新回复和处理时间)
     *
     * @param feedback
     * @return
     */
    @Override
    public int uptFeedBack(Feedback feedback) {
        return feedbackMapper.updateByPrimaryKeySelective(feedback);
    }
}
