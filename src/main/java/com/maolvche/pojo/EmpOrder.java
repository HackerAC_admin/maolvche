package com.maolvche.pojo;

import java.util.Date;

public class EmpOrder {
    private String getorderno;

    private String employeeno;

    private String newcarorderno;

    private Date getordertime;

    private String state;

    public String getGetorderno() {
        return getorderno;
    }

    public void setGetorderno(String getorderno) {
        this.getorderno = getorderno == null ? null : getorderno.trim();
    }

    public String getEmployeeno() {
        return employeeno;
    }

    public void setEmployeeno(String employeeno) {
        this.employeeno = employeeno == null ? null : employeeno.trim();
    }

    public String getNewcarorderno() {
        return newcarorderno;
    }

    public void setNewcarorderno(String newcarorderno) {
        this.newcarorderno = newcarorderno == null ? null : newcarorderno.trim();
    }

    public Date getGetordertime() {
        return getordertime;
    }

    public void setGetordertime(Date getordertime) {
        this.getordertime = getordertime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }
}