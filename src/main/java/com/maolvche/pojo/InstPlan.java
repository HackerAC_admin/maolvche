package com.maolvche.pojo;

public class InstPlan {
    private String planno;

    private Integer percent;

    private Integer times;

    public String getPlanno() {
        return planno;
    }

    public void setPlanno(String planno) {
        this.planno = planno == null ? null : planno.trim();
    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }
}