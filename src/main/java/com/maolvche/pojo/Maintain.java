package com.maolvche.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Maintain {
    private Integer maintainno;

    private String carno;

    private String useracc;

    private String maintainitem;

    private Float price;

    private Date maintaindate;

    private String employeeno;

    public Integer getMaintainno() {
        return maintainno;
    }

    public void setMaintainno(Integer maintainno) {
        this.maintainno = maintainno;
    }

    public String getCarno() {
        return carno;
    }

    public void setCarno(String carno) {
        this.carno = carno == null ? null : carno.trim();
    }

    public String getUseracc() {
        return useracc;
    }

    public void setUseracc(String useracc) {
        this.useracc = useracc == null ? null : useracc.trim();
    }

    public String getMaintainitem() {
        return maintainitem;
    }

    public void setMaintainitem(String maintainitem) {
        this.maintainitem = maintainitem == null ? null : maintainitem.trim();
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getMaintaindate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(maintaindate);
    }

    public void setMaintaindate(Date maintaindate) {
        this.maintaindate = maintaindate;
    }

    public String getEmployeeno() {
        return employeeno;
    }

    public void setEmployeeno(String employeeno) {
        this.employeeno = employeeno == null ? null : employeeno.trim();
    }
}