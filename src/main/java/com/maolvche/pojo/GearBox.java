package com.maolvche.pojo;

public class GearBox {
    private String gearboxno;

    private Integer gearsnum;

    private String transmissiontype;

    public String getGearboxno() {
        return gearboxno;
    }

    public void setGearboxno(String gearboxno) {
        this.gearboxno = gearboxno == null ? null : gearboxno.trim();
    }

    public Integer getGearsnum() {
        return gearsnum;
    }

    public void setGearsnum(Integer gearsnum) {
        this.gearsnum = gearsnum;
    }

    public String getTransmissiontype() {
        return transmissiontype;
    }

    public void setTransmissiontype(String transmissiontype) {
        this.transmissiontype = transmissiontype == null ? null : transmissiontype.trim();
    }
}