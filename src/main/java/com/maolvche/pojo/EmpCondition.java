package com.maolvche.pojo;

import java.util.Date;

public class EmpCondition {
    private Integer empcondiotionno;

    private String employeeno;

    private Date outdate;

    private String outres;

    private String remarks;

    public Integer getEmpcondiotionno() {
        return empcondiotionno;
    }

    public void setEmpcondiotionno(Integer empcondiotionno) {
        this.empcondiotionno = empcondiotionno;
    }

    public String getEmployeeno() {
        return employeeno;
    }

    public void setEmployeeno(String employeeno) {
        this.employeeno = employeeno == null ? null : employeeno.trim();
    }

    public Date getOutdate() {
        return outdate;
    }

    public void setOutdate(Date outdate) {
        this.outdate = outdate;
    }

    public String getOutres() {
        return outres;
    }

    public void setOutres(String outres) {
        this.outres = outres == null ? null : outres.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }
}