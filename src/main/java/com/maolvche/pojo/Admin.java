package com.maolvche.pojo;

public class Admin {
    private String adminno;

    private String admname;

    private String sex;

    private String admemail;

    private String phone;

    private String admtype;

    public String getAdminno() {
        return adminno;
    }

    public void setAdminno(String adminno) {
        this.adminno = adminno == null ? null : adminno.trim();
    }

    public String getAdmname() {
        return admname;
    }

    public void setAdmname(String admname) {
        this.admname = admname == null ? null : admname.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getAdmemail() {
        return admemail;
    }

    public void setAdmemail(String admemail) {
        this.admemail = admemail == null ? null : admemail.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getAdmtype() {
        return admtype;
    }

    public void setAdmtype(String admtype) {
        this.admtype = admtype == null ? null : admtype.trim();
    }
}