package com.maolvche.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CreditFile {
    private Integer creaditfileno;

    private String useracc;

    private String topic;

    private String filename;

    private String filesrc;

    private Date sendtime;

    public Integer getCreaditfileno() {
        return creaditfileno;
    }

    public void setCreaditfileno(Integer creaditfileno) {
        this.creaditfileno = creaditfileno;
    }

    public String getUseracc() {
        return useracc;
    }

    public void setUseracc(String useracc) {
        this.useracc = useracc == null ? null : useracc.trim();
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic == null ? null : topic.trim();
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename == null ? null : filename.trim();
    }

    public String getFilesrc() {
        return filesrc;
    }

    public void setFilesrc(String filesrc) {
        this.filesrc = filesrc == null ? null : filesrc.trim();
    }

    public String getSendtime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(sendtime);
    }

    public void setSendtime(Date sendtime) {
        this.sendtime = sendtime;
    }
}