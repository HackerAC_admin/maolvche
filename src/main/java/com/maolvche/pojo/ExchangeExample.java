package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExchangeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ExchangeExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andExchangednoIsNull() {
            addCriterion("exchangedNo is null");
            return (Criteria) this;
        }

        public Criteria andExchangednoIsNotNull() {
            addCriterion("exchangedNo is not null");
            return (Criteria) this;
        }

        public Criteria andExchangednoEqualTo(Integer value) {
            addCriterion("exchangedNo =", value, "exchangedno");
            return (Criteria) this;
        }

        public Criteria andExchangednoNotEqualTo(Integer value) {
            addCriterion("exchangedNo <>", value, "exchangedno");
            return (Criteria) this;
        }

        public Criteria andExchangednoGreaterThan(Integer value) {
            addCriterion("exchangedNo >", value, "exchangedno");
            return (Criteria) this;
        }

        public Criteria andExchangednoGreaterThanOrEqualTo(Integer value) {
            addCriterion("exchangedNo >=", value, "exchangedno");
            return (Criteria) this;
        }

        public Criteria andExchangednoLessThan(Integer value) {
            addCriterion("exchangedNo <", value, "exchangedno");
            return (Criteria) this;
        }

        public Criteria andExchangednoLessThanOrEqualTo(Integer value) {
            addCriterion("exchangedNo <=", value, "exchangedno");
            return (Criteria) this;
        }

        public Criteria andExchangednoIn(List<Integer> values) {
            addCriterion("exchangedNo in", values, "exchangedno");
            return (Criteria) this;
        }

        public Criteria andExchangednoNotIn(List<Integer> values) {
            addCriterion("exchangedNo not in", values, "exchangedno");
            return (Criteria) this;
        }

        public Criteria andExchangednoBetween(Integer value1, Integer value2) {
            addCriterion("exchangedNo between", value1, value2, "exchangedno");
            return (Criteria) this;
        }

        public Criteria andExchangednoNotBetween(Integer value1, Integer value2) {
            addCriterion("exchangedNo not between", value1, value2, "exchangedno");
            return (Criteria) this;
        }

        public Criteria andCarnoIsNull() {
            addCriterion("carNo is null");
            return (Criteria) this;
        }

        public Criteria andCarnoIsNotNull() {
            addCriterion("carNo is not null");
            return (Criteria) this;
        }

        public Criteria andCarnoEqualTo(String value) {
            addCriterion("carNo =", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotEqualTo(String value) {
            addCriterion("carNo <>", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoGreaterThan(String value) {
            addCriterion("carNo >", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoGreaterThanOrEqualTo(String value) {
            addCriterion("carNo >=", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLessThan(String value) {
            addCriterion("carNo <", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLessThanOrEqualTo(String value) {
            addCriterion("carNo <=", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLike(String value) {
            addCriterion("carNo like", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotLike(String value) {
            addCriterion("carNo not like", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoIn(List<String> values) {
            addCriterion("carNo in", values, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotIn(List<String> values) {
            addCriterion("carNo not in", values, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoBetween(String value1, String value2) {
            addCriterion("carNo between", value1, value2, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotBetween(String value1, String value2) {
            addCriterion("carNo not between", value1, value2, "carno");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNull() {
            addCriterion("userAcc is null");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNotNull() {
            addCriterion("userAcc is not null");
            return (Criteria) this;
        }

        public Criteria andUseraccEqualTo(String value) {
            addCriterion("userAcc =", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotEqualTo(String value) {
            addCriterion("userAcc <>", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThan(String value) {
            addCriterion("userAcc >", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThanOrEqualTo(String value) {
            addCriterion("userAcc >=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThan(String value) {
            addCriterion("userAcc <", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThanOrEqualTo(String value) {
            addCriterion("userAcc <=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLike(String value) {
            addCriterion("userAcc like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotLike(String value) {
            addCriterion("userAcc not like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccIn(List<String> values) {
            addCriterion("userAcc in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotIn(List<String> values) {
            addCriterion("userAcc not in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccBetween(String value1, String value2) {
            addCriterion("userAcc between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotBetween(String value1, String value2) {
            addCriterion("userAcc not between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andExchangereasonIsNull() {
            addCriterion("exchangeReason is null");
            return (Criteria) this;
        }

        public Criteria andExchangereasonIsNotNull() {
            addCriterion("exchangeReason is not null");
            return (Criteria) this;
        }

        public Criteria andExchangereasonEqualTo(String value) {
            addCriterion("exchangeReason =", value, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andExchangereasonNotEqualTo(String value) {
            addCriterion("exchangeReason <>", value, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andExchangereasonGreaterThan(String value) {
            addCriterion("exchangeReason >", value, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andExchangereasonGreaterThanOrEqualTo(String value) {
            addCriterion("exchangeReason >=", value, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andExchangereasonLessThan(String value) {
            addCriterion("exchangeReason <", value, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andExchangereasonLessThanOrEqualTo(String value) {
            addCriterion("exchangeReason <=", value, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andExchangereasonLike(String value) {
            addCriterion("exchangeReason like", value, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andExchangereasonNotLike(String value) {
            addCriterion("exchangeReason not like", value, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andExchangereasonIn(List<String> values) {
            addCriterion("exchangeReason in", values, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andExchangereasonNotIn(List<String> values) {
            addCriterion("exchangeReason not in", values, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andExchangereasonBetween(String value1, String value2) {
            addCriterion("exchangeReason between", value1, value2, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andExchangereasonNotBetween(String value1, String value2) {
            addCriterion("exchangeReason not between", value1, value2, "exchangereason");
            return (Criteria) this;
        }

        public Criteria andApplydateIsNull() {
            addCriterion("applyDate is null");
            return (Criteria) this;
        }

        public Criteria andApplydateIsNotNull() {
            addCriterion("applyDate is not null");
            return (Criteria) this;
        }

        public Criteria andApplydateEqualTo(Date value) {
            addCriterion("applyDate =", value, "applydate");
            return (Criteria) this;
        }

        public Criteria andApplydateNotEqualTo(Date value) {
            addCriterion("applyDate <>", value, "applydate");
            return (Criteria) this;
        }

        public Criteria andApplydateGreaterThan(Date value) {
            addCriterion("applyDate >", value, "applydate");
            return (Criteria) this;
        }

        public Criteria andApplydateGreaterThanOrEqualTo(Date value) {
            addCriterion("applyDate >=", value, "applydate");
            return (Criteria) this;
        }

        public Criteria andApplydateLessThan(Date value) {
            addCriterion("applyDate <", value, "applydate");
            return (Criteria) this;
        }

        public Criteria andApplydateLessThanOrEqualTo(Date value) {
            addCriterion("applyDate <=", value, "applydate");
            return (Criteria) this;
        }

        public Criteria andApplydateIn(List<Date> values) {
            addCriterion("applyDate in", values, "applydate");
            return (Criteria) this;
        }

        public Criteria andApplydateNotIn(List<Date> values) {
            addCriterion("applyDate not in", values, "applydate");
            return (Criteria) this;
        }

        public Criteria andApplydateBetween(Date value1, Date value2) {
            addCriterion("applyDate between", value1, value2, "applydate");
            return (Criteria) this;
        }

        public Criteria andApplydateNotBetween(Date value1, Date value2) {
            addCriterion("applyDate not between", value1, value2, "applydate");
            return (Criteria) this;
        }

        public Criteria andExchangeddateIsNull() {
            addCriterion("exchangedDate is null");
            return (Criteria) this;
        }

        public Criteria andExchangeddateIsNotNull() {
            addCriterion("exchangedDate is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeddateEqualTo(Date value) {
            addCriterion("exchangedDate =", value, "exchangeddate");
            return (Criteria) this;
        }

        public Criteria andExchangeddateNotEqualTo(Date value) {
            addCriterion("exchangedDate <>", value, "exchangeddate");
            return (Criteria) this;
        }

        public Criteria andExchangeddateGreaterThan(Date value) {
            addCriterion("exchangedDate >", value, "exchangeddate");
            return (Criteria) this;
        }

        public Criteria andExchangeddateGreaterThanOrEqualTo(Date value) {
            addCriterion("exchangedDate >=", value, "exchangeddate");
            return (Criteria) this;
        }

        public Criteria andExchangeddateLessThan(Date value) {
            addCriterion("exchangedDate <", value, "exchangeddate");
            return (Criteria) this;
        }

        public Criteria andExchangeddateLessThanOrEqualTo(Date value) {
            addCriterion("exchangedDate <=", value, "exchangeddate");
            return (Criteria) this;
        }

        public Criteria andExchangeddateIn(List<Date> values) {
            addCriterion("exchangedDate in", values, "exchangeddate");
            return (Criteria) this;
        }

        public Criteria andExchangeddateNotIn(List<Date> values) {
            addCriterion("exchangedDate not in", values, "exchangeddate");
            return (Criteria) this;
        }

        public Criteria andExchangeddateBetween(Date value1, Date value2) {
            addCriterion("exchangedDate between", value1, value2, "exchangeddate");
            return (Criteria) this;
        }

        public Criteria andExchangeddateNotBetween(Date value1, Date value2) {
            addCriterion("exchangedDate not between", value1, value2, "exchangeddate");
            return (Criteria) this;
        }

        public Criteria andAuditresultIsNull() {
            addCriterion("auditResult is null");
            return (Criteria) this;
        }

        public Criteria andAuditresultIsNotNull() {
            addCriterion("auditResult is not null");
            return (Criteria) this;
        }

        public Criteria andAuditresultEqualTo(String value) {
            addCriterion("auditResult =", value, "auditresult");
            return (Criteria) this;
        }

        public Criteria andAuditresultNotEqualTo(String value) {
            addCriterion("auditResult <>", value, "auditresult");
            return (Criteria) this;
        }

        public Criteria andAuditresultGreaterThan(String value) {
            addCriterion("auditResult >", value, "auditresult");
            return (Criteria) this;
        }

        public Criteria andAuditresultGreaterThanOrEqualTo(String value) {
            addCriterion("auditResult >=", value, "auditresult");
            return (Criteria) this;
        }

        public Criteria andAuditresultLessThan(String value) {
            addCriterion("auditResult <", value, "auditresult");
            return (Criteria) this;
        }

        public Criteria andAuditresultLessThanOrEqualTo(String value) {
            addCriterion("auditResult <=", value, "auditresult");
            return (Criteria) this;
        }

        public Criteria andAuditresultLike(String value) {
            addCriterion("auditResult like", value, "auditresult");
            return (Criteria) this;
        }

        public Criteria andAuditresultNotLike(String value) {
            addCriterion("auditResult not like", value, "auditresult");
            return (Criteria) this;
        }

        public Criteria andAuditresultIn(List<String> values) {
            addCriterion("auditResult in", values, "auditresult");
            return (Criteria) this;
        }

        public Criteria andAuditresultNotIn(List<String> values) {
            addCriterion("auditResult not in", values, "auditresult");
            return (Criteria) this;
        }

        public Criteria andAuditresultBetween(String value1, String value2) {
            addCriterion("auditResult between", value1, value2, "auditresult");
            return (Criteria) this;
        }

        public Criteria andAuditresultNotBetween(String value1, String value2) {
            addCriterion("auditResult not between", value1, value2, "auditresult");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(String value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(String value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(String value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(String value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(String value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(String value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLike(String value) {
            addCriterion("state like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotLike(String value) {
            addCriterion("state not like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<String> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<String> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(String value1, String value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(String value1, String value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}