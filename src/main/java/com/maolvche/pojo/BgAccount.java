package com.maolvche.pojo;

public class BgAccount {
    private Integer admaccno;

    private String adminno;

    private String loginname;

    private String admpwd;

    private String nickname;

    public Integer getAdmaccno() {
        return admaccno;
    }

    public void setAdmaccno(Integer admaccno) {
        this.admaccno = admaccno;
    }

    public String getAdminno() {
        return adminno;
    }

    public void setAdminno(String adminno) {
        this.adminno = adminno == null ? null : adminno.trim();
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname == null ? null : loginname.trim();
    }

    public String getAdmpwd() {
        return admpwd;
    }

    public void setAdmpwd(String admpwd) {
        this.admpwd = admpwd == null ? null : admpwd.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }
}