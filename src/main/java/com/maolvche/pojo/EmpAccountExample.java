package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.List;

public class EmpAccountExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public EmpAccountExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEplyaccnoIsNull() {
            addCriterion("eplyAccNo is null");
            return (Criteria) this;
        }

        public Criteria andEplyaccnoIsNotNull() {
            addCriterion("eplyAccNo is not null");
            return (Criteria) this;
        }

        public Criteria andEplyaccnoEqualTo(Integer value) {
            addCriterion("eplyAccNo =", value, "eplyaccno");
            return (Criteria) this;
        }

        public Criteria andEplyaccnoNotEqualTo(Integer value) {
            addCriterion("eplyAccNo <>", value, "eplyaccno");
            return (Criteria) this;
        }

        public Criteria andEplyaccnoGreaterThan(Integer value) {
            addCriterion("eplyAccNo >", value, "eplyaccno");
            return (Criteria) this;
        }

        public Criteria andEplyaccnoGreaterThanOrEqualTo(Integer value) {
            addCriterion("eplyAccNo >=", value, "eplyaccno");
            return (Criteria) this;
        }

        public Criteria andEplyaccnoLessThan(Integer value) {
            addCriterion("eplyAccNo <", value, "eplyaccno");
            return (Criteria) this;
        }

        public Criteria andEplyaccnoLessThanOrEqualTo(Integer value) {
            addCriterion("eplyAccNo <=", value, "eplyaccno");
            return (Criteria) this;
        }

        public Criteria andEplyaccnoIn(List<Integer> values) {
            addCriterion("eplyAccNo in", values, "eplyaccno");
            return (Criteria) this;
        }

        public Criteria andEplyaccnoNotIn(List<Integer> values) {
            addCriterion("eplyAccNo not in", values, "eplyaccno");
            return (Criteria) this;
        }

        public Criteria andEplyaccnoBetween(Integer value1, Integer value2) {
            addCriterion("eplyAccNo between", value1, value2, "eplyaccno");
            return (Criteria) this;
        }

        public Criteria andEplyaccnoNotBetween(Integer value1, Integer value2) {
            addCriterion("eplyAccNo not between", value1, value2, "eplyaccno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIsNull() {
            addCriterion("employeeNo is null");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIsNotNull() {
            addCriterion("employeeNo is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeenoEqualTo(String value) {
            addCriterion("employeeNo =", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotEqualTo(String value) {
            addCriterion("employeeNo <>", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoGreaterThan(String value) {
            addCriterion("employeeNo >", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoGreaterThanOrEqualTo(String value) {
            addCriterion("employeeNo >=", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLessThan(String value) {
            addCriterion("employeeNo <", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLessThanOrEqualTo(String value) {
            addCriterion("employeeNo <=", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLike(String value) {
            addCriterion("employeeNo like", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotLike(String value) {
            addCriterion("employeeNo not like", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIn(List<String> values) {
            addCriterion("employeeNo in", values, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotIn(List<String> values) {
            addCriterion("employeeNo not in", values, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoBetween(String value1, String value2) {
            addCriterion("employeeNo between", value1, value2, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotBetween(String value1, String value2) {
            addCriterion("employeeNo not between", value1, value2, "employeeno");
            return (Criteria) this;
        }

        public Criteria andLoginnameIsNull() {
            addCriterion("loginName is null");
            return (Criteria) this;
        }

        public Criteria andLoginnameIsNotNull() {
            addCriterion("loginName is not null");
            return (Criteria) this;
        }

        public Criteria andLoginnameEqualTo(String value) {
            addCriterion("loginName =", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameNotEqualTo(String value) {
            addCriterion("loginName <>", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameGreaterThan(String value) {
            addCriterion("loginName >", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameGreaterThanOrEqualTo(String value) {
            addCriterion("loginName >=", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameLessThan(String value) {
            addCriterion("loginName <", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameLessThanOrEqualTo(String value) {
            addCriterion("loginName <=", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameLike(String value) {
            addCriterion("loginName like", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameNotLike(String value) {
            addCriterion("loginName not like", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameIn(List<String> values) {
            addCriterion("loginName in", values, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameNotIn(List<String> values) {
            addCriterion("loginName not in", values, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameBetween(String value1, String value2) {
            addCriterion("loginName between", value1, value2, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameNotBetween(String value1, String value2) {
            addCriterion("loginName not between", value1, value2, "loginname");
            return (Criteria) this;
        }

        public Criteria andEplypwdIsNull() {
            addCriterion("eplyPwd is null");
            return (Criteria) this;
        }

        public Criteria andEplypwdIsNotNull() {
            addCriterion("eplyPwd is not null");
            return (Criteria) this;
        }

        public Criteria andEplypwdEqualTo(String value) {
            addCriterion("eplyPwd =", value, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andEplypwdNotEqualTo(String value) {
            addCriterion("eplyPwd <>", value, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andEplypwdGreaterThan(String value) {
            addCriterion("eplyPwd >", value, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andEplypwdGreaterThanOrEqualTo(String value) {
            addCriterion("eplyPwd >=", value, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andEplypwdLessThan(String value) {
            addCriterion("eplyPwd <", value, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andEplypwdLessThanOrEqualTo(String value) {
            addCriterion("eplyPwd <=", value, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andEplypwdLike(String value) {
            addCriterion("eplyPwd like", value, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andEplypwdNotLike(String value) {
            addCriterion("eplyPwd not like", value, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andEplypwdIn(List<String> values) {
            addCriterion("eplyPwd in", values, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andEplypwdNotIn(List<String> values) {
            addCriterion("eplyPwd not in", values, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andEplypwdBetween(String value1, String value2) {
            addCriterion("eplyPwd between", value1, value2, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andEplypwdNotBetween(String value1, String value2) {
            addCriterion("eplyPwd not between", value1, value2, "eplypwd");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNull() {
            addCriterion("nickName is null");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNotNull() {
            addCriterion("nickName is not null");
            return (Criteria) this;
        }

        public Criteria andNicknameEqualTo(String value) {
            addCriterion("nickName =", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotEqualTo(String value) {
            addCriterion("nickName <>", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThan(String value) {
            addCriterion("nickName >", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThanOrEqualTo(String value) {
            addCriterion("nickName >=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThan(String value) {
            addCriterion("nickName <", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThanOrEqualTo(String value) {
            addCriterion("nickName <=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLike(String value) {
            addCriterion("nickName like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotLike(String value) {
            addCriterion("nickName not like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameIn(List<String> values) {
            addCriterion("nickName in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotIn(List<String> values) {
            addCriterion("nickName not in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameBetween(String value1, String value2) {
            addCriterion("nickName between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotBetween(String value1, String value2) {
            addCriterion("nickName not between", value1, value2, "nickname");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}