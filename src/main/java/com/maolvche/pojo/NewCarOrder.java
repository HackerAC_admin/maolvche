package com.maolvche.pojo;

import java.util.Date;

public class NewCarOrder {
    private String newcarorderno;

    private String useracc;

    private String newcarno;

    private Date sendtime;

    private String planno;

    private String status;

    private String result;

    public String getNewcarorderno() {
        return newcarorderno;
    }

    public void setNewcarorderno(String newcarorderno) {
        this.newcarorderno = newcarorderno == null ? null : newcarorderno.trim();
    }

    public String getUseracc() {
        return useracc;
    }

    public void setUseracc(String useracc) {
        this.useracc = useracc == null ? null : useracc.trim();
    }

    public String getNewcarno() {
        return newcarno;
    }

    public void setNewcarno(String newcarno) {
        this.newcarno = newcarno == null ? null : newcarno.trim();
    }

    public Date getSendtime() {
        return sendtime;
    }

    public void setSendtime(Date sendtime) {
        this.sendtime = sendtime;
    }

    public String getPlanno() {
        return planno;
    }

    public void setPlanno(String planno) {
        this.planno = planno == null ? null : planno.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }
}