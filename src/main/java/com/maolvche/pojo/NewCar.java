package com.maolvche.pojo;

import java.util.Date;

public class NewCar {
    private String newcarno;

    private String carengineno;

    private String basicconno;

    private String carbodyno;

    private String gearboxno;

    private String carname;

    private String caridtype;

    private Date releasedate;

    private String saleprice;

    private String currentprice;

    private String status;

    public String getNewcarno() {
        return newcarno;
    }

    public void setNewcarno(String newcarno) {
        this.newcarno = newcarno == null ? null : newcarno.trim();
    }

    public String getCarengineno() {
        return carengineno;
    }

    public void setCarengineno(String carengineno) {
        this.carengineno = carengineno == null ? null : carengineno.trim();
    }

    public String getBasicconno() {
        return basicconno;
    }

    public void setBasicconno(String basicconno) {
        this.basicconno = basicconno == null ? null : basicconno.trim();
    }

    public String getCarbodyno() {
        return carbodyno;
    }

    public void setCarbodyno(String carbodyno) {
        this.carbodyno = carbodyno == null ? null : carbodyno.trim();
    }

    public String getGearboxno() {
        return gearboxno;
    }

    public void setGearboxno(String gearboxno) {
        this.gearboxno = gearboxno == null ? null : gearboxno.trim();
    }

    public String getCarname() {
        return carname;
    }

    public void setCarname(String carname) {
        this.carname = carname == null ? null : carname.trim();
    }

    public String getCaridtype() {
        return caridtype;
    }

    public void setCaridtype(String caridtype) {
        this.caridtype = caridtype == null ? null : caridtype.trim();
    }

    public Date getReleasedate() {
        return releasedate;
    }

    public void setReleasedate(Date releasedate) {
        this.releasedate = releasedate;
    }

    public String getSaleprice() {
        return saleprice;
    }

    public void setSaleprice(String saleprice) {
        this.saleprice = saleprice == null ? null : saleprice.trim();
    }

    public String getCurrentprice() {
        return currentprice;
    }

    public void setCurrentprice(String currentprice) {
        this.currentprice = currentprice == null ? null : currentprice.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}