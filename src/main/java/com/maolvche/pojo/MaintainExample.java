package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MaintainExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MaintainExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMaintainnoIsNull() {
            addCriterion("maintainNo is null");
            return (Criteria) this;
        }

        public Criteria andMaintainnoIsNotNull() {
            addCriterion("maintainNo is not null");
            return (Criteria) this;
        }

        public Criteria andMaintainnoEqualTo(Integer value) {
            addCriterion("maintainNo =", value, "maintainno");
            return (Criteria) this;
        }

        public Criteria andMaintainnoNotEqualTo(Integer value) {
            addCriterion("maintainNo <>", value, "maintainno");
            return (Criteria) this;
        }

        public Criteria andMaintainnoGreaterThan(Integer value) {
            addCriterion("maintainNo >", value, "maintainno");
            return (Criteria) this;
        }

        public Criteria andMaintainnoGreaterThanOrEqualTo(Integer value) {
            addCriterion("maintainNo >=", value, "maintainno");
            return (Criteria) this;
        }

        public Criteria andMaintainnoLessThan(Integer value) {
            addCriterion("maintainNo <", value, "maintainno");
            return (Criteria) this;
        }

        public Criteria andMaintainnoLessThanOrEqualTo(Integer value) {
            addCriterion("maintainNo <=", value, "maintainno");
            return (Criteria) this;
        }

        public Criteria andMaintainnoIn(List<Integer> values) {
            addCriterion("maintainNo in", values, "maintainno");
            return (Criteria) this;
        }

        public Criteria andMaintainnoNotIn(List<Integer> values) {
            addCriterion("maintainNo not in", values, "maintainno");
            return (Criteria) this;
        }

        public Criteria andMaintainnoBetween(Integer value1, Integer value2) {
            addCriterion("maintainNo between", value1, value2, "maintainno");
            return (Criteria) this;
        }

        public Criteria andMaintainnoNotBetween(Integer value1, Integer value2) {
            addCriterion("maintainNo not between", value1, value2, "maintainno");
            return (Criteria) this;
        }

        public Criteria andCarnoIsNull() {
            addCriterion("carNo is null");
            return (Criteria) this;
        }

        public Criteria andCarnoIsNotNull() {
            addCriterion("carNo is not null");
            return (Criteria) this;
        }

        public Criteria andCarnoEqualTo(String value) {
            addCriterion("carNo =", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotEqualTo(String value) {
            addCriterion("carNo <>", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoGreaterThan(String value) {
            addCriterion("carNo >", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoGreaterThanOrEqualTo(String value) {
            addCriterion("carNo >=", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLessThan(String value) {
            addCriterion("carNo <", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLessThanOrEqualTo(String value) {
            addCriterion("carNo <=", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLike(String value) {
            addCriterion("carNo like", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotLike(String value) {
            addCriterion("carNo not like", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoIn(List<String> values) {
            addCriterion("carNo in", values, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotIn(List<String> values) {
            addCriterion("carNo not in", values, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoBetween(String value1, String value2) {
            addCriterion("carNo between", value1, value2, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotBetween(String value1, String value2) {
            addCriterion("carNo not between", value1, value2, "carno");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNull() {
            addCriterion("userAcc is null");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNotNull() {
            addCriterion("userAcc is not null");
            return (Criteria) this;
        }

        public Criteria andUseraccEqualTo(String value) {
            addCriterion("userAcc =", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotEqualTo(String value) {
            addCriterion("userAcc <>", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThan(String value) {
            addCriterion("userAcc >", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThanOrEqualTo(String value) {
            addCriterion("userAcc >=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThan(String value) {
            addCriterion("userAcc <", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThanOrEqualTo(String value) {
            addCriterion("userAcc <=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLike(String value) {
            addCriterion("userAcc like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotLike(String value) {
            addCriterion("userAcc not like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccIn(List<String> values) {
            addCriterion("userAcc in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotIn(List<String> values) {
            addCriterion("userAcc not in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccBetween(String value1, String value2) {
            addCriterion("userAcc between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotBetween(String value1, String value2) {
            addCriterion("userAcc not between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andMaintainitemIsNull() {
            addCriterion("maintainItem is null");
            return (Criteria) this;
        }

        public Criteria andMaintainitemIsNotNull() {
            addCriterion("maintainItem is not null");
            return (Criteria) this;
        }

        public Criteria andMaintainitemEqualTo(String value) {
            addCriterion("maintainItem =", value, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andMaintainitemNotEqualTo(String value) {
            addCriterion("maintainItem <>", value, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andMaintainitemGreaterThan(String value) {
            addCriterion("maintainItem >", value, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andMaintainitemGreaterThanOrEqualTo(String value) {
            addCriterion("maintainItem >=", value, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andMaintainitemLessThan(String value) {
            addCriterion("maintainItem <", value, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andMaintainitemLessThanOrEqualTo(String value) {
            addCriterion("maintainItem <=", value, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andMaintainitemLike(String value) {
            addCriterion("maintainItem like", value, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andMaintainitemNotLike(String value) {
            addCriterion("maintainItem not like", value, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andMaintainitemIn(List<String> values) {
            addCriterion("maintainItem in", values, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andMaintainitemNotIn(List<String> values) {
            addCriterion("maintainItem not in", values, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andMaintainitemBetween(String value1, String value2) {
            addCriterion("maintainItem between", value1, value2, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andMaintainitemNotBetween(String value1, String value2) {
            addCriterion("maintainItem not between", value1, value2, "maintainitem");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(Float value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(Float value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(Float value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(Float value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(Float value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(Float value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<Float> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<Float> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(Float value1, Float value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(Float value1, Float value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andMaintaindateIsNull() {
            addCriterion("maintainDate is null");
            return (Criteria) this;
        }

        public Criteria andMaintaindateIsNotNull() {
            addCriterion("maintainDate is not null");
            return (Criteria) this;
        }

        public Criteria andMaintaindateEqualTo(Date value) {
            addCriterion("maintainDate =", value, "maintaindate");
            return (Criteria) this;
        }

        public Criteria andMaintaindateNotEqualTo(Date value) {
            addCriterion("maintainDate <>", value, "maintaindate");
            return (Criteria) this;
        }

        public Criteria andMaintaindateGreaterThan(Date value) {
            addCriterion("maintainDate >", value, "maintaindate");
            return (Criteria) this;
        }

        public Criteria andMaintaindateGreaterThanOrEqualTo(Date value) {
            addCriterion("maintainDate >=", value, "maintaindate");
            return (Criteria) this;
        }

        public Criteria andMaintaindateLessThan(Date value) {
            addCriterion("maintainDate <", value, "maintaindate");
            return (Criteria) this;
        }

        public Criteria andMaintaindateLessThanOrEqualTo(Date value) {
            addCriterion("maintainDate <=", value, "maintaindate");
            return (Criteria) this;
        }

        public Criteria andMaintaindateIn(List<Date> values) {
            addCriterion("maintainDate in", values, "maintaindate");
            return (Criteria) this;
        }

        public Criteria andMaintaindateNotIn(List<Date> values) {
            addCriterion("maintainDate not in", values, "maintaindate");
            return (Criteria) this;
        }

        public Criteria andMaintaindateBetween(Date value1, Date value2) {
            addCriterion("maintainDate between", value1, value2, "maintaindate");
            return (Criteria) this;
        }

        public Criteria andMaintaindateNotBetween(Date value1, Date value2) {
            addCriterion("maintainDate not between", value1, value2, "maintaindate");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIsNull() {
            addCriterion("employeeNo is null");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIsNotNull() {
            addCriterion("employeeNo is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeenoEqualTo(String value) {
            addCriterion("employeeNo =", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotEqualTo(String value) {
            addCriterion("employeeNo <>", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoGreaterThan(String value) {
            addCriterion("employeeNo >", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoGreaterThanOrEqualTo(String value) {
            addCriterion("employeeNo >=", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLessThan(String value) {
            addCriterion("employeeNo <", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLessThanOrEqualTo(String value) {
            addCriterion("employeeNo <=", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLike(String value) {
            addCriterion("employeeNo like", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotLike(String value) {
            addCriterion("employeeNo not like", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIn(List<String> values) {
            addCriterion("employeeNo in", values, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotIn(List<String> values) {
            addCriterion("employeeNo not in", values, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoBetween(String value1, String value2) {
            addCriterion("employeeNo between", value1, value2, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotBetween(String value1, String value2) {
            addCriterion("employeeNo not between", value1, value2, "employeeno");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}