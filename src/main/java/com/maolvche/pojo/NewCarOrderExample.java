package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NewCarOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public NewCarOrderExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andNewcarordernoIsNull() {
            addCriterion("newCarOrderNo is null");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoIsNotNull() {
            addCriterion("newCarOrderNo is not null");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoEqualTo(String value) {
            addCriterion("newCarOrderNo =", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoNotEqualTo(String value) {
            addCriterion("newCarOrderNo <>", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoGreaterThan(String value) {
            addCriterion("newCarOrderNo >", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoGreaterThanOrEqualTo(String value) {
            addCriterion("newCarOrderNo >=", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoLessThan(String value) {
            addCriterion("newCarOrderNo <", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoLessThanOrEqualTo(String value) {
            addCriterion("newCarOrderNo <=", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoLike(String value) {
            addCriterion("newCarOrderNo like", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoNotLike(String value) {
            addCriterion("newCarOrderNo not like", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoIn(List<String> values) {
            addCriterion("newCarOrderNo in", values, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoNotIn(List<String> values) {
            addCriterion("newCarOrderNo not in", values, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoBetween(String value1, String value2) {
            addCriterion("newCarOrderNo between", value1, value2, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoNotBetween(String value1, String value2) {
            addCriterion("newCarOrderNo not between", value1, value2, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNull() {
            addCriterion("userAcc is null");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNotNull() {
            addCriterion("userAcc is not null");
            return (Criteria) this;
        }

        public Criteria andUseraccEqualTo(String value) {
            addCriterion("userAcc =", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotEqualTo(String value) {
            addCriterion("userAcc <>", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThan(String value) {
            addCriterion("userAcc >", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThanOrEqualTo(String value) {
            addCriterion("userAcc >=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThan(String value) {
            addCriterion("userAcc <", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThanOrEqualTo(String value) {
            addCriterion("userAcc <=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLike(String value) {
            addCriterion("userAcc like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotLike(String value) {
            addCriterion("userAcc not like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccIn(List<String> values) {
            addCriterion("userAcc in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotIn(List<String> values) {
            addCriterion("userAcc not in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccBetween(String value1, String value2) {
            addCriterion("userAcc between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotBetween(String value1, String value2) {
            addCriterion("userAcc not between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andNewcarnoIsNull() {
            addCriterion("newCarNo is null");
            return (Criteria) this;
        }

        public Criteria andNewcarnoIsNotNull() {
            addCriterion("newCarNo is not null");
            return (Criteria) this;
        }

        public Criteria andNewcarnoEqualTo(String value) {
            addCriterion("newCarNo =", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotEqualTo(String value) {
            addCriterion("newCarNo <>", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoGreaterThan(String value) {
            addCriterion("newCarNo >", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoGreaterThanOrEqualTo(String value) {
            addCriterion("newCarNo >=", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoLessThan(String value) {
            addCriterion("newCarNo <", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoLessThanOrEqualTo(String value) {
            addCriterion("newCarNo <=", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoLike(String value) {
            addCriterion("newCarNo like", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotLike(String value) {
            addCriterion("newCarNo not like", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoIn(List<String> values) {
            addCriterion("newCarNo in", values, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotIn(List<String> values) {
            addCriterion("newCarNo not in", values, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoBetween(String value1, String value2) {
            addCriterion("newCarNo between", value1, value2, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotBetween(String value1, String value2) {
            addCriterion("newCarNo not between", value1, value2, "newcarno");
            return (Criteria) this;
        }

        public Criteria andSendtimeIsNull() {
            addCriterion("sendTime is null");
            return (Criteria) this;
        }

        public Criteria andSendtimeIsNotNull() {
            addCriterion("sendTime is not null");
            return (Criteria) this;
        }

        public Criteria andSendtimeEqualTo(Date value) {
            addCriterion("sendTime =", value, "sendtime");
            return (Criteria) this;
        }

        public Criteria andSendtimeNotEqualTo(Date value) {
            addCriterion("sendTime <>", value, "sendtime");
            return (Criteria) this;
        }

        public Criteria andSendtimeGreaterThan(Date value) {
            addCriterion("sendTime >", value, "sendtime");
            return (Criteria) this;
        }

        public Criteria andSendtimeGreaterThanOrEqualTo(Date value) {
            addCriterion("sendTime >=", value, "sendtime");
            return (Criteria) this;
        }

        public Criteria andSendtimeLessThan(Date value) {
            addCriterion("sendTime <", value, "sendtime");
            return (Criteria) this;
        }

        public Criteria andSendtimeLessThanOrEqualTo(Date value) {
            addCriterion("sendTime <=", value, "sendtime");
            return (Criteria) this;
        }

        public Criteria andSendtimeIn(List<Date> values) {
            addCriterion("sendTime in", values, "sendtime");
            return (Criteria) this;
        }

        public Criteria andSendtimeNotIn(List<Date> values) {
            addCriterion("sendTime not in", values, "sendtime");
            return (Criteria) this;
        }

        public Criteria andSendtimeBetween(Date value1, Date value2) {
            addCriterion("sendTime between", value1, value2, "sendtime");
            return (Criteria) this;
        }

        public Criteria andSendtimeNotBetween(Date value1, Date value2) {
            addCriterion("sendTime not between", value1, value2, "sendtime");
            return (Criteria) this;
        }

        public Criteria andPlannoIsNull() {
            addCriterion("planNo is null");
            return (Criteria) this;
        }

        public Criteria andPlannoIsNotNull() {
            addCriterion("planNo is not null");
            return (Criteria) this;
        }

        public Criteria andPlannoEqualTo(String value) {
            addCriterion("planNo =", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoNotEqualTo(String value) {
            addCriterion("planNo <>", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoGreaterThan(String value) {
            addCriterion("planNo >", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoGreaterThanOrEqualTo(String value) {
            addCriterion("planNo >=", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoLessThan(String value) {
            addCriterion("planNo <", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoLessThanOrEqualTo(String value) {
            addCriterion("planNo <=", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoLike(String value) {
            addCriterion("planNo like", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoNotLike(String value) {
            addCriterion("planNo not like", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoIn(List<String> values) {
            addCriterion("planNo in", values, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoNotIn(List<String> values) {
            addCriterion("planNo not in", values, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoBetween(String value1, String value2) {
            addCriterion("planNo between", value1, value2, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoNotBetween(String value1, String value2) {
            addCriterion("planNo not between", value1, value2, "planno");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andResultIsNull() {
            addCriterion("result is null");
            return (Criteria) this;
        }

        public Criteria andResultIsNotNull() {
            addCriterion("result is not null");
            return (Criteria) this;
        }

        public Criteria andResultEqualTo(String value) {
            addCriterion("result =", value, "result");
            return (Criteria) this;
        }

        public Criteria andResultNotEqualTo(String value) {
            addCriterion("result <>", value, "result");
            return (Criteria) this;
        }

        public Criteria andResultGreaterThan(String value) {
            addCriterion("result >", value, "result");
            return (Criteria) this;
        }

        public Criteria andResultGreaterThanOrEqualTo(String value) {
            addCriterion("result >=", value, "result");
            return (Criteria) this;
        }

        public Criteria andResultLessThan(String value) {
            addCriterion("result <", value, "result");
            return (Criteria) this;
        }

        public Criteria andResultLessThanOrEqualTo(String value) {
            addCriterion("result <=", value, "result");
            return (Criteria) this;
        }

        public Criteria andResultLike(String value) {
            addCriterion("result like", value, "result");
            return (Criteria) this;
        }

        public Criteria andResultNotLike(String value) {
            addCriterion("result not like", value, "result");
            return (Criteria) this;
        }

        public Criteria andResultIn(List<String> values) {
            addCriterion("result in", values, "result");
            return (Criteria) this;
        }

        public Criteria andResultNotIn(List<String> values) {
            addCriterion("result not in", values, "result");
            return (Criteria) this;
        }

        public Criteria andResultBetween(String value1, String value2) {
            addCriterion("result between", value1, value2, "result");
            return (Criteria) this;
        }

        public Criteria andResultNotBetween(String value1, String value2) {
            addCriterion("result not between", value1, value2, "result");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}