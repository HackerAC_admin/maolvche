package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class NewCarExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public NewCarExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andNewcarnoIsNull() {
            addCriterion("newcarNo is null");
            return (Criteria) this;
        }

        public Criteria andNewcarnoIsNotNull() {
            addCriterion("newcarNo is not null");
            return (Criteria) this;
        }

        public Criteria andNewcarnoEqualTo(String value) {
            addCriterion("newcarNo =", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotEqualTo(String value) {
            addCriterion("newcarNo <>", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoGreaterThan(String value) {
            addCriterion("newcarNo >", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoGreaterThanOrEqualTo(String value) {
            addCriterion("newcarNo >=", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoLessThan(String value) {
            addCriterion("newcarNo <", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoLessThanOrEqualTo(String value) {
            addCriterion("newcarNo <=", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoLike(String value) {
            addCriterion("newcarNo like", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotLike(String value) {
            addCriterion("newcarNo not like", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoIn(List<String> values) {
            addCriterion("newcarNo in", values, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotIn(List<String> values) {
            addCriterion("newcarNo not in", values, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoBetween(String value1, String value2) {
            addCriterion("newcarNo between", value1, value2, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotBetween(String value1, String value2) {
            addCriterion("newcarNo not between", value1, value2, "newcarno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoIsNull() {
            addCriterion("carEngineNo is null");
            return (Criteria) this;
        }

        public Criteria andCarenginenoIsNotNull() {
            addCriterion("carEngineNo is not null");
            return (Criteria) this;
        }

        public Criteria andCarenginenoEqualTo(String value) {
            addCriterion("carEngineNo =", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoNotEqualTo(String value) {
            addCriterion("carEngineNo <>", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoGreaterThan(String value) {
            addCriterion("carEngineNo >", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoGreaterThanOrEqualTo(String value) {
            addCriterion("carEngineNo >=", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoLessThan(String value) {
            addCriterion("carEngineNo <", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoLessThanOrEqualTo(String value) {
            addCriterion("carEngineNo <=", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoLike(String value) {
            addCriterion("carEngineNo like", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoNotLike(String value) {
            addCriterion("carEngineNo not like", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoIn(List<String> values) {
            addCriterion("carEngineNo in", values, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoNotIn(List<String> values) {
            addCriterion("carEngineNo not in", values, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoBetween(String value1, String value2) {
            addCriterion("carEngineNo between", value1, value2, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoNotBetween(String value1, String value2) {
            addCriterion("carEngineNo not between", value1, value2, "carengineno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoIsNull() {
            addCriterion("basicconNo is null");
            return (Criteria) this;
        }

        public Criteria andBasicconnoIsNotNull() {
            addCriterion("basicconNo is not null");
            return (Criteria) this;
        }

        public Criteria andBasicconnoEqualTo(String value) {
            addCriterion("basicconNo =", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoNotEqualTo(String value) {
            addCriterion("basicconNo <>", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoGreaterThan(String value) {
            addCriterion("basicconNo >", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoGreaterThanOrEqualTo(String value) {
            addCriterion("basicconNo >=", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoLessThan(String value) {
            addCriterion("basicconNo <", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoLessThanOrEqualTo(String value) {
            addCriterion("basicconNo <=", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoLike(String value) {
            addCriterion("basicconNo like", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoNotLike(String value) {
            addCriterion("basicconNo not like", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoIn(List<String> values) {
            addCriterion("basicconNo in", values, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoNotIn(List<String> values) {
            addCriterion("basicconNo not in", values, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoBetween(String value1, String value2) {
            addCriterion("basicconNo between", value1, value2, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoNotBetween(String value1, String value2) {
            addCriterion("basicconNo not between", value1, value2, "basicconno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoIsNull() {
            addCriterion("carBodyNo is null");
            return (Criteria) this;
        }

        public Criteria andCarbodynoIsNotNull() {
            addCriterion("carBodyNo is not null");
            return (Criteria) this;
        }

        public Criteria andCarbodynoEqualTo(String value) {
            addCriterion("carBodyNo =", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoNotEqualTo(String value) {
            addCriterion("carBodyNo <>", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoGreaterThan(String value) {
            addCriterion("carBodyNo >", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoGreaterThanOrEqualTo(String value) {
            addCriterion("carBodyNo >=", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoLessThan(String value) {
            addCriterion("carBodyNo <", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoLessThanOrEqualTo(String value) {
            addCriterion("carBodyNo <=", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoLike(String value) {
            addCriterion("carBodyNo like", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoNotLike(String value) {
            addCriterion("carBodyNo not like", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoIn(List<String> values) {
            addCriterion("carBodyNo in", values, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoNotIn(List<String> values) {
            addCriterion("carBodyNo not in", values, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoBetween(String value1, String value2) {
            addCriterion("carBodyNo between", value1, value2, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoNotBetween(String value1, String value2) {
            addCriterion("carBodyNo not between", value1, value2, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoIsNull() {
            addCriterion("gearboxNo is null");
            return (Criteria) this;
        }

        public Criteria andGearboxnoIsNotNull() {
            addCriterion("gearboxNo is not null");
            return (Criteria) this;
        }

        public Criteria andGearboxnoEqualTo(String value) {
            addCriterion("gearboxNo =", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoNotEqualTo(String value) {
            addCriterion("gearboxNo <>", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoGreaterThan(String value) {
            addCriterion("gearboxNo >", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoGreaterThanOrEqualTo(String value) {
            addCriterion("gearboxNo >=", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoLessThan(String value) {
            addCriterion("gearboxNo <", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoLessThanOrEqualTo(String value) {
            addCriterion("gearboxNo <=", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoLike(String value) {
            addCriterion("gearboxNo like", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoNotLike(String value) {
            addCriterion("gearboxNo not like", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoIn(List<String> values) {
            addCriterion("gearboxNo in", values, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoNotIn(List<String> values) {
            addCriterion("gearboxNo not in", values, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoBetween(String value1, String value2) {
            addCriterion("gearboxNo between", value1, value2, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoNotBetween(String value1, String value2) {
            addCriterion("gearboxNo not between", value1, value2, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andCarnameIsNull() {
            addCriterion("carName is null");
            return (Criteria) this;
        }

        public Criteria andCarnameIsNotNull() {
            addCriterion("carName is not null");
            return (Criteria) this;
        }

        public Criteria andCarnameEqualTo(String value) {
            addCriterion("carName =", value, "carname");
            return (Criteria) this;
        }

        public Criteria andCarnameNotEqualTo(String value) {
            addCriterion("carName <>", value, "carname");
            return (Criteria) this;
        }

        public Criteria andCarnameGreaterThan(String value) {
            addCriterion("carName >", value, "carname");
            return (Criteria) this;
        }

        public Criteria andCarnameGreaterThanOrEqualTo(String value) {
            addCriterion("carName >=", value, "carname");
            return (Criteria) this;
        }

        public Criteria andCarnameLessThan(String value) {
            addCriterion("carName <", value, "carname");
            return (Criteria) this;
        }

        public Criteria andCarnameLessThanOrEqualTo(String value) {
            addCriterion("carName <=", value, "carname");
            return (Criteria) this;
        }

        public Criteria andCarnameLike(String value) {
            addCriterion("carName like", value, "carname");
            return (Criteria) this;
        }

        public Criteria andCarnameNotLike(String value) {
            addCriterion("carName not like", value, "carname");
            return (Criteria) this;
        }

        public Criteria andCarnameIn(List<String> values) {
            addCriterion("carName in", values, "carname");
            return (Criteria) this;
        }

        public Criteria andCarnameNotIn(List<String> values) {
            addCriterion("carName not in", values, "carname");
            return (Criteria) this;
        }

        public Criteria andCarnameBetween(String value1, String value2) {
            addCriterion("carName between", value1, value2, "carname");
            return (Criteria) this;
        }

        public Criteria andCarnameNotBetween(String value1, String value2) {
            addCriterion("carName not between", value1, value2, "carname");
            return (Criteria) this;
        }

        public Criteria andCaridtypeIsNull() {
            addCriterion("carIdType is null");
            return (Criteria) this;
        }

        public Criteria andCaridtypeIsNotNull() {
            addCriterion("carIdType is not null");
            return (Criteria) this;
        }

        public Criteria andCaridtypeEqualTo(String value) {
            addCriterion("carIdType =", value, "caridtype");
            return (Criteria) this;
        }

        public Criteria andCaridtypeNotEqualTo(String value) {
            addCriterion("carIdType <>", value, "caridtype");
            return (Criteria) this;
        }

        public Criteria andCaridtypeGreaterThan(String value) {
            addCriterion("carIdType >", value, "caridtype");
            return (Criteria) this;
        }

        public Criteria andCaridtypeGreaterThanOrEqualTo(String value) {
            addCriterion("carIdType >=", value, "caridtype");
            return (Criteria) this;
        }

        public Criteria andCaridtypeLessThan(String value) {
            addCriterion("carIdType <", value, "caridtype");
            return (Criteria) this;
        }

        public Criteria andCaridtypeLessThanOrEqualTo(String value) {
            addCriterion("carIdType <=", value, "caridtype");
            return (Criteria) this;
        }

        public Criteria andCaridtypeLike(String value) {
            addCriterion("carIdType like", value, "caridtype");
            return (Criteria) this;
        }

        public Criteria andCaridtypeNotLike(String value) {
            addCriterion("carIdType not like", value, "caridtype");
            return (Criteria) this;
        }

        public Criteria andCaridtypeIn(List<String> values) {
            addCriterion("carIdType in", values, "caridtype");
            return (Criteria) this;
        }

        public Criteria andCaridtypeNotIn(List<String> values) {
            addCriterion("carIdType not in", values, "caridtype");
            return (Criteria) this;
        }

        public Criteria andCaridtypeBetween(String value1, String value2) {
            addCriterion("carIdType between", value1, value2, "caridtype");
            return (Criteria) this;
        }

        public Criteria andCaridtypeNotBetween(String value1, String value2) {
            addCriterion("carIdType not between", value1, value2, "caridtype");
            return (Criteria) this;
        }

        public Criteria andReleasedateIsNull() {
            addCriterion("releaseDate is null");
            return (Criteria) this;
        }

        public Criteria andReleasedateIsNotNull() {
            addCriterion("releaseDate is not null");
            return (Criteria) this;
        }

        public Criteria andReleasedateEqualTo(Date value) {
            addCriterionForJDBCDate("releaseDate =", value, "releasedate");
            return (Criteria) this;
        }

        public Criteria andReleasedateNotEqualTo(Date value) {
            addCriterionForJDBCDate("releaseDate <>", value, "releasedate");
            return (Criteria) this;
        }

        public Criteria andReleasedateGreaterThan(Date value) {
            addCriterionForJDBCDate("releaseDate >", value, "releasedate");
            return (Criteria) this;
        }

        public Criteria andReleasedateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("releaseDate >=", value, "releasedate");
            return (Criteria) this;
        }

        public Criteria andReleasedateLessThan(Date value) {
            addCriterionForJDBCDate("releaseDate <", value, "releasedate");
            return (Criteria) this;
        }

        public Criteria andReleasedateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("releaseDate <=", value, "releasedate");
            return (Criteria) this;
        }

        public Criteria andReleasedateIn(List<Date> values) {
            addCriterionForJDBCDate("releaseDate in", values, "releasedate");
            return (Criteria) this;
        }

        public Criteria andReleasedateNotIn(List<Date> values) {
            addCriterionForJDBCDate("releaseDate not in", values, "releasedate");
            return (Criteria) this;
        }

        public Criteria andReleasedateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("releaseDate between", value1, value2, "releasedate");
            return (Criteria) this;
        }

        public Criteria andReleasedateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("releaseDate not between", value1, value2, "releasedate");
            return (Criteria) this;
        }

        public Criteria andSalepriceIsNull() {
            addCriterion("salePrice is null");
            return (Criteria) this;
        }

        public Criteria andSalepriceIsNotNull() {
            addCriterion("salePrice is not null");
            return (Criteria) this;
        }

        public Criteria andSalepriceEqualTo(String value) {
            addCriterion("salePrice =", value, "saleprice");
            return (Criteria) this;
        }

        public Criteria andSalepriceNotEqualTo(String value) {
            addCriterion("salePrice <>", value, "saleprice");
            return (Criteria) this;
        }

        public Criteria andSalepriceGreaterThan(String value) {
            addCriterion("salePrice >", value, "saleprice");
            return (Criteria) this;
        }

        public Criteria andSalepriceGreaterThanOrEqualTo(String value) {
            addCriterion("salePrice >=", value, "saleprice");
            return (Criteria) this;
        }

        public Criteria andSalepriceLessThan(String value) {
            addCriterion("salePrice <", value, "saleprice");
            return (Criteria) this;
        }

        public Criteria andSalepriceLessThanOrEqualTo(String value) {
            addCriterion("salePrice <=", value, "saleprice");
            return (Criteria) this;
        }

        public Criteria andSalepriceLike(String value) {
            addCriterion("salePrice like", value, "saleprice");
            return (Criteria) this;
        }

        public Criteria andSalepriceNotLike(String value) {
            addCriterion("salePrice not like", value, "saleprice");
            return (Criteria) this;
        }

        public Criteria andSalepriceIn(List<String> values) {
            addCriterion("salePrice in", values, "saleprice");
            return (Criteria) this;
        }

        public Criteria andSalepriceNotIn(List<String> values) {
            addCriterion("salePrice not in", values, "saleprice");
            return (Criteria) this;
        }

        public Criteria andSalepriceBetween(String value1, String value2) {
            addCriterion("salePrice between", value1, value2, "saleprice");
            return (Criteria) this;
        }

        public Criteria andSalepriceNotBetween(String value1, String value2) {
            addCriterion("salePrice not between", value1, value2, "saleprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceIsNull() {
            addCriterion("currentPrice is null");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceIsNotNull() {
            addCriterion("currentPrice is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceEqualTo(String value) {
            addCriterion("currentPrice =", value, "currentprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceNotEqualTo(String value) {
            addCriterion("currentPrice <>", value, "currentprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceGreaterThan(String value) {
            addCriterion("currentPrice >", value, "currentprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceGreaterThanOrEqualTo(String value) {
            addCriterion("currentPrice >=", value, "currentprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceLessThan(String value) {
            addCriterion("currentPrice <", value, "currentprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceLessThanOrEqualTo(String value) {
            addCriterion("currentPrice <=", value, "currentprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceLike(String value) {
            addCriterion("currentPrice like", value, "currentprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceNotLike(String value) {
            addCriterion("currentPrice not like", value, "currentprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceIn(List<String> values) {
            addCriterion("currentPrice in", values, "currentprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceNotIn(List<String> values) {
            addCriterion("currentPrice not in", values, "currentprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceBetween(String value1, String value2) {
            addCriterion("currentPrice between", value1, value2, "currentprice");
            return (Criteria) this;
        }

        public Criteria andCurrentpriceNotBetween(String value1, String value2) {
            addCriterion("currentPrice not between", value1, value2, "currentprice");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("Status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("Status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("Status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("Status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("Status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("Status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("Status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("Status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("Status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("Status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("Status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("Status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("Status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("Status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}