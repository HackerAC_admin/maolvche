package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EmpOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public EmpOrderExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGetordernoIsNull() {
            addCriterion("getOrderNo is null");
            return (Criteria) this;
        }

        public Criteria andGetordernoIsNotNull() {
            addCriterion("getOrderNo is not null");
            return (Criteria) this;
        }

        public Criteria andGetordernoEqualTo(String value) {
            addCriterion("getOrderNo =", value, "getorderno");
            return (Criteria) this;
        }

        public Criteria andGetordernoNotEqualTo(String value) {
            addCriterion("getOrderNo <>", value, "getorderno");
            return (Criteria) this;
        }

        public Criteria andGetordernoGreaterThan(String value) {
            addCriterion("getOrderNo >", value, "getorderno");
            return (Criteria) this;
        }

        public Criteria andGetordernoGreaterThanOrEqualTo(String value) {
            addCriterion("getOrderNo >=", value, "getorderno");
            return (Criteria) this;
        }

        public Criteria andGetordernoLessThan(String value) {
            addCriterion("getOrderNo <", value, "getorderno");
            return (Criteria) this;
        }

        public Criteria andGetordernoLessThanOrEqualTo(String value) {
            addCriterion("getOrderNo <=", value, "getorderno");
            return (Criteria) this;
        }

        public Criteria andGetordernoLike(String value) {
            addCriterion("getOrderNo like", value, "getorderno");
            return (Criteria) this;
        }

        public Criteria andGetordernoNotLike(String value) {
            addCriterion("getOrderNo not like", value, "getorderno");
            return (Criteria) this;
        }

        public Criteria andGetordernoIn(List<String> values) {
            addCriterion("getOrderNo in", values, "getorderno");
            return (Criteria) this;
        }

        public Criteria andGetordernoNotIn(List<String> values) {
            addCriterion("getOrderNo not in", values, "getorderno");
            return (Criteria) this;
        }

        public Criteria andGetordernoBetween(String value1, String value2) {
            addCriterion("getOrderNo between", value1, value2, "getorderno");
            return (Criteria) this;
        }

        public Criteria andGetordernoNotBetween(String value1, String value2) {
            addCriterion("getOrderNo not between", value1, value2, "getorderno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIsNull() {
            addCriterion("employeeNo is null");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIsNotNull() {
            addCriterion("employeeNo is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeenoEqualTo(String value) {
            addCriterion("employeeNo =", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotEqualTo(String value) {
            addCriterion("employeeNo <>", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoGreaterThan(String value) {
            addCriterion("employeeNo >", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoGreaterThanOrEqualTo(String value) {
            addCriterion("employeeNo >=", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLessThan(String value) {
            addCriterion("employeeNo <", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLessThanOrEqualTo(String value) {
            addCriterion("employeeNo <=", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLike(String value) {
            addCriterion("employeeNo like", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotLike(String value) {
            addCriterion("employeeNo not like", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIn(List<String> values) {
            addCriterion("employeeNo in", values, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotIn(List<String> values) {
            addCriterion("employeeNo not in", values, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoBetween(String value1, String value2) {
            addCriterion("employeeNo between", value1, value2, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotBetween(String value1, String value2) {
            addCriterion("employeeNo not between", value1, value2, "employeeno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoIsNull() {
            addCriterion("newCarOrderNo is null");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoIsNotNull() {
            addCriterion("newCarOrderNo is not null");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoEqualTo(String value) {
            addCriterion("newCarOrderNo =", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoNotEqualTo(String value) {
            addCriterion("newCarOrderNo <>", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoGreaterThan(String value) {
            addCriterion("newCarOrderNo >", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoGreaterThanOrEqualTo(String value) {
            addCriterion("newCarOrderNo >=", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoLessThan(String value) {
            addCriterion("newCarOrderNo <", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoLessThanOrEqualTo(String value) {
            addCriterion("newCarOrderNo <=", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoLike(String value) {
            addCriterion("newCarOrderNo like", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoNotLike(String value) {
            addCriterion("newCarOrderNo not like", value, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoIn(List<String> values) {
            addCriterion("newCarOrderNo in", values, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoNotIn(List<String> values) {
            addCriterion("newCarOrderNo not in", values, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoBetween(String value1, String value2) {
            addCriterion("newCarOrderNo between", value1, value2, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andNewcarordernoNotBetween(String value1, String value2) {
            addCriterion("newCarOrderNo not between", value1, value2, "newcarorderno");
            return (Criteria) this;
        }

        public Criteria andGetordertimeIsNull() {
            addCriterion("getOrderTime is null");
            return (Criteria) this;
        }

        public Criteria andGetordertimeIsNotNull() {
            addCriterion("getOrderTime is not null");
            return (Criteria) this;
        }

        public Criteria andGetordertimeEqualTo(Date value) {
            addCriterion("getOrderTime =", value, "getordertime");
            return (Criteria) this;
        }

        public Criteria andGetordertimeNotEqualTo(Date value) {
            addCriterion("getOrderTime <>", value, "getordertime");
            return (Criteria) this;
        }

        public Criteria andGetordertimeGreaterThan(Date value) {
            addCriterion("getOrderTime >", value, "getordertime");
            return (Criteria) this;
        }

        public Criteria andGetordertimeGreaterThanOrEqualTo(Date value) {
            addCriterion("getOrderTime >=", value, "getordertime");
            return (Criteria) this;
        }

        public Criteria andGetordertimeLessThan(Date value) {
            addCriterion("getOrderTime <", value, "getordertime");
            return (Criteria) this;
        }

        public Criteria andGetordertimeLessThanOrEqualTo(Date value) {
            addCriterion("getOrderTime <=", value, "getordertime");
            return (Criteria) this;
        }

        public Criteria andGetordertimeIn(List<Date> values) {
            addCriterion("getOrderTime in", values, "getordertime");
            return (Criteria) this;
        }

        public Criteria andGetordertimeNotIn(List<Date> values) {
            addCriterion("getOrderTime not in", values, "getordertime");
            return (Criteria) this;
        }

        public Criteria andGetordertimeBetween(Date value1, Date value2) {
            addCriterion("getOrderTime between", value1, value2, "getordertime");
            return (Criteria) this;
        }

        public Criteria andGetordertimeNotBetween(Date value1, Date value2) {
            addCriterion("getOrderTime not between", value1, value2, "getordertime");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("State is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("State is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(String value) {
            addCriterion("State =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(String value) {
            addCriterion("State <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(String value) {
            addCriterion("State >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(String value) {
            addCriterion("State >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(String value) {
            addCriterion("State <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(String value) {
            addCriterion("State <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLike(String value) {
            addCriterion("State like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotLike(String value) {
            addCriterion("State not like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<String> values) {
            addCriterion("State in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<String> values) {
            addCriterion("State not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(String value1, String value2) {
            addCriterion("State between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(String value1, String value2) {
            addCriterion("State not between", value1, value2, "state");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}