package com.maolvche.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Favorites {
    private Integer favoritesno;

    private String carid;

    private String useracc;

    private Date addtime;

    public Integer getFavoritesno() {
        return favoritesno;
    }

    public void setFavoritesno(Integer favoritesno) {
        this.favoritesno = favoritesno;
    }

    public String getCarid() {
        return carid;
    }

    public void setCarid(String carid) {
        this.carid = carid == null ? null : carid.trim();
    }

    public String getUseracc() {
        return useracc;
    }

    public void setUseracc(String useracc) {
        this.useracc = useracc == null ? null : useracc.trim();
    }

    public String getAddtime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(addtime);
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}