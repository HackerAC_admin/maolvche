package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.List;

public class CarEngineExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CarEngineExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCarenginenoIsNull() {
            addCriterion("carEngineNo is null");
            return (Criteria) this;
        }

        public Criteria andCarenginenoIsNotNull() {
            addCriterion("carEngineNo is not null");
            return (Criteria) this;
        }

        public Criteria andCarenginenoEqualTo(String value) {
            addCriterion("carEngineNo =", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoNotEqualTo(String value) {
            addCriterion("carEngineNo <>", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoGreaterThan(String value) {
            addCriterion("carEngineNo >", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoGreaterThanOrEqualTo(String value) {
            addCriterion("carEngineNo >=", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoLessThan(String value) {
            addCriterion("carEngineNo <", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoLessThanOrEqualTo(String value) {
            addCriterion("carEngineNo <=", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoLike(String value) {
            addCriterion("carEngineNo like", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoNotLike(String value) {
            addCriterion("carEngineNo not like", value, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoIn(List<String> values) {
            addCriterion("carEngineNo in", values, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoNotIn(List<String> values) {
            addCriterion("carEngineNo not in", values, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoBetween(String value1, String value2) {
            addCriterion("carEngineNo between", value1, value2, "carengineno");
            return (Criteria) this;
        }

        public Criteria andCarenginenoNotBetween(String value1, String value2) {
            addCriterion("carEngineNo not between", value1, value2, "carengineno");
            return (Criteria) this;
        }

        public Criteria andEnginetypeIsNull() {
            addCriterion("engineType is null");
            return (Criteria) this;
        }

        public Criteria andEnginetypeIsNotNull() {
            addCriterion("engineType is not null");
            return (Criteria) this;
        }

        public Criteria andEnginetypeEqualTo(String value) {
            addCriterion("engineType =", value, "enginetype");
            return (Criteria) this;
        }

        public Criteria andEnginetypeNotEqualTo(String value) {
            addCriterion("engineType <>", value, "enginetype");
            return (Criteria) this;
        }

        public Criteria andEnginetypeGreaterThan(String value) {
            addCriterion("engineType >", value, "enginetype");
            return (Criteria) this;
        }

        public Criteria andEnginetypeGreaterThanOrEqualTo(String value) {
            addCriterion("engineType >=", value, "enginetype");
            return (Criteria) this;
        }

        public Criteria andEnginetypeLessThan(String value) {
            addCriterion("engineType <", value, "enginetype");
            return (Criteria) this;
        }

        public Criteria andEnginetypeLessThanOrEqualTo(String value) {
            addCriterion("engineType <=", value, "enginetype");
            return (Criteria) this;
        }

        public Criteria andEnginetypeLike(String value) {
            addCriterion("engineType like", value, "enginetype");
            return (Criteria) this;
        }

        public Criteria andEnginetypeNotLike(String value) {
            addCriterion("engineType not like", value, "enginetype");
            return (Criteria) this;
        }

        public Criteria andEnginetypeIn(List<String> values) {
            addCriterion("engineType in", values, "enginetype");
            return (Criteria) this;
        }

        public Criteria andEnginetypeNotIn(List<String> values) {
            addCriterion("engineType not in", values, "enginetype");
            return (Criteria) this;
        }

        public Criteria andEnginetypeBetween(String value1, String value2) {
            addCriterion("engineType between", value1, value2, "enginetype");
            return (Criteria) this;
        }

        public Criteria andEnginetypeNotBetween(String value1, String value2) {
            addCriterion("engineType not between", value1, value2, "enginetype");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeIsNull() {
            addCriterion("outputVolume is null");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeIsNotNull() {
            addCriterion("outputVolume is not null");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeEqualTo(String value) {
            addCriterion("outputVolume =", value, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeNotEqualTo(String value) {
            addCriterion("outputVolume <>", value, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeGreaterThan(String value) {
            addCriterion("outputVolume >", value, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeGreaterThanOrEqualTo(String value) {
            addCriterion("outputVolume >=", value, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeLessThan(String value) {
            addCriterion("outputVolume <", value, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeLessThanOrEqualTo(String value) {
            addCriterion("outputVolume <=", value, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeLike(String value) {
            addCriterion("outputVolume like", value, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeNotLike(String value) {
            addCriterion("outputVolume not like", value, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeIn(List<String> values) {
            addCriterion("outputVolume in", values, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeNotIn(List<String> values) {
            addCriterion("outputVolume not in", values, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeBetween(String value1, String value2) {
            addCriterion("outputVolume between", value1, value2, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andOutputvolumeNotBetween(String value1, String value2) {
            addCriterion("outputVolume not between", value1, value2, "outputvolume");
            return (Criteria) this;
        }

        public Criteria andAirinletIsNull() {
            addCriterion("airInlet is null");
            return (Criteria) this;
        }

        public Criteria andAirinletIsNotNull() {
            addCriterion("airInlet is not null");
            return (Criteria) this;
        }

        public Criteria andAirinletEqualTo(String value) {
            addCriterion("airInlet =", value, "airinlet");
            return (Criteria) this;
        }

        public Criteria andAirinletNotEqualTo(String value) {
            addCriterion("airInlet <>", value, "airinlet");
            return (Criteria) this;
        }

        public Criteria andAirinletGreaterThan(String value) {
            addCriterion("airInlet >", value, "airinlet");
            return (Criteria) this;
        }

        public Criteria andAirinletGreaterThanOrEqualTo(String value) {
            addCriterion("airInlet >=", value, "airinlet");
            return (Criteria) this;
        }

        public Criteria andAirinletLessThan(String value) {
            addCriterion("airInlet <", value, "airinlet");
            return (Criteria) this;
        }

        public Criteria andAirinletLessThanOrEqualTo(String value) {
            addCriterion("airInlet <=", value, "airinlet");
            return (Criteria) this;
        }

        public Criteria andAirinletLike(String value) {
            addCriterion("airInlet like", value, "airinlet");
            return (Criteria) this;
        }

        public Criteria andAirinletNotLike(String value) {
            addCriterion("airInlet not like", value, "airinlet");
            return (Criteria) this;
        }

        public Criteria andAirinletIn(List<String> values) {
            addCriterion("airInlet in", values, "airinlet");
            return (Criteria) this;
        }

        public Criteria andAirinletNotIn(List<String> values) {
            addCriterion("airInlet not in", values, "airinlet");
            return (Criteria) this;
        }

        public Criteria andAirinletBetween(String value1, String value2) {
            addCriterion("airInlet between", value1, value2, "airinlet");
            return (Criteria) this;
        }

        public Criteria andAirinletNotBetween(String value1, String value2) {
            addCriterion("airInlet not between", value1, value2, "airinlet");
            return (Criteria) this;
        }

        public Criteria andCylindersnoIsNull() {
            addCriterion("cylindersNo is null");
            return (Criteria) this;
        }

        public Criteria andCylindersnoIsNotNull() {
            addCriterion("cylindersNo is not null");
            return (Criteria) this;
        }

        public Criteria andCylindersnoEqualTo(Integer value) {
            addCriterion("cylindersNo =", value, "cylindersno");
            return (Criteria) this;
        }

        public Criteria andCylindersnoNotEqualTo(Integer value) {
            addCriterion("cylindersNo <>", value, "cylindersno");
            return (Criteria) this;
        }

        public Criteria andCylindersnoGreaterThan(Integer value) {
            addCriterion("cylindersNo >", value, "cylindersno");
            return (Criteria) this;
        }

        public Criteria andCylindersnoGreaterThanOrEqualTo(Integer value) {
            addCriterion("cylindersNo >=", value, "cylindersno");
            return (Criteria) this;
        }

        public Criteria andCylindersnoLessThan(Integer value) {
            addCriterion("cylindersNo <", value, "cylindersno");
            return (Criteria) this;
        }

        public Criteria andCylindersnoLessThanOrEqualTo(Integer value) {
            addCriterion("cylindersNo <=", value, "cylindersno");
            return (Criteria) this;
        }

        public Criteria andCylindersnoIn(List<Integer> values) {
            addCriterion("cylindersNo in", values, "cylindersno");
            return (Criteria) this;
        }

        public Criteria andCylindersnoNotIn(List<Integer> values) {
            addCriterion("cylindersNo not in", values, "cylindersno");
            return (Criteria) this;
        }

        public Criteria andCylindersnoBetween(Integer value1, Integer value2) {
            addCriterion("cylindersNo between", value1, value2, "cylindersno");
            return (Criteria) this;
        }

        public Criteria andCylindersnoNotBetween(Integer value1, Integer value2) {
            addCriterion("cylindersNo not between", value1, value2, "cylindersno");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerIsNull() {
            addCriterion("maxhorsePower is null");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerIsNotNull() {
            addCriterion("maxhorsePower is not null");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerEqualTo(String value) {
            addCriterion("maxhorsePower =", value, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerNotEqualTo(String value) {
            addCriterion("maxhorsePower <>", value, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerGreaterThan(String value) {
            addCriterion("maxhorsePower >", value, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerGreaterThanOrEqualTo(String value) {
            addCriterion("maxhorsePower >=", value, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerLessThan(String value) {
            addCriterion("maxhorsePower <", value, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerLessThanOrEqualTo(String value) {
            addCriterion("maxhorsePower <=", value, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerLike(String value) {
            addCriterion("maxhorsePower like", value, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerNotLike(String value) {
            addCriterion("maxhorsePower not like", value, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerIn(List<String> values) {
            addCriterion("maxhorsePower in", values, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerNotIn(List<String> values) {
            addCriterion("maxhorsePower not in", values, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerBetween(String value1, String value2) {
            addCriterion("maxhorsePower between", value1, value2, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxhorsepowerNotBetween(String value1, String value2) {
            addCriterion("maxhorsePower not between", value1, value2, "maxhorsepower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerIsNull() {
            addCriterion("maxPower is null");
            return (Criteria) this;
        }

        public Criteria andMaxpowerIsNotNull() {
            addCriterion("maxPower is not null");
            return (Criteria) this;
        }

        public Criteria andMaxpowerEqualTo(String value) {
            addCriterion("maxPower =", value, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerNotEqualTo(String value) {
            addCriterion("maxPower <>", value, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerGreaterThan(String value) {
            addCriterion("maxPower >", value, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerGreaterThanOrEqualTo(String value) {
            addCriterion("maxPower >=", value, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerLessThan(String value) {
            addCriterion("maxPower <", value, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerLessThanOrEqualTo(String value) {
            addCriterion("maxPower <=", value, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerLike(String value) {
            addCriterion("maxPower like", value, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerNotLike(String value) {
            addCriterion("maxPower not like", value, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerIn(List<String> values) {
            addCriterion("maxPower in", values, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerNotIn(List<String> values) {
            addCriterion("maxPower not in", values, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerBetween(String value1, String value2) {
            addCriterion("maxPower between", value1, value2, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerNotBetween(String value1, String value2) {
            addCriterion("maxPower not between", value1, value2, "maxpower");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedIsNull() {
            addCriterion("maxpowerSpeed is null");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedIsNotNull() {
            addCriterion("maxpowerSpeed is not null");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedEqualTo(String value) {
            addCriterion("maxpowerSpeed =", value, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedNotEqualTo(String value) {
            addCriterion("maxpowerSpeed <>", value, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedGreaterThan(String value) {
            addCriterion("maxpowerSpeed >", value, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedGreaterThanOrEqualTo(String value) {
            addCriterion("maxpowerSpeed >=", value, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedLessThan(String value) {
            addCriterion("maxpowerSpeed <", value, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedLessThanOrEqualTo(String value) {
            addCriterion("maxpowerSpeed <=", value, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedLike(String value) {
            addCriterion("maxpowerSpeed like", value, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedNotLike(String value) {
            addCriterion("maxpowerSpeed not like", value, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedIn(List<String> values) {
            addCriterion("maxpowerSpeed in", values, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedNotIn(List<String> values) {
            addCriterion("maxpowerSpeed not in", values, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedBetween(String value1, String value2) {
            addCriterion("maxpowerSpeed between", value1, value2, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andMaxpowerspeedNotBetween(String value1, String value2) {
            addCriterion("maxpowerSpeed not between", value1, value2, "maxpowerspeed");
            return (Criteria) this;
        }

        public Criteria andFueltypeIsNull() {
            addCriterion("fuelType is null");
            return (Criteria) this;
        }

        public Criteria andFueltypeIsNotNull() {
            addCriterion("fuelType is not null");
            return (Criteria) this;
        }

        public Criteria andFueltypeEqualTo(String value) {
            addCriterion("fuelType =", value, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFueltypeNotEqualTo(String value) {
            addCriterion("fuelType <>", value, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFueltypeGreaterThan(String value) {
            addCriterion("fuelType >", value, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFueltypeGreaterThanOrEqualTo(String value) {
            addCriterion("fuelType >=", value, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFueltypeLessThan(String value) {
            addCriterion("fuelType <", value, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFueltypeLessThanOrEqualTo(String value) {
            addCriterion("fuelType <=", value, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFueltypeLike(String value) {
            addCriterion("fuelType like", value, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFueltypeNotLike(String value) {
            addCriterion("fuelType not like", value, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFueltypeIn(List<String> values) {
            addCriterion("fuelType in", values, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFueltypeNotIn(List<String> values) {
            addCriterion("fuelType not in", values, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFueltypeBetween(String value1, String value2) {
            addCriterion("fuelType between", value1, value2, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFueltypeNotBetween(String value1, String value2) {
            addCriterion("fuelType not between", value1, value2, "fueltype");
            return (Criteria) this;
        }

        public Criteria andFuellabelIsNull() {
            addCriterion("fuelLabel is null");
            return (Criteria) this;
        }

        public Criteria andFuellabelIsNotNull() {
            addCriterion("fuelLabel is not null");
            return (Criteria) this;
        }

        public Criteria andFuellabelEqualTo(Integer value) {
            addCriterion("fuelLabel =", value, "fuellabel");
            return (Criteria) this;
        }

        public Criteria andFuellabelNotEqualTo(Integer value) {
            addCriterion("fuelLabel <>", value, "fuellabel");
            return (Criteria) this;
        }

        public Criteria andFuellabelGreaterThan(Integer value) {
            addCriterion("fuelLabel >", value, "fuellabel");
            return (Criteria) this;
        }

        public Criteria andFuellabelGreaterThanOrEqualTo(Integer value) {
            addCriterion("fuelLabel >=", value, "fuellabel");
            return (Criteria) this;
        }

        public Criteria andFuellabelLessThan(Integer value) {
            addCriterion("fuelLabel <", value, "fuellabel");
            return (Criteria) this;
        }

        public Criteria andFuellabelLessThanOrEqualTo(Integer value) {
            addCriterion("fuelLabel <=", value, "fuellabel");
            return (Criteria) this;
        }

        public Criteria andFuellabelIn(List<Integer> values) {
            addCriterion("fuelLabel in", values, "fuellabel");
            return (Criteria) this;
        }

        public Criteria andFuellabelNotIn(List<Integer> values) {
            addCriterion("fuelLabel not in", values, "fuellabel");
            return (Criteria) this;
        }

        public Criteria andFuellabelBetween(Integer value1, Integer value2) {
            addCriterion("fuelLabel between", value1, value2, "fuellabel");
            return (Criteria) this;
        }

        public Criteria andFuellabelNotBetween(Integer value1, Integer value2) {
            addCriterion("fuelLabel not between", value1, value2, "fuellabel");
            return (Criteria) this;
        }

        public Criteria andOildriveIsNull() {
            addCriterion("oilDrive is null");
            return (Criteria) this;
        }

        public Criteria andOildriveIsNotNull() {
            addCriterion("oilDrive is not null");
            return (Criteria) this;
        }

        public Criteria andOildriveEqualTo(String value) {
            addCriterion("oilDrive =", value, "oildrive");
            return (Criteria) this;
        }

        public Criteria andOildriveNotEqualTo(String value) {
            addCriterion("oilDrive <>", value, "oildrive");
            return (Criteria) this;
        }

        public Criteria andOildriveGreaterThan(String value) {
            addCriterion("oilDrive >", value, "oildrive");
            return (Criteria) this;
        }

        public Criteria andOildriveGreaterThanOrEqualTo(String value) {
            addCriterion("oilDrive >=", value, "oildrive");
            return (Criteria) this;
        }

        public Criteria andOildriveLessThan(String value) {
            addCriterion("oilDrive <", value, "oildrive");
            return (Criteria) this;
        }

        public Criteria andOildriveLessThanOrEqualTo(String value) {
            addCriterion("oilDrive <=", value, "oildrive");
            return (Criteria) this;
        }

        public Criteria andOildriveLike(String value) {
            addCriterion("oilDrive like", value, "oildrive");
            return (Criteria) this;
        }

        public Criteria andOildriveNotLike(String value) {
            addCriterion("oilDrive not like", value, "oildrive");
            return (Criteria) this;
        }

        public Criteria andOildriveIn(List<String> values) {
            addCriterion("oilDrive in", values, "oildrive");
            return (Criteria) this;
        }

        public Criteria andOildriveNotIn(List<String> values) {
            addCriterion("oilDrive not in", values, "oildrive");
            return (Criteria) this;
        }

        public Criteria andOildriveBetween(String value1, String value2) {
            addCriterion("oilDrive between", value1, value2, "oildrive");
            return (Criteria) this;
        }

        public Criteria andOildriveNotBetween(String value1, String value2) {
            addCriterion("oilDrive not between", value1, value2, "oildrive");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}