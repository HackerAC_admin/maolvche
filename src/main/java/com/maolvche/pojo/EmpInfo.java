package com.maolvche.pojo;

import java.util.Date;

public class EmpInfo {
    private String employeeno;

    private String employeename;

    private String employeesex;

    private String employeeidno;

    private String employeeemail;

    private String employeephone;

    private String employeeaddress;

    private String job;

    private Date indate;

    public String getEmployeeno() {
        return employeeno;
    }

    public void setEmployeeno(String employeeno) {
        this.employeeno = employeeno == null ? null : employeeno.trim();
    }

    public String getEmployeename() {
        return employeename;
    }

    public void setEmployeename(String employeename) {
        this.employeename = employeename == null ? null : employeename.trim();
    }

    public String getEmployeesex() {
        return employeesex;
    }

    public void setEmployeesex(String employeesex) {
        this.employeesex = employeesex == null ? null : employeesex.trim();
    }

    public String getEmployeeidno() {
        return employeeidno;
    }

    public void setEmployeeidno(String employeeidno) {
        this.employeeidno = employeeidno == null ? null : employeeidno.trim();
    }

    public String getEmployeeemail() {
        return employeeemail;
    }

    public void setEmployeeemail(String employeeemail) {
        this.employeeemail = employeeemail == null ? null : employeeemail.trim();
    }

    public String getEmployeephone() {
        return employeephone;
    }

    public void setEmployeephone(String employeephone) {
        this.employeephone = employeephone == null ? null : employeephone.trim();
    }

    public String getEmployeeaddress() {
        return employeeaddress;
    }

    public void setEmployeeaddress(String employeeaddress) {
        this.employeeaddress = employeeaddress == null ? null : employeeaddress.trim();
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job == null ? null : job.trim();
    }

    public Date getIndate() {
        return indate;
    }

    public void setIndate(Date indate) {
        this.indate = indate;
    }
}