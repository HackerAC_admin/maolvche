package com.maolvche.pojo;

import java.util.Date;

public class UserInstlInfo {
    private Integer userinstlinfono;

    private String useracc;

    private Float totalmoney;

    private Float firstpaymoney;

    private Float monthpaymoney;

    private Date startdate;

    public Integer getUserinstlinfono() {
        return userinstlinfono;
    }

    public void setUserinstlinfono(Integer userinstlinfono) {
        this.userinstlinfono = userinstlinfono;
    }

    public String getUseracc() {
        return useracc;
    }

    public void setUseracc(String useracc) {
        this.useracc = useracc == null ? null : useracc.trim();
    }

    public Float getTotalmoney() {
        return totalmoney;
    }

    public void setTotalmoney(Float totalmoney) {
        this.totalmoney = totalmoney;
    }

    public Float getFirstpaymoney() {
        return firstpaymoney;
    }

    public void setFirstpaymoney(Float firstpaymoney) {
        this.firstpaymoney = firstpaymoney;
    }

    public Float getMonthpaymoney() {
        return monthpaymoney;
    }

    public void setMonthpaymoney(Float monthpaymoney) {
        this.monthpaymoney = monthpaymoney;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }
}