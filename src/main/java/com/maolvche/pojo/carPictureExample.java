package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.List;

public class carPictureExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public carPictureExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPicturenoIsNull() {
            addCriterion("pictureNo is null");
            return (Criteria) this;
        }

        public Criteria andPicturenoIsNotNull() {
            addCriterion("pictureNo is not null");
            return (Criteria) this;
        }

        public Criteria andPicturenoEqualTo(Integer value) {
            addCriterion("pictureNo =", value, "pictureno");
            return (Criteria) this;
        }

        public Criteria andPicturenoNotEqualTo(Integer value) {
            addCriterion("pictureNo <>", value, "pictureno");
            return (Criteria) this;
        }

        public Criteria andPicturenoGreaterThan(Integer value) {
            addCriterion("pictureNo >", value, "pictureno");
            return (Criteria) this;
        }

        public Criteria andPicturenoGreaterThanOrEqualTo(Integer value) {
            addCriterion("pictureNo >=", value, "pictureno");
            return (Criteria) this;
        }

        public Criteria andPicturenoLessThan(Integer value) {
            addCriterion("pictureNo <", value, "pictureno");
            return (Criteria) this;
        }

        public Criteria andPicturenoLessThanOrEqualTo(Integer value) {
            addCriterion("pictureNo <=", value, "pictureno");
            return (Criteria) this;
        }

        public Criteria andPicturenoIn(List<Integer> values) {
            addCriterion("pictureNo in", values, "pictureno");
            return (Criteria) this;
        }

        public Criteria andPicturenoNotIn(List<Integer> values) {
            addCriterion("pictureNo not in", values, "pictureno");
            return (Criteria) this;
        }

        public Criteria andPicturenoBetween(Integer value1, Integer value2) {
            addCriterion("pictureNo between", value1, value2, "pictureno");
            return (Criteria) this;
        }

        public Criteria andPicturenoNotBetween(Integer value1, Integer value2) {
            addCriterion("pictureNo not between", value1, value2, "pictureno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoIsNull() {
            addCriterion("newcarNo is null");
            return (Criteria) this;
        }

        public Criteria andNewcarnoIsNotNull() {
            addCriterion("newcarNo is not null");
            return (Criteria) this;
        }

        public Criteria andNewcarnoEqualTo(String value) {
            addCriterion("newcarNo =", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotEqualTo(String value) {
            addCriterion("newcarNo <>", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoGreaterThan(String value) {
            addCriterion("newcarNo >", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoGreaterThanOrEqualTo(String value) {
            addCriterion("newcarNo >=", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoLessThan(String value) {
            addCriterion("newcarNo <", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoLessThanOrEqualTo(String value) {
            addCriterion("newcarNo <=", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoLike(String value) {
            addCriterion("newcarNo like", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotLike(String value) {
            addCriterion("newcarNo not like", value, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoIn(List<String> values) {
            addCriterion("newcarNo in", values, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotIn(List<String> values) {
            addCriterion("newcarNo not in", values, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoBetween(String value1, String value2) {
            addCriterion("newcarNo between", value1, value2, "newcarno");
            return (Criteria) this;
        }

        public Criteria andNewcarnoNotBetween(String value1, String value2) {
            addCriterion("newcarNo not between", value1, value2, "newcarno");
            return (Criteria) this;
        }

        public Criteria andPicturesrcIsNull() {
            addCriterion("pictureSrc is null");
            return (Criteria) this;
        }

        public Criteria andPicturesrcIsNotNull() {
            addCriterion("pictureSrc is not null");
            return (Criteria) this;
        }

        public Criteria andPicturesrcEqualTo(String value) {
            addCriterion("pictureSrc =", value, "picturesrc");
            return (Criteria) this;
        }

        public Criteria andPicturesrcNotEqualTo(String value) {
            addCriterion("pictureSrc <>", value, "picturesrc");
            return (Criteria) this;
        }

        public Criteria andPicturesrcGreaterThan(String value) {
            addCriterion("pictureSrc >", value, "picturesrc");
            return (Criteria) this;
        }

        public Criteria andPicturesrcGreaterThanOrEqualTo(String value) {
            addCriterion("pictureSrc >=", value, "picturesrc");
            return (Criteria) this;
        }

        public Criteria andPicturesrcLessThan(String value) {
            addCriterion("pictureSrc <", value, "picturesrc");
            return (Criteria) this;
        }

        public Criteria andPicturesrcLessThanOrEqualTo(String value) {
            addCriterion("pictureSrc <=", value, "picturesrc");
            return (Criteria) this;
        }

        public Criteria andPicturesrcLike(String value) {
            addCriterion("pictureSrc like", value, "picturesrc");
            return (Criteria) this;
        }

        public Criteria andPicturesrcNotLike(String value) {
            addCriterion("pictureSrc not like", value, "picturesrc");
            return (Criteria) this;
        }

        public Criteria andPicturesrcIn(List<String> values) {
            addCriterion("pictureSrc in", values, "picturesrc");
            return (Criteria) this;
        }

        public Criteria andPicturesrcNotIn(List<String> values) {
            addCriterion("pictureSrc not in", values, "picturesrc");
            return (Criteria) this;
        }

        public Criteria andPicturesrcBetween(String value1, String value2) {
            addCriterion("pictureSrc between", value1, value2, "picturesrc");
            return (Criteria) this;
        }

        public Criteria andPicturesrcNotBetween(String value1, String value2) {
            addCriterion("pictureSrc not between", value1, value2, "picturesrc");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}