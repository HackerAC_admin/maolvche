package com.maolvche.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class InstRecord {
    private Integer instlmntno;

    private String planno;

    private String useracc;

    private Float lastmoney;

    private Date repaymentdate;

    private Float repaymentmoney;

    public Integer getInstlmntno() {
        return instlmntno;
    }

    public void setInstlmntno(Integer instlmntno) {
        this.instlmntno = instlmntno;
    }

    public String getPlanno() {
        return planno;
    }

    public void setPlanno(String planno) {
        this.planno = planno == null ? null : planno.trim();
    }

    public String getUseracc() {
        return useracc;
    }

    public void setUseracc(String useracc) {
        this.useracc = useracc == null ? null : useracc.trim();
    }

    public Float getLastmoney() {
        return lastmoney;
    }

    public void setLastmoney(Float lastmoney) {
        this.lastmoney = lastmoney;
    }

    public String getRepaymentdate() {
        if(repaymentdate!=null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(repaymentdate);
        }else{
            return null;
        }

    }

    public void setRepaymentdate(Date repaymentdate) {
        this.repaymentdate = repaymentdate;
    }

    public Float getRepaymentmoney() {
        return repaymentmoney;
    }

    public void setRepaymentmoney(Float repaymentmoney) {
        this.repaymentmoney = repaymentmoney;
    }
}