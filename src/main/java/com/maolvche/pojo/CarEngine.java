package com.maolvche.pojo;

public class CarEngine {
    private String carengineno;

    private String enginetype;

    private String outputvolume;

    private String airinlet;

    private Integer cylindersno;

    private String maxhorsepower;

    private String maxpower;

    private String maxpowerspeed;

    private String fueltype;

    private Integer fuellabel;

    private String oildrive;

    public String getCarengineno() {
        return carengineno;
    }

    public void setCarengineno(String carengineno) {
        this.carengineno = carengineno == null ? null : carengineno.trim();
    }

    public String getEnginetype() {
        return enginetype;
    }

    public void setEnginetype(String enginetype) {
        this.enginetype = enginetype == null ? null : enginetype.trim();
    }

    public String getOutputvolume() {
        return outputvolume;
    }

    public void setOutputvolume(String outputvolume) {
        this.outputvolume = outputvolume == null ? null : outputvolume.trim();
    }

    public String getAirinlet() {
        return airinlet;
    }

    public void setAirinlet(String airinlet) {
        this.airinlet = airinlet == null ? null : airinlet.trim();
    }

    public Integer getCylindersno() {
        return cylindersno;
    }

    public void setCylindersno(Integer cylindersno) {
        this.cylindersno = cylindersno;
    }

    public String getMaxhorsepower() {
        return maxhorsepower;
    }

    public void setMaxhorsepower(String maxhorsepower) {
        this.maxhorsepower = maxhorsepower == null ? null : maxhorsepower.trim();
    }

    public String getMaxpower() {
        return maxpower;
    }

    public void setMaxpower(String maxpower) {
        this.maxpower = maxpower == null ? null : maxpower.trim();
    }

    public String getMaxpowerspeed() {
        return maxpowerspeed;
    }

    public void setMaxpowerspeed(String maxpowerspeed) {
        this.maxpowerspeed = maxpowerspeed == null ? null : maxpowerspeed.trim();
    }

    public String getFueltype() {
        return fueltype;
    }

    public void setFueltype(String fueltype) {
        this.fueltype = fueltype == null ? null : fueltype.trim();
    }

    public Integer getFuellabel() {
        return fuellabel;
    }

    public void setFuellabel(Integer fuellabel) {
        this.fuellabel = fuellabel;
    }

    public String getOildrive() {
        return oildrive;
    }

    public void setOildrive(String oildrive) {
        this.oildrive = oildrive == null ? null : oildrive.trim();
    }
}