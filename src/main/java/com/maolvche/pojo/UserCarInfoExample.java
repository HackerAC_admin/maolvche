package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserCarInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserCarInfoExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUsercarinfonoIsNull() {
            addCriterion("userCarInfoNo is null");
            return (Criteria) this;
        }

        public Criteria andUsercarinfonoIsNotNull() {
            addCriterion("userCarInfoNo is not null");
            return (Criteria) this;
        }

        public Criteria andUsercarinfonoEqualTo(Integer value) {
            addCriterion("userCarInfoNo =", value, "usercarinfono");
            return (Criteria) this;
        }

        public Criteria andUsercarinfonoNotEqualTo(Integer value) {
            addCriterion("userCarInfoNo <>", value, "usercarinfono");
            return (Criteria) this;
        }

        public Criteria andUsercarinfonoGreaterThan(Integer value) {
            addCriterion("userCarInfoNo >", value, "usercarinfono");
            return (Criteria) this;
        }

        public Criteria andUsercarinfonoGreaterThanOrEqualTo(Integer value) {
            addCriterion("userCarInfoNo >=", value, "usercarinfono");
            return (Criteria) this;
        }

        public Criteria andUsercarinfonoLessThan(Integer value) {
            addCriterion("userCarInfoNo <", value, "usercarinfono");
            return (Criteria) this;
        }

        public Criteria andUsercarinfonoLessThanOrEqualTo(Integer value) {
            addCriterion("userCarInfoNo <=", value, "usercarinfono");
            return (Criteria) this;
        }

        public Criteria andUsercarinfonoIn(List<Integer> values) {
            addCriterion("userCarInfoNo in", values, "usercarinfono");
            return (Criteria) this;
        }

        public Criteria andUsercarinfonoNotIn(List<Integer> values) {
            addCriterion("userCarInfoNo not in", values, "usercarinfono");
            return (Criteria) this;
        }

        public Criteria andUsercarinfonoBetween(Integer value1, Integer value2) {
            addCriterion("userCarInfoNo between", value1, value2, "usercarinfono");
            return (Criteria) this;
        }

        public Criteria andUsercarinfonoNotBetween(Integer value1, Integer value2) {
            addCriterion("userCarInfoNo not between", value1, value2, "usercarinfono");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoIsNull() {
            addCriterion("userNewCarNo is null");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoIsNotNull() {
            addCriterion("userNewCarNo is not null");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoEqualTo(String value) {
            addCriterion("userNewCarNo =", value, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoNotEqualTo(String value) {
            addCriterion("userNewCarNo <>", value, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoGreaterThan(String value) {
            addCriterion("userNewCarNo >", value, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoGreaterThanOrEqualTo(String value) {
            addCriterion("userNewCarNo >=", value, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoLessThan(String value) {
            addCriterion("userNewCarNo <", value, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoLessThanOrEqualTo(String value) {
            addCriterion("userNewCarNo <=", value, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoLike(String value) {
            addCriterion("userNewCarNo like", value, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoNotLike(String value) {
            addCriterion("userNewCarNo not like", value, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoIn(List<String> values) {
            addCriterion("userNewCarNo in", values, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoNotIn(List<String> values) {
            addCriterion("userNewCarNo not in", values, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoBetween(String value1, String value2) {
            addCriterion("userNewCarNo between", value1, value2, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsernewcarnoNotBetween(String value1, String value2) {
            addCriterion("userNewCarNo not between", value1, value2, "usernewcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoIsNull() {
            addCriterion("userSecondCarNo is null");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoIsNotNull() {
            addCriterion("userSecondCarNo is not null");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoEqualTo(String value) {
            addCriterion("userSecondCarNo =", value, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoNotEqualTo(String value) {
            addCriterion("userSecondCarNo <>", value, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoGreaterThan(String value) {
            addCriterion("userSecondCarNo >", value, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoGreaterThanOrEqualTo(String value) {
            addCriterion("userSecondCarNo >=", value, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoLessThan(String value) {
            addCriterion("userSecondCarNo <", value, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoLessThanOrEqualTo(String value) {
            addCriterion("userSecondCarNo <=", value, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoLike(String value) {
            addCriterion("userSecondCarNo like", value, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoNotLike(String value) {
            addCriterion("userSecondCarNo not like", value, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoIn(List<String> values) {
            addCriterion("userSecondCarNo in", values, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoNotIn(List<String> values) {
            addCriterion("userSecondCarNo not in", values, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoBetween(String value1, String value2) {
            addCriterion("userSecondCarNo between", value1, value2, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUsersecondcarnoNotBetween(String value1, String value2) {
            addCriterion("userSecondCarNo not between", value1, value2, "usersecondcarno");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNull() {
            addCriterion("userAcc is null");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNotNull() {
            addCriterion("userAcc is not null");
            return (Criteria) this;
        }

        public Criteria andUseraccEqualTo(String value) {
            addCriterion("userAcc =", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotEqualTo(String value) {
            addCriterion("userAcc <>", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThan(String value) {
            addCriterion("userAcc >", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThanOrEqualTo(String value) {
            addCriterion("userAcc >=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThan(String value) {
            addCriterion("userAcc <", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThanOrEqualTo(String value) {
            addCriterion("userAcc <=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLike(String value) {
            addCriterion("userAcc like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotLike(String value) {
            addCriterion("userAcc not like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccIn(List<String> values) {
            addCriterion("userAcc in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotIn(List<String> values) {
            addCriterion("userAcc not in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccBetween(String value1, String value2) {
            addCriterion("userAcc between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotBetween(String value1, String value2) {
            addCriterion("userAcc not between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andUptdateIsNull() {
            addCriterion("uptDate is null");
            return (Criteria) this;
        }

        public Criteria andUptdateIsNotNull() {
            addCriterion("uptDate is not null");
            return (Criteria) this;
        }

        public Criteria andUptdateEqualTo(Date value) {
            addCriterion("uptDate =", value, "uptdate");
            return (Criteria) this;
        }

        public Criteria andUptdateNotEqualTo(Date value) {
            addCriterion("uptDate <>", value, "uptdate");
            return (Criteria) this;
        }

        public Criteria andUptdateGreaterThan(Date value) {
            addCriterion("uptDate >", value, "uptdate");
            return (Criteria) this;
        }

        public Criteria andUptdateGreaterThanOrEqualTo(Date value) {
            addCriterion("uptDate >=", value, "uptdate");
            return (Criteria) this;
        }

        public Criteria andUptdateLessThan(Date value) {
            addCriterion("uptDate <", value, "uptdate");
            return (Criteria) this;
        }

        public Criteria andUptdateLessThanOrEqualTo(Date value) {
            addCriterion("uptDate <=", value, "uptdate");
            return (Criteria) this;
        }

        public Criteria andUptdateIn(List<Date> values) {
            addCriterion("uptDate in", values, "uptdate");
            return (Criteria) this;
        }

        public Criteria andUptdateNotIn(List<Date> values) {
            addCriterion("uptDate not in", values, "uptdate");
            return (Criteria) this;
        }

        public Criteria andUptdateBetween(Date value1, Date value2) {
            addCriterion("uptDate between", value1, value2, "uptdate");
            return (Criteria) this;
        }

        public Criteria andUptdateNotBetween(Date value1, Date value2) {
            addCriterion("uptDate not between", value1, value2, "uptdate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}