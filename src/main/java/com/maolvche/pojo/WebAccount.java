package com.maolvche.pojo;

import java.util.Date;

public class WebAccount {
    /**
     * 下面这个玩意仅起到一个唯一标识作用
     * 不得不说，这个主键当得真尴尬 v_v
     */
    private String useracc;
    /**
     * 这是用户信息表的用户编号
     */
    private String userno;
    /**
     * 这才是用来进行登录使用的用户名?
     * 当时设计数据库的人，头有包！！！
     */
    private String username;

    private String userpwd;

    private String nickname;

    private Date registerdate;

    private String acctype;

    private String headimage;

    public String getUseracc() {
        return useracc;
    }

    public void setUseracc(String useracc) {
        this.useracc = useracc == null ? null : useracc.trim();
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno == null ? null : userno.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getUserpwd() {
        return userpwd;
    }

    public void setUserpwd(String userpwd) {
        this.userpwd = userpwd == null ? null : userpwd.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public Date getRegisterdate() {
        return registerdate;
    }

    public void setRegisterdate(Date registerdate) {
        this.registerdate = registerdate;
    }

    public String getAcctype() {
        return acctype;
    }

    public void setAcctype(String acctype) {
        this.acctype = acctype == null ? null : acctype.trim();
    }

    public String getHeadimage() {
        return headimage;
    }

    public void setHeadimage(String headimage) {
        this.headimage = headimage == null ? null : headimage.trim();
    }
}