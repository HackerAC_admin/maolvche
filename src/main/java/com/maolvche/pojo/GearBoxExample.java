package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.List;

public class GearBoxExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GearBoxExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andGearboxnoIsNull() {
            addCriterion("gearboxNo is null");
            return (Criteria) this;
        }

        public Criteria andGearboxnoIsNotNull() {
            addCriterion("gearboxNo is not null");
            return (Criteria) this;
        }

        public Criteria andGearboxnoEqualTo(String value) {
            addCriterion("gearboxNo =", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoNotEqualTo(String value) {
            addCriterion("gearboxNo <>", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoGreaterThan(String value) {
            addCriterion("gearboxNo >", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoGreaterThanOrEqualTo(String value) {
            addCriterion("gearboxNo >=", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoLessThan(String value) {
            addCriterion("gearboxNo <", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoLessThanOrEqualTo(String value) {
            addCriterion("gearboxNo <=", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoLike(String value) {
            addCriterion("gearboxNo like", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoNotLike(String value) {
            addCriterion("gearboxNo not like", value, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoIn(List<String> values) {
            addCriterion("gearboxNo in", values, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoNotIn(List<String> values) {
            addCriterion("gearboxNo not in", values, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoBetween(String value1, String value2) {
            addCriterion("gearboxNo between", value1, value2, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearboxnoNotBetween(String value1, String value2) {
            addCriterion("gearboxNo not between", value1, value2, "gearboxno");
            return (Criteria) this;
        }

        public Criteria andGearsnumIsNull() {
            addCriterion("gearsNum is null");
            return (Criteria) this;
        }

        public Criteria andGearsnumIsNotNull() {
            addCriterion("gearsNum is not null");
            return (Criteria) this;
        }

        public Criteria andGearsnumEqualTo(Integer value) {
            addCriterion("gearsNum =", value, "gearsnum");
            return (Criteria) this;
        }

        public Criteria andGearsnumNotEqualTo(Integer value) {
            addCriterion("gearsNum <>", value, "gearsnum");
            return (Criteria) this;
        }

        public Criteria andGearsnumGreaterThan(Integer value) {
            addCriterion("gearsNum >", value, "gearsnum");
            return (Criteria) this;
        }

        public Criteria andGearsnumGreaterThanOrEqualTo(Integer value) {
            addCriterion("gearsNum >=", value, "gearsnum");
            return (Criteria) this;
        }

        public Criteria andGearsnumLessThan(Integer value) {
            addCriterion("gearsNum <", value, "gearsnum");
            return (Criteria) this;
        }

        public Criteria andGearsnumLessThanOrEqualTo(Integer value) {
            addCriterion("gearsNum <=", value, "gearsnum");
            return (Criteria) this;
        }

        public Criteria andGearsnumIn(List<Integer> values) {
            addCriterion("gearsNum in", values, "gearsnum");
            return (Criteria) this;
        }

        public Criteria andGearsnumNotIn(List<Integer> values) {
            addCriterion("gearsNum not in", values, "gearsnum");
            return (Criteria) this;
        }

        public Criteria andGearsnumBetween(Integer value1, Integer value2) {
            addCriterion("gearsNum between", value1, value2, "gearsnum");
            return (Criteria) this;
        }

        public Criteria andGearsnumNotBetween(Integer value1, Integer value2) {
            addCriterion("gearsNum not between", value1, value2, "gearsnum");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeIsNull() {
            addCriterion("transmissionType is null");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeIsNotNull() {
            addCriterion("transmissionType is not null");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeEqualTo(String value) {
            addCriterion("transmissionType =", value, "transmissiontype");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeNotEqualTo(String value) {
            addCriterion("transmissionType <>", value, "transmissiontype");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeGreaterThan(String value) {
            addCriterion("transmissionType >", value, "transmissiontype");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeGreaterThanOrEqualTo(String value) {
            addCriterion("transmissionType >=", value, "transmissiontype");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeLessThan(String value) {
            addCriterion("transmissionType <", value, "transmissiontype");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeLessThanOrEqualTo(String value) {
            addCriterion("transmissionType <=", value, "transmissiontype");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeLike(String value) {
            addCriterion("transmissionType like", value, "transmissiontype");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeNotLike(String value) {
            addCriterion("transmissionType not like", value, "transmissiontype");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeIn(List<String> values) {
            addCriterion("transmissionType in", values, "transmissiontype");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeNotIn(List<String> values) {
            addCriterion("transmissionType not in", values, "transmissiontype");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeBetween(String value1, String value2) {
            addCriterion("transmissionType between", value1, value2, "transmissiontype");
            return (Criteria) this;
        }

        public Criteria andTransmissiontypeNotBetween(String value1, String value2) {
            addCriterion("transmissionType not between", value1, value2, "transmissiontype");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}