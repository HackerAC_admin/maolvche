package com.maolvche.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UserCarInfo {
    private Integer usercarinfono;

    private String usernewcarno;

    private String usersecondcarno;

    private String useracc;

    private Date uptdate;

    public Integer getUsercarinfono() {
        return usercarinfono;
    }

    public void setUsercarinfono(Integer usercarinfono) {
        this.usercarinfono = usercarinfono;
    }

    public String getUsernewcarno() {
        return usernewcarno;
    }

    public void setUsernewcarno(String usernewcarno) {
        this.usernewcarno = usernewcarno == null ? null : usernewcarno.trim();
    }

    public String getUsersecondcarno() {
        return usersecondcarno;
    }

    public void setUsersecondcarno(String usersecondcarno) {
        this.usersecondcarno = usersecondcarno == null ? null : usersecondcarno.trim();
    }

    public String getUseracc() {
        return useracc;
    }

    public void setUseracc(String useracc) {
        this.useracc = useracc == null ? null : useracc.trim();
    }

    public String getUptdate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(uptdate);
    }

    public void setUptdate(Date uptdate) {
        this.uptdate = uptdate;
    }
}