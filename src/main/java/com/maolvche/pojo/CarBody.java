package com.maolvche.pojo;

public class CarBody {
    private String carbodyno;

    private String carlength;

    private String carheight;

    private String carwidth;

    private String wheelbase;

    private String carweight;

    private String minground;

    private String carstructure;

    private String fueltankvolume;

    private String luggagecarriervolume;

    public String getCarbodyno() {
        return carbodyno;
    }

    public void setCarbodyno(String carbodyno) {
        this.carbodyno = carbodyno == null ? null : carbodyno.trim();
    }

    public String getCarlength() {
        return carlength;
    }

    public void setCarlength(String carlength) {
        this.carlength = carlength == null ? null : carlength.trim();
    }

    public String getCarheight() {
        return carheight;
    }

    public void setCarheight(String carheight) {
        this.carheight = carheight == null ? null : carheight.trim();
    }

    public String getCarwidth() {
        return carwidth;
    }

    public void setCarwidth(String carwidth) {
        this.carwidth = carwidth == null ? null : carwidth.trim();
    }

    public String getWheelbase() {
        return wheelbase;
    }

    public void setWheelbase(String wheelbase) {
        this.wheelbase = wheelbase == null ? null : wheelbase.trim();
    }

    public String getCarweight() {
        return carweight;
    }

    public void setCarweight(String carweight) {
        this.carweight = carweight == null ? null : carweight.trim();
    }

    public String getMinground() {
        return minground;
    }

    public void setMinground(String minground) {
        this.minground = minground == null ? null : minground.trim();
    }

    public String getCarstructure() {
        return carstructure;
    }

    public void setCarstructure(String carstructure) {
        this.carstructure = carstructure == null ? null : carstructure.trim();
    }

    public String getFueltankvolume() {
        return fueltankvolume;
    }

    public void setFueltankvolume(String fueltankvolume) {
        this.fueltankvolume = fueltankvolume == null ? null : fueltankvolume.trim();
    }

    public String getLuggagecarriervolume() {
        return luggagecarriervolume;
    }

    public void setLuggagecarriervolume(String luggagecarriervolume) {
        this.luggagecarriervolume = luggagecarriervolume == null ? null : luggagecarriervolume.trim();
    }
}