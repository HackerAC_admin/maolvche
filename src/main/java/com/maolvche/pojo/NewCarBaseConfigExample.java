package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.List;

public class NewCarBaseConfigExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public NewCarBaseConfigExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBasicconnoIsNull() {
            addCriterion("basicconNo is null");
            return (Criteria) this;
        }

        public Criteria andBasicconnoIsNotNull() {
            addCriterion("basicconNo is not null");
            return (Criteria) this;
        }

        public Criteria andBasicconnoEqualTo(String value) {
            addCriterion("basicconNo =", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoNotEqualTo(String value) {
            addCriterion("basicconNo <>", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoGreaterThan(String value) {
            addCriterion("basicconNo >", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoGreaterThanOrEqualTo(String value) {
            addCriterion("basicconNo >=", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoLessThan(String value) {
            addCriterion("basicconNo <", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoLessThanOrEqualTo(String value) {
            addCriterion("basicconNo <=", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoLike(String value) {
            addCriterion("basicconNo like", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoNotLike(String value) {
            addCriterion("basicconNo not like", value, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoIn(List<String> values) {
            addCriterion("basicconNo in", values, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoNotIn(List<String> values) {
            addCriterion("basicconNo not in", values, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoBetween(String value1, String value2) {
            addCriterion("basicconNo between", value1, value2, "basicconno");
            return (Criteria) this;
        }

        public Criteria andBasicconnoNotBetween(String value1, String value2) {
            addCriterion("basicconNo not between", value1, value2, "basicconno");
            return (Criteria) this;
        }

        public Criteria andFirmIsNull() {
            addCriterion("Firm is null");
            return (Criteria) this;
        }

        public Criteria andFirmIsNotNull() {
            addCriterion("Firm is not null");
            return (Criteria) this;
        }

        public Criteria andFirmEqualTo(String value) {
            addCriterion("Firm =", value, "firm");
            return (Criteria) this;
        }

        public Criteria andFirmNotEqualTo(String value) {
            addCriterion("Firm <>", value, "firm");
            return (Criteria) this;
        }

        public Criteria andFirmGreaterThan(String value) {
            addCriterion("Firm >", value, "firm");
            return (Criteria) this;
        }

        public Criteria andFirmGreaterThanOrEqualTo(String value) {
            addCriterion("Firm >=", value, "firm");
            return (Criteria) this;
        }

        public Criteria andFirmLessThan(String value) {
            addCriterion("Firm <", value, "firm");
            return (Criteria) this;
        }

        public Criteria andFirmLessThanOrEqualTo(String value) {
            addCriterion("Firm <=", value, "firm");
            return (Criteria) this;
        }

        public Criteria andFirmLike(String value) {
            addCriterion("Firm like", value, "firm");
            return (Criteria) this;
        }

        public Criteria andFirmNotLike(String value) {
            addCriterion("Firm not like", value, "firm");
            return (Criteria) this;
        }

        public Criteria andFirmIn(List<String> values) {
            addCriterion("Firm in", values, "firm");
            return (Criteria) this;
        }

        public Criteria andFirmNotIn(List<String> values) {
            addCriterion("Firm not in", values, "firm");
            return (Criteria) this;
        }

        public Criteria andFirmBetween(String value1, String value2) {
            addCriterion("Firm between", value1, value2, "firm");
            return (Criteria) this;
        }

        public Criteria andFirmNotBetween(String value1, String value2) {
            addCriterion("Firm not between", value1, value2, "firm");
            return (Criteria) this;
        }

        public Criteria andCarmodelIsNull() {
            addCriterion("carModel is null");
            return (Criteria) this;
        }

        public Criteria andCarmodelIsNotNull() {
            addCriterion("carModel is not null");
            return (Criteria) this;
        }

        public Criteria andCarmodelEqualTo(String value) {
            addCriterion("carModel =", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelNotEqualTo(String value) {
            addCriterion("carModel <>", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelGreaterThan(String value) {
            addCriterion("carModel >", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelGreaterThanOrEqualTo(String value) {
            addCriterion("carModel >=", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelLessThan(String value) {
            addCriterion("carModel <", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelLessThanOrEqualTo(String value) {
            addCriterion("carModel <=", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelLike(String value) {
            addCriterion("carModel like", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelNotLike(String value) {
            addCriterion("carModel not like", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelIn(List<String> values) {
            addCriterion("carModel in", values, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelNotIn(List<String> values) {
            addCriterion("carModel not in", values, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelBetween(String value1, String value2) {
            addCriterion("carModel between", value1, value2, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelNotBetween(String value1, String value2) {
            addCriterion("carModel not between", value1, value2, "carmodel");
            return (Criteria) this;
        }

        public Criteria andOutcolorIsNull() {
            addCriterion("outColor is null");
            return (Criteria) this;
        }

        public Criteria andOutcolorIsNotNull() {
            addCriterion("outColor is not null");
            return (Criteria) this;
        }

        public Criteria andOutcolorEqualTo(String value) {
            addCriterion("outColor =", value, "outcolor");
            return (Criteria) this;
        }

        public Criteria andOutcolorNotEqualTo(String value) {
            addCriterion("outColor <>", value, "outcolor");
            return (Criteria) this;
        }

        public Criteria andOutcolorGreaterThan(String value) {
            addCriterion("outColor >", value, "outcolor");
            return (Criteria) this;
        }

        public Criteria andOutcolorGreaterThanOrEqualTo(String value) {
            addCriterion("outColor >=", value, "outcolor");
            return (Criteria) this;
        }

        public Criteria andOutcolorLessThan(String value) {
            addCriterion("outColor <", value, "outcolor");
            return (Criteria) this;
        }

        public Criteria andOutcolorLessThanOrEqualTo(String value) {
            addCriterion("outColor <=", value, "outcolor");
            return (Criteria) this;
        }

        public Criteria andOutcolorLike(String value) {
            addCriterion("outColor like", value, "outcolor");
            return (Criteria) this;
        }

        public Criteria andOutcolorNotLike(String value) {
            addCriterion("outColor not like", value, "outcolor");
            return (Criteria) this;
        }

        public Criteria andOutcolorIn(List<String> values) {
            addCriterion("outColor in", values, "outcolor");
            return (Criteria) this;
        }

        public Criteria andOutcolorNotIn(List<String> values) {
            addCriterion("outColor not in", values, "outcolor");
            return (Criteria) this;
        }

        public Criteria andOutcolorBetween(String value1, String value2) {
            addCriterion("outColor between", value1, value2, "outcolor");
            return (Criteria) this;
        }

        public Criteria andOutcolorNotBetween(String value1, String value2) {
            addCriterion("outColor not between", value1, value2, "outcolor");
            return (Criteria) this;
        }

        public Criteria andIncolorIsNull() {
            addCriterion("inColor is null");
            return (Criteria) this;
        }

        public Criteria andIncolorIsNotNull() {
            addCriterion("inColor is not null");
            return (Criteria) this;
        }

        public Criteria andIncolorEqualTo(String value) {
            addCriterion("inColor =", value, "incolor");
            return (Criteria) this;
        }

        public Criteria andIncolorNotEqualTo(String value) {
            addCriterion("inColor <>", value, "incolor");
            return (Criteria) this;
        }

        public Criteria andIncolorGreaterThan(String value) {
            addCriterion("inColor >", value, "incolor");
            return (Criteria) this;
        }

        public Criteria andIncolorGreaterThanOrEqualTo(String value) {
            addCriterion("inColor >=", value, "incolor");
            return (Criteria) this;
        }

        public Criteria andIncolorLessThan(String value) {
            addCriterion("inColor <", value, "incolor");
            return (Criteria) this;
        }

        public Criteria andIncolorLessThanOrEqualTo(String value) {
            addCriterion("inColor <=", value, "incolor");
            return (Criteria) this;
        }

        public Criteria andIncolorLike(String value) {
            addCriterion("inColor like", value, "incolor");
            return (Criteria) this;
        }

        public Criteria andIncolorNotLike(String value) {
            addCriterion("inColor not like", value, "incolor");
            return (Criteria) this;
        }

        public Criteria andIncolorIn(List<String> values) {
            addCriterion("inColor in", values, "incolor");
            return (Criteria) this;
        }

        public Criteria andIncolorNotIn(List<String> values) {
            addCriterion("inColor not in", values, "incolor");
            return (Criteria) this;
        }

        public Criteria andIncolorBetween(String value1, String value2) {
            addCriterion("inColor between", value1, value2, "incolor");
            return (Criteria) this;
        }

        public Criteria andIncolorNotBetween(String value1, String value2) {
            addCriterion("inColor not between", value1, value2, "incolor");
            return (Criteria) this;
        }

        public Criteria andMaxspeedIsNull() {
            addCriterion("maxSpeed is null");
            return (Criteria) this;
        }

        public Criteria andMaxspeedIsNotNull() {
            addCriterion("maxSpeed is not null");
            return (Criteria) this;
        }

        public Criteria andMaxspeedEqualTo(String value) {
            addCriterion("maxSpeed =", value, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andMaxspeedNotEqualTo(String value) {
            addCriterion("maxSpeed <>", value, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andMaxspeedGreaterThan(String value) {
            addCriterion("maxSpeed >", value, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andMaxspeedGreaterThanOrEqualTo(String value) {
            addCriterion("maxSpeed >=", value, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andMaxspeedLessThan(String value) {
            addCriterion("maxSpeed <", value, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andMaxspeedLessThanOrEqualTo(String value) {
            addCriterion("maxSpeed <=", value, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andMaxspeedLike(String value) {
            addCriterion("maxSpeed like", value, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andMaxspeedNotLike(String value) {
            addCriterion("maxSpeed not like", value, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andMaxspeedIn(List<String> values) {
            addCriterion("maxSpeed in", values, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andMaxspeedNotIn(List<String> values) {
            addCriterion("maxSpeed not in", values, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andMaxspeedBetween(String value1, String value2) {
            addCriterion("maxSpeed between", value1, value2, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andMaxspeedNotBetween(String value1, String value2) {
            addCriterion("maxSpeed not between", value1, value2, "maxspeed");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionIsNull() {
            addCriterion("oilConsumption is null");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionIsNotNull() {
            addCriterion("oilConsumption is not null");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionEqualTo(String value) {
            addCriterion("oilConsumption =", value, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionNotEqualTo(String value) {
            addCriterion("oilConsumption <>", value, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionGreaterThan(String value) {
            addCriterion("oilConsumption >", value, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionGreaterThanOrEqualTo(String value) {
            addCriterion("oilConsumption >=", value, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionLessThan(String value) {
            addCriterion("oilConsumption <", value, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionLessThanOrEqualTo(String value) {
            addCriterion("oilConsumption <=", value, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionLike(String value) {
            addCriterion("oilConsumption like", value, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionNotLike(String value) {
            addCriterion("oilConsumption not like", value, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionIn(List<String> values) {
            addCriterion("oilConsumption in", values, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionNotIn(List<String> values) {
            addCriterion("oilConsumption not in", values, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionBetween(String value1, String value2) {
            addCriterion("oilConsumption between", value1, value2, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andOilconsumptionNotBetween(String value1, String value2) {
            addCriterion("oilConsumption not between", value1, value2, "oilconsumption");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeIsNull() {
            addCriterion("parkingbrakeType is null");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeIsNotNull() {
            addCriterion("parkingbrakeType is not null");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeEqualTo(String value) {
            addCriterion("parkingbrakeType =", value, "parkingbraketype");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeNotEqualTo(String value) {
            addCriterion("parkingbrakeType <>", value, "parkingbraketype");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeGreaterThan(String value) {
            addCriterion("parkingbrakeType >", value, "parkingbraketype");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeGreaterThanOrEqualTo(String value) {
            addCriterion("parkingbrakeType >=", value, "parkingbraketype");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeLessThan(String value) {
            addCriterion("parkingbrakeType <", value, "parkingbraketype");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeLessThanOrEqualTo(String value) {
            addCriterion("parkingbrakeType <=", value, "parkingbraketype");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeLike(String value) {
            addCriterion("parkingbrakeType like", value, "parkingbraketype");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeNotLike(String value) {
            addCriterion("parkingbrakeType not like", value, "parkingbraketype");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeIn(List<String> values) {
            addCriterion("parkingbrakeType in", values, "parkingbraketype");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeNotIn(List<String> values) {
            addCriterion("parkingbrakeType not in", values, "parkingbraketype");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeBetween(String value1, String value2) {
            addCriterion("parkingbrakeType between", value1, value2, "parkingbraketype");
            return (Criteria) this;
        }

        public Criteria andParkingbraketypeNotBetween(String value1, String value2) {
            addCriterion("parkingbrakeType not between", value1, value2, "parkingbraketype");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}