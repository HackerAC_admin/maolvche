package com.maolvche.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Exchange {
    private Integer exchangedno;

    private String carno;

    private String useracc;

    private String exchangereason;

    private Date applydate;

    private Date exchangeddate;

    private String auditresult;

    private String state;

    public Integer getExchangedno() {
        return exchangedno;
    }

    public void setExchangedno(Integer exchangedno) {
        this.exchangedno = exchangedno;
    }

    public String getCarno() {
        return carno;
    }

    public void setCarno(String carno) {
        this.carno = carno == null ? null : carno.trim();
    }

    public String getUseracc() {
        return useracc;
    }

    public void setUseracc(String useracc) {
        this.useracc = useracc == null ? null : useracc.trim();
    }

    public String getExchangereason() {
        return exchangereason;
    }

    public void setExchangereason(String exchangereason) {
        this.exchangereason = exchangereason == null ? null : exchangereason.trim();
    }

    public String getApplydate() {
        if(applydate!=null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(applydate);
        }else{
            return null;
        }
    }

    public void setApplydate(Date applydate) {
        this.applydate = applydate;
    }

    public String getExchangeddate() {
        if(exchangeddate!=null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(exchangeddate);
        }else{
            return null;
        }
    }

    public void setExchangeddate(Date exchangeddate) {
        this.exchangeddate = exchangeddate;
    }

    public String getAuditresult() {
        return auditresult;
    }

    public void setAuditresult(String auditresult) {
        this.auditresult = auditresult == null ? null : auditresult.trim();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }
}