package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class EmpInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public EmpInfoExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andEmployeenoIsNull() {
            addCriterion("employeeNo is null");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIsNotNull() {
            addCriterion("employeeNo is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeenoEqualTo(String value) {
            addCriterion("employeeNo =", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotEqualTo(String value) {
            addCriterion("employeeNo <>", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoGreaterThan(String value) {
            addCriterion("employeeNo >", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoGreaterThanOrEqualTo(String value) {
            addCriterion("employeeNo >=", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLessThan(String value) {
            addCriterion("employeeNo <", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLessThanOrEqualTo(String value) {
            addCriterion("employeeNo <=", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLike(String value) {
            addCriterion("employeeNo like", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotLike(String value) {
            addCriterion("employeeNo not like", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIn(List<String> values) {
            addCriterion("employeeNo in", values, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotIn(List<String> values) {
            addCriterion("employeeNo not in", values, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoBetween(String value1, String value2) {
            addCriterion("employeeNo between", value1, value2, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotBetween(String value1, String value2) {
            addCriterion("employeeNo not between", value1, value2, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenameIsNull() {
            addCriterion("employeeName is null");
            return (Criteria) this;
        }

        public Criteria andEmployeenameIsNotNull() {
            addCriterion("employeeName is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeenameEqualTo(String value) {
            addCriterion("employeeName =", value, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeenameNotEqualTo(String value) {
            addCriterion("employeeName <>", value, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeenameGreaterThan(String value) {
            addCriterion("employeeName >", value, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeenameGreaterThanOrEqualTo(String value) {
            addCriterion("employeeName >=", value, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeenameLessThan(String value) {
            addCriterion("employeeName <", value, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeenameLessThanOrEqualTo(String value) {
            addCriterion("employeeName <=", value, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeenameLike(String value) {
            addCriterion("employeeName like", value, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeenameNotLike(String value) {
            addCriterion("employeeName not like", value, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeenameIn(List<String> values) {
            addCriterion("employeeName in", values, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeenameNotIn(List<String> values) {
            addCriterion("employeeName not in", values, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeenameBetween(String value1, String value2) {
            addCriterion("employeeName between", value1, value2, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeenameNotBetween(String value1, String value2) {
            addCriterion("employeeName not between", value1, value2, "employeename");
            return (Criteria) this;
        }

        public Criteria andEmployeesexIsNull() {
            addCriterion("employeeSex is null");
            return (Criteria) this;
        }

        public Criteria andEmployeesexIsNotNull() {
            addCriterion("employeeSex is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeesexEqualTo(String value) {
            addCriterion("employeeSex =", value, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeesexNotEqualTo(String value) {
            addCriterion("employeeSex <>", value, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeesexGreaterThan(String value) {
            addCriterion("employeeSex >", value, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeesexGreaterThanOrEqualTo(String value) {
            addCriterion("employeeSex >=", value, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeesexLessThan(String value) {
            addCriterion("employeeSex <", value, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeesexLessThanOrEqualTo(String value) {
            addCriterion("employeeSex <=", value, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeesexLike(String value) {
            addCriterion("employeeSex like", value, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeesexNotLike(String value) {
            addCriterion("employeeSex not like", value, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeesexIn(List<String> values) {
            addCriterion("employeeSex in", values, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeesexNotIn(List<String> values) {
            addCriterion("employeeSex not in", values, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeesexBetween(String value1, String value2) {
            addCriterion("employeeSex between", value1, value2, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeesexNotBetween(String value1, String value2) {
            addCriterion("employeeSex not between", value1, value2, "employeesex");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoIsNull() {
            addCriterion("employeeidNo is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoIsNotNull() {
            addCriterion("employeeidNo is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoEqualTo(String value) {
            addCriterion("employeeidNo =", value, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoNotEqualTo(String value) {
            addCriterion("employeeidNo <>", value, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoGreaterThan(String value) {
            addCriterion("employeeidNo >", value, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoGreaterThanOrEqualTo(String value) {
            addCriterion("employeeidNo >=", value, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoLessThan(String value) {
            addCriterion("employeeidNo <", value, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoLessThanOrEqualTo(String value) {
            addCriterion("employeeidNo <=", value, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoLike(String value) {
            addCriterion("employeeidNo like", value, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoNotLike(String value) {
            addCriterion("employeeidNo not like", value, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoIn(List<String> values) {
            addCriterion("employeeidNo in", values, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoNotIn(List<String> values) {
            addCriterion("employeeidNo not in", values, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoBetween(String value1, String value2) {
            addCriterion("employeeidNo between", value1, value2, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeidnoNotBetween(String value1, String value2) {
            addCriterion("employeeidNo not between", value1, value2, "employeeidno");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailIsNull() {
            addCriterion("employeeEmail is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailIsNotNull() {
            addCriterion("employeeEmail is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailEqualTo(String value) {
            addCriterion("employeeEmail =", value, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailNotEqualTo(String value) {
            addCriterion("employeeEmail <>", value, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailGreaterThan(String value) {
            addCriterion("employeeEmail >", value, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailGreaterThanOrEqualTo(String value) {
            addCriterion("employeeEmail >=", value, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailLessThan(String value) {
            addCriterion("employeeEmail <", value, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailLessThanOrEqualTo(String value) {
            addCriterion("employeeEmail <=", value, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailLike(String value) {
            addCriterion("employeeEmail like", value, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailNotLike(String value) {
            addCriterion("employeeEmail not like", value, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailIn(List<String> values) {
            addCriterion("employeeEmail in", values, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailNotIn(List<String> values) {
            addCriterion("employeeEmail not in", values, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailBetween(String value1, String value2) {
            addCriterion("employeeEmail between", value1, value2, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeeemailNotBetween(String value1, String value2) {
            addCriterion("employeeEmail not between", value1, value2, "employeeemail");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneIsNull() {
            addCriterion("employeePhone is null");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneIsNotNull() {
            addCriterion("employeePhone is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneEqualTo(String value) {
            addCriterion("employeePhone =", value, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneNotEqualTo(String value) {
            addCriterion("employeePhone <>", value, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneGreaterThan(String value) {
            addCriterion("employeePhone >", value, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneGreaterThanOrEqualTo(String value) {
            addCriterion("employeePhone >=", value, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneLessThan(String value) {
            addCriterion("employeePhone <", value, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneLessThanOrEqualTo(String value) {
            addCriterion("employeePhone <=", value, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneLike(String value) {
            addCriterion("employeePhone like", value, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneNotLike(String value) {
            addCriterion("employeePhone not like", value, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneIn(List<String> values) {
            addCriterion("employeePhone in", values, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneNotIn(List<String> values) {
            addCriterion("employeePhone not in", values, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneBetween(String value1, String value2) {
            addCriterion("employeePhone between", value1, value2, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeephoneNotBetween(String value1, String value2) {
            addCriterion("employeePhone not between", value1, value2, "employeephone");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressIsNull() {
            addCriterion("employeeAddress is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressIsNotNull() {
            addCriterion("employeeAddress is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressEqualTo(String value) {
            addCriterion("employeeAddress =", value, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressNotEqualTo(String value) {
            addCriterion("employeeAddress <>", value, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressGreaterThan(String value) {
            addCriterion("employeeAddress >", value, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressGreaterThanOrEqualTo(String value) {
            addCriterion("employeeAddress >=", value, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressLessThan(String value) {
            addCriterion("employeeAddress <", value, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressLessThanOrEqualTo(String value) {
            addCriterion("employeeAddress <=", value, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressLike(String value) {
            addCriterion("employeeAddress like", value, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressNotLike(String value) {
            addCriterion("employeeAddress not like", value, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressIn(List<String> values) {
            addCriterion("employeeAddress in", values, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressNotIn(List<String> values) {
            addCriterion("employeeAddress not in", values, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressBetween(String value1, String value2) {
            addCriterion("employeeAddress between", value1, value2, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andEmployeeaddressNotBetween(String value1, String value2) {
            addCriterion("employeeAddress not between", value1, value2, "employeeaddress");
            return (Criteria) this;
        }

        public Criteria andJobIsNull() {
            addCriterion("Job is null");
            return (Criteria) this;
        }

        public Criteria andJobIsNotNull() {
            addCriterion("Job is not null");
            return (Criteria) this;
        }

        public Criteria andJobEqualTo(String value) {
            addCriterion("Job =", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobNotEqualTo(String value) {
            addCriterion("Job <>", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobGreaterThan(String value) {
            addCriterion("Job >", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobGreaterThanOrEqualTo(String value) {
            addCriterion("Job >=", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobLessThan(String value) {
            addCriterion("Job <", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobLessThanOrEqualTo(String value) {
            addCriterion("Job <=", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobLike(String value) {
            addCriterion("Job like", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobNotLike(String value) {
            addCriterion("Job not like", value, "job");
            return (Criteria) this;
        }

        public Criteria andJobIn(List<String> values) {
            addCriterion("Job in", values, "job");
            return (Criteria) this;
        }

        public Criteria andJobNotIn(List<String> values) {
            addCriterion("Job not in", values, "job");
            return (Criteria) this;
        }

        public Criteria andJobBetween(String value1, String value2) {
            addCriterion("Job between", value1, value2, "job");
            return (Criteria) this;
        }

        public Criteria andJobNotBetween(String value1, String value2) {
            addCriterion("Job not between", value1, value2, "job");
            return (Criteria) this;
        }

        public Criteria andIndateIsNull() {
            addCriterion("inDate is null");
            return (Criteria) this;
        }

        public Criteria andIndateIsNotNull() {
            addCriterion("inDate is not null");
            return (Criteria) this;
        }

        public Criteria andIndateEqualTo(Date value) {
            addCriterionForJDBCDate("inDate =", value, "indate");
            return (Criteria) this;
        }

        public Criteria andIndateNotEqualTo(Date value) {
            addCriterionForJDBCDate("inDate <>", value, "indate");
            return (Criteria) this;
        }

        public Criteria andIndateGreaterThan(Date value) {
            addCriterionForJDBCDate("inDate >", value, "indate");
            return (Criteria) this;
        }

        public Criteria andIndateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("inDate >=", value, "indate");
            return (Criteria) this;
        }

        public Criteria andIndateLessThan(Date value) {
            addCriterionForJDBCDate("inDate <", value, "indate");
            return (Criteria) this;
        }

        public Criteria andIndateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("inDate <=", value, "indate");
            return (Criteria) this;
        }

        public Criteria andIndateIn(List<Date> values) {
            addCriterionForJDBCDate("inDate in", values, "indate");
            return (Criteria) this;
        }

        public Criteria andIndateNotIn(List<Date> values) {
            addCriterionForJDBCDate("inDate not in", values, "indate");
            return (Criteria) this;
        }

        public Criteria andIndateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("inDate between", value1, value2, "indate");
            return (Criteria) this;
        }

        public Criteria andIndateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("inDate not between", value1, value2, "indate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}