package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InstRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public InstRecordExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andInstlmntnoIsNull() {
            addCriterion("instlmntNo is null");
            return (Criteria) this;
        }

        public Criteria andInstlmntnoIsNotNull() {
            addCriterion("instlmntNo is not null");
            return (Criteria) this;
        }

        public Criteria andInstlmntnoEqualTo(Integer value) {
            addCriterion("instlmntNo =", value, "instlmntno");
            return (Criteria) this;
        }

        public Criteria andInstlmntnoNotEqualTo(Integer value) {
            addCriterion("instlmntNo <>", value, "instlmntno");
            return (Criteria) this;
        }

        public Criteria andInstlmntnoGreaterThan(Integer value) {
            addCriterion("instlmntNo >", value, "instlmntno");
            return (Criteria) this;
        }

        public Criteria andInstlmntnoGreaterThanOrEqualTo(Integer value) {
            addCriterion("instlmntNo >=", value, "instlmntno");
            return (Criteria) this;
        }

        public Criteria andInstlmntnoLessThan(Integer value) {
            addCriterion("instlmntNo <", value, "instlmntno");
            return (Criteria) this;
        }

        public Criteria andInstlmntnoLessThanOrEqualTo(Integer value) {
            addCriterion("instlmntNo <=", value, "instlmntno");
            return (Criteria) this;
        }

        public Criteria andInstlmntnoIn(List<Integer> values) {
            addCriterion("instlmntNo in", values, "instlmntno");
            return (Criteria) this;
        }

        public Criteria andInstlmntnoNotIn(List<Integer> values) {
            addCriterion("instlmntNo not in", values, "instlmntno");
            return (Criteria) this;
        }

        public Criteria andInstlmntnoBetween(Integer value1, Integer value2) {
            addCriterion("instlmntNo between", value1, value2, "instlmntno");
            return (Criteria) this;
        }

        public Criteria andInstlmntnoNotBetween(Integer value1, Integer value2) {
            addCriterion("instlmntNo not between", value1, value2, "instlmntno");
            return (Criteria) this;
        }

        public Criteria andPlannoIsNull() {
            addCriterion("planNo is null");
            return (Criteria) this;
        }

        public Criteria andPlannoIsNotNull() {
            addCriterion("planNo is not null");
            return (Criteria) this;
        }

        public Criteria andPlannoEqualTo(String value) {
            addCriterion("planNo =", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoNotEqualTo(String value) {
            addCriterion("planNo <>", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoGreaterThan(String value) {
            addCriterion("planNo >", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoGreaterThanOrEqualTo(String value) {
            addCriterion("planNo >=", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoLessThan(String value) {
            addCriterion("planNo <", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoLessThanOrEqualTo(String value) {
            addCriterion("planNo <=", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoLike(String value) {
            addCriterion("planNo like", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoNotLike(String value) {
            addCriterion("planNo not like", value, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoIn(List<String> values) {
            addCriterion("planNo in", values, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoNotIn(List<String> values) {
            addCriterion("planNo not in", values, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoBetween(String value1, String value2) {
            addCriterion("planNo between", value1, value2, "planno");
            return (Criteria) this;
        }

        public Criteria andPlannoNotBetween(String value1, String value2) {
            addCriterion("planNo not between", value1, value2, "planno");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNull() {
            addCriterion("userAcc is null");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNotNull() {
            addCriterion("userAcc is not null");
            return (Criteria) this;
        }

        public Criteria andUseraccEqualTo(String value) {
            addCriterion("userAcc =", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotEqualTo(String value) {
            addCriterion("userAcc <>", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThan(String value) {
            addCriterion("userAcc >", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThanOrEqualTo(String value) {
            addCriterion("userAcc >=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThan(String value) {
            addCriterion("userAcc <", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThanOrEqualTo(String value) {
            addCriterion("userAcc <=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLike(String value) {
            addCriterion("userAcc like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotLike(String value) {
            addCriterion("userAcc not like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccIn(List<String> values) {
            addCriterion("userAcc in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotIn(List<String> values) {
            addCriterion("userAcc not in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccBetween(String value1, String value2) {
            addCriterion("userAcc between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotBetween(String value1, String value2) {
            addCriterion("userAcc not between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andLastmoneyIsNull() {
            addCriterion("lastMoney is null");
            return (Criteria) this;
        }

        public Criteria andLastmoneyIsNotNull() {
            addCriterion("lastMoney is not null");
            return (Criteria) this;
        }

        public Criteria andLastmoneyEqualTo(Float value) {
            addCriterion("lastMoney =", value, "lastmoney");
            return (Criteria) this;
        }

        public Criteria andLastmoneyNotEqualTo(Float value) {
            addCriterion("lastMoney <>", value, "lastmoney");
            return (Criteria) this;
        }

        public Criteria andLastmoneyGreaterThan(Float value) {
            addCriterion("lastMoney >", value, "lastmoney");
            return (Criteria) this;
        }

        public Criteria andLastmoneyGreaterThanOrEqualTo(Float value) {
            addCriterion("lastMoney >=", value, "lastmoney");
            return (Criteria) this;
        }

        public Criteria andLastmoneyLessThan(Float value) {
            addCriterion("lastMoney <", value, "lastmoney");
            return (Criteria) this;
        }

        public Criteria andLastmoneyLessThanOrEqualTo(Float value) {
            addCriterion("lastMoney <=", value, "lastmoney");
            return (Criteria) this;
        }

        public Criteria andLastmoneyIn(List<Float> values) {
            addCriterion("lastMoney in", values, "lastmoney");
            return (Criteria) this;
        }

        public Criteria andLastmoneyNotIn(List<Float> values) {
            addCriterion("lastMoney not in", values, "lastmoney");
            return (Criteria) this;
        }

        public Criteria andLastmoneyBetween(Float value1, Float value2) {
            addCriterion("lastMoney between", value1, value2, "lastmoney");
            return (Criteria) this;
        }

        public Criteria andLastmoneyNotBetween(Float value1, Float value2) {
            addCriterion("lastMoney not between", value1, value2, "lastmoney");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateIsNull() {
            addCriterion("repaymentDate is null");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateIsNotNull() {
            addCriterion("repaymentDate is not null");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateEqualTo(Date value) {
            addCriterion("repaymentDate =", value, "repaymentdate");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateNotEqualTo(Date value) {
            addCriterion("repaymentDate <>", value, "repaymentdate");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateGreaterThan(Date value) {
            addCriterion("repaymentDate >", value, "repaymentdate");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateGreaterThanOrEqualTo(Date value) {
            addCriterion("repaymentDate >=", value, "repaymentdate");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateLessThan(Date value) {
            addCriterion("repaymentDate <", value, "repaymentdate");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateLessThanOrEqualTo(Date value) {
            addCriterion("repaymentDate <=", value, "repaymentdate");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateIn(List<Date> values) {
            addCriterion("repaymentDate in", values, "repaymentdate");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateNotIn(List<Date> values) {
            addCriterion("repaymentDate not in", values, "repaymentdate");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateBetween(Date value1, Date value2) {
            addCriterion("repaymentDate between", value1, value2, "repaymentdate");
            return (Criteria) this;
        }

        public Criteria andRepaymentdateNotBetween(Date value1, Date value2) {
            addCriterion("repaymentDate not between", value1, value2, "repaymentdate");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyIsNull() {
            addCriterion("repaymentMoney is null");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyIsNotNull() {
            addCriterion("repaymentMoney is not null");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyEqualTo(Float value) {
            addCriterion("repaymentMoney =", value, "repaymentmoney");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyNotEqualTo(Float value) {
            addCriterion("repaymentMoney <>", value, "repaymentmoney");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyGreaterThan(Float value) {
            addCriterion("repaymentMoney >", value, "repaymentmoney");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyGreaterThanOrEqualTo(Float value) {
            addCriterion("repaymentMoney >=", value, "repaymentmoney");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyLessThan(Float value) {
            addCriterion("repaymentMoney <", value, "repaymentmoney");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyLessThanOrEqualTo(Float value) {
            addCriterion("repaymentMoney <=", value, "repaymentmoney");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyIn(List<Float> values) {
            addCriterion("repaymentMoney in", values, "repaymentmoney");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyNotIn(List<Float> values) {
            addCriterion("repaymentMoney not in", values, "repaymentmoney");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyBetween(Float value1, Float value2) {
            addCriterion("repaymentMoney between", value1, value2, "repaymentmoney");
            return (Criteria) this;
        }

        public Criteria andRepaymentmoneyNotBetween(Float value1, Float value2) {
            addCriterion("repaymentMoney not between", value1, value2, "repaymentmoney");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}