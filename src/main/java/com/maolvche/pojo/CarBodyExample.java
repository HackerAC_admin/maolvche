package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.List;

public class CarBodyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CarBodyExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCarbodynoIsNull() {
            addCriterion("carBodyNo is null");
            return (Criteria) this;
        }

        public Criteria andCarbodynoIsNotNull() {
            addCriterion("carBodyNo is not null");
            return (Criteria) this;
        }

        public Criteria andCarbodynoEqualTo(String value) {
            addCriterion("carBodyNo =", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoNotEqualTo(String value) {
            addCriterion("carBodyNo <>", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoGreaterThan(String value) {
            addCriterion("carBodyNo >", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoGreaterThanOrEqualTo(String value) {
            addCriterion("carBodyNo >=", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoLessThan(String value) {
            addCriterion("carBodyNo <", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoLessThanOrEqualTo(String value) {
            addCriterion("carBodyNo <=", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoLike(String value) {
            addCriterion("carBodyNo like", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoNotLike(String value) {
            addCriterion("carBodyNo not like", value, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoIn(List<String> values) {
            addCriterion("carBodyNo in", values, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoNotIn(List<String> values) {
            addCriterion("carBodyNo not in", values, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoBetween(String value1, String value2) {
            addCriterion("carBodyNo between", value1, value2, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarbodynoNotBetween(String value1, String value2) {
            addCriterion("carBodyNo not between", value1, value2, "carbodyno");
            return (Criteria) this;
        }

        public Criteria andCarlengthIsNull() {
            addCriterion("carLength is null");
            return (Criteria) this;
        }

        public Criteria andCarlengthIsNotNull() {
            addCriterion("carLength is not null");
            return (Criteria) this;
        }

        public Criteria andCarlengthEqualTo(String value) {
            addCriterion("carLength =", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthNotEqualTo(String value) {
            addCriterion("carLength <>", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthGreaterThan(String value) {
            addCriterion("carLength >", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthGreaterThanOrEqualTo(String value) {
            addCriterion("carLength >=", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthLessThan(String value) {
            addCriterion("carLength <", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthLessThanOrEqualTo(String value) {
            addCriterion("carLength <=", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthLike(String value) {
            addCriterion("carLength like", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthNotLike(String value) {
            addCriterion("carLength not like", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthIn(List<String> values) {
            addCriterion("carLength in", values, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthNotIn(List<String> values) {
            addCriterion("carLength not in", values, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthBetween(String value1, String value2) {
            addCriterion("carLength between", value1, value2, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthNotBetween(String value1, String value2) {
            addCriterion("carLength not between", value1, value2, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarheightIsNull() {
            addCriterion("carHeight is null");
            return (Criteria) this;
        }

        public Criteria andCarheightIsNotNull() {
            addCriterion("carHeight is not null");
            return (Criteria) this;
        }

        public Criteria andCarheightEqualTo(String value) {
            addCriterion("carHeight =", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightNotEqualTo(String value) {
            addCriterion("carHeight <>", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightGreaterThan(String value) {
            addCriterion("carHeight >", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightGreaterThanOrEqualTo(String value) {
            addCriterion("carHeight >=", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightLessThan(String value) {
            addCriterion("carHeight <", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightLessThanOrEqualTo(String value) {
            addCriterion("carHeight <=", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightLike(String value) {
            addCriterion("carHeight like", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightNotLike(String value) {
            addCriterion("carHeight not like", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightIn(List<String> values) {
            addCriterion("carHeight in", values, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightNotIn(List<String> values) {
            addCriterion("carHeight not in", values, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightBetween(String value1, String value2) {
            addCriterion("carHeight between", value1, value2, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightNotBetween(String value1, String value2) {
            addCriterion("carHeight not between", value1, value2, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarwidthIsNull() {
            addCriterion("carWidth is null");
            return (Criteria) this;
        }

        public Criteria andCarwidthIsNotNull() {
            addCriterion("carWidth is not null");
            return (Criteria) this;
        }

        public Criteria andCarwidthEqualTo(String value) {
            addCriterion("carWidth =", value, "carwidth");
            return (Criteria) this;
        }

        public Criteria andCarwidthNotEqualTo(String value) {
            addCriterion("carWidth <>", value, "carwidth");
            return (Criteria) this;
        }

        public Criteria andCarwidthGreaterThan(String value) {
            addCriterion("carWidth >", value, "carwidth");
            return (Criteria) this;
        }

        public Criteria andCarwidthGreaterThanOrEqualTo(String value) {
            addCriterion("carWidth >=", value, "carwidth");
            return (Criteria) this;
        }

        public Criteria andCarwidthLessThan(String value) {
            addCriterion("carWidth <", value, "carwidth");
            return (Criteria) this;
        }

        public Criteria andCarwidthLessThanOrEqualTo(String value) {
            addCriterion("carWidth <=", value, "carwidth");
            return (Criteria) this;
        }

        public Criteria andCarwidthLike(String value) {
            addCriterion("carWidth like", value, "carwidth");
            return (Criteria) this;
        }

        public Criteria andCarwidthNotLike(String value) {
            addCriterion("carWidth not like", value, "carwidth");
            return (Criteria) this;
        }

        public Criteria andCarwidthIn(List<String> values) {
            addCriterion("carWidth in", values, "carwidth");
            return (Criteria) this;
        }

        public Criteria andCarwidthNotIn(List<String> values) {
            addCriterion("carWidth not in", values, "carwidth");
            return (Criteria) this;
        }

        public Criteria andCarwidthBetween(String value1, String value2) {
            addCriterion("carWidth between", value1, value2, "carwidth");
            return (Criteria) this;
        }

        public Criteria andCarwidthNotBetween(String value1, String value2) {
            addCriterion("carWidth not between", value1, value2, "carwidth");
            return (Criteria) this;
        }

        public Criteria andWheelbaseIsNull() {
            addCriterion("wheelBase is null");
            return (Criteria) this;
        }

        public Criteria andWheelbaseIsNotNull() {
            addCriterion("wheelBase is not null");
            return (Criteria) this;
        }

        public Criteria andWheelbaseEqualTo(String value) {
            addCriterion("wheelBase =", value, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andWheelbaseNotEqualTo(String value) {
            addCriterion("wheelBase <>", value, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andWheelbaseGreaterThan(String value) {
            addCriterion("wheelBase >", value, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andWheelbaseGreaterThanOrEqualTo(String value) {
            addCriterion("wheelBase >=", value, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andWheelbaseLessThan(String value) {
            addCriterion("wheelBase <", value, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andWheelbaseLessThanOrEqualTo(String value) {
            addCriterion("wheelBase <=", value, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andWheelbaseLike(String value) {
            addCriterion("wheelBase like", value, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andWheelbaseNotLike(String value) {
            addCriterion("wheelBase not like", value, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andWheelbaseIn(List<String> values) {
            addCriterion("wheelBase in", values, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andWheelbaseNotIn(List<String> values) {
            addCriterion("wheelBase not in", values, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andWheelbaseBetween(String value1, String value2) {
            addCriterion("wheelBase between", value1, value2, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andWheelbaseNotBetween(String value1, String value2) {
            addCriterion("wheelBase not between", value1, value2, "wheelbase");
            return (Criteria) this;
        }

        public Criteria andCarweightIsNull() {
            addCriterion("carWeight is null");
            return (Criteria) this;
        }

        public Criteria andCarweightIsNotNull() {
            addCriterion("carWeight is not null");
            return (Criteria) this;
        }

        public Criteria andCarweightEqualTo(String value) {
            addCriterion("carWeight =", value, "carweight");
            return (Criteria) this;
        }

        public Criteria andCarweightNotEqualTo(String value) {
            addCriterion("carWeight <>", value, "carweight");
            return (Criteria) this;
        }

        public Criteria andCarweightGreaterThan(String value) {
            addCriterion("carWeight >", value, "carweight");
            return (Criteria) this;
        }

        public Criteria andCarweightGreaterThanOrEqualTo(String value) {
            addCriterion("carWeight >=", value, "carweight");
            return (Criteria) this;
        }

        public Criteria andCarweightLessThan(String value) {
            addCriterion("carWeight <", value, "carweight");
            return (Criteria) this;
        }

        public Criteria andCarweightLessThanOrEqualTo(String value) {
            addCriterion("carWeight <=", value, "carweight");
            return (Criteria) this;
        }

        public Criteria andCarweightLike(String value) {
            addCriterion("carWeight like", value, "carweight");
            return (Criteria) this;
        }

        public Criteria andCarweightNotLike(String value) {
            addCriterion("carWeight not like", value, "carweight");
            return (Criteria) this;
        }

        public Criteria andCarweightIn(List<String> values) {
            addCriterion("carWeight in", values, "carweight");
            return (Criteria) this;
        }

        public Criteria andCarweightNotIn(List<String> values) {
            addCriterion("carWeight not in", values, "carweight");
            return (Criteria) this;
        }

        public Criteria andCarweightBetween(String value1, String value2) {
            addCriterion("carWeight between", value1, value2, "carweight");
            return (Criteria) this;
        }

        public Criteria andCarweightNotBetween(String value1, String value2) {
            addCriterion("carWeight not between", value1, value2, "carweight");
            return (Criteria) this;
        }

        public Criteria andMingroundIsNull() {
            addCriterion("minGround is null");
            return (Criteria) this;
        }

        public Criteria andMingroundIsNotNull() {
            addCriterion("minGround is not null");
            return (Criteria) this;
        }

        public Criteria andMingroundEqualTo(String value) {
            addCriterion("minGround =", value, "minground");
            return (Criteria) this;
        }

        public Criteria andMingroundNotEqualTo(String value) {
            addCriterion("minGround <>", value, "minground");
            return (Criteria) this;
        }

        public Criteria andMingroundGreaterThan(String value) {
            addCriterion("minGround >", value, "minground");
            return (Criteria) this;
        }

        public Criteria andMingroundGreaterThanOrEqualTo(String value) {
            addCriterion("minGround >=", value, "minground");
            return (Criteria) this;
        }

        public Criteria andMingroundLessThan(String value) {
            addCriterion("minGround <", value, "minground");
            return (Criteria) this;
        }

        public Criteria andMingroundLessThanOrEqualTo(String value) {
            addCriterion("minGround <=", value, "minground");
            return (Criteria) this;
        }

        public Criteria andMingroundLike(String value) {
            addCriterion("minGround like", value, "minground");
            return (Criteria) this;
        }

        public Criteria andMingroundNotLike(String value) {
            addCriterion("minGround not like", value, "minground");
            return (Criteria) this;
        }

        public Criteria andMingroundIn(List<String> values) {
            addCriterion("minGround in", values, "minground");
            return (Criteria) this;
        }

        public Criteria andMingroundNotIn(List<String> values) {
            addCriterion("minGround not in", values, "minground");
            return (Criteria) this;
        }

        public Criteria andMingroundBetween(String value1, String value2) {
            addCriterion("minGround between", value1, value2, "minground");
            return (Criteria) this;
        }

        public Criteria andMingroundNotBetween(String value1, String value2) {
            addCriterion("minGround not between", value1, value2, "minground");
            return (Criteria) this;
        }

        public Criteria andCarstructureIsNull() {
            addCriterion("carStructure is null");
            return (Criteria) this;
        }

        public Criteria andCarstructureIsNotNull() {
            addCriterion("carStructure is not null");
            return (Criteria) this;
        }

        public Criteria andCarstructureEqualTo(String value) {
            addCriterion("carStructure =", value, "carstructure");
            return (Criteria) this;
        }

        public Criteria andCarstructureNotEqualTo(String value) {
            addCriterion("carStructure <>", value, "carstructure");
            return (Criteria) this;
        }

        public Criteria andCarstructureGreaterThan(String value) {
            addCriterion("carStructure >", value, "carstructure");
            return (Criteria) this;
        }

        public Criteria andCarstructureGreaterThanOrEqualTo(String value) {
            addCriterion("carStructure >=", value, "carstructure");
            return (Criteria) this;
        }

        public Criteria andCarstructureLessThan(String value) {
            addCriterion("carStructure <", value, "carstructure");
            return (Criteria) this;
        }

        public Criteria andCarstructureLessThanOrEqualTo(String value) {
            addCriterion("carStructure <=", value, "carstructure");
            return (Criteria) this;
        }

        public Criteria andCarstructureLike(String value) {
            addCriterion("carStructure like", value, "carstructure");
            return (Criteria) this;
        }

        public Criteria andCarstructureNotLike(String value) {
            addCriterion("carStructure not like", value, "carstructure");
            return (Criteria) this;
        }

        public Criteria andCarstructureIn(List<String> values) {
            addCriterion("carStructure in", values, "carstructure");
            return (Criteria) this;
        }

        public Criteria andCarstructureNotIn(List<String> values) {
            addCriterion("carStructure not in", values, "carstructure");
            return (Criteria) this;
        }

        public Criteria andCarstructureBetween(String value1, String value2) {
            addCriterion("carStructure between", value1, value2, "carstructure");
            return (Criteria) this;
        }

        public Criteria andCarstructureNotBetween(String value1, String value2) {
            addCriterion("carStructure not between", value1, value2, "carstructure");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeIsNull() {
            addCriterion("fueltankVolume is null");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeIsNotNull() {
            addCriterion("fueltankVolume is not null");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeEqualTo(String value) {
            addCriterion("fueltankVolume =", value, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeNotEqualTo(String value) {
            addCriterion("fueltankVolume <>", value, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeGreaterThan(String value) {
            addCriterion("fueltankVolume >", value, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeGreaterThanOrEqualTo(String value) {
            addCriterion("fueltankVolume >=", value, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeLessThan(String value) {
            addCriterion("fueltankVolume <", value, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeLessThanOrEqualTo(String value) {
            addCriterion("fueltankVolume <=", value, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeLike(String value) {
            addCriterion("fueltankVolume like", value, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeNotLike(String value) {
            addCriterion("fueltankVolume not like", value, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeIn(List<String> values) {
            addCriterion("fueltankVolume in", values, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeNotIn(List<String> values) {
            addCriterion("fueltankVolume not in", values, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeBetween(String value1, String value2) {
            addCriterion("fueltankVolume between", value1, value2, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andFueltankvolumeNotBetween(String value1, String value2) {
            addCriterion("fueltankVolume not between", value1, value2, "fueltankvolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeIsNull() {
            addCriterion("luggagecarrierVolume is null");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeIsNotNull() {
            addCriterion("luggagecarrierVolume is not null");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeEqualTo(String value) {
            addCriterion("luggagecarrierVolume =", value, "luggagecarriervolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeNotEqualTo(String value) {
            addCriterion("luggagecarrierVolume <>", value, "luggagecarriervolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeGreaterThan(String value) {
            addCriterion("luggagecarrierVolume >", value, "luggagecarriervolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeGreaterThanOrEqualTo(String value) {
            addCriterion("luggagecarrierVolume >=", value, "luggagecarriervolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeLessThan(String value) {
            addCriterion("luggagecarrierVolume <", value, "luggagecarriervolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeLessThanOrEqualTo(String value) {
            addCriterion("luggagecarrierVolume <=", value, "luggagecarriervolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeLike(String value) {
            addCriterion("luggagecarrierVolume like", value, "luggagecarriervolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeNotLike(String value) {
            addCriterion("luggagecarrierVolume not like", value, "luggagecarriervolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeIn(List<String> values) {
            addCriterion("luggagecarrierVolume in", values, "luggagecarriervolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeNotIn(List<String> values) {
            addCriterion("luggagecarrierVolume not in", values, "luggagecarriervolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeBetween(String value1, String value2) {
            addCriterion("luggagecarrierVolume between", value1, value2, "luggagecarriervolume");
            return (Criteria) this;
        }

        public Criteria andLuggagecarriervolumeNotBetween(String value1, String value2) {
            addCriterion("luggagecarrierVolume not between", value1, value2, "luggagecarriervolume");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}