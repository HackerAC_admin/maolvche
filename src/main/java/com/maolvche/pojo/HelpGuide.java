package com.maolvche.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HelpGuide {
    private Integer helpguidno;

    private String module;

    private String topic;

    private String state;

    private Date updatetime;

    private String content;

    public Integer getHelpguidno() {
        return helpguidno;
    }

    public void setHelpguidno(Integer helpguidno) {
        this.helpguidno = helpguidno;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module == null ? null : module.trim();
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic == null ? null : topic.trim();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public String getUpdatetime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(updatetime);
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}