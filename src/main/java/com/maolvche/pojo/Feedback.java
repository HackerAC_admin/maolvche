package com.maolvche.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Feedback {
    private Integer feedbackno;

    private String useracc;

    private String topic;

    private Date sendtime;

    private String handleresult;

    private Date handletime;

    private String fbcontents;

    public Integer getFeedbackno() {
        return feedbackno;
    }

    public void setFeedbackno(Integer feedbackno) {
        this.feedbackno = feedbackno;
    }

    public String getUseracc() {
        return useracc;
    }

    public void setUseracc(String useracc) {
        this.useracc = useracc == null ? null : useracc.trim();
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic == null ? null : topic.trim();
    }

    public String getSendtime() {
        if(sendtime!=null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(sendtime);
        }else{
            return null;
        }
    }

    public String getHandletime() {
        if(handletime!=null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(handletime);
        }else{
            return null;
        }
    }

    public void setSendtime(Date sendtime) {
        this.sendtime = sendtime;
    }

    public String getHandleresult() {
        return handleresult;
    }

    public void setHandleresult(String handleresult) {
        this.handleresult = handleresult == null ? null : handleresult.trim();
    }

    public void setHandletime(Date handletime) {
        this.handletime = handletime;
    }

    public String getFbcontents() {
        return fbcontents;
    }

    public void setFbcontents(String fbcontents) {
        this.fbcontents = fbcontents == null ? null : fbcontents.trim();
    }
}