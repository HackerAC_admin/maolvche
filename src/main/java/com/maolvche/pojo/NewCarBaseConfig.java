package com.maolvche.pojo;

public class NewCarBaseConfig {
    private String basicconno;

    private String firm;

    private String carmodel;

    private String outcolor;

    private String incolor;

    private String maxspeed;

    private String oilconsumption;

    private String parkingbraketype;

    public String getBasicconno() {
        return basicconno;
    }

    public void setBasicconno(String basicconno) {
        this.basicconno = basicconno == null ? null : basicconno.trim();
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm == null ? null : firm.trim();
    }

    public String getCarmodel() {
        return carmodel;
    }

    public void setCarmodel(String carmodel) {
        this.carmodel = carmodel == null ? null : carmodel.trim();
    }

    public String getOutcolor() {
        return outcolor;
    }

    public void setOutcolor(String outcolor) {
        this.outcolor = outcolor == null ? null : outcolor.trim();
    }

    public String getIncolor() {
        return incolor;
    }

    public void setIncolor(String incolor) {
        this.incolor = incolor == null ? null : incolor.trim();
    }

    public String getMaxspeed() {
        return maxspeed;
    }

    public void setMaxspeed(String maxspeed) {
        this.maxspeed = maxspeed == null ? null : maxspeed.trim();
    }

    public String getOilconsumption() {
        return oilconsumption;
    }

    public void setOilconsumption(String oilconsumption) {
        this.oilconsumption = oilconsumption == null ? null : oilconsumption.trim();
    }

    public String getParkingbraketype() {
        return parkingbraketype;
    }

    public void setParkingbraketype(String parkingbraketype) {
        this.parkingbraketype = parkingbraketype == null ? null : parkingbraketype.trim();
    }
}