package com.maolvche.pojo;

public class EmpAccount {
    private Integer eplyaccno;

    private String employeeno;

    private String loginname;

    private String eplypwd;

    private String nickname;

    public Integer getEplyaccno() {
        return eplyaccno;
    }

    public void setEplyaccno(Integer eplyaccno) {
        this.eplyaccno = eplyaccno;
    }

    public String getEmployeeno() {
        return employeeno;
    }

    public void setEmployeeno(String employeeno) {
        this.employeeno = employeeno == null ? null : employeeno.trim();
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname == null ? null : loginname.trim();
    }

    public String getEplypwd() {
        return eplypwd;
    }

    public void setEplypwd(String eplypwd) {
        this.eplypwd = eplypwd == null ? null : eplypwd.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }
}