package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.List;

public class BgAccountExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BgAccountExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAdmaccnoIsNull() {
            addCriterion("admAccNo is null");
            return (Criteria) this;
        }

        public Criteria andAdmaccnoIsNotNull() {
            addCriterion("admAccNo is not null");
            return (Criteria) this;
        }

        public Criteria andAdmaccnoEqualTo(Integer value) {
            addCriterion("admAccNo =", value, "admaccno");
            return (Criteria) this;
        }

        public Criteria andAdmaccnoNotEqualTo(Integer value) {
            addCriterion("admAccNo <>", value, "admaccno");
            return (Criteria) this;
        }

        public Criteria andAdmaccnoGreaterThan(Integer value) {
            addCriterion("admAccNo >", value, "admaccno");
            return (Criteria) this;
        }

        public Criteria andAdmaccnoGreaterThanOrEqualTo(Integer value) {
            addCriterion("admAccNo >=", value, "admaccno");
            return (Criteria) this;
        }

        public Criteria andAdmaccnoLessThan(Integer value) {
            addCriterion("admAccNo <", value, "admaccno");
            return (Criteria) this;
        }

        public Criteria andAdmaccnoLessThanOrEqualTo(Integer value) {
            addCriterion("admAccNo <=", value, "admaccno");
            return (Criteria) this;
        }

        public Criteria andAdmaccnoIn(List<Integer> values) {
            addCriterion("admAccNo in", values, "admaccno");
            return (Criteria) this;
        }

        public Criteria andAdmaccnoNotIn(List<Integer> values) {
            addCriterion("admAccNo not in", values, "admaccno");
            return (Criteria) this;
        }

        public Criteria andAdmaccnoBetween(Integer value1, Integer value2) {
            addCriterion("admAccNo between", value1, value2, "admaccno");
            return (Criteria) this;
        }

        public Criteria andAdmaccnoNotBetween(Integer value1, Integer value2) {
            addCriterion("admAccNo not between", value1, value2, "admaccno");
            return (Criteria) this;
        }

        public Criteria andAdminnoIsNull() {
            addCriterion("adminNo is null");
            return (Criteria) this;
        }

        public Criteria andAdminnoIsNotNull() {
            addCriterion("adminNo is not null");
            return (Criteria) this;
        }

        public Criteria andAdminnoEqualTo(String value) {
            addCriterion("adminNo =", value, "adminno");
            return (Criteria) this;
        }

        public Criteria andAdminnoNotEqualTo(String value) {
            addCriterion("adminNo <>", value, "adminno");
            return (Criteria) this;
        }

        public Criteria andAdminnoGreaterThan(String value) {
            addCriterion("adminNo >", value, "adminno");
            return (Criteria) this;
        }

        public Criteria andAdminnoGreaterThanOrEqualTo(String value) {
            addCriterion("adminNo >=", value, "adminno");
            return (Criteria) this;
        }

        public Criteria andAdminnoLessThan(String value) {
            addCriterion("adminNo <", value, "adminno");
            return (Criteria) this;
        }

        public Criteria andAdminnoLessThanOrEqualTo(String value) {
            addCriterion("adminNo <=", value, "adminno");
            return (Criteria) this;
        }

        public Criteria andAdminnoLike(String value) {
            addCriterion("adminNo like", value, "adminno");
            return (Criteria) this;
        }

        public Criteria andAdminnoNotLike(String value) {
            addCriterion("adminNo not like", value, "adminno");
            return (Criteria) this;
        }

        public Criteria andAdminnoIn(List<String> values) {
            addCriterion("adminNo in", values, "adminno");
            return (Criteria) this;
        }

        public Criteria andAdminnoNotIn(List<String> values) {
            addCriterion("adminNo not in", values, "adminno");
            return (Criteria) this;
        }

        public Criteria andAdminnoBetween(String value1, String value2) {
            addCriterion("adminNo between", value1, value2, "adminno");
            return (Criteria) this;
        }

        public Criteria andAdminnoNotBetween(String value1, String value2) {
            addCriterion("adminNo not between", value1, value2, "adminno");
            return (Criteria) this;
        }

        public Criteria andLoginnameIsNull() {
            addCriterion("loginName is null");
            return (Criteria) this;
        }

        public Criteria andLoginnameIsNotNull() {
            addCriterion("loginName is not null");
            return (Criteria) this;
        }

        public Criteria andLoginnameEqualTo(String value) {
            addCriterion("loginName =", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameNotEqualTo(String value) {
            addCriterion("loginName <>", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameGreaterThan(String value) {
            addCriterion("loginName >", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameGreaterThanOrEqualTo(String value) {
            addCriterion("loginName >=", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameLessThan(String value) {
            addCriterion("loginName <", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameLessThanOrEqualTo(String value) {
            addCriterion("loginName <=", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameLike(String value) {
            addCriterion("loginName like", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameNotLike(String value) {
            addCriterion("loginName not like", value, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameIn(List<String> values) {
            addCriterion("loginName in", values, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameNotIn(List<String> values) {
            addCriterion("loginName not in", values, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameBetween(String value1, String value2) {
            addCriterion("loginName between", value1, value2, "loginname");
            return (Criteria) this;
        }

        public Criteria andLoginnameNotBetween(String value1, String value2) {
            addCriterion("loginName not between", value1, value2, "loginname");
            return (Criteria) this;
        }

        public Criteria andAdmpwdIsNull() {
            addCriterion("admPwd is null");
            return (Criteria) this;
        }

        public Criteria andAdmpwdIsNotNull() {
            addCriterion("admPwd is not null");
            return (Criteria) this;
        }

        public Criteria andAdmpwdEqualTo(String value) {
            addCriterion("admPwd =", value, "admpwd");
            return (Criteria) this;
        }

        public Criteria andAdmpwdNotEqualTo(String value) {
            addCriterion("admPwd <>", value, "admpwd");
            return (Criteria) this;
        }

        public Criteria andAdmpwdGreaterThan(String value) {
            addCriterion("admPwd >", value, "admpwd");
            return (Criteria) this;
        }

        public Criteria andAdmpwdGreaterThanOrEqualTo(String value) {
            addCriterion("admPwd >=", value, "admpwd");
            return (Criteria) this;
        }

        public Criteria andAdmpwdLessThan(String value) {
            addCriterion("admPwd <", value, "admpwd");
            return (Criteria) this;
        }

        public Criteria andAdmpwdLessThanOrEqualTo(String value) {
            addCriterion("admPwd <=", value, "admpwd");
            return (Criteria) this;
        }

        public Criteria andAdmpwdLike(String value) {
            addCriterion("admPwd like", value, "admpwd");
            return (Criteria) this;
        }

        public Criteria andAdmpwdNotLike(String value) {
            addCriterion("admPwd not like", value, "admpwd");
            return (Criteria) this;
        }

        public Criteria andAdmpwdIn(List<String> values) {
            addCriterion("admPwd in", values, "admpwd");
            return (Criteria) this;
        }

        public Criteria andAdmpwdNotIn(List<String> values) {
            addCriterion("admPwd not in", values, "admpwd");
            return (Criteria) this;
        }

        public Criteria andAdmpwdBetween(String value1, String value2) {
            addCriterion("admPwd between", value1, value2, "admpwd");
            return (Criteria) this;
        }

        public Criteria andAdmpwdNotBetween(String value1, String value2) {
            addCriterion("admPwd not between", value1, value2, "admpwd");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNull() {
            addCriterion("nickName is null");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNotNull() {
            addCriterion("nickName is not null");
            return (Criteria) this;
        }

        public Criteria andNicknameEqualTo(String value) {
            addCriterion("nickName =", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotEqualTo(String value) {
            addCriterion("nickName <>", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThan(String value) {
            addCriterion("nickName >", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThanOrEqualTo(String value) {
            addCriterion("nickName >=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThan(String value) {
            addCriterion("nickName <", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThanOrEqualTo(String value) {
            addCriterion("nickName <=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLike(String value) {
            addCriterion("nickName like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotLike(String value) {
            addCriterion("nickName not like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameIn(List<String> values) {
            addCriterion("nickName in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotIn(List<String> values) {
            addCriterion("nickName not in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameBetween(String value1, String value2) {
            addCriterion("nickName between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotBetween(String value1, String value2) {
            addCriterion("nickName not between", value1, value2, "nickname");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}