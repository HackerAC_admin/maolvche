package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserInstlInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserInstlInfoExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUserinstlinfonoIsNull() {
            addCriterion("UserInstlInfoNo is null");
            return (Criteria) this;
        }

        public Criteria andUserinstlinfonoIsNotNull() {
            addCriterion("UserInstlInfoNo is not null");
            return (Criteria) this;
        }

        public Criteria andUserinstlinfonoEqualTo(Integer value) {
            addCriterion("UserInstlInfoNo =", value, "userinstlinfono");
            return (Criteria) this;
        }

        public Criteria andUserinstlinfonoNotEqualTo(Integer value) {
            addCriterion("UserInstlInfoNo <>", value, "userinstlinfono");
            return (Criteria) this;
        }

        public Criteria andUserinstlinfonoGreaterThan(Integer value) {
            addCriterion("UserInstlInfoNo >", value, "userinstlinfono");
            return (Criteria) this;
        }

        public Criteria andUserinstlinfonoGreaterThanOrEqualTo(Integer value) {
            addCriterion("UserInstlInfoNo >=", value, "userinstlinfono");
            return (Criteria) this;
        }

        public Criteria andUserinstlinfonoLessThan(Integer value) {
            addCriterion("UserInstlInfoNo <", value, "userinstlinfono");
            return (Criteria) this;
        }

        public Criteria andUserinstlinfonoLessThanOrEqualTo(Integer value) {
            addCriterion("UserInstlInfoNo <=", value, "userinstlinfono");
            return (Criteria) this;
        }

        public Criteria andUserinstlinfonoIn(List<Integer> values) {
            addCriterion("UserInstlInfoNo in", values, "userinstlinfono");
            return (Criteria) this;
        }

        public Criteria andUserinstlinfonoNotIn(List<Integer> values) {
            addCriterion("UserInstlInfoNo not in", values, "userinstlinfono");
            return (Criteria) this;
        }

        public Criteria andUserinstlinfonoBetween(Integer value1, Integer value2) {
            addCriterion("UserInstlInfoNo between", value1, value2, "userinstlinfono");
            return (Criteria) this;
        }

        public Criteria andUserinstlinfonoNotBetween(Integer value1, Integer value2) {
            addCriterion("UserInstlInfoNo not between", value1, value2, "userinstlinfono");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNull() {
            addCriterion("UserAcc is null");
            return (Criteria) this;
        }

        public Criteria andUseraccIsNotNull() {
            addCriterion("UserAcc is not null");
            return (Criteria) this;
        }

        public Criteria andUseraccEqualTo(String value) {
            addCriterion("UserAcc =", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotEqualTo(String value) {
            addCriterion("UserAcc <>", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThan(String value) {
            addCriterion("UserAcc >", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccGreaterThanOrEqualTo(String value) {
            addCriterion("UserAcc >=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThan(String value) {
            addCriterion("UserAcc <", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLessThanOrEqualTo(String value) {
            addCriterion("UserAcc <=", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccLike(String value) {
            addCriterion("UserAcc like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotLike(String value) {
            addCriterion("UserAcc not like", value, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccIn(List<String> values) {
            addCriterion("UserAcc in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotIn(List<String> values) {
            addCriterion("UserAcc not in", values, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccBetween(String value1, String value2) {
            addCriterion("UserAcc between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andUseraccNotBetween(String value1, String value2) {
            addCriterion("UserAcc not between", value1, value2, "useracc");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyIsNull() {
            addCriterion("totalMoney is null");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyIsNotNull() {
            addCriterion("totalMoney is not null");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyEqualTo(Float value) {
            addCriterion("totalMoney =", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyNotEqualTo(Float value) {
            addCriterion("totalMoney <>", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyGreaterThan(Float value) {
            addCriterion("totalMoney >", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyGreaterThanOrEqualTo(Float value) {
            addCriterion("totalMoney >=", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyLessThan(Float value) {
            addCriterion("totalMoney <", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyLessThanOrEqualTo(Float value) {
            addCriterion("totalMoney <=", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyIn(List<Float> values) {
            addCriterion("totalMoney in", values, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyNotIn(List<Float> values) {
            addCriterion("totalMoney not in", values, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyBetween(Float value1, Float value2) {
            addCriterion("totalMoney between", value1, value2, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyNotBetween(Float value1, Float value2) {
            addCriterion("totalMoney not between", value1, value2, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyIsNull() {
            addCriterion("firstPayMoney is null");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyIsNotNull() {
            addCriterion("firstPayMoney is not null");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyEqualTo(Float value) {
            addCriterion("firstPayMoney =", value, "firstpaymoney");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyNotEqualTo(Float value) {
            addCriterion("firstPayMoney <>", value, "firstpaymoney");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyGreaterThan(Float value) {
            addCriterion("firstPayMoney >", value, "firstpaymoney");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyGreaterThanOrEqualTo(Float value) {
            addCriterion("firstPayMoney >=", value, "firstpaymoney");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyLessThan(Float value) {
            addCriterion("firstPayMoney <", value, "firstpaymoney");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyLessThanOrEqualTo(Float value) {
            addCriterion("firstPayMoney <=", value, "firstpaymoney");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyIn(List<Float> values) {
            addCriterion("firstPayMoney in", values, "firstpaymoney");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyNotIn(List<Float> values) {
            addCriterion("firstPayMoney not in", values, "firstpaymoney");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyBetween(Float value1, Float value2) {
            addCriterion("firstPayMoney between", value1, value2, "firstpaymoney");
            return (Criteria) this;
        }

        public Criteria andFirstpaymoneyNotBetween(Float value1, Float value2) {
            addCriterion("firstPayMoney not between", value1, value2, "firstpaymoney");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyIsNull() {
            addCriterion("monthPayMoney is null");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyIsNotNull() {
            addCriterion("monthPayMoney is not null");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyEqualTo(Float value) {
            addCriterion("monthPayMoney =", value, "monthpaymoney");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyNotEqualTo(Float value) {
            addCriterion("monthPayMoney <>", value, "monthpaymoney");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyGreaterThan(Float value) {
            addCriterion("monthPayMoney >", value, "monthpaymoney");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyGreaterThanOrEqualTo(Float value) {
            addCriterion("monthPayMoney >=", value, "monthpaymoney");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyLessThan(Float value) {
            addCriterion("monthPayMoney <", value, "monthpaymoney");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyLessThanOrEqualTo(Float value) {
            addCriterion("monthPayMoney <=", value, "monthpaymoney");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyIn(List<Float> values) {
            addCriterion("monthPayMoney in", values, "monthpaymoney");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyNotIn(List<Float> values) {
            addCriterion("monthPayMoney not in", values, "monthpaymoney");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyBetween(Float value1, Float value2) {
            addCriterion("monthPayMoney between", value1, value2, "monthpaymoney");
            return (Criteria) this;
        }

        public Criteria andMonthpaymoneyNotBetween(Float value1, Float value2) {
            addCriterion("monthPayMoney not between", value1, value2, "monthpaymoney");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNull() {
            addCriterion("startDate is null");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNotNull() {
            addCriterion("startDate is not null");
            return (Criteria) this;
        }

        public Criteria andStartdateEqualTo(Date value) {
            addCriterion("startDate =", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotEqualTo(Date value) {
            addCriterion("startDate <>", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThan(Date value) {
            addCriterion("startDate >", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("startDate >=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThan(Date value) {
            addCriterion("startDate <", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThanOrEqualTo(Date value) {
            addCriterion("startDate <=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateIn(List<Date> values) {
            addCriterion("startDate in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotIn(List<Date> values) {
            addCriterion("startDate not in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateBetween(Date value1, Date value2) {
            addCriterion("startDate between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotBetween(Date value1, Date value2) {
            addCriterion("startDate not between", value1, value2, "startdate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}