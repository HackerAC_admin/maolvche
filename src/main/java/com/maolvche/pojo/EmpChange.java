package com.maolvche.pojo;

import java.util.Date;

public class EmpChange {
    private String employeeno;

    private Date changedate;

    private String bejob;

    private String afjob;

    private String remarks;

    public String getEmployeeno() {
        return employeeno;
    }

    public void setEmployeeno(String employeeno) {
        this.employeeno = employeeno == null ? null : employeeno.trim();
    }

    public Date getChangedate() {
        return changedate;
    }

    public void setChangedate(Date changedate) {
        this.changedate = changedate;
    }

    public String getBejob() {
        return bejob;
    }

    public void setBejob(String bejob) {
        this.bejob = bejob == null ? null : bejob.trim();
    }

    public String getAfjob() {
        return afjob;
    }

    public void setAfjob(String afjob) {
        this.afjob = afjob == null ? null : afjob.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }
}