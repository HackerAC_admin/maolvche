package com.maolvche.pojo;

public class carPicture {
    private Integer pictureno;

    private String newcarno;

    private String picturesrc;

    public Integer getPictureno() {
        return pictureno;
    }

    public void setPictureno(Integer pictureno) {
        this.pictureno = pictureno;
    }

    public String getNewcarno() {
        return newcarno;
    }

    public void setNewcarno(String newcarno) {
        this.newcarno = newcarno == null ? null : newcarno.trim();
    }

    public String getPicturesrc() {
        return picturesrc;
    }

    public void setPicturesrc(String picturesrc) {
        this.picturesrc = picturesrc == null ? null : picturesrc.trim();
    }
}