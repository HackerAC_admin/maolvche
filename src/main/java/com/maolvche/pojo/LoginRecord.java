package com.maolvche.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LoginRecord {
    private Integer recoredno;

    private String useracc;

    private String loginip;

    private Date logintime;

    private String loginarea;

    public Integer getRecoredno() {
        return recoredno;
    }

    public void setRecoredno(Integer recoredno) {
        this.recoredno = recoredno;
    }

    public String getUseracc() {
        return useracc;
    }

    public void setUseracc(String useracc) {
        this.useracc = useracc == null ? null : useracc.trim();
    }

    public String getLoginip() {
        return loginip;
    }

    public void setLoginip(String loginip) {
        this.loginip = loginip == null ? null : loginip.trim();
    }

    public String getLogintime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(logintime);
    }

    public void setLogintime(Date logintime) {
        this.logintime = logintime;
    }

    public String getLoginarea() {
        return loginarea;
    }

    public void setLoginarea(String loginarea) {
        this.loginarea = loginarea == null ? null : loginarea.trim();
    }
}