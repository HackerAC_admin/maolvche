package com.maolvche.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class EmpConditionExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public EmpConditionExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andEmpcondiotionnoIsNull() {
            addCriterion("empCondiotionNo is null");
            return (Criteria) this;
        }

        public Criteria andEmpcondiotionnoIsNotNull() {
            addCriterion("empCondiotionNo is not null");
            return (Criteria) this;
        }

        public Criteria andEmpcondiotionnoEqualTo(Integer value) {
            addCriterion("empCondiotionNo =", value, "empcondiotionno");
            return (Criteria) this;
        }

        public Criteria andEmpcondiotionnoNotEqualTo(Integer value) {
            addCriterion("empCondiotionNo <>", value, "empcondiotionno");
            return (Criteria) this;
        }

        public Criteria andEmpcondiotionnoGreaterThan(Integer value) {
            addCriterion("empCondiotionNo >", value, "empcondiotionno");
            return (Criteria) this;
        }

        public Criteria andEmpcondiotionnoGreaterThanOrEqualTo(Integer value) {
            addCriterion("empCondiotionNo >=", value, "empcondiotionno");
            return (Criteria) this;
        }

        public Criteria andEmpcondiotionnoLessThan(Integer value) {
            addCriterion("empCondiotionNo <", value, "empcondiotionno");
            return (Criteria) this;
        }

        public Criteria andEmpcondiotionnoLessThanOrEqualTo(Integer value) {
            addCriterion("empCondiotionNo <=", value, "empcondiotionno");
            return (Criteria) this;
        }

        public Criteria andEmpcondiotionnoIn(List<Integer> values) {
            addCriterion("empCondiotionNo in", values, "empcondiotionno");
            return (Criteria) this;
        }

        public Criteria andEmpcondiotionnoNotIn(List<Integer> values) {
            addCriterion("empCondiotionNo not in", values, "empcondiotionno");
            return (Criteria) this;
        }

        public Criteria andEmpcondiotionnoBetween(Integer value1, Integer value2) {
            addCriterion("empCondiotionNo between", value1, value2, "empcondiotionno");
            return (Criteria) this;
        }

        public Criteria andEmpcondiotionnoNotBetween(Integer value1, Integer value2) {
            addCriterion("empCondiotionNo not between", value1, value2, "empcondiotionno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIsNull() {
            addCriterion("employeeNo is null");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIsNotNull() {
            addCriterion("employeeNo is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeenoEqualTo(String value) {
            addCriterion("employeeNo =", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotEqualTo(String value) {
            addCriterion("employeeNo <>", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoGreaterThan(String value) {
            addCriterion("employeeNo >", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoGreaterThanOrEqualTo(String value) {
            addCriterion("employeeNo >=", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLessThan(String value) {
            addCriterion("employeeNo <", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLessThanOrEqualTo(String value) {
            addCriterion("employeeNo <=", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoLike(String value) {
            addCriterion("employeeNo like", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotLike(String value) {
            addCriterion("employeeNo not like", value, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoIn(List<String> values) {
            addCriterion("employeeNo in", values, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotIn(List<String> values) {
            addCriterion("employeeNo not in", values, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoBetween(String value1, String value2) {
            addCriterion("employeeNo between", value1, value2, "employeeno");
            return (Criteria) this;
        }

        public Criteria andEmployeenoNotBetween(String value1, String value2) {
            addCriterion("employeeNo not between", value1, value2, "employeeno");
            return (Criteria) this;
        }

        public Criteria andOutdateIsNull() {
            addCriterion("outDate is null");
            return (Criteria) this;
        }

        public Criteria andOutdateIsNotNull() {
            addCriterion("outDate is not null");
            return (Criteria) this;
        }

        public Criteria andOutdateEqualTo(Date value) {
            addCriterionForJDBCDate("outDate =", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateNotEqualTo(Date value) {
            addCriterionForJDBCDate("outDate <>", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateGreaterThan(Date value) {
            addCriterionForJDBCDate("outDate >", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("outDate >=", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateLessThan(Date value) {
            addCriterionForJDBCDate("outDate <", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("outDate <=", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateIn(List<Date> values) {
            addCriterionForJDBCDate("outDate in", values, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateNotIn(List<Date> values) {
            addCriterionForJDBCDate("outDate not in", values, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("outDate between", value1, value2, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("outDate not between", value1, value2, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutresIsNull() {
            addCriterion("outRes is null");
            return (Criteria) this;
        }

        public Criteria andOutresIsNotNull() {
            addCriterion("outRes is not null");
            return (Criteria) this;
        }

        public Criteria andOutresEqualTo(String value) {
            addCriterion("outRes =", value, "outres");
            return (Criteria) this;
        }

        public Criteria andOutresNotEqualTo(String value) {
            addCriterion("outRes <>", value, "outres");
            return (Criteria) this;
        }

        public Criteria andOutresGreaterThan(String value) {
            addCriterion("outRes >", value, "outres");
            return (Criteria) this;
        }

        public Criteria andOutresGreaterThanOrEqualTo(String value) {
            addCriterion("outRes >=", value, "outres");
            return (Criteria) this;
        }

        public Criteria andOutresLessThan(String value) {
            addCriterion("outRes <", value, "outres");
            return (Criteria) this;
        }

        public Criteria andOutresLessThanOrEqualTo(String value) {
            addCriterion("outRes <=", value, "outres");
            return (Criteria) this;
        }

        public Criteria andOutresLike(String value) {
            addCriterion("outRes like", value, "outres");
            return (Criteria) this;
        }

        public Criteria andOutresNotLike(String value) {
            addCriterion("outRes not like", value, "outres");
            return (Criteria) this;
        }

        public Criteria andOutresIn(List<String> values) {
            addCriterion("outRes in", values, "outres");
            return (Criteria) this;
        }

        public Criteria andOutresNotIn(List<String> values) {
            addCriterion("outRes not in", values, "outres");
            return (Criteria) this;
        }

        public Criteria andOutresBetween(String value1, String value2) {
            addCriterion("outRes between", value1, value2, "outres");
            return (Criteria) this;
        }

        public Criteria andOutresNotBetween(String value1, String value2) {
            addCriterion("outRes not between", value1, value2, "outres");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNull() {
            addCriterion("Remarks is null");
            return (Criteria) this;
        }

        public Criteria andRemarksIsNotNull() {
            addCriterion("Remarks is not null");
            return (Criteria) this;
        }

        public Criteria andRemarksEqualTo(String value) {
            addCriterion("Remarks =", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotEqualTo(String value) {
            addCriterion("Remarks <>", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThan(String value) {
            addCriterion("Remarks >", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksGreaterThanOrEqualTo(String value) {
            addCriterion("Remarks >=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThan(String value) {
            addCriterion("Remarks <", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLessThanOrEqualTo(String value) {
            addCriterion("Remarks <=", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksLike(String value) {
            addCriterion("Remarks like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotLike(String value) {
            addCriterion("Remarks not like", value, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksIn(List<String> values) {
            addCriterion("Remarks in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotIn(List<String> values) {
            addCriterion("Remarks not in", values, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksBetween(String value1, String value2) {
            addCriterion("Remarks between", value1, value2, "remarks");
            return (Criteria) this;
        }

        public Criteria andRemarksNotBetween(String value1, String value2) {
            addCriterion("Remarks not between", value1, value2, "remarks");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}