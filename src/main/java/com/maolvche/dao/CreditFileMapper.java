package com.maolvche.dao;

import com.maolvche.pojo.CreditFile;
import com.maolvche.pojo.CreditFileExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CreditFileMapper {
    long countByExample(CreditFileExample example);

    int deleteByExample(CreditFileExample example);

    int deleteByPrimaryKey(Integer creaditfileno);

    int insert(CreditFile record);

    int insertSelective(CreditFile record);

    List<CreditFile> selectByExample(CreditFileExample example);

    CreditFile selectByPrimaryKey(Integer creaditfileno);

    int updateByExampleSelective(@Param("record") CreditFile record, @Param("example") CreditFileExample example);

    int updateByExample(@Param("record") CreditFile record, @Param("example") CreditFileExample example);

    int updateByPrimaryKeySelective(CreditFile record);

    int updateByPrimaryKey(CreditFile record);
}