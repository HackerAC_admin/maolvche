package com.maolvche.dao;

import com.maolvche.pojo.HelpGuide;
import com.maolvche.pojo.HelpGuideExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface HelpGuideMapper {
    long countByExample(HelpGuideExample example);

    int deleteByExample(HelpGuideExample example);

    int deleteByPrimaryKey(Integer helpguidno);

    int insert(HelpGuide record);

    int insertSelective(HelpGuide record);

    List<HelpGuide> selectByExampleWithBLOBs(HelpGuideExample example);

    List<HelpGuide> selectByExample(HelpGuideExample example);

    HelpGuide selectByPrimaryKey(Integer helpguidno);

    int updateByExampleSelective(@Param("record") HelpGuide record, @Param("example") HelpGuideExample example);

    int updateByExampleWithBLOBs(@Param("record") HelpGuide record, @Param("example") HelpGuideExample example);

    int updateByExample(@Param("record") HelpGuide record, @Param("example") HelpGuideExample example);

    int updateByPrimaryKeySelective(HelpGuide record);

    int updateByPrimaryKeyWithBLOBs(HelpGuide record);

    int updateByPrimaryKey(HelpGuide record);
}