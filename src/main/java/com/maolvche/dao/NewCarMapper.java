package com.maolvche.dao;

import com.maolvche.pojo.NewCar;
import com.maolvche.pojo.NewCarExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NewCarMapper {
    long countByExample(NewCarExample example);

    int deleteByExample(NewCarExample example);

    int deleteByPrimaryKey(String newcarno);

    int insert(NewCar record);

    int insertSelective(NewCar record);

    List<NewCar> selectByExample(NewCarExample example);

    NewCar selectByPrimaryKey(String newcarno);

    int updateByExampleSelective(@Param("record") NewCar record, @Param("example") NewCarExample example);

    int updateByExample(@Param("record") NewCar record, @Param("example") NewCarExample example);

    int updateByPrimaryKeySelective(NewCar record);

    int updateByPrimaryKey(NewCar record);
}