package com.maolvche.dao;

import com.maolvche.pojo.GearBox;
import com.maolvche.pojo.GearBoxExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GearBoxMapper {
    long countByExample(GearBoxExample example);

    int deleteByExample(GearBoxExample example);

    int deleteByPrimaryKey(String gearboxno);

    int insert(GearBox record);

    int insertSelective(GearBox record);

    List<GearBox> selectByExample(GearBoxExample example);

    GearBox selectByPrimaryKey(String gearboxno);

    int updateByExampleSelective(@Param("record") GearBox record, @Param("example") GearBoxExample example);

    int updateByExample(@Param("record") GearBox record, @Param("example") GearBoxExample example);

    int updateByPrimaryKeySelective(GearBox record);

    int updateByPrimaryKey(GearBox record);
}