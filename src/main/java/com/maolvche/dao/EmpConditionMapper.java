package com.maolvche.dao;

import com.maolvche.pojo.EmpCondition;
import com.maolvche.pojo.EmpConditionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EmpConditionMapper {
    long countByExample(EmpConditionExample example);

    int deleteByExample(EmpConditionExample example);

    int deleteByPrimaryKey(Integer empcondiotionno);

    int insert(EmpCondition record);

    int insertSelective(EmpCondition record);

    List<EmpCondition> selectByExample(EmpConditionExample example);

    EmpCondition selectByPrimaryKey(Integer empcondiotionno);

    int updateByExampleSelective(@Param("record") EmpCondition record, @Param("example") EmpConditionExample example);

    int updateByExample(@Param("record") EmpCondition record, @Param("example") EmpConditionExample example);

    int updateByPrimaryKeySelective(EmpCondition record);

    int updateByPrimaryKey(EmpCondition record);
}