package com.maolvche.dao;

import com.maolvche.pojo.carPicture;
import com.maolvche.pojo.carPictureExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface carPictureMapper {
    long countByExample(carPictureExample example);

    int deleteByExample(carPictureExample example);

    int deleteByPrimaryKey(Integer pictureno);

    int insert(carPicture record);

    int insertSelective(carPicture record);

    List<carPicture> selectByExample(carPictureExample example);

    carPicture selectByPrimaryKey(Integer pictureno);

    int updateByExampleSelective(@Param("record") carPicture record, @Param("example") carPictureExample example);

    int updateByExample(@Param("record") carPicture record, @Param("example") carPictureExample example);

    int updateByPrimaryKeySelective(carPicture record);

    int updateByPrimaryKey(carPicture record);
}