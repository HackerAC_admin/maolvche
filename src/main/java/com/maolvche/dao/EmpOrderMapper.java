package com.maolvche.dao;

import com.maolvche.pojo.EmpOrder;
import com.maolvche.pojo.EmpOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EmpOrderMapper {
    long countByExample(EmpOrderExample example);

    int deleteByExample(EmpOrderExample example);

    int deleteByPrimaryKey(String getorderno);

    int insert(EmpOrder record);

    int insertSelective(EmpOrder record);

    List<EmpOrder> selectByExample(EmpOrderExample example);

    EmpOrder selectByPrimaryKey(String getorderno);

    int updateByExampleSelective(@Param("record") EmpOrder record, @Param("example") EmpOrderExample example);

    int updateByExample(@Param("record") EmpOrder record, @Param("example") EmpOrderExample example);

    int updateByPrimaryKeySelective(EmpOrder record);

    int updateByPrimaryKey(EmpOrder record);
}