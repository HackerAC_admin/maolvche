package com.maolvche.dao;

import com.maolvche.pojo.EmpInfo;
import com.maolvche.pojo.EmpInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EmpInfoMapper {
    long countByExample(EmpInfoExample example);

    int deleteByExample(EmpInfoExample example);

    int deleteByPrimaryKey(String employeeno);

    int insert(EmpInfo record);

    int insertSelective(EmpInfo record);

    List<EmpInfo> selectByExample(EmpInfoExample example);

    EmpInfo selectByPrimaryKey(String employeeno);

    int updateByExampleSelective(@Param("record") EmpInfo record, @Param("example") EmpInfoExample example);

    int updateByExample(@Param("record") EmpInfo record, @Param("example") EmpInfoExample example);

    int updateByPrimaryKeySelective(EmpInfo record);

    int updateByPrimaryKey(EmpInfo record);
}