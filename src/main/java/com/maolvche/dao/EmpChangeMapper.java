package com.maolvche.dao;

import com.maolvche.pojo.EmpChange;
import com.maolvche.pojo.EmpChangeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EmpChangeMapper {
    long countByExample(EmpChangeExample example);

    int deleteByExample(EmpChangeExample example);

    int deleteByPrimaryKey(String employeeno);

    int insert(EmpChange record);

    int insertSelective(EmpChange record);

    List<EmpChange> selectByExample(EmpChangeExample example);

    EmpChange selectByPrimaryKey(String employeeno);

    int updateByExampleSelective(@Param("record") EmpChange record, @Param("example") EmpChangeExample example);

    int updateByExample(@Param("record") EmpChange record, @Param("example") EmpChangeExample example);

    int updateByPrimaryKeySelective(EmpChange record);

    int updateByPrimaryKey(EmpChange record);
}