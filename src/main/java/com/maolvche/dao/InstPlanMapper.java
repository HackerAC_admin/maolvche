package com.maolvche.dao;

import com.maolvche.pojo.InstPlan;
import com.maolvche.pojo.InstPlanExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InstPlanMapper {
    long countByExample(InstPlanExample example);

    int deleteByExample(InstPlanExample example);

    int deleteByPrimaryKey(String planno);

    int insert(InstPlan record);

    int insertSelective(InstPlan record);

    List<InstPlan> selectByExample(InstPlanExample example);

    InstPlan selectByPrimaryKey(String planno);

    int updateByExampleSelective(@Param("record") InstPlan record, @Param("example") InstPlanExample example);

    int updateByExample(@Param("record") InstPlan record, @Param("example") InstPlanExample example);

    int updateByPrimaryKeySelective(InstPlan record);

    int updateByPrimaryKey(InstPlan record);
}