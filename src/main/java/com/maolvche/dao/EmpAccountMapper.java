package com.maolvche.dao;

import com.maolvche.pojo.EmpAccount;
import com.maolvche.pojo.EmpAccountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EmpAccountMapper {
    long countByExample(EmpAccountExample example);

    int deleteByExample(EmpAccountExample example);

    int deleteByPrimaryKey(Integer eplyaccno);

    int insert(EmpAccount record);

    int insertSelective(EmpAccount record);

    List<EmpAccount> selectByExample(EmpAccountExample example);

    EmpAccount selectByPrimaryKey(Integer eplyaccno);

    int updateByExampleSelective(@Param("record") EmpAccount record, @Param("example") EmpAccountExample example);

    int updateByExample(@Param("record") EmpAccount record, @Param("example") EmpAccountExample example);

    int updateByPrimaryKeySelective(EmpAccount record);

    int updateByPrimaryKey(EmpAccount record);
}