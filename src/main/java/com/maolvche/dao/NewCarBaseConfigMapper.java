package com.maolvche.dao;

import com.maolvche.pojo.NewCarBaseConfig;
import com.maolvche.pojo.NewCarBaseConfigExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NewCarBaseConfigMapper {
    long countByExample(NewCarBaseConfigExample example);

    int deleteByExample(NewCarBaseConfigExample example);

    int deleteByPrimaryKey(String basicconno);

    int insert(NewCarBaseConfig record);

    int insertSelective(NewCarBaseConfig record);

    List<NewCarBaseConfig> selectByExample(NewCarBaseConfigExample example);

    NewCarBaseConfig selectByPrimaryKey(String basicconno);

    int updateByExampleSelective(@Param("record") NewCarBaseConfig record, @Param("example") NewCarBaseConfigExample example);

    int updateByExample(@Param("record") NewCarBaseConfig record, @Param("example") NewCarBaseConfigExample example);

    int updateByPrimaryKeySelective(NewCarBaseConfig record);

    int updateByPrimaryKey(NewCarBaseConfig record);
}