package com.maolvche.dao;

import com.maolvche.pojo.BgAccount;
import com.maolvche.pojo.BgAccountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BgAccountMapper {
    long countByExample(BgAccountExample example);

    int deleteByExample(BgAccountExample example);

    int deleteByPrimaryKey(Integer admaccno);

    int insert(BgAccount record);

    int insertSelective(BgAccount record);

    List<BgAccount> selectByExample(BgAccountExample example);

    BgAccount selectByPrimaryKey(Integer admaccno);

    int updateByExampleSelective(@Param("record") BgAccount record, @Param("example") BgAccountExample example);

    int updateByExample(@Param("record") BgAccount record, @Param("example") BgAccountExample example);

    int updateByPrimaryKeySelective(BgAccount record);

    int updateByPrimaryKey(BgAccount record);
}