package com.maolvche.dao;

import com.maolvche.pojo.InstRecord;
import com.maolvche.pojo.InstRecordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InstRecordMapper {
    long countByExample(InstRecordExample example);

    int deleteByExample(InstRecordExample example);

    int deleteByPrimaryKey(Integer instlmntno);

    int insert(InstRecord record);

    int insertSelective(InstRecord record);

    List<InstRecord> selectByExample(InstRecordExample example);

    InstRecord selectByPrimaryKey(Integer instlmntno);

    int updateByExampleSelective(@Param("record") InstRecord record, @Param("example") InstRecordExample example);

    int updateByExample(@Param("record") InstRecord record, @Param("example") InstRecordExample example);

    int updateByPrimaryKeySelective(InstRecord record);

    int updateByPrimaryKey(InstRecord record);
}