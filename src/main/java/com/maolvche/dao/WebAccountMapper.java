package com.maolvche.dao;

import com.maolvche.pojo.WebAccount;
import com.maolvche.pojo.WebAccountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WebAccountMapper {
    long countByExample(WebAccountExample example);

    int deleteByExample(WebAccountExample example);

    int deleteByPrimaryKey(String useracc);

    int insert(WebAccount record);

    int insertSelective(WebAccount record);

    List<WebAccount> selectByExample(WebAccountExample example);

    WebAccount selectByPrimaryKey(String useracc);

    int updateByExampleSelective(@Param("record") WebAccount record, @Param("example") WebAccountExample example);

    int updateByExample(@Param("record") WebAccount record, @Param("example") WebAccountExample example);

    int updateByPrimaryKeySelective(WebAccount record);

    int updateByPrimaryKey(WebAccount record);
}