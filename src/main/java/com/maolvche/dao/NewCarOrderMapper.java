package com.maolvche.dao;

import com.maolvche.pojo.NewCarOrder;
import com.maolvche.pojo.NewCarOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NewCarOrderMapper {
    long countByExample(NewCarOrderExample example);

    int deleteByExample(NewCarOrderExample example);

    int deleteByPrimaryKey(String newcarorderno);

    int insert(NewCarOrder record);

    int insertSelective(NewCarOrder record);

    List<NewCarOrder> selectByExample(NewCarOrderExample example);

    NewCarOrder selectByPrimaryKey(String newcarorderno);

    int updateByExampleSelective(@Param("record") NewCarOrder record, @Param("example") NewCarOrderExample example);

    int updateByExample(@Param("record") NewCarOrder record, @Param("example") NewCarOrderExample example);

    int updateByPrimaryKeySelective(NewCarOrder record);

    int updateByPrimaryKey(NewCarOrder record);
}