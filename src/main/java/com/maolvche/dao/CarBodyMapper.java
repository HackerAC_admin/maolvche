package com.maolvche.dao;

import com.maolvche.pojo.CarBody;
import com.maolvche.pojo.CarBodyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarBodyMapper {
    long countByExample(CarBodyExample example);

    int deleteByExample(CarBodyExample example);

    int deleteByPrimaryKey(String carbodyno);

    int insert(CarBody record);

    int insertSelective(CarBody record);

    List<CarBody> selectByExample(CarBodyExample example);

    CarBody selectByPrimaryKey(String carbodyno);

    int updateByExampleSelective(@Param("record") CarBody record, @Param("example") CarBodyExample example);

    int updateByExample(@Param("record") CarBody record, @Param("example") CarBodyExample example);

    int updateByPrimaryKeySelective(CarBody record);

    int updateByPrimaryKey(CarBody record);
}