package com.maolvche.dao;

import com.maolvche.pojo.UserInstlInfo;
import com.maolvche.pojo.UserInstlInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserInstlInfoMapper {
    long countByExample(UserInstlInfoExample example);

    int deleteByExample(UserInstlInfoExample example);

    int deleteByPrimaryKey(Integer userinstlinfono);

    int insert(UserInstlInfo record);

    int insertSelective(UserInstlInfo record);

    List<UserInstlInfo> selectByExample(UserInstlInfoExample example);

    UserInstlInfo selectByPrimaryKey(Integer userinstlinfono);

    int updateByExampleSelective(@Param("record") UserInstlInfo record, @Param("example") UserInstlInfoExample example);

    int updateByExample(@Param("record") UserInstlInfo record, @Param("example") UserInstlInfoExample example);

    int updateByPrimaryKeySelective(UserInstlInfo record);

    int updateByPrimaryKey(UserInstlInfo record);
}