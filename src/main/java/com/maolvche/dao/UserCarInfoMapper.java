package com.maolvche.dao;

import com.maolvche.pojo.UserCarInfo;
import com.maolvche.pojo.UserCarInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCarInfoMapper {
    long countByExample(UserCarInfoExample example);

    int deleteByExample(UserCarInfoExample example);

    int deleteByPrimaryKey(Integer usercarinfono);

    int insert(UserCarInfo record);

    int insertSelective(UserCarInfo record);

    List<UserCarInfo> selectByExample(UserCarInfoExample example);

    UserCarInfo selectByPrimaryKey(Integer usercarinfono);

    int updateByExampleSelective(@Param("record") UserCarInfo record, @Param("example") UserCarInfoExample example);

    int updateByExample(@Param("record") UserCarInfo record, @Param("example") UserCarInfoExample example);

    int updateByPrimaryKeySelective(UserCarInfo record);

    int updateByPrimaryKey(UserCarInfo record);
}