package com.maolvche.dao;

import com.maolvche.pojo.CarEngine;
import com.maolvche.pojo.CarEngineExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarEngineMapper {
    long countByExample(CarEngineExample example);

    int deleteByExample(CarEngineExample example);

    int deleteByPrimaryKey(String carengineno);

    int insert(CarEngine record);

    int insertSelective(CarEngine record);

    List<CarEngine> selectByExample(CarEngineExample example);

    CarEngine selectByPrimaryKey(String carengineno);

    int updateByExampleSelective(@Param("record") CarEngine record, @Param("example") CarEngineExample example);

    int updateByExample(@Param("record") CarEngine record, @Param("example") CarEngineExample example);

    int updateByPrimaryKeySelective(CarEngine record);

    int updateByPrimaryKey(CarEngine record);
}