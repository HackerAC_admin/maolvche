package com.maolvche.service;

import com.maolvche.pojo.HelpGuide;

import java.util.List;

public interface BGMShelpGuideService {

    List<HelpGuide> getAllHelpGuideList();

    List<HelpGuide> queryHelpGuideList(String item,String value);

    List<HelpGuide> queryHelpGuideList2(String item,String value);

    int addHelpGuide(HelpGuide helpGuide);

    int uptHelpGuide(HelpGuide helpGuide);

    int delHelpGuide(String no);
}
