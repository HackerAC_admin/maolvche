package com.maolvche.service;

import com.maolvche.pojo.CreditFile;
import com.maolvche.pojo.Exchange;
import com.maolvche.pojo.Feedback;

import java.util.List;

public interface BGMSexchangeService {
    /**
     * 查询所有退换记录信息
     * @return 所有退换记录
     */
    List<Exchange> getAllExchange();

    /**
     * 根据退换记录编号删除
     * @param exchangeNo
     * @return
     */
    int delExchange(String exchangeNo);

    /**
     * 更新退换记录
     * @param exchange
     * @return
     */
    int uptExchange(Exchange exchange);

    /**
     * 查询项查询退换记录
     * @param query
     * @param value
     * @return
     */
    List<Exchange> queryExchanges(String query, String value);
}
