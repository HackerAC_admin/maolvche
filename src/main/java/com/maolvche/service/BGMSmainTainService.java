package com.maolvche.service;

import com.maolvche.pojo.Maintain;

import java.util.List;

public interface BGMSmainTainService {
    /**
     * 查询所有维保养护记录
     * @param maintain
     * @return
     */
    List<Maintain> selAllMaintainRecord();

    /**
     * 添加维保养护记录
     * @param maintain
     * @return
     */
    int addMaintainRecord(Maintain maintain);

    /**
     * 删除维保养护记录
     * @param maintainNo
     * @return
     */
    int delMaintainRecord(String maintainNo);

    /**
     * 根据查询项查询维保养护记录
     * @param queryitem
     * @param value
     * @return
     */
    List<Maintain> queryMaintainRecord(String queryitem ,String value);

}
