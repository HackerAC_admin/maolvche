package com.maolvche.service;

import com.maolvche.pojo.InstRecord;
import com.maolvche.pojo.Maintain;

import java.util.List;

public interface BGMSinstlRecordService {
    /**
     * 查询所有分期还款记录
     * @return
     */
    List<InstRecord> getAllInstRecords();

    /**
     * 删除分期还款记录
     * @param id
     * @return
     */
    int delInstRecord(String id);

    /**
     * 根据查询项查询分期还款记录
     * @param queryitem
     * @param value
     * @return
     */
    List<InstRecord> queryInstRecord(String queryitem ,String value);

    int addInstlRecords(InstRecord instRecord);
}
