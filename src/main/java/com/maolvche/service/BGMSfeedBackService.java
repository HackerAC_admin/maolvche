package com.maolvche.service;

import com.maolvche.pojo.Feedback;

import java.util.List;

/**
 * @author HackerAC
 */
public interface BGMSfeedBackService {
    /**
     * 查询所有投诉反馈信息
     * @return 所有投诉反馈
     */
    List<Feedback> getAllFeedBacks();

    /**
     * 根据投诉反馈编号删除
     * @param feedBackNo
     * @return
     */
    int delFeedBack(String feedBackNo);

    /**
     * 更新投诉反馈
     * @param feedback
     * @return
     */
    int uptFeedBack(Feedback feedback);
}
