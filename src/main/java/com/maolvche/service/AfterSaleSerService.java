package com.maolvche.service;

import com.maolvche.pojo.*;

import java.util.List;

/**
 * 网站用户售后服务业务接口
 * @author HackerAC
 */
public interface AfterSaleSerService {

    /**
     * 信审资料查询
     */
    List<CreditFile> getCreditFileByUserAcc(String userAcc);

    /**
     * 查询所有信审资料(后台)
     */
    List<CreditFile> getAllCreditFile();

    /**
     * 根据查找项查询用户信用资料
     */
    List<CreditFile> queryCreditFile(String query,String value);

    /**
     * 提交信审资料
     */
    int sendCreditFile(CreditFile creditFile);

    /**
     * 删除信审资料
     */
    int delCreditFile(String creditFileNo);

    /**
     * 养护记录查询
     * @param userAcc 用户账户编号
     * @return 养护记录集合
     */
    List<Maintain> getMaintainInfoByUserAcc(String userAcc);

    /**
     * 退换记录查询
     */
    List<Exchange> getExchangeByUserAcc(String userAcc);

    /**
     * 根据汽车id和用户账号查询退换记录
     *
     * @param userAcc 用户账户编号
     * @param carid 汽车id
     */
    public Exchange getExchangeByUserAccAndCarNo(String userAcc,String carid);

    /**
     * 新增退换申请
     */
    int addExchangeApplication(Exchange exchange);

    /**
     * 投诉反馈查询
     */
    List<Feedback> getFeedBackByUserAcc(String userAcc);

    /**
     * 新增投诉反馈
     */
    int addFeedbackInfo(Feedback feedback);

    /**
     * 删除投诉反馈
     * @param feedback
     * @return
     */
    int delFeedBack(Feedback feedback);

    /**
     * 分期还款记录查询
     */
    List<InstRecord> selInstRecordByUserAcc(String userAcc);

    /**
     * 根据分期方案编号查询分期方案
     */
    InstPlan selInstPlanByNo(String planNo);

    /**
     * 新增分期还款记录
     * @param instRecord 还款记录
     * @return 记录数
     */
    int addInstRecord(InstRecord instRecord);

    /**
     * 根据用户账号查询用户分期信息
     */
    UserInstlInfo selInstInfoByUserAcc(String userAcc);

    /**
     * 根据用户账号查询用户拥有的汽车
     */
    List<UserCarInfo> selUserCarByUserAcc(String userAcc);


}
