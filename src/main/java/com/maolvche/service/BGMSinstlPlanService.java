package com.maolvche.service;

import com.maolvche.pojo.InstPlan;

import java.util.List;

public interface BGMSinstlPlanService {
    /**
     * 查询所有分期方案
     * @return
     */
    List<InstPlan> getAllinstPlan();

    /**
     * 查询分期方案
     * @return
     */
    List<InstPlan> selinstPlan(InstPlan instPlan);

    /**
     * 添加新的分期方案
     * @param instPlan
     * @return
     */
    int addInstPlan(InstPlan instPlan);

    /**
     * 根据id删除分期方案
     * @param id
     * @return
     */
    int delInstPlan(String id);

}
