package com.maolvche.service;

import com.maolvche.pojo.LoginRecord;

import java.util.List;

public interface BGMSloginRecordService {
    /**
     * 获取所有用户的登录日志
     * @return
     */
    List<LoginRecord> getAllRecord();

    /**
     * 根据查询项查询登录日志
     */
    List<LoginRecord> queryLoginRecord(String item , String value);
}
