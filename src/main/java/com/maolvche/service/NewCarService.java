package com.maolvche.service;

import com.maolvche.pojo.NewCar;
import com.maolvche.vo.CarSelOption;
import com.maolvche.vo.NewCarCard;

import java.util.List;

/**
 * @author HackerAC
 */
public interface NewCarService {
    /**
     * 查询最新添加的前10辆新车
     * @return com.maolvche.vo包下的NewCarCard类List集合
     */
   List<NewCarCard> getTenLatestNewCar();

    /**
     * 根据查询条件中不为空的值进行查询
     */
    List<NewCar> selByOptions(CarSelOption carSelOption);
}
