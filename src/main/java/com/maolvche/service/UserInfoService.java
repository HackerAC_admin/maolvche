package com.maolvche.service;

import com.maolvche.pojo.UserInfo;

public interface UserInfoService {
    /**
     * 根据用户编号查询用户信息
     * @param userNo 用户编号
     * @return UserInfo
     */
    public UserInfo selByUserNo(String userNo);

    /**
     * 根据用户手机号查询用户信息
     * @param phone 用户编号
     * @return UserInfo
     */
    public UserInfo selByUserPhone(String phone);

    /**
     * 新增用户信息
     *
     */
    public int addUserInfo(UserInfo userInfo);

    /**
     * 删除用户信息
     *
     */
    public int delUserInfo(UserInfo userInfo);
}
