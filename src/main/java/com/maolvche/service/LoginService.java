package com.maolvche.service;

import com.maolvche.pojo.*;

import java.util.List;

/**
 * 本接口包含所有登录登出和日志业务。
 * @author HackerAC
 */
public interface LoginService {

    /**
     * 根据用户名和密码查询网站用户
     * @param webUserName 用户账号
     * @param pwd 密码
     * @return 网站用户
     */
    WebAccount selByUsernameAndPwd(String webUserName, String pwd);


    /**
     * 根据用户编号和密码查询网站用户
     * @param userNo 用户手机号
     * @param pwd 密码
     * @return 网站用户
     */
    WebAccount selByUserNoAndPwd(String userNo,String pwd);

    /**
     * 根据用户名和密码查询管理员用户
     * @param bgUserName 管理员用户名
     * @param pwd 密码
     * @return 管理员用户
     */
    BgAccount selAdminByUsernameAndPwd(String bgUserName, String pwd);

    /**
     * 根据用户名和密码查询员工用户
     * @param bgUserName 员工用户名
     * @param pwd 密码
     * @return 员工用户
     */
    EmpAccount selEmployByUsernameAndPwd(String bgUserName, String pwd);

    /**
     * 新增登陆日志
     * @param loginRecord 登陆日志
     */
    void insertLoginRecord(LoginRecord loginRecord);

    /**
     * 根据用户账号查询登陆记录
     * @param userAcc 用户账户编号
     * @return 日志记录集合
     */
    List<LoginRecord> selLoginRecordByUserAcc(String userAcc);

    /**
     * 删除登录日志
     * @param recordNo 将被删除的日志id
     */
    void delLoginRecord(String recordNo);

    /**
     * 修改密码
     * @param webAccount 封装新密码的webAccount对象
     * @return 成功数据条数
     */
    int uptPwdByAccAndNewPwd(WebAccount webAccount);

    /**
     * 修改用户基本信息
     *
     */
    int uptUserInfo(UserInfo userInfo);

    /**
     * 修改用户账号信息
     *
     */
    int uptUserAccInfo(WebAccount webAccount);

    /**
     * 添加用户账号信息
     *
     */
    int addUserAccInfo(WebAccount webAccount);

    /**
     * 删除用户账号信息
     *
     */
    int delUserAccInfo(WebAccount webAccount);

    /**
     * 根据用户名查询账号信息
     * @return 账号信息
     */
    WebAccount getWebAccountByUsername(String username);
}
