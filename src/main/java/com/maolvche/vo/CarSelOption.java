package com.maolvche.vo;

/**
 * 汽车查询条件包装类
 * 用于包装前端对汽车的查询条件
 * @author HackerAC
 */
public class CarSelOption {
    /**
     * 汽车品牌（厂商）
     */
    private String carFactory;

    /**
     *车型（轿车、MPV、SUV）
     */
    private String carModel;

    /**
     * 车价
     */
    private float carPrice;

    /**
     * 首付
     */
    private int firstPayment;

    /**
     * 月供
     */
    private int monthPay;

    public void setCarPrice(float carPrice) {
        this.carPrice = carPrice;
    }

    public void setCarFactory(String carFactory) {
        this.carFactory = carFactory;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public void setFirstPayment(int firstPayment) {
        this.firstPayment = firstPayment;
    }

    public void setMonthPay(int monthPay) {
        this.monthPay = monthPay;
    }

    public float getCarPrice() {
        return carPrice;
    }

    public int getFirstPayment() {
        return firstPayment;
    }

    public int getMonthPay() {
        return monthPay;
    }

    public String getCarFactory() {
        return carFactory;
    }

    public String getCarModel() {
        return carModel;
    }
}
