package com.maolvche.vo;

import com.maolvche.pojo.NewCar;
import com.maolvche.pojo.NewCarBaseConfig;

/**
 * 用于展示的新车卡片类
 * @author HackerAC
 */
public class NewCarCard {
    /**
     * 新车编号
     */
    private String newCarNo;
    /**
     * 新车名称（如：大众宝来）
     */
    private String carName;
    /**
     * 汽车型号和款式（如：2019款宝来传奇自动时尚型）
     */
    private String carType;
    /**
     * 排量或油耗（如1.5）
     */
    private String oilConsumption;
    /**
     * 车价（以万元为单位的数值）
     */
    private float carPrice;

    public void setNewCarNo(String newCarNo) {
        this.newCarNo = newCarNo;
    }

    public String getNewCarNo() {
        return newCarNo;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public void setCarPrice(float carPrice) {
        this.carPrice = carPrice;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public void setOilConsumption(String oilConsumption) {
        this.oilConsumption = oilConsumption;
    }

    public float getCarPrice() {
        return carPrice;
    }

    public String getCarName() {
        return carName;
    }

    public String getCarType() {
        return carType;
    }

    public String getOilConsumption() {
        return oilConsumption;
    }
}
