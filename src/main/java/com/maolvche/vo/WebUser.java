package com.maolvche.vo;

import com.maolvche.pojo.UserCarInfo;
import com.maolvche.pojo.UserInfo;
import com.maolvche.pojo.WebAccount;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author HackerAC
 */
@Repository
public class WebUser {
    /**
     * 用户账号信息
     */
    private WebAccount accountInfo;
    /**
     * 用户基本信息
     */
    private UserInfo userInfo;



    public void setAccountInfo(WebAccount accountInfo) {
        this.accountInfo = accountInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public WebAccount getAccountInfo() {
        return accountInfo;
    }

}
