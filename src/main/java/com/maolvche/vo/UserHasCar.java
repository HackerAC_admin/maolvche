package com.maolvche.vo;

/**
 * 用户拥有的汽车封装类
 * @author HackerAC
 */
public class UserHasCar {
    /**
     * 用户账号
     */
    private String userAcc;

    /**
     * 用户汽车编号
     */
    private String userCarId;

    /**
     * 用户汽车名称
     */
    private String userCarName;

    /**
     * 用户汽车图片地址
     */
    private String picturesrc;

    /**
     * 用户汽车型号信息
     */
    private String caridtype;



}
