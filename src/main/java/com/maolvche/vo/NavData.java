package com.maolvche.vo;

import com.maolvche.pojo.NewCar;
import com.maolvche.pojo.UserCarInfo;

import java.util.List;

/**
 * 网站实时导航栏数据信息
 * @author HackerAC
 */
public class NavData {
    /**
     * 当前城市
     */
    private String city;
    /**
     * 当前导航栏位置
     */
    private int thisNav;
    /**
     * 当前已登录用户个人信息及账号信息
     */
    private WebUser webUser;

    /**
     * 当前车源城市前10新车列表
     */
    private List<NewCar> newCarList;

    /**
     * 当前车源城市前10二手车车列表
     */
    //private List<SecondCar> secondCarList;

    public void setCity(String city) {
        this.city = city;
    }

    public void setThisNav(int thisNav) {
        this.thisNav = thisNav;
    }

    public void setWebUser(WebUser webUser) {
        this.webUser = webUser;
    }

    public int getThisNav() {
        return thisNav;
    }

    public String getCity() {
        return city;
    }

    public WebUser getWebUser() {
        return webUser;
    }
}
