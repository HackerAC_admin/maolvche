/**
 * Created by HackerAC on 2020-11-06.
 */
$(function(){
    //城市选择下拉
    $("#selCityBtn").click(function(e){
        $("#cityList").toggle();
        $("#dropDownLink").toggle();
        e.stopPropagation();
    });
    /*点击其他地方收回弹框，包括城市选择框，用户信息显示框*/
    $(document).on("click", function (e) {
        var target = $(e.target);
        if ((target.closest("#cityList").length === 0)
            &&(target.closest("#personalLogin").length === 0)
        &&(target.closest("#personalCenterLi").length === 0)) {
            $("#cityList").hide();
            $("#dropDownLink").hide();
            $(".personalDropdown").hide();
            $(".personalDropdown2").hide();
        }
    });

    //选择城市后跳转相应城市车源首页
    $(".cityName").click(function () {
        var city=$(this).text();
        self.location="cityMain?city="+city;
    });




    //个人登录
    $("#personalLogin").click(function(){
        $(".personalDropdown").toggle();
    });
    //个人中心
    $("#personalCenterLi").click(function(){
        $(".personalDropdown2").toggle();
    });

    //当前所在页判断
    const thisNav = $("#thisNav").val();
    if(thisNav==="1"){
        //当前在首页，设置导航栏首页被选中。
        $(".nav-item").eq(1).addClass("active");
    }else if(thisNav==="2"){
        //当前在买新车页面，设置导航栏买新车被选中。
        $(".nav-item").eq(2).addClass("active");
    }else if(thisNav==="3"){
        $(".nav-item").eq(3).addClass("active");
    }else if(thisNav==="4"){
        $(".nav-item").eq(4).addClass("active");
    }else if(thisNav==="5"){
        $(".nav-item").eq(5).addClass("active");
    }else if(thisNav==="6"){
        $(".nav-item").eq(6).addClass("active");
    }else if(thisNav==="7"){
        $(".nav-item").eq(7).addClass("active");
    }else{
        //当前不属于任何导航栏指示页面，移除导航栏选中状态
        $(".nav-item").removeClass("active");
    }

    //是否登录判断
    const ifLoginInfo = $("#info").val();
    //如果是已经登陆，则显示个人中心，隐藏个人登录。
    if(ifLoginInfo==="200"){
        const personalCenter = $("#personalCenterLi");
        personalCenter.siblings().hide();
        personalCenter.show();
    }else{
        const personalCenter = $("#personalCenterLi");
        personalCenter.hide();
        personalCenter.siblings().show();
    }
});
