/**
 * Created by HackerAC on 2020-11-07.
 */
$(function(){
    gotoTop();
});
//回顶部
function gotoTop(minHeight) {
    $("#toTop").click(function () {
        $('html,body').animate({scrollTop: '0px'}, 'slow');
    });
    // 获取页面的最小高度，无传入值则默认为600像素
    minHeight?minHeight = minHeight:minHeight = 600;
    $(window).scroll(function(){
        // 获取窗口的滚动条的垂直滚动距离
        var s = $(window).scrollTop();
        // 当窗口的滚动条的垂直距离大于页面的最小高度时，让返回顶部图标渐现，否则渐隐
        if( s > minHeight){
            $("#toTop").fadeIn(500);
        }else{
            $("#toTop").fadeOut(500);
        };
    });
}