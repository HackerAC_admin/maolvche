/**
 * Created by HackerAC on 2020-11-08.
 */
$(function(){
    //计算首付和月供
    calculateData();
    //变动分期数时，同步更新首付和月供
    $("#qishu").change(function(){
        calculateData();
    });
    //点击首付时，计算并同步首付和月供
    $(".finance ul li").click(function(){
        $(this).addClass("activeFinance");
        $(this).siblings().removeClass("activeFinance");
        calculateData();
    });
    //查看全部配置
    $(".carAllInfoBtn").click(function(){
        $(".carAllInfoBody").toggle();
        $("html,body").animate({scrollTop:550},300);
    });

    //全款买新车
    $("#buyCarBtnAml").click(function(){
        self.location="toBuyByAml1";
    });
});
function calculateData(){
    //获取车价
    var price=$(".showCarRight #price").text();
    //获取分期期数
    var times = $("#qishu").val();
    //获取首付比例
    var percent = $(".activeFinance label").text();
    //计算首付金额
    var shoufu=(parseFloat(price)*(parseFloat(percent)/100));

    //计算管理费，税费，利息等额外费用,综合得每期利息为购车价的1.6%。
    var otherFee=(parseFloat(price)*10000*0.016)*parseInt(times);
    //计算月供金额
    var yugong =(parseFloat(price)*10000+parseFloat(otherFee)-(parseFloat(shoufu)*10000))/parseInt(times);
    //显示
    $(".shoufu label").text(shoufu.toFixed(3));
    $(".yuegong label").text(yugong.toFixed(0));
}
