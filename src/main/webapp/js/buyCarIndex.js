/**
 * Created by HackerAC on 2020-11-06.
 */
$(function(){
    //当用户进行筛选时
    $(".newCarSel table tr td ul li").click(function(){
        $(this).addClass("selActive");
        $(this).siblings().removeClass("selActive");

        //获取每行被选择的值
        var pingPai=$(".pingPai td ul .selActive").children("a").get(0).innerHTML;
        var cheXing=$(".cheXing td ul .selActive").children("a").get(0).innerHTML;
        var cheJia=$(".cheJia td ul .selActive").children("a").get(0).innerHTML;
        var shouFu=$(".shouFu td ul .selActive").children("a").get(0).innerHTML;
        var yueGong=$(".yueGong td ul .selActive").children("a").get(0).innerHTML;

        //用于存放每行被选择的值
        var $liNode1=" ";
        var $liNode2=" ";
        var $liNode3=" ";
        var $liNode4=" ";
        var $liNode5=" ";

        //用于存放每行被选择的值,并赋值
        $liNode1=(pingPai!=="全部")?"<li><a href='#' name='pingPai'>"+pingPai+"<i>X</i></a></li>":" ";
        $liNode2=(cheXing!=="全部")?"<li><a href='#' name='cheXing'>" + cheXing + "<i>X</i></a></li>":" ";
        $liNode3=(cheJia!=="全部")?"<li><a href='#' name='cheJia'>"+cheJia+"<i>X</i></a></li>":" ";
        $liNode4=(shouFu!=="全部")?"<li><a href='#' name='shouFu'>"+shouFu+"<i>X</i></a></li>":" ";
        $liNode5=(yueGong!=="全部")?"<li><a href='#' name='yueGong'>"+yueGong+"<i>X</i></a></li>":" ";


        //追加之前先清空之前的内容
        var options = $("#options");
        options.empty();

        //追加非空节点
        options.append($liNode1);
        options.append($liNode2);
        options.append($liNode3);
        options.append($liNode4);
        options.append($liNode5);

        //alert(pingPai+"/"+cheXing+"/"+cheJia+"/"+shouFu+"/"+yueGong);
    });

    //当用户删除选项时
    $("body").on("click",".selOption i",function(){
        //确定被删除的选项所属的类别（品牌、车型、车价...）
        var selKind=$(this).parent().attr("name");
        var selJindName="."+selKind;

        //移除相应类别选项的选择状态
        var clickLine=$(".newCarSel table "+selJindName+" td li:first");
        clickLine.addClass("selActive");
        clickLine.siblings().removeClass("selActive");

        //删除用户想要删除的选项
        $(this).parent().parent().remove();
    });

    //点击车辆图片时，跳转到车辆详情页
    $(".selShowList ul li").click(function(){
        self.location="newCarDetail";
        //使用ajax请求控制器，

    });
});
