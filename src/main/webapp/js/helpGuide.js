/**
 * Created by HackerAC on 2020-11-21.
 */
$(function(){

    const navid = $("#navId").val();
    const amite = $("ul li");
    amite.eq(navid).addClass("active");
    amite.eq(navid).siblings().removeClass("active");
    amite.eq(navid).find("a")[0].click();

    amite.click(function(){
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
    });

});
