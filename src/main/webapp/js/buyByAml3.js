/**
 * Created by HackerAC on 2020-11-09.
 */
$(function(){
    //点击上一步按钮
    $("#preStep").click(function(){
        self.location="toBuyByAml2";
    });
    //点击取消按钮
    $("#backToCarInfo").click(function(){
        self.location="newCarDetail";
    });
    //点击完成按钮
    $("#nextStep").click(function(){
        self.location="toPersonalCenter";
    });

});