/**
 * @Author Hackerac
 */
//总数据条数、当前页数
var totalRecord,currentPage;
$(function(){
    //清除复选框样式
    $("#check_all").removeProp("checked");
    $(".check_item").removeProp("checked");
    //页面加载完成时开始查询 管理员列表，并将结果渲染显示
    bgadminList(1);

});

/*请求并解析 管理员列表*/
function bgadminList(pn){
    $.ajax({
        url:"../backMgt/BgAdminList.do",
        data:"pn="+pn,
        type:"GET",
        success:function(result){
            //1、解析并渲染显示 管理员列表
            console.log(result);
            build_admin_table(result);
            //2、解析并显示分页信息
            build_page_info(result);
            //3、解析显示分页条数据
            build_page_nav(result);
        }
    });
}
/*将数据渲染成表格*/
function build_admin_table(result){
    //清空table表格
    $("#admins_table tbody").empty();

    var Admins = result.extend.bgadminList.list;
    $.each(Admins,function(index,item){
        var checkBoxTd = $("<td><input type='checkbox' class='check_item'/></td>");
        var aa = $("<td></td>").append(item.bgGlybh);
        var bb = $("<td></td>").append(item.bgGlyxm);
        var cc = $("<td></td>").append(item.bgGlyxb);
        var dd = $("<td></td>").append(item.bgGlyjs);
        var ff = $("<td></td>").append(item.bgGlysj);
        var gg = $("<td></td>").append(item.bgGlyyx);
        //编辑按钮
        var editBtn = $("<button></button>").addClass("btn btn-primary btn-sm edit_btn")
            .append($("<span></span>").addClass("glyphicon glyphicon-pencil")).append("编辑");
        //为编辑按钮添加一个自定义的属性，来表示当前 管理员id
        editBtn.attr("edit-id",item.bgGlybh);
        //删除按钮
        var delBtn =  $("<button></button>").addClass("btn btn-danger btn-sm delete_btn")
            .append($("<span></span>").addClass("glyphicon glyphicon-trash")).append("删除");
        //为删除按钮添加一个自定义的属性来表示当前删除的 id
        delBtn.attr("del-id",item.bgGlybh);
        //编辑按钮和删除按钮
        var btnTd = $("<td></td>").append(editBtn).append(" ").append(delBtn);

        //将数据渲染到表格显示
        $("<tr></tr>").append(checkBoxTd)
            .append(aa)
            .append(bb)
            .append(cc)
            .append(dd)
            .append(ff)
            .append(gg)
            .append(btnTd)
            .appendTo("#admins_table tbody");
    });
}
////////////////////////////分页逻辑/////////////////////////////////////////////////////////////////
//解析显示分页信息
function build_page_info(result){
    //清空之前的显示
    $("#page_info_area").empty();
    //分页信息
    $("#page_info_area").append("当前"+result.extend.bgadminList.pageNum+"页,总"+
        result.extend.bgadminList.pages+"页,总"+
        result.extend.bgadminList.total+"条记录");
    totalRecord = result.extend.bgadminList.total;
    currentPage = result.extend.bgadminList.pageNum;
}
//解析显示分页条
function build_page_nav(result){
    //清空上一次的显示
    $("#page_nav_area").empty();
    //清除复选框样式
    $("#check_all").removeProp("checked");
    $(".check_item").removeProp("checked");

    var ul = $("<ul></ul>").addClass("pagination");

    //构建首页、末页元素
    var firstPageLi = $("<li></li>").append($("<a></a>").append("首页").attr("href","#"));
    var prePageLi = $("<li></li>").append($("<a></a>").append("&laquo;"));
    //如果当前没有前一页，首页按钮和前一页按钮不可用。
    if(result.extend.bgadminList.hasPreviousPage == false){
        firstPageLi.addClass("disabled");
        prePageLi.addClass("disabled");
    }else{
        //跳转到第一页
        firstPageLi.click(function(){
            bgadminList(1);
        });
        //跳转到当前页减1页
        prePageLi.click(function(){
            bgadminList(result.extend.bgadminList.pageNum -1);
        });
    }
    //下一页
    var nextPageLi = $("<li></li>").append($("<a></a>").append("&raquo;"));
    //末页
    var lastPageLi = $("<li></li>").append($("<a></a>").append("末页").attr("href","#"));
    //当前页没有下一页时，下一页和末页按钮不可用。
    if(result.extend.bgadminList.hasNextPage == false){
        nextPageLi.addClass("disabled");
        lastPageLi.addClass("disabled");
    }else{
        //点击下一页，跳转到当前页+1页
        nextPageLi.click(function(){
            bgadminList(result.extend.bgadminList.pageNum +1);
        });
        //跳转到末页
        lastPageLi.click(function(){
            bgadminList(result.extend.bgadminList.pages);
        });
    }

    //添加首页和前一页到分页条
    ul.append(firstPageLi).append(prePageLi);
    //页码遍历显示到分页条
    $.each(result.extend.bgadminList.navigatepageNums,function(index,item){

        var numLi = $("<li></li>").append($("<a></a>").append(item));
        if(result.extend.bgadminList.pageNum == item){
            numLi.addClass("active");
        }
        numLi.click(function(){
            bgadminList(item);
        });
        ul.append(numLi);
    });
    //添加下一页和末页到分页条
    ul.append(nextPageLi).append(lastPageLi);

    //将分页条显示到分页区
    var navEle = $("<nav></nav>").append(ul);
    navEle.appendTo("#page_nav_area");
}
////////////////////////////////////新增业务///////////////////////////////////////////////////////
//清空表单样式及内容
function reset_form(ele){
    //清空表单数据
    $(ele)[0].reset();
    //清空表单样式
    $(ele).find("*").removeClass("has-error has-success");
    $(ele).find(".help-block").text("");
    //清除复选框样式
    $("#check_all").removeProp("checked");
    $(".check_item").removeProp("checked");
}

//点击新增按钮弹出新增模态框。
$("#admin_add_modal_btn").click(function(){
    //清除表单数据（表单完整重置（表单的数据，表单的样式））
    reset_form("#AdminsAddModal form");
    //发送ajax请求，查出管理员信息，显示在下拉列表中
    getOtherInfosa("#adminRoleSele");
    //弹出模态框
    $("#AdminsAddModal").modal({
        backdrop:"static"
    });
});

//查出所有的管理员信息并显示在下拉列表中
function getOtherInfosa(ele){
    //清空之前下拉列表的值
    $(ele).empty();
    //更新下拉列表
    $.ajax({
        url:"../backMgt/Role.do",
        type:"GET",
        success:function(result){
            console.log(result);
            $.each(result.extend.Roles,function(){
                var optionEle = $("<option></option>").append(this.bgJsm).attr("value",this.bgJsbh);
                optionEle.appendTo(ele);
            });
        }
    });

}

//校验表单数据
function validate_add_form1(){
    //校验 管理员编号
    var AdminNum = $("#adminBh_add_input").val();
    var regNum = /^[a-zA-Z0-9_-]{8}$/;
    if(!regNum.test(AdminNum)){
        show_validate_msg("#adminBh_add_input", "error", " 管理员编号为8位字母数字序列");
        return false;
    }else{
        show_validate_msg("#adminBh_add_input", "success", "");
    };

    //校验邮箱
    var email = $("#email_add_input").val();
    var regEmail = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
    if(!regEmail.test(email)){
        show_validate_msg("#email_add_input", "error", "邮箱格式不正确");
        return false;
    }else{
        show_validate_msg("#email_add_input", "success", "");
    }
    return true;
}

//校验提示信息函数
function show_validate_msg(ele,status,msg){
    //清除当前元素的校验状态
    $(ele).parent().removeClass("has-success has-error");
    $(ele).next("span").text("");
    if("success"==status){
        $(ele).parent().addClass("has-success");
        $(ele).next("span").text(msg);
    }else if("error" == status){
        $(ele).parent().addClass("has-error");
        $(ele).next("span").text(msg);
    }
}

//校验 管理员编号是否可用
$("#adminBh_add_input").change(function(){
    //发送ajax请求校验 管理员编号是否可用
    var bgGlybh = this.value;
    $.ajax({
        url:"../backMgt/checkAdminBh.do",
        data:"bgGlybh="+bgGlybh,
        type:"POST",
        success:function(result){
            if(result.code==100){
                show_validate_msg("#adminBh_add_input","success"," 管理员编号可用");
                $("#Admin_save_btn").attr("ajax-va","success");
            }else{
                show_validate_msg("#adminBh_add_input","error",result.extend.va_msg);
                $("#Admin_save_btn").attr("ajax-va","error");
            }
        }
    });
});
//点击保存，保存 管理员。(前后端双重校验，保证数据安全)
$("#Admin_save_btn").click(function(){
    //1、模态框中填写的表单数据提交给服务器进行保存
    //1、先对要提交给服务器的数据进行校验
    if(!validate_add_form1()){
        return false;
    };
    //1、判断之前的ajax 管理员编号校验是否成功。如果校验失败，则取消保存操作。
    if($(this).attr("ajax-va")=="error"){
        return false;
    }
    //获取表单输入的值
    var adminBh=$("#adminBh_add_input").val();
    var adminxm=$("#adminxm_add_input").val();
    var Adminxb=$("#Adminxb_add_input").val();
    var adminRoleSele=$("#adminRoleSele").val();
    var phoneNum=$("#phoneNum_add_input").val();
    var email=$("#email_add_input").val();


    //2、发送ajax请求保存 管理员信息
    $.ajax({
        url:"../backMgt/BgAdmin.do",
        type:"POST",
        data:{
            "bgGlybh": adminBh,
            "bgGlyxm": adminxm,
            "bgGlyxb": Adminxb,
            "bgGlyjs": adminRoleSele,
            "bgGlysj": phoneNum,
            "bgGlyyx": email
        },
        dataType:"json",
        success:function(result){
            //返回的自定义状态码为100(表示表示添加成功)。
            if(result.code == 100){
                //1、关闭模态框
                $("#AdminsAddModal").modal('hide');
                //2、跳转到最后一页，显示刚才保存的数据
                //发送ajax请求显示最后一页数据即可
                bgadminList(totalRecord);
            }else{
                console.log(result);
                //显示失败信息
                //有哪个字段的错误信息就显示哪个字段的；
                if(undefined != result.extend.errorInfos.bgGlyyx){
                    //显示邮箱错误信息
                    show_validate_msg("#email_add_input", "error", result.extend.errorInfos.bgGlyyx);
                }
                if(undefined != result.extend.errorInfos.bgGlyzh){
                    //显示 管理员编号的错误信息
                    show_validate_msg("#adminBh_add_input", "error", result.extend.errorInfos.bgGlybh);
                }
            }
        }
    });
});
///////////////////////////////// 管理员信息编辑///////////////////////////////////////////////////

$(document).on("click",".edit_btn",function(){
    //1、查出管理员信息，并显示管理员选择列表
    getOtherInfosa("#AdminsUpdateModal select");
    //2、查出 管理员信息，显示 管理员信息
    getAdmin($(this).attr("edit-id"));
    //清空复选框样式
    $("#check_all").removeProp("checked");
    $(".check_item").removeProp("checked");
    //3、把 管理员的id传递给模态框的更新按钮
    $("#Admin_update_btn").attr("edit-id",$(this).attr("edit-id"));
    $("#AdminsUpdateModal").modal({
        backdrop:"static"
    });
});

function getAdmin(id){
    $.ajax({
        url:"../backMgt/BgAdmin.do",
        data:"bgGlybh="+id,
        type:"GET",
        success:function(result){
            console.log(result);
            var AdminData = result.extend.bgAdmin;
            $("#Adminbh_update_input").val(AdminData.bgGlybh);
            $("#Adminxm_update_input").val(AdminData.bgGlyxm);
            $("#Adminxb_update_input").val(AdminData.bgGlyxb);
            $("#adminRoleSele2").val([AdminData.bgGlyjs]);
            $("#phoneNum_update_input").val(AdminData.bgGlysj);
            $("#email_update_input").val(AdminData.bgGlyyx);
            //清除上一次选择状态
            $("#email_update_input input[name=bgGlyztai]").removeAttr("checked");
        }
    });
}

//点击更新，更新 信息
$("#Admin_update_btn").click(function(){
    //验证邮箱是否合法
    //1、校验邮箱信息
    var email = $("#email_update_input").val();
    var regEmail = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
    if(!regEmail.test(email)){
        show_validate_msg("#email_update_input", "error", "邮箱格式不正确");
        return false;
    }else{
        show_validate_msg("#email_update_input", "success", "");
    }

    //2、发送ajax请求保存更新的 数据
    $.ajax({
        url:"../backMgt/updBgAdmin.do",
        type:"POST",
        data:"bgGlybh="+$("#Adminbh_update_input").val()+"&"+$("#AdminsUpdateModal form").serialize(),
        success:function(result){
            //1、关闭对话框
            $("#AdminsUpdateModal").modal("hide");
            //2、回到本页面
            bgadminList(currentPage);
        }
    });
});
//////////////////////////////////////删除 管理员数据///////////////////////////////////////////
//单个删除
$(document).on("click",".delete_btn",function(){
    //1、弹出是否确认删除对话框
    var AdminNum = $(this).parents("tr").find("td:eq(2)").text();
    var bgGlybh = $(this).attr("del-id");
    if(confirm("确认删除【"+AdminNum+"】吗？")){
        //确认，发送ajax请求删除即可
        $.ajax({
            url:"../backMgt/delBgAdmin.do",
            type:"POST",
            data:"bgGlybh="+bgGlybh,
            success:function(result){
                //回到本页
                bgadminList(currentPage);
            }
        });
    }
});
//-------------------------------------全选/全不选，批量删除--------------------------
$("#check_all").click(function(){
    //prop修改和读取dom原生属性的值
    $(".check_item").prop("checked",$(this).prop("checked"));
});

//check_item
$(document).on("click",".check_item",function(){
    //判断当前所有复选框是否全选中。
    var flag = $(".check_item:checked").length==$(".check_item").length;
    $("#check_all").prop("checked",flag);
});

//点击全部删除，就批量删除
$("#admin_delete_all_btn").click(function(){
    // 管理员昵称
    var bgGlyncs = "";
    //将要被删除的 管理员的 管理员编号
    var del_bgGlybhs = "";
    $.each($(".check_item:checked"),function(){
        //this
        bgGlyncs += $(this).parents("tr").find("td:eq(2)").text()+",";
        //组装 管理员编号字符串
        del_bgGlybhs += $(this).parents("tr").find("td:eq(1)").text()+"-";
    });
    //去除bgGlyncs多余的逗号分隔符
    bgGlyncs = bgGlyncs.substring(0, bgGlyncs.length-1);
    //去除删除的 管理员编号多余的短横线分隔符
    del_bgGlybhs = del_bgGlybhs.substring(0, del_bgGlybhs.length-1);
    if(confirm("确认删除【"+bgGlyncs+"】吗？")){
        //发送ajax请求批量删除
        $.ajax({
            url:"../backMgt/delBatchBgAdmin.do",
            data:"bgGlybhs="+del_bgGlybhs,
            type:"GET",
            success:function(result){
                alert(result.msg);
                //清除复选框样式
                $("#check_all").removeProp("checked");
                $(".check_item").removeProp("checked");
                //回到当前页面
                bgadminList(currentPage);
            }
        });
    }
});
/////////////////////////////////////搜索框//////////////////////////
//点击搜索按钮时,提交ajax请求查询 管理员
$("#queryAdmin").click(function(){
    var queryItem=$("#queryItem").val();
    var itemValue=$("#itemValue").val();
    $.ajax({
        url:"../backMgt/SearchBgAdmin.do",
        data:"queryItem="+queryItem+"&itemValue="+itemValue,
        type:"GET",
        success:function(result){

            console.log(result);
            //将查询结果渲染成表格显示
            build_admin_table(result);
        }
    });
});