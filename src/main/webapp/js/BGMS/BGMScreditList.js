
$(function(){
    //页面加载完成时查询并渲染显示数据列表
    buildTableList();
    /////////////////////////////////////搜索框//////////////////////////
//点击搜索按钮时,提交ajax请求查询
    $("#queryCreditFiles").click(function(){
        var queryItem=$("#queryItem").val();
        var itemValue=$("#itemValue").val();
        $.ajax({
            url:"queryBGMScreditFiles",
            data:"queryItem="+queryItem+"&itemValue="+itemValue,
            type:"GET",
            success:function(result){
                $("#creditFileTab tbody").empty();
                $.each(result, function (index, item) {
                    var fileA = $("<a target='_blank'></a>").attr("href", item.filesrc).append(item.filename);
                    var file = $("<td></td>").append(fileA);
                    var filename = $("<td></td>").append(item.filename);
                    var time = $("<td></td>").append(item.sendtime);
                    var topic = $("<td></td>").append(item.topic);
                    var userAcc = $("<td></td>").append(item.useracc);
                    const delbtn = $("<button></button>").addClass('btn btn-danger btn-sm delCreditFileBtn')
                        .append($("<span></span>").addClass("glyphicon glyphicon-trash")).append("删除");
                    //为删除按钮添加一个自定义的属性来表示当前删除的 id
                    delbtn.attr("del-id", item.creaditfileno);
                    var btnTad = $("<td></td>").append(delbtn);
                    $("<tr></tr>").append(file)
                        .append(filename)
                        .append(time)
                        .append(topic)
                        .append(userAcc)
                        .append(btnTad)
                        .appendTo("#creditFileTab tbody");
                });
            }
        });
    });
});

//ajax进行数据查询请求发送，并解析查询到的数据到页面中显示
function buildTableList() {
    $.ajax({
        url: "BGMScreditFiles",
        data: "",
        type: "GET",
        success: function (result1) {
            $("#creditFileTab tbody").empty();
            $.each(result1, function (index, item) {
                var fileA =$("<a target='_blank'></a>").attr("href",item.filesrc).append(item.filename);
                var file = $("<td></td>").append(fileA);
                var filename = $("<td></td>").append(item.filename);
                var time = $("<td></td>").append(item.sendtime);
                var topic = $("<td></td>").append(item.topic);
                var userAcc = $("<td></td>").append(item.useracc);
                const delbtn = $("<button></button>").addClass('btn btn-danger btn-sm delCreditFileBtn')
                    .append($("<span></span>").addClass("glyphicon glyphicon-trash")).append("删除");
                //为删除按钮添加一个自定义的属性来表示当前删除的 id
                delbtn.attr("del-id", item.creaditfileno);
                var btnTad = $("<td></td>").append(delbtn);
                $("<tr></tr>").append(file)
                    .append(filename)
                    .append(time)
                    .append(topic)
                    .append(userAcc)
                    .append(btnTad)
                    .appendTo("#creditFileTab tbody");
            });
        }
    });
}

//删除信用资料
$(document).on("click", ".delCreditFileBtn", function () {
    //1.弹出是否确认删除信用资料
    var delfileNo = $(this).attr("del-id");
    if (confirm("确认删除这个信用资料吗？")) {
        //2.确认，发送ajax请求删除
        $.ajax({
            url: "delCreditFile",
            type: "POST",
            data: "creditFileNo=" + delfileNo,
            success: function (result) {
                if(result===1){
                    alert("删除成功！");
                    //刷新并显示信用资料
                    buildTableList();
                }else{
                    alert("删除失败！")
                }
            }
        });
    }
});

