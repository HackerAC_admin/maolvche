
$(function(){
    //页面加载完成时开始查询并将结果渲染显示
    buildTableList();

    /////////////////////////////////////搜索框//////////////////////////
    //点击搜索按钮时,提交ajax请求查询
    $("#queryHelpGuide").click(function(){
        var queryItem=$("#queryItem").val();
        var itemValue=$("#itemValue").val();
        $.ajax({
            url:"queryHelpGuide",
            data:"queryItem="+queryItem+"&itemValue="+itemValue,
            type:"GET",
            success:function(result){
                console.log(result);
                //将查询结果渲染成表格显示
                buildTable(result);
            }
        });
    });

    //点击提交添加按钮完成帮助向导信息添加。
    $("#submitAdd").click(function() {
        const module = $("#module").val();
        const topic = $("#topic").val();
        const content = $("#content").val();
        const state = $("#state").val();

        if(module===""||topic===""||content===""){
            alert("输入数据不能为空！");
            return false;
        }else{
            $.ajax({
                url: "addHelpGuide",
                data: {"module":module,"topic":topic,"content":content,"state":state},
                type: "POST",
                success: function (result) {
                    if(result===1){
                        alert("添加成功！");
                        window.location.reload();
                    }
                }
            });
        }
    });
});

//ajax进行数据查询请求发送，并解析查询到的数据到页面中显示
function buildTableList() {
    $.ajax({
        url:"helpGuides",
        data:"",
        type:"GET",
        success:function(result){
            console.log(result);
            buildTable(result);
        }
    });
}
/*将数据渲染成表格*/
function buildTable(result){
    //清空table表格
    $("#HelpGuideTable tbody").empty();
    $.each(result,function(index,item){
        const td1 = $("<td></td>").append(item.module);
        const td2 = $("<td></td>").append(item.topic);
        const td3 = $("<td></td>").append(item.content);
        const td4 = $("<td></td>").append(item.state);
        const td5 = $("<td></td>").append(item.updatetime);

        //删除按钮
        const delBtn =  $("<button></button>").addClass("btn btn-danger btn-sm deleteBtn").append("删除");
        //为删除按钮添加一个自定义的属性来表示当前instPlan的id
        delBtn.attr("delNo",item.helpguidno);

        //编辑按钮和删除按钮添加进去显示
        const btnTd = $("<td></td>").append(delBtn);

        //将数据渲染到表格显示
        $("<tr></tr>").append(td1)
            .append(td2)
            .append(td3)
            .append(td4)
            .append(td5)
            .append(btnTd)
            .appendTo("#HelpGuideTable tbody");
    });
}

//删除帮助向导
$(document).on("click", ".deleteBtn", function () {
    //1、弹出是否确认删除对话框
    var helpGuideNo = $(this).attr("delNo");
    if (confirm("确认删除该条帮助向导信息吗？")) {
        //确认，发送ajax请求删除
        $.ajax({
            url: "delHelpGuide",
            type: "POST",
            data: "id=" + helpGuideNo,
            success: function (result) {
                if(result===1){
                    alert("删除成功！");
                    //刷新并显示反馈列表
                    buildTableList();
                }

            }
        });
    }
});

