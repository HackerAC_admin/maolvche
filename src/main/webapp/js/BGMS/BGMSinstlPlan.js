
$(function(){
    //页面加载完成时开始查询并将结果渲染显示
    buildTableList();

    //点击提交添加按钮完成分期方案添加。
    $("#submitAdd").click(function() {
        const planNo = $("#planNo").val();
        const percent = $("#percent").val();
        const times = $("#times").val();

        if(planNo===""||percent===""||times===""){
            alert("输入数据不能为空！");
            return false;
        }else{
            $.ajax({
                url: "addInstlPlan",
                data: {"planNo":planNo,"percent":percent,"times":times},
                type: "POST",
                success: function (result) {
                    if(result==="planNoError"){
                        alert("编号不可用！");
                    }else if(result==="planError"){
                        alert("该类分期方案已存在");
                    }else if(result==="bigError"){
                        alert("未知错误，添加失败");
                    }else{
                        alert("添加成功！")
                        window.location.reload();
                    }

                }
            });
        }
    });

    /////////////////////////////////////搜索框//////////////////////////
//点击搜索按钮时,提交ajax请求查询
    $("#queryAdmin").click(function(){
        var queryItem=$("#queryItem").val();
        var itemValue=$("#itemValue").val();
        $.ajax({
            url:"queryInstlPlan",
            data:"queryItem="+queryItem+"&itemValue="+itemValue,
            type:"GET",
            success:function(result){
                console.log(result);
                //将查询结果渲染成表格显示
                buildTable(result);
            }
        });
    });
});

//ajax进行数据查询请求发送，并解析查询到的数据到页面中显示
function buildTableList() {
    $.ajax({
        url:"instlPlans",
        data:"",
        type:"GET",
        success:function(result){
            console.log(result);
            buildTable(result);
        }
    });
}
/*将数据渲染成表格*/
function buildTable(result){
    //清空table表格
    $("#InstlPlanTable tbody").empty();
    $.each(result,function(index,item){
        const td1 = $("<td></td>").append(item.planno);
        const td2 = $("<td></td>").append(item.percent);
        const td3 = $("<td></td>").append(item.times);

        //删除按钮
        const delBtn =  $("<button></button>").addClass("btn btn-danger btn-sm deleteBtn").append("废除");
        //为删除按钮添加一个自定义的属性来表示当前instPlan的id
        delBtn.attr("delNo",item.planno);
        //编辑按钮和删除按钮添加进去显示
        const btnTd = $("<td></td>").append(delBtn);

        //将数据渲染到表格显示
        $("<tr></tr>").append(td1)
            .append(td2)
            .append(td3)
            .append(btnTd)
            .appendTo("#InstlPlanTable tbody");
    });
}

//废除分期方案
$(document).on("click", ".deleteBtn", function () {
    //1、弹出是否确认删除对话框
    var planno = $(this).attr("delNo");
    if (confirm("确认废除该条方案吗？")) {
        //确认，发送ajax请求删除
        $.ajax({
            url: "delFeedbackRecord",
            type: "POST",
            data: "fbid=" + planno,
            success: function (result) {
                if(result===1){
                    alert("废除成功！");
                    window.location.reload();
                }else{
                    alert("该数据被其他数据引用，无法废除！");
                }

            }
        });
    }
});

