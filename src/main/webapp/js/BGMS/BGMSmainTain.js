$(function(){
    //页面加载完成时开始查询并将结果渲染显示
    buildTableList();

    //点击提交添加按钮完成维保记录添加。
    $("#submitAdd").click(function() {
        const useracc = $("#useracc").val();
        const carNo = $("#carNo").val();
        const MaintainItems = $("#MaintainItems").val();
        const price = $("#price").val();

        if(useracc===""||carNo===""||MaintainItems===""||price===""){
            alert("输入数据不能为空！");
            return false;
        }else{
            $.ajax({
                url: "addMaintainRecord",
                data: {"useracc":useracc,"carNo":carNo,"MaintainItems":MaintainItems,"price":price},
                type: "POST",
                success: function (result) {
                    if(result===1){
                        alert("添加成功！");
                        window.location.reload();
                    }else{
                        alert("添加失败！");
                    }
                }
            });
        }
    });

    /////////////////////////////////////搜索框//////////////////////////
//点击搜索按钮时,提交ajax请求查询
    $("#queryAdmin").click(function(){
        var queryItem=$("#queryItem").val();
        var itemValue=$("#itemValue").val();
        $.ajax({
            url:"queryMaintainRecord",
            data:"queryItem="+queryItem+"&itemValue="+itemValue,
            type:"GET",
            success:function(result){
                console.log(result);
                //将查询结果渲染成表格显示
                buildTable(result);
            }
        });
    });
});

//ajax进行数据查询请求发送，并解析查询到的数据到页面中显示
function buildTableList() {
    $.ajax({
        url:"BGmaintainRecord",
        data:"",
        type:"GET",
        success:function(result){
            console.log(result);
            buildTable(result);
        }
    });
}
/*将数据渲染成表格*/
function buildTable(result){
    //清空table表格
    $("#maintainTab tbody").empty();
    $.each(result,function(index,item){
        const td1 = $("<td></td>").append(item.useracc);
        const td2 = $("<td></td>").append(item.carno);
        const td3 = $("<td></td>").append(item.maintainitem);
        const td4 = $("<td></td>").append(item.price);
        const td5 = $("<td></td>").append(item.maintaindate);
        const td6 = $("<td></td>").append(item.employeeno);

        //删除按钮
        const delBtn =  $("<button></button>").addClass("btn btn-danger btn-sm deleteBtn").append("删除");
        //为删除按钮添加一个自定义的属性来表示当前maintain的id
        delBtn.attr("delNo",item.maintainno);
        //删除按钮添加进去显示
        const btnTd = $("<td></td>").append(delBtn);

        //将数据渲染到表格显示
        $("<tr></tr>").append(td1)
            .append(td2)
            .append(td3)
            .append(td4)
            .append(td5)
            .append(td6)
            .append(btnTd)
            .appendTo("#maintainTab tbody");
    });
}

//删除
$(document).on("click", ".deleteBtn", function () {
    //1、弹出是否确认删除对话框
    var id = $(this).attr("delNo");
    if (confirm("确认删除吗？")) {
        //确认，发送ajax请求删除
        $.ajax({
            url: "delMaintainRecord",
            type: "POST",
            data: "id=" + id,
            success: function (result) {
                if(result===1){
                    alert("废除成功！");
                    //刷新并显示反馈列表
                    buildTableList();
                }

            }
        });
    }
});

