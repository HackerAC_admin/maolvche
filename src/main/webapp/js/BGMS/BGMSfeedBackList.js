
$(function(){
    //页面加载完成时开始查询显示第一页，并将结果渲染显示
    buildTableList();

    //点击提交回复按钮完成回复。
    $("#submitReplay").click(function() {
        const feedbackid=$("#tempNo").val();
        const content = $("#replayInput").val();
        if(feedbackid===""||content===""){
            alert("请输入回复！");
            return false;
        }else{
        $.ajax({
            url: "addFeedbackReplay",
            data: {"feedbackid":feedbackid,"content":content},
            type: "POST",
            success: function (result) {
                if(result===1){
                    alert("回复成功");
                }else{
                    alert("回复失败");
                }
                window.location.reload();
            }
        });
        }
    });
});

//ajax进行数据查询请求发送，并解析查询到的数据到页面中显示
function buildTableList() {
    $.ajax({
    url:"feedbackRecords",
    data:"",
    type:"GET",
    success:function(result){
        console.log(result);
        buildTable(result);
    }
});
}
/*将数据渲染成表格*/
function buildTable(result){
    //清空table表格
    $("#feedbackTable tbody").empty();
    $.each(result,function(index,item){
        const td1 = $("<td></td>").append(item.feedbackno);
        const td2 = $("<td></td>").append(item.useracc);
        const td3 = $("<td></td>").append(item.topic);
        const td4 = $("<td></td>").append(item.sendtime);
        const td5 = $("<td></td>").append(item.handleresult);
        const td6 = $("<td></td>").append(item.handletime);
        const td7 = $("<td></td>").append(item.fbcontents);

        //添加回复按钮
        const replayBtn = $("<button></button>").addClass("btn btn-primary btn-sm replayBtn").append("回复");
        //为编辑按钮添加一个自定义的属性来表示当前feedback的id
        replayBtn.attr("replayNo",item.feedbackno);
        replayBtn.attr("data-toggle","modal");
        replayBtn.attr("data-target","#replayModal");
        if(item.handleresult!=="未处理"){
            replayBtn.attr("disabled",true);
        }
        //删除按钮
        const delBtn =  $("<button></button>").addClass("btn btn-danger btn-sm deleteBtn").append("删除");
        //为删除按钮添加一个自定义的属性来表示当前feedback的id
        delBtn.attr("delNo",item.feedbackno);
        //编辑按钮和删除按钮添加进去显示
        const btnTd = $("<td></td>").append(replayBtn).append(" ").append(delBtn);

        //将数据渲染到表格显示
        $("<tr></tr>").append(td1)
            .append(td2)
            .append(td3)
            .append(td7)
            .append(td4)
            .append(td5)
            .append(td6)
            .append(btnTd)
            .appendTo("#feedbackTable tbody");
    });
}

//删除投诉反馈
$(document).on("click", ".deleteBtn", function () {
    //1、弹出是否确认删除对话框
    var feedbackNo = $(this).attr("delNo");
    if (confirm("确认删除该条投诉反馈吗？")) {
        //确认，发送ajax请求删除
        $.ajax({
            url: "delFeedbackRecord",
            type: "POST",
            data: "fbid=" + feedbackNo,
            success: function (result) {
                //刷新并显示反馈列表
                buildTableList();
            }
        });
    }
});
//点击回复按钮弹出回复模态框。
$(document).on("click", ".replayBtn", function () {
    var feedbackNo = $(this).attr("replayNo");
    $("#tempNo").val(feedbackNo);
});




/////////////////////////////////////搜索框//////////////////////////
//点击搜索按钮时,提交ajax请求查询
$("#queryAdmin").click(function(){
    var queryItem=$("#queryItem").val();
    var itemValue=$("#itemValue").val();
    $.ajax({
        url:"../backMgt/SearchBgAdmin.do",
        data:"queryItem="+queryItem+"&itemValue="+itemValue,
        type:"GET",
        success:function(result){

            console.log(result);
            //将查询结果渲染成表格显示
            build_admin_table(result);
        }
    });
});