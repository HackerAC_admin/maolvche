
$(function(){
    //页面加载完成时开始查询并将结果渲染显示
    buildTableList();

    /////////////////////////////////////搜索框//////////////////////////
//点击搜索按钮时,提交ajax请求查询
    $("#queryLoginRecord").click(function(){
        var queryItem=$("#queryItem").val();
        var itemValue=$("#itemValue").val();
        $.ajax({
            url:"queryLoginRecord",
            data:"queryItem="+queryItem+"&itemValue="+itemValue,
            type:"GET",
            success:function(result){
                console.log(result);
                //将查询结果渲染成表格显示
                buildTable(result);
            }
        });
    });
});

//ajax进行数据查询请求发送，并解析查询到的数据到页面中显示
function buildTableList() {
    $.ajax({
        url:"allLoginRecords",
        data:"",
        type:"GET",
        success:function(result){
            console.log(result);
            buildTable(result);
        }
    });
}
/*将数据渲染成表格*/
function buildTable(result){
    //清空table表格
    $("#loginRecordTable tbody").empty();
    $.each(result,function(index,item){
        const td1 = $("<td></td>").append(item.useracc);
        const td2 = $("<td></td>").append(item.loginip);
        const td3 = $("<td></td>").append(item.logintime);

        //删除按钮
        const delBtn =  $("<button></button>").addClass("btn btn-danger btn-sm deleteBtn").append("删除");
        //为删除按钮添加一个自定义的属性来表示当前instPlan的id
        delBtn.attr("delNo",item.recoredno);
        //编辑按钮和删除按钮添加进去显示
        const btnTd = $("<td></td>").append(delBtn);

        //将数据渲染到表格显示
        $("<tr></tr>").append(td1)
            .append(td2)
            .append(td3)
            .append(btnTd)
            .appendTo("#loginRecordTable tbody");
    });
}

//删除
$(document).on("click", ".deleteBtn", function () {
    //1、弹出是否确认删除对话框
    var recordNo = $(this).attr("delNo");
    if (confirm("确认删除该条日志吗？")) {
        //确认，发送ajax请求删除
        $.ajax({
            url: "delLoginRecord",
            type: "POST",
            data: "recordNo=" + recordNo,
            success: function (result) {
                //刷新显示日志表
                buildTableList();
            }
        });
    }
});

