
$(function(){
    //页面加载完成时开始查询并将结果渲染显示
    buildTableList();

    //查询分期方案，更新下拉列表
    $.ajax({
        url: "instlPlans",
        data: "",
        type: "GET",
        success: function (result) {
            $("#instlPlanSel").empty();
            $.each(result,function(index,item){
                const option1 = $("<option></option>").append(item.percent+"%首付"+"分"+item.times+"期还款");
                option1.val(item.planno);
                //将数据渲染到下拉列表
                option1.appendTo("#instlPlanSel");
            });
        }
    });

    //点击提交添加按钮完成分期还款记录添加。
    $("#submitAdd").click(function() {
        const planNo = $("#instlPlanSel").val();
        const userAcc = $("#userAcc").val();
        const repaymentMoney = $("#repaymentMoney").val();
        const lastMoney = $("#lastMoney").val();

        if(planNo===""||userAcc===""||repaymentMoney===""||lastMoney===""){
            alert("输入数据不能为空！");
            return false;
        }else{
            $.ajax({
                url: "addInstlRecords",
                data: {"planNo":planNo,"userAcc":userAcc,"repaymentMoney":repaymentMoney,"lastMoney":lastMoney},
                type: "POST",
                success: function (result) {
                    if(result===1){
                        alert("添加成功！");
                        window.location.reload();
                    }else{
                        alert("添加失败！");
                        window.location.reload();
                    }
                }
            });
        }
    });

    /////////////////////////////////////搜索框//////////////////////////
//点击搜索按钮时,提交ajax请求查询
    $("#queryInstlPlan").click(function(){
        var queryItem=$("#queryItem").val();
        var itemValue=$("#itemValue").val();
        $.ajax({
            url:"queryInstlRecord",
            data:"queryItem="+queryItem+"&itemValue="+itemValue,
            type:"GET",
            success:function(result){
                console.log(result);
                //将查询结果渲染成表格显示
                buildTable(result);
            }
        });
    });
});

//ajax进行数据查询请求发送，并解析查询到的数据到页面中显示
function buildTableList() {
    $.ajax({
        url:"instlRecords",
        data:"",
        type:"GET",
        success:function(result){
            console.log(result);
            buildTable(result);
        }
    });
}
/*将数据渲染成表格*/
function buildTable(result){
    //清空table表格
    $("#InstlRecordTable tbody").empty();
    $.each(result,function(index,item){
        const td1 = $("<td></td>").append(item.useracc);
        const td2 = $("<td></td>").append(item.planno);
        const td3 = $("<td></td>").append(item.repaymentmoney);
        const td4 = $("<td></td>").append(item.repaymentdate);
        const td5 = $("<td></td>").append(item.lastmoney);

        //删除按钮
        const delBtn =  $("<button></button>").addClass("btn btn-danger btn-sm deleteBtn").append("删除");
        //为删除按钮添加一个自定义的属性来表示当前instRecord的id
        delBtn.attr("delNo",item.instlmntno);
        //删除按钮添加进去显示
        const btnTd = $("<td></td>").append(delBtn);

        //将数据渲染到表格显示
        $("<tr></tr>").append(td1)
            .append(td2)
            .append(td3)
            .append(td4)
            .append(td5)
            .append(btnTd)
            .appendTo("#InstlRecordTable tbody");
    });
}

//删除分期还款记录
$(document).on("click", ".deleteBtn", function () {
    //1、弹出是否确认删除对话框
    var instlmntno = $(this).attr("delNo");
    if (confirm("确认删除该条分期记录吗？")) {
        //确认，发送ajax请求删除
        $.ajax({
            url: "delinstlRecord",
            type: "POST",
            data: "id=" + instlmntno,
            success: function (result) {
                if(result===2){
                    alert("该还款任务还未完成，无法删除！");
                }else{
                    alert("删除成功！");
                    //刷新并显示反馈列表
                    buildTableList();
                }

            }
        });
    }
});

/////////////////////////////////////搜索框//////////////////////////
//点击搜索按钮时,提交ajax请求查询
$("#queryAdmin").click(function(){
    var queryItem=$("#queryItem").val();
    var itemValue=$("#itemValue").val();
    $.ajax({
        url:"queryInstlRecord",
        data:"queryItem="+queryItem+"&itemValue="+itemValue,
        type:"GET",
        success:function(result){
            console.log(result);
            //将查询结果渲染成表格显示
            buildTable(result);
        }
    });
});