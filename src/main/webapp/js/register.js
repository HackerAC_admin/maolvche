$(function () {
    $(".submitBtn").click(function(){
         const phone =  $("#phone").val();
         const pwd = $("#pwd").val();
         const pwd2 = $("#pwd2").val();
         const nickname = $("#nickname").val();

         //正则表达式
        const mobileReg = /^1[34578]\d{9}$/;
        const nickNameReg =/^[\u4E00-\u9FA5A-Za-z0-9_]{1,16}$/;
        const pwdReg = new RegExp("^(?=.{6,16})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");

        if(!mobileReg.test(phone)){
            alert("手机号格式错误！");
            return false;
        }else if(!pwdReg.test(pwd)){
            alert("密码格式错误！6-16位字母数字组合");
            return false;
        }else if(pwd!==pwd2){
            alert("两次输入的密码不同！");
            return false;
        }else if(!nickNameReg.test(nickname)){
            alert("昵称格式有误！应为非空非特殊字符");
            return false;
        }else{
            $.ajax({
                url:"signup",
                data:{"phone":phone,"pwd":pwd,"nickname":nickname},
                type:"GET",
                success:function (result) {
                    if(result==2){
                        alert("注册成功！请登录！");
                        self.location="login";
                    }else if(result==1){
                        alert("注册失败，该手机号已经注册过账号。");
                    }else{
                        alert("注册失败，未知错误。");
                    }
                }
            });
        }
    });
});