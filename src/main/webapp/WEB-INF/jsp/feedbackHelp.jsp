<%@ page import="com.maolvche.pojo.HelpGuide" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-21
  Time: 15:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>投诉反馈帮助</title>
    <link rel="stylesheet" type="text/css" href="css/WebhelpPage.css">
</head>
<body>
<h1>投诉反馈帮助</h1>
<div id="contentMsg" style="font-size: 14px;color: gray">
<%
    List<HelpGuide> helps = (List<HelpGuide>) request.getAttribute("helps");
    for (HelpGuide help : helps) {
%>
    <b><%=help.getTopic()%>
    </b><p>
    <i><%=help.getContent()%>
    </i></p>
    <% } %>
</div>
</body>
</html>
