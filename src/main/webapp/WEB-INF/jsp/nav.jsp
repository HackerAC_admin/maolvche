<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-13
  Time: 8:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
<%--    <link rel="icon" href="favicon.ico" sizes="32x32">--%>
<%--    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">--%>
    <link rel="stylesheet" type="text/css" href="css/nav.css">
<%--    <script src="webjars/jquery/3.5.1/jquery.js"></script>--%>
    <script src="js/nav.js"></script>
<%--    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>--%>
</head>
<body>
<%--<%--%>
<%--    String thisNav = request.getParameter("thisNav");//获取当其所在页--%>
<%--%>--%>
<!--导航栏start-->
<nav class="navbar navbar-expand-lg">
    <a class="navbar-brand" href="main">
        <img src="images/logo.png" id="Logo" alt="网站logo">
    </a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="selCityBtn" role="button">
                    ${sessionScope.thisCity}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="main">首页<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="buyCarIndex">买新车</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="buySecondCarIndex">买二手车</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">卖车</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">资讯</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">论坛</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">买家秀</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="搜索你想要的车" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">查询</button>
        </form>
        <ul class="navbar-nav">
            <li class="userInfo" id="personalLogin">
                <a class="nav-link dropdown-toggle" href="#" role="button">
                    个人登录
                </a>
                <div class="personalDropdown">
                    <a href="login">登录</a>
                    <a href="register">注册</a>
                </div>
            </li>
            <li class="userInfo" id="personalCenterLi">
                <a class="nav-link dropdown-toggle" href="#" role="button">
                    个人中心
                </a>
                <div class="personalDropdown2">
                    <a href="toPersonalCenter">个人中心</a>
                    <a href="SignOut">退出登录</a>
                </div>
            </li>
        </ul>
    </div>

    <!--城市列表-->
    <div id="dropDownLink"></div>
    <div id="cityList">
        <div id="indexList">
            <a href="#">全</a>
            <a href="#">A</a>
            <a href="#">B</a>
            <a href="#">C</a>
            <a href="#">D</a>
            <a href="#">E</a>
            <a href="#">F</a>
            <a href="#">G</a>
            <a href="#">H</a>
            <a href="#">J</a>
            <a href="#">K</a>
            <a href="#">L</a>
            <a href="#">M</a>
            <a href="#">N</a>
            <a href="#">P</a>
            <a href="#">Q</a>
            <a href="#">R</a>
            <a href="#">S</a>
            <a href="#">T</a>
            <a href="#">W</a>
            <a href="#">X</a>
            <a href="#">Y</a>
            <a href="#">Z</a>
        </div>
        <div id="selectedList">
            <div class="city">
                <ul class="city-list">
                    <li id="citybox-全">
                        <a class="key" href="#">全</a>
                        <div class="cities">
                            <a class="cityName" id="quanguo" href="#">全国</a>
                        </div>
                    </li>
                    <li id="citybox-B">
                        <a class="key" href="#">B</a>
                        <div class="cities">
                            <a class="cityName" id="beijing" href="#">北京</a>
                            <a class="cityName" id="baoding" href="#">保定</a>
                            <a class="cityName" id="bijie" href="#">毕节</a>
                            <a class="cityName" id="baicheng" href="#">白城</a>
                            <a class="cityName" id="beihai" href="#">北海</a>
                            <a class="cityName" id="bozhou" href="#">亳州</a>
                            <a class="cityName" id="bazhong" href="#">巴中</a>
                        </div>
                    </li>
                    <li id="citybox-C">
                        <a class="key" href="#">C</a>
                        <div class="cities">
                            <a class="cityName" id="chongqing" href="#">重庆</a>
                        </div>
                    </li>
                    <li id="citybox-T">
                        <a class="key" href="#">T</a>
                        <div class="cities">
                            <a class="cityName" id="天津" href="#">天津</a>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</nav>
<input id="thisNav" hidden value="${requestScope.thisNav}"/>
<input id="info" hidden value="${sessionScope.info}"/>
<!--导航栏end-->
</body>
</html>
