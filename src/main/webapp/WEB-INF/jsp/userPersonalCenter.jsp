<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-13
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>个人中心</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/userPersonalCenter.css">
    <script src="js/userPersonalCenter.js"></script>
</head>
<body>
<%@ include file="nav.jsp"%>
<!--个人中心start-->
<section class="personalSection">
    <div class="personalContent">
        <div class="topSection">
            <img src="images/1_xlt_jbwkj.png" alt="头像">
            <span id="userNickname">${sessionScope.LOGIN_USER.accountInfo.nickname}</span>
        </div>
        <div class="LeftSection">
            <ul>
                <li class="accMenu">账户信息</li>
                <li class="secondList order1" id="selfInfo">基本信息</li>
                <li class="secondList order1" id="loginRecord">登录日志</li>
                <li class="carMenu">我的车辆信息</li>
                <li class="secondList order2" id="mySellCar">我出售的车</li>
                <li class="secondList order2" id="myBuyCar">我购买的车</li>
                <li id="myFavorite">我的收藏</li>
                <li id="myYuYue">我的预约</li>
                <li id="myPaper">我提交的资料</li>
                <li id="myQieYue">我的签约</li>
                <li id="myHuanKuan">分期还款</li>
                <li class="serMenu">售后服务</li>
                <li class="secondList order3" id="myWeiHu">维保记录查询</li>
                <li class="secondList order3" id="myTuiHuan">我的退换</li>
                <li id="myFeedBack">反馈与投诉</li>
            </ul>
        </div>
        <div class="RightSection">
            <!--个人基本信息start-->
            <div class="selfInfo">
                <span class="titleSpan">基本信息</span>
                <ul>
                    <li>头像：<img alt="头像" src="images/1_xlt_jbwkj.png">
                    </li>
                    <li>登陆账号：<label>${sessionScope.LOGIN_USER.accountInfo.username}</label>
                    </li>
                    <li>昵称：<label>${sessionScope.LOGIN_USER.accountInfo.nickname}</label>
                    </li>
                    <li>手机号：<label>${sessionScope.LOGIN_USER.userInfo.phone}</label>
                    </li>
                    <li>邮箱：<label>${sessionScope.LOGIN_USER.userInfo.useremail}</label>
                    </li>
                    <li>联系地址：<label>${sessionScope.LOGIN_USER.userInfo.address}</label>
                    </li>
                </ul>
                <button class="btn btn-sm btn-success" id="uptPwd" data-toggle="modal" data-target="#modifyPwdModal">修改密码</button>

                <button class="btn btn-sm btn-success" id="updPersonalInfo" data-toggle="modal" data-target="#modifyPersonInfoModal">编辑个人信息</button>
            </div>
            <!--个人基本信息end-->
            <!--登录日志start-->
            <div class="loginRecord">
                <span class="titleSpan">我的登录日志</span>
                <table class="table table-bordered table-hover table-striped" id="recordTab">
                    <thead>
                    <tr>
                        <td>登陆IP</td>
                        <td>登陆时间</td>
                        <td>操作</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <!--登录日志end-->
            <!--我出售的车start-->
            <div class="mySellCar">
                <div id="mySellCarBody">
                    <span class="titleSpan">我出售的车</span>
                    <table>
                        <tr>
                            <td colspan="2">
                                <img src="images/car5.jpg" alt="mySellCar"><br>
                                <button id="delSellCarImg" class="btn btn-sm btn-info">更换图片</button>
                            </td>
                        </tr>
                        <tr>
                            <td>汽车名称</td>
                            <td>宝骏360</td>
                        </tr>
                        <tr>
                            <td>上牌日期</td>
                            <td>2019-12-11</td>
                        </tr>
                        <tr>
                            <td>公里数</td>
                            <td>20000</td>
                        </tr>
                        <tr>
                            <td>售价(万)</td>
                            <td>4.38</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button class="btn btn-sm btn-primary">修改</button>
                                <button class="btn btn-sm btn-dark">下架</button>
                                <button class="btn btn-sm btn-danger">删除</button>
                            </td>
                        </tr>
                    </table>
                    <table >
                        <tr>
                            <td colspan="2">
                                <img src="images/car5.jpg" alt="mySellCar"><br>
                                <button name="delSellCarImg" class="btn btn-sm btn-info">更换图片</button>
                            </td>
                        </tr>
                        <tr>
                            <td>汽车名称</td>
                            <td>宝骏360</td>
                        </tr>
                        <tr>
                            <td>上牌日期</td>
                            <td>2019-12-11</td>
                        </tr>
                        <tr>
                            <td>公里数</td>
                            <td>20000</td>
                        </tr>
                        <tr>
                            <td>售价(万)</td>
                            <td>4.38</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button class="btn btn-sm btn-primary">修改</button>
                                <button class="btn btn-sm btn-dark">下架</button>
                                <button class="btn btn-sm btn-danger">删除</button>
                            </td>
                        </tr>
                    </table>
                </div>
                <br><br>
                <button class="btn btn-sm btn-success">再卖一辆</button>
            </div>
            <!--我出售的车end-->
            <!--我购买的车start-->
            <div class="myBuyCar">
                <span class="titleSpan">我购买的车</span>
                <br>
                <img src="images/car4.jpg" alt="我购买的车" width="300" height="200"><br>
                <h4>我的订单</h4>
                <table class="table table-bordered table-hover table-striped" id="MyNewCarOrder">
                    <thead>
                    <tr>
                        <td>汽车</td>
                        <td>期望分期方案</td>
                        <td>下单时间</td>
                        <td>状态</td>
                        <td>处理结果</td>
                        <td>操作</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table><br>
            </div>
            <!--我购买的车end-->

            <!--我的收藏start-->
            <div class="myFavorite">
                <span class="titleSpan">我的收藏</span>
                <table class="table table-bordered table-hover table-striped" id="MyFavoriteCar">
                    <thead>
                    <tr>
                        <td>汽车信息</td>
                        <td>操作</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table><br>
            </div>
            <!--我的收藏end-->

            <!--/*我的预约start*/-->
            <div class="myYuYue">
                <span class="titleSpan">我的预约</span>
            </div>
            <!--/*我的预约end*/-->

            <!--/*我提交的资料start*/-->
            <div class="myPaper">
                <span class="titleSpan">我提交的材料</span>
                <table class="table table-bordered table-hover table-striped" id="creditFileTab">
                    <thead>
                    <tr>
                        <td>文件</td>
                        <td>文件名</td>
                        <td>上传时间</td>
                        <td>主题</td>
                        <td>操作</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table><br>
                <form  id="creditForm">
                上传新资料：<input type="file" name="file" id="uploadfile">
                    <select name="topic">
                        <option value="身份验证">身份验证</option>
                        <option value="消费流水凭证">消费流水凭证</option>
                        <option value="驾驶资格">驾驶资格</option>
                    </select>
                    <button type="button" class="btn btn-sm btn-success" id="submitCreditfile">上传</button>
                </form>
            </div>
            <!--/*我提交的资料end*/-->

            <!--/*我的签约start*/-->
            <div class="myQieYue">
                <span class="titleSpan">我的签约</span>
            </div>
            <!--/*我的签约end*/-->

            <!--/*分期还款start*/-->
            <div class="myHuanKuan">
                <span class="titleSpan">我的分期还款记录</span>
                <br>
                <b>我的分期方案：</b>首付：${sessionScope.userInstlInfo.firstpaymoney}元，月供:<label id="yuegong">${sessionScope.userInstlInfo.monthpaymoney}</label>元
                <table class="table table-bordered table-hover table-striped" id="instRecordTab">
                    <thead>
                    <tr>
                        <td>期数</td>
                        <td>还款日期</td>
                        <td>还款金额</td>
                        <td>剩余待还金额</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table><br>
                <button class="btn btn-sm btn-primary" id="wannaPayIntsl">我要还款</button>
                <br>
                <div id="payThisInstInfo">
                   <b>当前期数</b>:<label id="thisTimes"></label>
                    <b>本期应还金额</b>：<label id="shouldPay"></label>元
                    <br>
                    <button class="btn btn-primary btn-lg" id="PayInstSure">确认还款</button>
                </div>
            </div>
            <!--/*分期还款end*/-->

            <!--/*维保记录查询start*/-->
            <div class="myWeiHu">
                <sapn class="titleSpan">我的维保记录</sapn>
                <table class="table table-bordered table-hover table-striped" id="maintainRecordTab">
                    <thead>
                    <tr>
                        <td>汽车</td>
                        <td>项目</td>
                        <td>消费</td>
                        <td>操作员</td>
                        <td>日期</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <!--/*维保记录查询end*/-->

            <!--/*我的退换start*/-->
            <div class="myTuiHuan">
                <sapn class="titleSpan">我的退换记录</sapn>
                <table class="table table-bordered table-hover table-striped" id="ExchangeRecordTab">
                    <thead>
                    <tr>
                        <td>汽车</td>
                        <td>退换理由</td>
                        <td>申请日期</td>
                        <td>处理状态</td>
                        <td>处理结果</td>
                        <td>退换日期</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table><br>
                <button class="btn btn-sm btn-primary" id="addExchange" data-toggle="modal" data-target="#exchangeModal">发起退换申请</button>

            </div>
            <!--/*我的退换end*/-->

            <!--/*反馈与投诉start*/-->
            <div class="myFeedBack">
                <sapn class="titleSpan">投诉与反馈记录</sapn>
                <table class="table table-bordered table-hover table-striped" id="feedBackRecordTab">
                    <thead>
                    <tr>
                        <td>主题</td>
                        <td>内容</td>
                        <td>提交日期</td>
                        <td>处理结果</td>
                        <td>处理日期</td>
                        <td>操作</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table><br>
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#feedbackModal">发起反馈或投诉</button>
            </div>

            <!--/*反馈与投诉end*/-->

        </div>
    </div>
</section>
<!--个人中心end-->
<%--修改密码模态框start--%>
<div class="modal fade" id="modifyPwdModal" tabindex="-5" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">修改密码</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                原密码：<input class="pwdinput" type="password" maxlength="20" width="30" id="originPwd"/><br>
                新密码：<input class="pwdinput" type="password" maxlength="20" width="30" id="newPwd" placeholder="至少6位数字字母组合"/><br>
                再次输入新密码：<input class="pwdinput" type="password" maxlength="20" width="30" id="newPwd2"/><br>
                <br>
                <button class="btn btn-primary" data-dismiss="modal" aria-label="Close" id="submitModifyPwd">提交</button>
            </div>
        </div>
    </div>
</div>
<%--修改密码模态框end--%>

<%--编辑个人信息模态框start--%>
<div class="modal fade" id="modifyPersonInfoModal" tabindex="-5" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">编辑个人信息</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                账号(登录使用)：<input type="text" required maxlength="20" width="30" id="newUsername" value="${sessionScope.LOGIN_USER.accountInfo.username}"/><br>
                手机号：<input type="number" required maxlength="20" width="30" id="newPhone" value="${sessionScope.LOGIN_USER.userInfo.phone}"/><br>
                邮箱：<input type="email" required maxlength="20" width="30" id="newEmail" value="${sessionScope.LOGIN_USER.userInfo.useremail}"/><br>
                昵称：<input type="text" required maxlength="20" width="30" id="newNickName" value="${sessionScope.LOGIN_USER.accountInfo.nickname}"/><br>
                联系地址：<input type="text" required maxlength="20" width="30" id="newAddress" value="${sessionScope.LOGIN_USER.userInfo.address}"/><br>
                <br>
                <button class="btn btn-primary" data-dismiss="modal" aria-label="Close" id="submtMdfyPrsnInfo">提交</button>
            </div>
        </div>
    </div>
</div>
<%--编辑个人信息模态框end--%>

<%--投诉反馈模态框start--%>
<div class="modal fade" id="feedbackModal" tabindex="-5" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">编辑投诉和反馈</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                主题：<input name="topic" type="text" maxlength="20" width="30" id="feedbackTopic"/><br>
                投诉或反馈内容：<br><textarea rows="3" cols="42" id="feedbackContent"></textarea><br>
                <br>
                <button class="btn btn-primary" data-dismiss="modal" aria-label="Close" id="submitFeedback">提交</button>
            </div>
        </div>
    </div>
</div>
<%--投诉反馈模态框end--%>

<%--申请退换模态框start--%>
<div class="modal fade" id="exchangeModal" tabindex="-5" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">编辑退换申请</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-hover table-striped" id="exchangeCar">
                    <thead>
                    <tr>
                        <td>我的汽车编号</td>
                        <td>购买日期</td>
                        <td>选择</td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table><br>
                简要说明原因：<br><textarea rows="3" cols="42" id="exchangeReason"></textarea><br>
                <br>
                <button class="btn btn-primary" id="submitExchange" data-dismiss="modal"  aria-label="Close">提交</button>
                <button class="btn btn-dark"  data-dismiss="modal"  aria-label="Close">关闭</button>
            </div>
        </div>
    </div>
</div>
<%--申请退换模态框end--%>

<input id="Referer" value="${requestScope.prePage}" hidden>
<!--footer start-->
<%@ include file="footer.jsp"%>
<!--footer end-->
</body>
</html>