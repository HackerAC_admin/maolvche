<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-16
  Time: 8:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/buyByAml2.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script src="js/buyByAml2.js"></script>
</head>
<body>
<%@include file="nav.jsp"%>
<section>
    <div class="buyCarBody">
        <div class="allMoneyBuyTopDiv">
            <span class="allMoneyBuyTitle">全款购车操作流程</span>
        </div>

        <div class="stepBar">
            <div class="stepOne">
                <img src="images/caricon.png" alt="..">
                <br>
                <b>确认购车信息</b>
            </div>
            <div class="stepTwo">
                <img src="images/caricon.png" alt="..">
                <br>
                <b>在线签约</b>
            </div>
            <div class="stepThree">
                <img src="images/caricon.png" alt="..">
                <br>
                <b>提交定金</b>
            </div>
            <div class="progress" style="height: 30px;">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="showStepInfo">
            <div id="agreementPaper" class="card">
                <h3 class="card-header">购车定金协议书</h3>
                <div class="card-body">
                    <h5>出售方：毛驴汽车网</h5>
                    <h5>订车方：<label id="clientName">彭于晏</label>(身份证号：<label id="idCardNum">520190199911111111</label>)</h5>
                    <p>依据《中华人民共和国合同法》及其他有关法律法规的规定，
                        甲、乙双方在平等、自愿、协商、一致的基础上，就汽车买卖有关事宜，订立本合同。</p>
                    <p>乙方于<label id="dateTime">2020年11月27日</label>向甲方预定购买<label id="carFactory">上汽通用五菱</label>牌汽车1辆
                        ；型号：<label>宝骏360 2019款 CVT精英型</label>；颜色：<label>糖果白</label>，并按照甲方规定预付相应的定金。待车辆成交后，定金作为车辆全款。</p>
                    <p>乙方承诺交付定金后7日内到甲方线下门店签订正式销售合同并交纳尾款，如乙方资金不到位应事先通知甲方，双方协商可以延迟（不得超过十五日）；否则视为乙方违约。</p>
                    <p>若甲方与乙方提车日不能按时交付车辆，又不事先提前2日通知乙方并说明情况，则视为甲方违约。</p>
                    <p>乙方在签订本协议时向甲方支付定金人民币<label>74800.00</label>元，大写<label>柒万肆仟捌佰</label>元整。甲方收到定金后应于签订正式销售合同日给与乙方书面收据</p>
                    <p>本协议签订后至交车开始前，如甲方违约，则双倍返还上述定金与乙方；如乙方违约，则甲方不予退回定金。</p>
                    <i>点击下一步将视作同意上述协议，若有其他问题可点此<a href="#">反馈</a>或致电010-88888888</i>
                </div>
            </div>
        </div>
        <img class="webLogo" src="images/weblogo.png"><i>拒绝繁琐分期，快速提车，纵享高效生活！</i>
        <button id="backToCarInfo" class="btn btn-dark">取消</button>
        <button id="preStep" class="btn btn-success">上一步</button>
        <button id="nextStep" class="btn btn-primary">下一步</button>
    </div>
</section>
<%@include file="footer.jsp"%>
</body>
</html>
