<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-14
  Time: 18:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>毛驴汽车网后台管理</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="js/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="js/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="js/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="js/static/h-ui.admin/skin/red/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="js/static/h-ui.admin/css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/BGMS/bgindex1.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="js/BGMS/bgmsLoginVerify.js"></script>
</head>
<body>
<nav>
    <div class="titleBar">
        <img src="images/logo.png">
        <span>毛驴汽车网后台管理系统</span><b>员工操作子系统</b>
    </div>
    <div class="loginerInfo">
        <span>工号：<i>${sessionScope.BGMS_EMPLOY.nickname}</i></span>
        <a href="logoutemp"><button class="btn btn-sm btn-danger">退出登录</button></a>
    </div>
</nav>
<div id="content">
    <aside class="Hui-aside">
        <div class="menu_dropdown bk_1">
            <dl id="menu-article">
                <dt><i class="Hui-iconfont">&#xe616;</i>接单大厅<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="article-list.html" data-title="待接订单" href="javascript:void(0)">待接订单</a></li>
                        <li><a data-href="article-list.html" data-title="我的接单" href="javascript:void(0)">我的接单</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="menu-picture">
                <dt><i class="Hui-iconfont">&#xe62e;</i>养护管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="picture-list.html" data-title="养护记录" href="javascript:void(0)">养护记录</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="menu-product">
                <dt><i class="Hui-iconfont">&#xe620;</i>退换订单<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="product-brand.html" data-title="退换记录" href="javascript:void(0)">退换记录</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="menu-member">
                <dt><i class="Hui-iconfont">&#xe60d;</i>我的信息<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="member-list.html" data-title="个人信息" href="javascript:;">个人信息</a></li>
                    </ul>
                </dd>
            </dl>
        </div>
    </aside>
    <div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a></div>
    <section class="Hui-article-box">
        <div id="Hui-tabNav" class="Hui-tabNav hidden-xs">
            <div class="Hui-tabNav-wp">
                <ul id="min_title_list" class="acrossTab cl">
                    <li class="active">
                        <span title="首页" data-href="welcome.html">首页</span>
                        <em></em></li>
                </ul>
            </div>
            <div class="Hui-tabNav-more btn-group">
                <a id="js-tabNav-prev" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i></a>
                <a id="js-tabNav-next" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i></a>
            </div>
        </div>
        <div id="iframe_box" class="Hui-article">
            <div class="show_iframe">
                <div style="display:none" class="loading"></div>
                <iframe id="iframe-welcome" data-scrolltop="0" scrolling="yes" frameborder="0" src="welcome.html"></iframe>
            </div>
        </div>
    </section>
</div>
<!--_footer 作为公共模版分离出去-->
<!--<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script>-->
<script type="text/javascript" src="js/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="js/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="js/static/h-ui.admin/js/H-ui.admin.js"></script>
<!--/_footer 作为公共模版分离出去-->

</body>
</html>
