<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-13
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <title>买新车</title>
    <meta charset="UTF-8">
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/buyCarIndex.css">
    <link rel="stylesheet" type="text/css" href="css/floatBar.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script src="js/buyCarIndex.js"></script>
</head>
<body>
<%@ include file="nav.jsp"%>
<hr>
<!--选择菜单start-->
<div class="newCarSel">
    <table>
        <tr class="pingPai">
            <td class="tdTitle">品牌</td>
            <td>
                <ul>
                    <li class="selActive"><a href="#" name="*">全部</a></li>
                    <li><a href="#">奔驰</a></li>
                    <li><a href="#">大众</a></li>
                    <li><a href="#">奥迪</a></li>
                    <li><a href="#">日产</a></li>
                    <li><a href="#">宝骏</a></li>
                    <li><a href="#">别克</a></li>
                    <li><a href="#">吉利</a></li>
                    <li><a href="#">五菱</a></li>
                    <li><a href="#">比亚迪</a></li>
                    <li><a href="#">雪佛兰</a></li>
                    <li><a href="#">丰田</a></li>
                </ul>
            </td>
        </tr>
        <tr class="cheXing">
            <td class="tdTitle">车型</td>
            <td>
                <ul>
                    <li class="selActive"><a href="#" name="*">全部</a></li>
                    <li><a href="#">轿车</a></li>
                    <li><a href="#">SUV</a></li>
                    <li><a href="#">MPV</a></li>
                </ul>
            </td>
        </tr>
        <tr class="cheJia">
            <td class="tdTitle">车价</td>
            <td>
                <ul>
                    <li class="selActive"><a href="#" name="*">全部</a></li>
                    <li><a href="#">10万以下</a></li>
                    <li><a href="#">10-15万</a></li>
                    <li><a href="#">15-20万</a></li>
                    <li><a href="#">20万以上</a></li>
                </ul>
            </td>
        </tr>
        <tr class="shouFu">
            <td class="tdTitle">首付</td>
            <td>
                <ul>
                    <li class="selActive"><a href="#" name="*">全部</a></li>
                    <li><a href="#">1万以内</a></li>
                    <li><a href="#">1-2万</a></li>
                    <li><a href="#">2-3万</a></li>
                    <li><a href="#">3-4万</a></li>
                    <li><a href="#">4万以上</a></li>
                    <li><a href="#">全款</a></li>
                </ul>
            </td>
        </tr>
        <tr class="yueGong">
            <td class="tdTitle">月供</td>
            <td>
                <ul>
                    <li class="selActive"><a href="#" name="*">全部</a></li>
                    <li><a href="#">2000元以内</a></li>
                    <li><a href="#">2000-3000元</a></li>
                    <li><a href="#">3000-4000元</a></li>
                    <li><a href="#">4000-5000元</a></li>
                    <li><a href="#">5000元以上</a></li>
                </ul>
            </td>
        </tr>
    </table>
    <div class="selOption">
        您已选择：
        <ul id="options">
        </ul>
    </div>
</div>
<!--选择菜单end-->
<!--选择结果展示区start-->
<div class="selShowSection">
    <div class="selShowBody">
        <div class="selShowList">
            <ul>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label class="CarInfo">宝骏360 2019款 1.5L CVT精英型</label>
                    <p>厂家指导价：<label class="factoryPrice">5.28万</label></p>
                    <div>
                        <span>
                            首付<label class="FirstPay">0.74</label>万
                        </span>
                        <i>月供<label class="monthPay">3084</label>元</i>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--选择结果展示区end-->
<!--悬浮start-->
<div id="toTop"><img src="images/toTop.jpg"></div>
<script src="js/floatBar.js"></script>
<!--悬浮end-->
<%@ include file="footer.jsp"%>
</body>
</html>
