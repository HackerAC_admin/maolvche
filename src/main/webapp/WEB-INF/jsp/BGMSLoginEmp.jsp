<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-14
  Time: 18:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>登录后台管理系统</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/BGMS/bgLogin.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
</head>
<body>
<div class="loginDiv">
    <div class="loginBox admLogin">
        <div class="left">
            <img src="images/weblogo.png">
            <h2>毛驴汽车网</h2>
            <h4>后台管理系统</h4>
        </div>
        <div class="right" id="emp">
            <h3>员工登录</h3>
            <form action="emplogin" method="post">
                <input type="text" name="employAcc" placeholder="账号"><br>
                <input type="password" name="employPwd" placeholder="密码"><br>
                <i>${errInfo}</i>
                <button type="submit" class="">登录</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>