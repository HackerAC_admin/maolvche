<%@ page import="com.maolvche.pojo.HelpGuide" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-21
  Time: 15:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>关于毛驴汽车网</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/WebhelpPage.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script src="js/helpGuide.js"></script>
</head>
<body>
<h1>关于我们</h1>
<div id="contentMsg" style="font-size: 14px;color: gray">
    <%
        List<HelpGuide> helps = (List<HelpGuide>) request.getAttribute("helps");
        for (HelpGuide help : helps) {
    %>
    <b><%=help.getTopic()%>
    </b><p>
    <i><%=help.getContent()%>
    </i></p>
    <% } %>
</div>
</body>
</html>
