<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-13
  Time: 11:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>注册成为毛驴用户</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/register.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script src="js/register.js"></script>
</head>
<body>
<%@ include file="nav.jsp"%>

<!--注册模态框start-->
<section>
    <div class="loginSection">
        <img class="moder" src="images/registerbg.png" alt="..">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title" id="RegisterModalLabel">注册成为<label>毛驴</label>用户</h4>
                <a href="login"><button class="btn btn-sm btn-success">去登录</button></a>
                </div>
                    <div class="modal-body">
                        <img src="images/logo.png"><br>
                        <i>看车/买车/卖车/售后/全方位服务</i><br>
                        <input class="textInput" id="phone" type="text" placeholder="请输入您的手机号"/><br>
                        <input class="textInput" id="pwd" type="password" placeholder="请设置密码"/><br>
                        <input class="textInput" id="pwd2" type="password" placeholder="请再次输入您设置的密码"/><br>
                        <input class="textInput" id="nickname" type="text" placeholder="请设置昵称"><br>
                        <button type="submit"  class="submitBtn">注册</button>
                        <p>免费咨询400-899-8899<br>
                        登录即视为同意<a href="images/maolvcheuserprinceple.pdf" target="_blank">《用户使用协议》</a>及《隐私权条款》</p>
                    </div>
            </div>
        </div>
    </div>
</section>
<!--注册模态框end-->

<%@ include file="footer.jsp"%>
</body>
</html>
