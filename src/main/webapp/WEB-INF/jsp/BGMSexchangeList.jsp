<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-19
  Time: 14:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/BGMS/BGMSexchangeList.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script src="js/BGMS/BGMSexchangeList.js"></script>
    <title>管理员管理</title>
</head>
<body>
<nav class="breadcrumb">首页>实体店运营>退换管理&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button style="float: right;" onclick="removeIframe()" class="btn btn-sm btn-primary radius">关闭</button></nav>

<!------------------------------- 审核退换申请模态框 ----------------------------->
<div class="modal fade" id="replayExchangeModal" tabindex="-5" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">处理退换申请</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="tempNo"></label><input id="tempNo" hidden><br>
                <label for="ifExchange">是否允许退换：</label>
                    <select id="ifExchange">
                        <option value="y">是</option>
                        <option value="n" selected>否</option>
                    </select>
                <br>
                <label for="replayInput">回复内容</label><br><textarea id="replayInput" cols="44" rows="5"></textarea><br>
                <br>
                <button id="submitReplay" class="btn btn-primary" style="float: right;">回复</button>
            </div>
        </div>
    </div>
</div>
<!-------------------------------退换列表页正文 ----------------------------->
<div class="container">
    <!-- 搜索框 -->
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
            <div class="input-group">
				<span class="input-group-btn">
				<select class="btn btn-info" id="queryItem">
					<option value="acc">用户账户编号</option>
					<option value="carId">汽车编号</option>
					<option value="state">处理状态</option>
				</select>
				</span>
                <input type="text" class="form-control" id="itemValue" placeholder="请输入要查找的信息...">
                <span class="input-group-btn">
                <button class="btn btn-success" type="button" id="queryExchange">查找</button>
                 </span>
            </div>
        </div>
    </div>
    <!-- 按钮 -->
    <div class="breadcrumb">
        <a class="btn btn-success" style="float:right;line-height:1.6em;margin-top:3px;margin-right: 10px" href="javascript:location.replace(location.href);" title="刷新" >刷新</a>
    </div>
    <div class="row">
        <table class="table table-bordered table-hover table-striped"  id="exchangeTab">
            <thead>
            <tr class="info">
                <th>汽车编号</th>
                <th>用户账号</th>
                <th>退换申请原因</th>
                <th>申请日期</th>
                <th>退换日期</th>
                <th>审核结果</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="js/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="js/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="js/static/h-ui.admin/js/H-ui.admin.js"></script>
</body>
</html>
