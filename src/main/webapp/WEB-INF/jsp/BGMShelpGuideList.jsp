<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-19
  Time: 14:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/BGMS/BGMShelpGuide.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script src="js/BGMS/BGMShelpGuide.js"></script>
    <title>帮助向导管理</title>
</head>
<body>
<nav class="breadcrumb">首页>系统运维管理>帮助向导管理&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button style="float: right;" onclick="removeIframe()" class="btn btn-sm btn-primary radius">关闭</button></nav>

<!------------------------------- 新增帮助向导模态框 ----------------------------->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">新增帮助向导信息</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="module">模块：</label><br><input id="module" placeholder="栏目"><br>
                <label for="topic">主题：</label><br><input id="topic" placeholder="标题"><br>
                <label for="content">内容：</label><br><textarea id="content" placeholder="具体内容" cols="44" rows="8"></textarea><br>
                <label for="state">状态：</label><select id="state"><option value="已发布">发布</option><option value="未发布">下架</option></select><br>
                <button id="submitAdd" class="btn btn-primary" style="float: right;">添加</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------- 帮助向导列表正文 ----------------------------->
<div class="container">
    <!-- 搜索框 -->
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
            <div class="input-group">
				<span class="input-group-btn">
				<select class="btn btn-primary" id="queryItem">
					<option value="mdl">模块</option>
					<option value="tpc">主题</option>
				</select>
				</span>
                <input type="text" class="form-control" id="itemValue" placeholder="请输入要查找的信息...">
                <span class="input-group-btn">
                <button class="btn btn-success" type="button" id="queryHelpGuide">查找</button>
                 </span>
            </div>
        </div>
    </div>
    <!-- 按钮 -->
    <div class="breadcrumb">
        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addModal">新增帮助向导</button>
        <a class="btn btn-success" style="float:right;line-height:1.6em;margin-top:3px;margin-right: 10px" href="javascript:location.replace(location.href);" title="刷新" >刷新</a>
    </div>
    <div class="row">
        <table class="table table-bordered table-hover" id="HelpGuideTable">
            <thead>
            <tr class="info">
                <th>模块</th>
                <th>主题</th>
                <th>内容</th>
                <th>状态</th>
                <th>更新时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="js/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="js/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="js/static/h-ui.admin/js/H-ui.admin.js"></script>
</body>
</html>