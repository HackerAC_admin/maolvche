<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-13
  Time: 10:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>页脚</title>
<%--    <link rel="icon" href="favicon.ico" sizes="32x32">--%>
<%--    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">--%>
    <link rel="stylesheet" type="text/css" href="css/footer.css">
<%--    <script src="webjars/jquery/3.5.1/jquery.js"></script>--%>
<%--    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>--%>
</head>
<body>
<!--footer start-->
<footer>
    <div class="aboutUs">
        <a href="toHelpabout">关于毛驴汽车网</a>
        <a href="toHelppaymentHelp">交易帮助</a>
        <a href="toHelpafterServiceHelp">售后服务</a>
        <a href="toHelpsalerServiceHelp">卖家服务</a>
        <a href="toHelpfeedbackHelp">投诉与建议</a>
    </div>
    <div class="copyRightInfo">
        毛驴汽车技术开发（北京）有限公司 010-88888888北京市海淀区上地十街8号院8号楼88层8888
        <br>
        互联网违法或不良信息举报联系方式 邮箱：jubao@163.com 电话：010-89899999
        <br>
        Copyright © 2020 www.maolvche.com All Rights Reserved
        京公网安备 11010502088888号京ICP备88888888号-8 京B8-20208888
    </div>
</footer>
<!--footer end-->
</body>
</html>
