<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-16
  Time: 14:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>新车签约</title>
</head>
<body>
<%@include file="nav.jsp"%>
<section>
    <div id="agreementPaper" class="card">
        <h3 class="card-header">购车协议书(网站电子合同)</h3>
        <div class="card-body">
            <h5>出售方：毛驴汽车网</h5>
            <h5>订车方：<label id="clientName">彭于晏</label>(身份证号：<label id="idCardNum">520190199911111111</label>)</h5>
            <p>依据《中华人民共和国合同法》及其他有关法律法规的规定，
                甲、乙双方在平等、自愿、协商、一致的基础上，就汽车买卖有关事宜，订立本合同。</p>
            <p>乙方于<label id="dateTime">2020年11月27日</label>向甲方预定购买<label id="carFactory">上汽通用五菱</label>牌汽车1辆
                ；型号：<label>宝骏360 2019款 CVT精英型</label>；颜色：<label>糖果白</label>，并按照甲方规定预付相应的定金。待车辆成交后，定金作为车辆全款。</p>
            <p>乙方承诺交付定金后7日内到甲方线下门店签订正式销售合同并交纳尾款，如乙方资金不到位应事先通知甲方，双方协商可以延迟（不得超过十五日）；否则视为乙方违约。</p>
            <p>若甲方与乙方提车日不能按时交付车辆，又不事先提前2日通知乙方并说明情况，则视为甲方违约。</p>
            <p>乙方在签订本协议时向甲方支付定金人民币<label>74800.00</label>元，大写<label>柒万肆仟捌佰</label>元整。甲方收到定金后应于签订正式销售合同日给与乙方书面收据</p>
            <p>本协议签订后至交车开始前，如甲方违约，则双倍返还上述定金与乙方；如乙方违约，则甲方不予退回定金。</p>
            <i>点击下一步将视作同意上述协议，若有其他问题可点此<a href="#">反馈</a>或致电010-88888888</i>
        </div>
    </div>
    <div id="payFirstPayment" class="card">
        <h3 class="card-header">缴纳首付款</h3>
        <div class="card-body">
            <div class="stepOneInfo">
                <img src="images/shoukuanma.jpg">
                <br>
                <b>请支付首付款金额<label>74800</label>元</b>
                <br>
                <i>支付完成后，点击完成按钮即可，注意查看购车进度</i>
            </div>
        </div>
    </div>
</section>
<%@include file="footer.jsp"%>
</body>
</html>
