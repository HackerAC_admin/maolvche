<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-19
  Time: 11:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,Admin-scalable=no" />
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <title>管理员管理</title>
</head>
<body>
<nav class="breadcrumb">首页>后台管理员管理>管理员列表&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button style="float: right;" onclick="removeIframe()" class="btn btn-sm btn-primary radius">关闭</button></nav>

<!------------------------------- 新增管理员数据模态框 ----------------------------->
<div class="modal fade" id="AdminsAddModal" tabindex="-5" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">新增</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">编号</label>
                        <div class="col-sm-10">
                            <input type="text" name="bgGlybh" class="form-control" id="adminBh_add_input" placeholder="admnXXXX">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">姓名</label>
                        <div class="col-sm-10">
                            <input type="text" name="bgGlyxm" class="form-control" id="adminxm_add_input">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">性别</label>
                        <div class="col-sm-10">
                            <input type="text" name="bgGlyxb" class="form-control" id="Adminxb_add_input">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">角色</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="bgGlyjs" id="adminRoleSele">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">手机</label>
                        <div class="col-sm-10">
                            <input type="text" name="bgGlysj" class="form-control" id="phoneNum_add_input" placeholder="请输入手机">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">邮箱</label>
                        <div class="col-sm-10">
                            <input type="text" name="bgGlyyx" class="form-control" id="email_add_input" placeholder="email@example.com">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!------------------------------- 编辑管理员数据模态框 ----------------------------->
<div class="modal fade" id="AdminsUpdateModal" tabindex="-5" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="z-index: 2051">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel2">管理员编辑</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">编号</label>
                        <div class="col-sm-10">
                            <input type="text" name="bgGlybh" class="form-control" id="Adminbh_update_input" disabled="disabled">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">姓名</label>
                        <div class="col-sm-10">
                            <input type="text" name="bgGlyxm" class="form-control" id="Adminxm_update_input">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">性别</label>
                        <div class="col-sm-10">
                            <input type="text" name="bgGlyxb" class="form-control" id="Adminxb_update_input">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">角色</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="bgGlyjs" id="adminRoleSele2">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">手机</label>
                        <div class="col-sm-10">
                            <input type="text" name="bgGlysj" class="form-control" id="phoneNum_update_input">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">邮箱</label>
                        <div class="col-sm-10">
                            <input type="text" name="bgGlyyx" class="form-control" id="email_update_input">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" id="Admin_update_btn">保存</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------- 管理员列表页正文 ----------------------------->
<div class="container">
    <!-- 搜索框 -->
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
            <div class="input-group">
				<span class="input-group-btn">
				<select class="btn btn-info" id="queryItem">
					<option value="bgGlybh">管理员编号</option>
					<option value="bgGlyxm">管理员姓名</option>
					<option value="bgGlysj">管理员手机</option>
					<option value="bgGlyyx">管理员邮箱</option>
					<option value="bgGlyjs">管理员角色</option>
				</select>
				</span>
                <input type="text" class="form-control" id="itemValue" placeholder="请输入要查找的信息...">
                <span class="input-group-btn">
                <button class="btn btn-success" type="button" id="queryAdmin">查找</button>
                 </span>
            </div>
        </div>
    </div>
    <!-- 按钮 -->
    <div class="breadcrumb">
        <button class="btn btn-primary btn-sm" id="admin_add_modal_btn" data-toggle="modal" data-target="#AdminsAddModal">新增管理员</button>
        <button class="btn btn-danger btn-sm" id="admin_delete_all_btn">批量删除</button>
        <a class="btn btn-success" style="float:right;line-height:1.6em;margin-top:3px;margin-right: 10px" href="javascript:location.replace(location.href);" title="刷新" >刷新</a>
    </div>
    <div class="row">
        <table class="table table-bordered table-hover table-striped"  id="admins_table">
            <thead>
            <tr class="info">
                <th><input type="checkbox" name="" value="" id="check_all"></th>
                <th>管理员编号</th>
                <th>管理员姓名</th>
                <th>性别</th>
                <th>角色</th>
                <th>手机</th>
                <th>邮箱</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <!-- 显示分页信息 -->
    <div class="row">
        <!--分页文字信息  -->
        <div class="col-md-6" id="page_info_area"></div>
        <!-- 分页条信息 -->
        <div class="col-md-6" id="page_nav_area">
        </div>
    </div>
</div>
<script type="text/javascript" src="js/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="js/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="js/static/h-ui.admin/js/H-ui.admin.js"></script>
</body>
</html>
