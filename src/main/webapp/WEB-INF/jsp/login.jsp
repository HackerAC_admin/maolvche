<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-13
  Time: 10:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录毛驴汽车网</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script src="js/login.js"></script>
</head>
<body>
<%@ include file="nav.jsp"%>

<!--登录模态框start-->
<section>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="loginModalLabel">登录<label>毛驴</label></h4>
            <a href="register"><button class="btn btn-sm btn-success">去注册</button></a>
        </div>
        <form action="loginToWeb" method="post">
            <div class="modal-body">
                <img src="images/logo.png"><br>
                <i>看车/买车/卖车/售后/全方位服务</i><br>
                <input class="textInput" required name="loginText" type="text" placeholder="请输入账号或手机号"/><br>
                <input class="textInput" required name="pwd" type="password" placeholder="请输入密码"/><br>
                <i id="errorInfo">${errInfo}</i><br>
                <button type="submit"  class="submitBtn">登录</button>
                <p>免费咨询400-899-8899<br>
                    登录即视为同意<a href="Media/maolvcheuserprinceple.pdf" target="_blank">《用户使用协议》</a>及《隐私权条款》</p>
            </div>
        </form>
    </div>
</div>
</section>
<!--登录模态框end-->

<%@ include file="footer.jsp"%>
</body>
</html>
