<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-21
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>帮助向导</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="css/helpGuide.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="js/helpGuide.js"></script>
</head>
<body>
<nav>
    <div>
        <a href="main"><img src="images/logo.png" width="80" height="80"></a>
        <ul>
            <li class="active"><a href="about" target="displayBody">关于毛驴汽车网</a></li>
            <li><a href="paymentHelp" target="displayBody">交易帮助</a></li>
            <li><a href="afterServiceHelp" target="displayBody">售后服务</a></li>
            <li><a href="salerServiceHelp" target="displayBody">卖家服务</a></li>
            <li><a href="feedbackHelp" target="displayBody">投诉与建议</a></li>
        </ul>
    </div>
</nav>
<section>
    <div>
        <iframe name="displayBody" src="about"></iframe>
    </div>
</section>
<input id="navId" value="${requestScope.navId}" hidden/>
<!--网站底栏end-->
<%@ include file="footer.jsp"%>
</body>
</html>
