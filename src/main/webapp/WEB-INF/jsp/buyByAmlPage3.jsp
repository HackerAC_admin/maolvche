<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-16
  Time: 8:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/buyByAml3.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script src="js/buyByAml3.js"></script>
</head>
<body>
<%@include file="nav.jsp"%>
<section>
    <div class="buyCarBody">
        <div class="allMoneyBuyTopDiv">
            <span class="allMoneyBuyTitle">全款购车操作流程</span>
        </div>

        <div class="stepBar">
            <div class="stepOne">
                <img src="images/caricon.png" alt="..">
                <br>
                <b>确认购车信息</b>
            </div>
            <div class="stepTwo">
                <img src="images/caricon.png" alt="..">
                <br>
                <b>在线签约</b>
            </div>
            <div class="stepThree">
                <img src="images/caricon.png" alt="..">
                <br>
                <b>提交定金</b>
            </div>
            <div class="progress" style="height: 30px;">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 75%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="showStepInfo">
            <div class="stepOneInfo">
                <img src="images/shoukuanma.jpg">
                <br>
                <b>请支付定金<label>74800</label>元</b>
                <br>
                <i>支付完成后，点击完成按钮即可，注意查看购车进度</i>
            </div>
        </div>
        <img class="webLogo" src="images/weblogo.png"><i>拒绝繁琐分期，快速提车，纵享高效生活！</i>
        <button id="backToCarInfo" class="btn btn-dark">取消</button>
        <button id="preStep" class="btn btn-success">上一步</button>
        <button id="nextStep" class="btn btn-primary">完成</button>
    </div>
</section>
<%@include file="footer.jsp"%>
</body>
</html>
