<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-14
  Time: 18:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>毛驴汽车网后台管理</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="js/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="js/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="js/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="js/static/h-ui.admin/css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/BGMS/bgindex1.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="js/BGMS/bgmsLoginVerifyadm.js"></script>
</head>
<body>
<nav>
    <div class="titleBar">
        <img src="images/logo.png">
        <span>毛驴汽车网后台管理系统</span>
    </div>
    <div class="loginerInfo">
        <span>管理员：<i>${sessionScope.BGMS_ADMIN.nickname}</i></span>
        <a href="logoutadm"><button class="btn btn-sm btn-danger">退出登录</button></a>
    </div>
</nav>
<div id="content">
    <aside class="Hui-aside">
        <div class="menu_dropdown bk_2">
            <dl id="menu-article">
                <dt><i class="Hui-iconfont">&#xe616;</i>资讯管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="article-list.html" data-title="资讯管理" href="javascript:void(0)">资讯管理</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="menu-picture">
                <dt><i class="Hui-iconfont">&#xe613;</i>图片管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="picture-list.html" data-title="图片管理" href="javascript:void(0)">图片管理</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="menu-product">
                <dt><i class="Hui-iconfont">&#xe620;</i>汽车管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="product-brand.html" data-title="二手车管理" href="javascript:void(0)">新车管理</a></li>
                        <li><a data-href="product-category.html" data-title="新车管理" href="javascript:void(0)">二手车管理</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="menu-comments">
                <dt><i class="Hui-iconfont">&#xe622;</i>评论和反馈<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="feedback-list.html" data-title="评论列表" href="javascript:;">评论列表</a></li>
                        <li><a data-href="toBgfeedbackList" data-title="投诉和反馈" href="javascript:void(0)">投诉和反馈</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="menu-tongji">
                <dt><i class="Hui-iconfont">&#xe61a;</i>资料合同管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="BGMSusrCreditFileList" data-title="用户信审资料" href="javascript:void(0)">用户信审资料</a></li>
                        <li><a data-href="charts-2.html" data-title="签约管理" href="javascript:void(0)">签约管理</a></li>
                        <li><a data-href="charts-3.html" data-title="合同管理" href="javascript:void(0)">合同管理</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="menu-member">
                <dt><i class="Hui-iconfont">&#xe60d;</i>实体店运营<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="member-list.html" data-title="员工管理" href="javascript:;">员工管理</a></li>
                        <li><a data-href="member-del.html" data-title="实体店面管理" href="javascript:;">实体店面管理</a></li>
                        <li><a data-href="toExchangeList" data-title="退换管理" href="javascript:;">退换管理</a></li>
                        <li><a data-href="toMaintain" data-title="养护管理" href="javascript:;">养护管理</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="menu-admin">
                <dt><i class="Hui-iconfont">&#xe62d;</i>分期管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="toInstlPlan" data-title="分期方案" href="javascript:void(0)">分期方案</a></li>
                        <li><a data-href="toInstlRecordPage" data-title="分期还款记录" href="javascript:void(0)">分期还款记录</a></li>
                    </ul>
                </dd>
            </dl>
            <dl id="menu-system">
                <dt><i class="Hui-iconfont">&#xe62e;</i>系统运维管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <li><a data-href="system-base.html" data-title="员工管理" href="javascript:void(0)">员工管理</a></li>
                        <li><a data-href="toAdminList" data-title="管理员管理" href="javascript:void(0)">管理员管理</a></li>
                        <li><a data-href="system-data.html" data-title="网站账户管理" href="javascript:void(0)">网站账户管理</a></li>
                        <li><a data-href="system-shielding.html" data-title="后台账户管理" href="javascript:void(0)">后台账户管理</a></li>
                        <li><a data-href="toLoginRecordPage" data-title="网站用户登录日志" href="javascript:void(0)">网站用户登录日志</a></li>
                        <li><a data-href="toHelpGuidePage" data-title="帮助向导管理" href="javascript:void(0)">帮助向导管理</a></li>
                    </ul>
                </dd>
            </dl>
        </div>
    </aside>
    <div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a></div>
    <section class="Hui-article-box">
        <div id="Hui-tabNav" class="Hui-tabNav hidden-xs">
            <div class="Hui-tabNav-wp">
                <ul id="min_title_list" class="acrossTab cl">
                    <li class="active">
                        <span title="我的桌面" data-href="toWelcome">我的桌面</span>
                        <em></em>
                    </li>
                </ul>
            </div>
            <div class="Hui-tabNav-more btn-group">
                <a id="js-tabNav-prev" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i></a>
                <a id="js-tabNav-next" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i></a>
            </div>
        </div>
        <div id="iframe_box" class="Hui-article">
            <div class="show_iframe">
                <div style="display:none" class="loading"></div>
                <iframe id="iframe-welcome" data-scrolltop="0" scrolling="yes" frameborder="0" src="toWelcome"></iframe>
            </div>
        </div>
    </section>
</div>
<!--_footer 作为公共模版分离出去-->
<!--<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script>-->
<script type="text/javascript" src="js/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="js/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="js/static/h-ui.admin/js/H-ui.admin.js"></script>
<!--/_footer 作为公共模版分离出去-->

</body>
</html>
