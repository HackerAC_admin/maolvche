<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-19
  Time: 14:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/BGMS/BGMSinstlRecord.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script src="js/BGMS/BGMSinstlRecord.js"></script>
    <title>分期还款管理</title>
</head>
<body>
<nav class="breadcrumb">首页>分期管理>分期还款记录&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button style="float: right;" onclick="removeIframe()" class="btn btn-sm btn-primary radius">关闭</button></nav>

<!------------------------------- 新增分期还款模态框 ----------------------------->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">新增分期还款项</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                选择分期方案：<br>
                <select id="instlPlanSel">
                    <option value="000">10%首付分12期还款</option>
                </select><br>
                用户账号编号：<br>
                <input id="userAcc" placeholder="用户账户编号"><br>
                还款金额(元)：<br>
                <input id="repaymentMoney" placeholder="还款金额"><br>
                剩余未还款金额：<br>
                <input id="lastMoney" placeholder="剩余未还款金额"><br>
                <button id="submitAdd" class="btn btn-primary" style="float: right;">添加</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------- 分期还款记录列表正文 ----------------------------->
<div class="container">
    <!-- 搜索框 -->
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
            <div class="input-group">
				<span class="input-group-btn">
				<select class="btn btn-primary" id="queryItem">
					<option value="acc">用户账户编号</option>
				</select>
				</span>
                <input type="text" class="form-control" id="itemValue" placeholder="请输入要查找的信息...">
                <span class="input-group-btn">
                <button class="btn btn-success" type="button" id="queryInstlPlan">查找</button>
                 </span>
            </div>
        </div>
    </div>
    <!-- 按钮 -->
    <div class="breadcrumb">
        <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addModal">新增分期还款任务</button>&nbsp;&nbsp;
        <a class="btn btn-success" style="float:right;line-height:1.6em;margin-top:3px;margin-right: 10px" href="javascript:location.replace(location.href);" title="刷新" >刷新</a>
    </div>
    <div class="row">
        <table class="table table-bordered table-hover" id="InstlRecordTable">
            <thead>
            <tr class="info">
                <th>用户账户编号</th>
                <th>分期方案编号</th>
                <th>还款金额</th>
                <th>还款日期</th>
                <th>剩余未还款金额</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="js/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="js/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="js/static/h-ui.admin/js/H-ui.admin.js"></script>
</body>
</html>