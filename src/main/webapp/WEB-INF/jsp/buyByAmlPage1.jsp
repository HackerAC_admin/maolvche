<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-16
  Time: 8:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/buyByAml.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script src="js/buyByAml.js"></script>
</head>
<body>
<%@include file="nav.jsp"%>
<section>
    <div class="buyCarBody">
        <div class="allMoneyBuyTopDiv">
            <span class="allMoneyBuyTitle">全款购车操作流程</span>
        </div>

        <div class="stepBar">
            <div class="stepOne">
                <img src="images/caricon.png" alt="..">
                <br>
                <b>确认购车信息</b>
            </div>
            <div class="stepTwo">
                <img src="images/caricon.png" alt="..">
                <br>
                <b>在线签约</b>
            </div>
            <div class="stepThree">
                <img src="images/caricon.png" alt="..">
                <br>
                <b>提交定金</b>
            </div>
            <div class="progress" style="height: 30px;">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="showStepInfo">
            <div class="stepOneInfo">
                <table>
                    <tr>
                        <td colspan="9">
                            <img src="images/car3.jpg">
                            <img src="images/car4.jpg">
                            <img src="images/car5.jpg">
                        </td>
                    </tr>
                    <tr class="carSumInfoItem">
                        <td>厂商</td>
                        <td>车型</td>
                        <td>发动机</td>
                        <td>变速箱</td>
                        <td>长*宽*高</td>
                        <td>能源</td>
                        <td>漆色</td>
                        <td>内饰颜色</td>
                        <td>售价(万元)</td>
                    </tr>
                    <tr id="carSumInfoItemValue">
                        <td>上汽通用五菱</td>
                        <td>MPV</td>
                        <td>1.5L 105匹 L4缸</td>
                        <td>自动8挡位</td>
                        <td>4615*1735*1660</td>
                        <td>汽油</td>
                        <td>糖果白</td>
                        <td>深色</td>
                        <td><label>7.48</label></td>
                    </tr>
                </table>
            </div>
        </div>
        <img class="webLogo" src="images/weblogo.png"><i>拒绝繁琐分期，快速提车，纵享高效生活！</i>
        <button id="backToCarInfo" class="btn btn-dark">取消</button>
        <button id="nextStep" class="btn btn-primary">下一步</button>
    </div>
</section>
<%@include file="footer.jsp"%>
</body>
</html>
