<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-13
  Time: 14:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>新车详情</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/newCarDetail.css">
    <link rel="stylesheet" type="text/css" href="css/floatBar.css">
    <script src="js/newCarDetail.js"></script>
</head>
<body>
<%@ include file="nav.jsp"%>
<hr>
<!--汽车展示start-->
<div class="showCarInfo">
    <div class="showCarLeft">
        <div class="slider">
            <div id="carShowSection" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="images/car2.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="images/car3.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="images/car4.jpg" class="d-block w-100" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carShowSection" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carShowSection" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <div class="showCarRight">
        <h2>宝骏360 2019款 1.5L CVT精英型</h2>
        <p>厂商指导价：<label id="price">7.48</label>万</p>
        <div class="caseTitle">车辆信息</div>
        <table class="carInfoTab">
            <tr class="carInfoTitle">
                <td>厂商</td>
                <td>车型</td>
                <td>排量</td>
                <td>变速箱</td>
                <td>漆色</td>
                <td>内饰颜色</td>
            </tr>
            <tr class="carInfo">
                <td>上汽通用五菱</td>
                <td>MPV</td>
                <td>1.5L</td>
                <td>自动8挡位</td>
                <td>白色</td>
                <td>深色</td>
            </tr>
        </table>
        <div class="case">
            <div class="caseTitle">分期详情</div>
            <div class="finance">
                <b>首付比例</b>
                <ul>
                    <li class="activeFinance"><label>10%</label>首付</li>
                    <li><label>20%</label>首付</li>
                    <li><label>30%</label>首付</li>
                </ul>
                <button id="buyCarBtnAml" class="btn btn-primary">全款购车</button>
            </div>
            <div class="finance-con">
                <table>
                    <tr class="tabTitle">
                        <td>首付</td>
                        <td>月供</td>
                        <td>期数</td>
                    </tr>
                    <tr class="tabBody">
                        <td class="shoufu"><label></label>万元</td>
                        <td class="yuegong"><label></label>元</td>
                        <td>
                            <label>
                                <select id="qishu">
                                    <option value="12">12期</option>
                                    <option value="24">24期</option>
                                    <option value="36">36期</option>
                                    <option value="48">48期</option>
                                </select>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <button class="addToFavorite">收藏</button>
        <button class="buyCarBtn">马上入手</button>
    </div>
</div>
<!--汽车展示end-->

<!--汽车配置start-->
<div class="carInfoSection">
    <div class="carInfoSectionBody">
        <!-- 基本信息-->
        <div class="carInfoSummary">
            <span class="carInfoSumTitle">基本信息</span>
            <table>
                <tr class="carSumInfoItem">
                    <td>厂商</td>
                    <td>车型</td>
                    <td>发动机</td>
                    <td>变速箱</td>
                    <td>长*宽*高</td>
                    <td>能源</td>
                    <td>漆色</td>
                    <td>内饰颜色</td>
                </tr>
                <tr id="carSumInfoItemValue">
                    <td>上汽通用五菱</td>
                    <td>MPV</td>
                    <td>1.5L 105匹 L4缸</td>
                    <td>自动8挡位</td>
                    <td>4615*1735*1660</td>
                    <td>汽油</td>
                    <td>糖果白</td>
                    <td>深色</td>
                </tr>
            </table>
        </div>
        <!-- 所有信息-->
        <div class="carAllInfo">
            <span class="carAllInfoBtn">>>全部配置<<</span>
            <div class="carAllInfoBody">
                <div class="carItemClass">
                    <h5>基本信息</h5>
                    <ul id="carBaseInfo">
                        <li>
                            <span class="infoItem">厂商</span><span class="itemValue">上汽通用五菱</span>
                        </li>
                        <li><span class="infoItem">车型</span><span class="itemValue">MPV</span></li>
                        <li><span class="infoItem">发动机</span><span class="itemValue">1.5L 105匹 L4缸</span></li>
                        <li><span class="infoItem">最高速度</span><span class="itemValue">160</span></li>
                    </ul>
                </div>
                <div class="carItemClass">
                    <h5>车身信息</h5>
                    <ul id="carBodyInfo">
                        <li>
                            <span class="infoItem">车长</span><span class="itemValue">4615</span>
                        </li>
                        <li><span class="infoItem">车宽(mm)</span><span class="itemValue">1735</span></li>
                        <li><span class="infoItem">车高(mm)</span><span class="itemValue">1660</span></li>
                        <li><span class="infoItem">轴距(mm)</span><span class="itemValue">2750</span></li>
                        <li><span class="infoItem">最小离地间隙(mm)</span><span class="itemValue">275</span></li>
                        <li><span class="infoItem">油箱容积(L)</span><span class="itemValue">40</span></li>
                    </ul>
                </div>
                <div class="carItemClass">
                    <h5>发动机信息</h5>
                    <ul id="carEngineInfo">
                        <li>
                            <span class="infoItem">发动机型号</span><span class="itemValue">L2B</span>
                        </li>
                        <li><span class="infoItem">排量</span><span class="itemValue">1.5</span></li>
                        <li><span class="infoItem">进气形式</span><span class="itemValue">自然吸气</span></li>
                        <li><span class="infoItem">气缸排列形式</span><span class="itemValue">L</span></li>
                        <li><span class="infoItem">气缸数</span><span class="itemValue">4</span></li>
                        <li><span class="infoItem">最大马力</span><span class="itemValue">160</span></li>
                        <li><span class="infoItem">最大功率</span><span class="itemValue">77</span></li>
                        <li><span class="infoItem">最大功率转速</span><span class="itemValue">3600-5200</span></li>
                        <li><span class="infoItem">能源形式</span><span class="itemValue">汽油</span></li>
                        <li><span class="infoItem">能源标号</span><span class="itemValue">92</span></li>
                        <li><span class="infoItem">环保标准</span><span class="itemValue">国6</span></li>
                    </ul>
                </div>
                <div class="carItemClass">
                    <h5>变速箱信息</h5>
                    <ul id="carGearboxInfo">
                        <li><span class="infoItem">挡位个数</span><span class="itemValue">8</span></li>
                        <li><span class="infoItem">变数箱类型</span><span class="itemValue">自动</span></li>
                        <li><span class="infoItem">优势</span><span class="itemValue">无级变速箱</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--汽车配置end-->

<!--悬浮start-->
<div id="toTop"><img src="images/toTop.jpg"></div>
<script src="js/floatBar.js"></script>
<!--悬浮end-->

<%@ include file="footer.jsp"%>
</body>
</html>
