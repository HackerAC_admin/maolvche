<%--
  Created by IntelliJ IDEA.
  User: HackerAC
  Date: 2020-11-13
  Time: 9:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>首页</title>
    <link rel="icon" href="favicon.ico" sizes="32x32">
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <script src="webjars/jquery/3.5.1/jquery.js"></script>
    <script src="webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script src="js/index.js"></script>
</head>
<body>
<%@ include file="nav.jsp"%>
<%--<jsp:include page="nav.jsp">--%>
<%--    <jsp:param name="thisNav" value="1"/>--%>
<%--</jsp:include>--%>
<!--首页巨幕start-->
<div class="jumbotron1">
    <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleInterval" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleInterval" data-slide-to="1"></li>
            <li data-target="#carouselExampleInterval" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active" data-interval="3000">
                <a href="#" target="_self"><img src="images/adver1.gif" class="d-block w-60" alt="..."></a>
            </div>
            <div class="carousel-item" data-interval="3000">
                <a href="https://lavida.svw-volkswagen.com/" target="_blank"><img src="images/adver2.gif" class="d-block w-60" alt="..."></a>
            </div>
            <div class="carousel-item" data-interval="15000">
                <div class="adver1">
                    <a class="cover" href="https://www.dongfeng-nissan.com.cn/car/sylphy" target="_blank"></a>
                    <video muted src="Media/tvc-0924.mp4" height="441" autoplay loop></video>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
        </a>
    </div>
</div>
<!--首页巨幕end-->
<!--买车卖车选择区start-->
<div id="filterArea">
    <div id="filterBrand">
        <!--买车start-->
        <div class="filter-buyCar">
            <div class="btn-group">
                <div class="btn-group">
                    <button type="button" class="btn btnActive" id="buyNewCarBtn">新车</button>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn" id="buySecondCarBtn">二手车</button>
                </div>
            </div>
            <div class="topBox">
                <div class="kind-filter-box">
                    <p>车型</p>
                    <a href="#">轿车</a>
                    <a href="#">SUV</a>
                    <a href="#">MPV</a>
                </div>
                <div class="price-filter-box">
                    <p>价格</p>
                    <a href="#">10万以下</a>
                    <a href="#">10-15万</a>
                    <a href="#">15-20万</a>
                    <a href="#">20-30万</a>
                    <a href="#">30-50万</a>
                </div>
            </div>
            <div class="bottomBox">
                <div class="brand-filter-Box">
                    <p>品牌</p>
                    <a href="#">奔驰</a>
                    <a href="#">大众</a>
                    <a href="#">丰田</a>
                    <a href="#">奥迪</a>
                    <a href="#">宝马</a>
                    <a href="#">本田</a>
                    <a href="#">保时捷</a>
                    <a href="#">五菱</a>
                    <a href="#">现代</a>
                    <a href="#">长安</a>
                    <a href="#">别克</a>
                    <a href="#">马自达</a>
                    <a href="#">北京</a>
                    <a href="#">比亚迪</a>
                    <a href="#">哈弗</a>
                </div>
            </div>
        </div>
        <!--买车end-->
        <!--卖车start-->
        <div class="filter-sellCar">
            <p>已经有<label>11291095</label>人发布了卖车信息</p>
            <input type="text" name="phoneNumber" placeholder="请输入您的手机号码" maxlength="11">
            <input type="submit" value="高价卖车">
        </div>
        <!--卖车end-->
    </div>
</div>
<!--买车卖车选择区end-->
<!--最新上架start-->
<div class="LatestSection">
    <div class="LatestBody">
        <div class="LatestTitle">
            <h4>最新上架</h4>
        </div>
        <div class="LatestList">
            <ul>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label>宝骏360 2019款 1.5L CVT精英型</label>
                    <p>5公里/2019/天津</p>
                    <div>
                        <span>
                            3.5
                            <em class="unit">万</em>
                        </span>
                        <s class="original-price">5.28万</s>
                    </div>
                </li>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label>宝骏360 2019款 1.5L CVT精英型</label>
                    <p>5公里/2019/天津</p>
                    <div>
                        <span>
                            3.5
                            <em class="unit">万</em>
                        </span>
                        <s class="original-price">5.28万</s>
                    </div>
                </li>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label>宝骏360 2019款 1.5L CVT精英型</label>
                    <p>5公里/2019/天津</p>
                    <div>
                        <span>
                            3.5
                            <em class="unit">万</em>
                        </span>
                        <s class="original-price">5.28万</s>
                    </div>
                </li>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label>宝骏360 2019款 1.5L CVT精英型</label>
                    <p>5公里/2019/天津</p>
                    <div>
                        <span>
                            3.5
                            <em class="unit">万</em>
                        </span>
                        <s class="original-price">5.28万</s>
                    </div>
                </li>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label>宝骏360 2019款 1.5L CVT精英型</label>
                    <p>5公里/2019/天津</p>
                    <div>
                        <span>
                            3.5
                            <em class="unit">万</em>
                        </span>
                        <s class="original-price">5.28万</s>
                    </div>
                </li>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label>宝骏360 2019款 1.5L CVT精英型</label>
                    <p>5公里/2019/天津</p>
                    <div>
                        <span>
                            3.5
                            <em class="unit">万</em>
                        </span>
                        <s class="original-price">5.28万</s>
                    </div>
                </li>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label>宝骏360 2019款 1.5L CVT精英型</label>
                    <p>5公里/2019/天津</p>
                    <div>
                        <span>
                            3.5
                            <em class="unit">万</em>
                        </span>
                        <s class="original-price">5.28万</s>
                    </div>
                </li>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label>宝骏360 2019款 1.5L CVT精英型</label>
                    <p>5公里/2019/天津</p>
                    <div>
                        <span>
                            3.5
                            <em class="unit">万</em>
                        </span>
                        <s class="original-price">5.28万</s>
                    </div>
                </li>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label>宝骏360 2019款 1.5L CVT精英型</label>
                    <p>5公里/2019/天津</p>
                    <div>
                        <span>
                            3.5
                            <em class="unit">万</em>
                        </span>
                        <s class="original-price">5.28万</s>
                    </div>
                </li>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label>宝骏360 2019款 1.5L CVT精英型</label>
                    <p>5公里/2019/天津</p>
                    <div>
                        <span>
                            3.5
                            <em class="unit">万</em>
                        </span>
                        <s class="original-price">5.28万</s>
                    </div>
                </li>
            </ul>
        </div>
        <div class="LearnMoreBody">
            <a role="button" class="MoreButton" href="buySecondCarIndex">查看更多二手车</a>
        </div>
    </div>
</div>
<!--最新上架end-->
<!--潮车来袭start-->
<div class="FashionCarSection">
    <div class="FashionCarBody">
        <div class="FashionCarTitle">
            <h4>潮车来袭</h4>
        </div>
        <div class="FashionCarList">
            <ul>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label class="CarInfo">宝骏360 2019款 1.5L CVT精英型</label>
                    <p>厂家指导价：<label class="factoryPrice">5.28</label>万</p>
                    <div>
                        <span>
                            首付<label class="FirstPay">0.74</label>万
                        </span>
                        <i>月供<label class="monthPay">3084</label>元</i>
                    </div>
                </li>
                <li>
                    <img src="images/baoqijun01.jpg" alt="汽车">
                    <label class="CarInfo">宝骏360 2019款 1.5L CVT精英型</label>
                    <p>厂家指导价：<label class="factoryPrice">5.28万</label></p>
                    <div>
                        <span>
                            首付<label class="FirstPay">0.74</label>万
                        </span>
                        <i>月供<label class="monthPay">3084</label>元</i>
                    </div>
                </li>
            </ul>
        </div>
        <div class="LearnMoreBody">
            <a role="button" class="MoreButton" href="buyCarIndex">查看更多新车>></a>
        </div>
    </div>
</div>
<!--潮车来袭end-->
<!--资讯start-->
<div class="CarNewsSection">
    <div class="CarNewsBody">
        <div class="CarNewsTitle">
            <h4>汽车资讯</h4>
        </div>
        <div class="CarNews">
            <div class="CarNewsImg">
                <div class="CarNewsImgLeft">
                    <img src="images/zixun2.jpg" alt="资讯">
                </div>
                <div class="CarNewsImgRight">
                    <img src="images/zixun1.jpg" alt="资讯">
                    <img src="images/zixun3.jpg" alt="资讯">
                </div>
            </div>
            <div class="CarNewsList">
                <ul>
                    <li><a href="#"><label>【二手车资讯】</label>“金九银十”卖车究竟有多快？ </a></li>
                    <li><a href="#"><label>【新车资讯】</label>年轻时尚的家用代步车 YARiS L 致炫X </a></li>
                    <li><a href="#"><label>【新车资讯】</label>BEIJING-X7纯粹致美探索之旅抵达郑州 </a></li>
                    <li><a href="#"><label>【二手车资讯】</label>“金九银十”卖车究竟有多快？ </a></li>
                    <li><a href="#"><label>【新车资讯】</label>年轻时尚的家用代步车 YARiS L 致炫X </a></li>
                    <li><a href="#"><label>【新车资讯】</label>BEIJING-X7纯粹致美探索之旅抵达郑州 </a></li>
                </ul>
            </div>
        </div>
        <div class="LearnMoreBody">
            <input type="button" class="MoreButton" value="查看更多资讯>>">
        </div>
    </div>
</div>
<!--资讯end-->
<!--网站底栏start-->
<div class="siteBottom">
    <div class="adverInfo">
        <img src="images/weblogo.png" alt="网站图标">
        <a href="buyCarIndex">买新车</a>
        <a href="buySecondCarIndex">买二手车</a>
        <a href="#">卖车</a>
    </div>
</div>
<!--网站底栏end-->
<%@ include file="footer.jsp"%>
</body>
</html>
