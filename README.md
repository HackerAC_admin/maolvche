# maolvche

#### 介绍
毛驴汽车网
为中国汽车消费者提供高效、专业、透明、放心的一站式交易平台，以“保真车源、保真车况、保真车价”为核心，致力于构建一个线上数据、技术驱动，融合线下毛驴汽车店深度服务的一站式汽车新零售服务平台。实现“让更多人拥有美好的购车体验”的企业使命。
网站提供新车选购，二手车交易，汽车资讯，汽车论坛等主要功能。

#### 软件架构

![输入图片说明](https://images.gitee.com/uploads/images/2020/1029/101802_9f282e3f_5474072.png "屏幕截图.png")

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
